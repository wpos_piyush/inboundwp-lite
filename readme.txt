﻿=== InboundWP - A Complete Inbound Marketing Pack ===
Contributors: wponlinesupport, anoopranawat, pratik-jain, ridhimashukla, piyushpatel123, jahnavwponlinesupport, rafikwp
Tags: Spin Wheel, Inbound, Inbound marketing,  Better Heading, Social Proof, Testimonial, Review, Deal Countdown Timer, Marketing PopUp, WhatsApp chat Support
Requires at least: 4.0
Tested up to: 5.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

InboundWP Marketing - Spin Wheel, Deal Countdown Timer, Social Proof, Marketing PopUp, WhatsApp Chat Support, Better heading and Review.

== Description ==

InboundWP marketing plugin is about creating valuable experiences that have a positive impact on people and your business. How do you do that? You attract prospects and customers to your website and blog through relevant and helpful content. Once they arrive, you engage with them using conversational tools.

Inbound is a method of attracting, engaging, and delighting people to grow a business that provides value and builds trust.

= Attract =

You want people who are most likely to become leads and, ultimately, happy customers.

= Engage =

Use tool to create lasting relationships with prospects on the channels they prefer — spinwheel, popup etc

= Delight =

Deliver the right information to the right person at the right time, every time.

**Available Modules :**

* **Spin Wheel** - WordPress SpinWheel offers visitors to fill in their email addresses to spins for prizes(coupon code ). This is the best way to collect email from visitors on your site, they will be pleased to fill in their email address.
* **Deal Countdown Timer** -  Sale Countdown Timer helps you to create flash sales and increase revenues. You can add multiple flash sales with a start and end date. Plugin also shows stock progress bar.
* **Social Proof** -  Display real time customer activity data from your website. ie this user purchased this product – Time and country. Easily customize the look and feel of notification to match with your site’s branding.
* **Marketing PopUp** -  This plugin is useful to Show offers on website when user land on website, To display subscription form, Call to action, Exit Intent Technology, Announce and When user scroll page to some %.
* **Testimonial/ Review** -  Testimonial/Review is important part where site owner can display testimonial given by other users.
* **Better heading** -  Better heading plugin is very useful for blogger. It helps author to create  better heading for the article. Plugin provide options where author can add 3 heading for a single article.
* **WhatsApp Chat Support** -  WhatsApp Chat for WordPress plugin allows you to integrate your WhatsApp experience directly into your website. This is one of the best way to connect and interact with your customer, you can offer support directly as well as build trust and increase customer loyalty.

Enable / Disable feature of particular module so enable only those modules which requires to your website and others will not disturb you.

== Installation ==

1. Upload the 'inboundwp-lite' folder to the '/wp-content/plugins/' directory.
2. Activate the "InboundWP Lite" plugin through the 'Plugins' menu in WordPress.

== Frequently Asked Questions ==

= Is InboundWP Lite Free? =

Yes! InboundWP's core features are and always will be free.

= Should I purchase a paid plan? =

InboundWP Pro includes advance features of each modules!

If you're interested in advance modules available, checkout our [Paid Plans](http://www.inboundwppro.com/).

= 3) Does InboundWP work with my WordPress themes? =

Yes! InboundWP works with all WordPress theme created with WordPress standard.

= 4) Does InboundWP slow down my website? =

InboundWP Lite is designed from the ground-up with performance, SEO and efficiency in mind.

Easy enable / disable feature of modules so use only those modules (Element) which is required to you!!!

== Screenshots ==

1. InboundWP Lite Dashboard - Enable various modules for your website.
2. Deal Countdown Timer - Work with WooCommerce and Easy Digital Download plugins.
3. Social Proof - Work with WooCommerce and Easy Digital Download plugins.
4. Marketing popup.
5. WhatsApp Chat Support.
6. Spin Wheel - Lead generation and to grow sales by Interactive user experience. 

== Changelog ==

= 1.0.2 =
* Add new Upgrade to Pro page and relate link to plugin.
* Fix minor change in function name and resolve free to pro conflicts.
* Improved functionality of spin wheel and deal countdown timer.

= 1.0.1 =
* Fix minor bug with 'Marketing Popup' module.
* Improved some UI for better user experience.

= 1.0 =
* Initial release.

== Upgrade Notice ==

= 1.0 =
* Initial release.