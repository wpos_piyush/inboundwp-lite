var timer;
var timeOut_Val = 300;
var timeOut = timeOut_Val; /* delay after last change to execute filter */
var preview_shortcode = jQuery('.ibwp-customizer').attr('data-shortcode');

var checked_show_dep	= [];
var dep_wrap 			= '.ibwp-customizer-control';
var dependency 			= jQuery(dep_wrap +' .ibwp-cust-dependency').attr('data-dependency');
dependency 				= jQuery.parseJSON( dependency );

jQuery( document ).ready(function($) {

	
	
	$(document).on('click', '.ibwp-cust-dwp', function() {
		$('body').toggleClass('ibwp-cust-full-preview');
		$(this).toggleClass( 'ibwp-cust-dwp-active' );
	});

	/* Customizer Accordian */
	$( "#ibwp-cust-accordion" ).accordion({
		collapsible: true,
		heightStyle: "content",
		icons : {
				header: "dashicons dashicons-arrow-down-alt2",
				activeHeader: "dashicons dashicons-arrow-up-alt2"
	    }
	});

	/* Color Picker */
    if( $('.ibwp-cust-color-box').length > 0 ) {
        $('.ibwp-cust-color-box').wpColorPicker({
        	change: function(event, ui) {
        		ibwp_generate_shortcode_preview();
        	},
        	clear: function() {
        		ibwp_generate_shortcode_preview();
        	}
        });
    }

	/* Generate Shortcode */
    $(document).on('change', '.ibwp-customizer-control select, .ibwp-customizer-control input[type="number"], .ibwp-customizer-control .ibwp-color-box', function() {
    	field_timeout 	= $(this).attr('data-timeout');
    	timeOut 		= (typeof(field_timeout) !== 'undefined') ? field_timeout : timeOut_Val;

    	ibwp_generate_shortcode_preview();
	});

	$(document).on('keyup', '.ibwp-customizer-control input[type="text"], .ibwp-customizer-control input[type="number"]', function() {
    	field_timeout 	= $(this).attr('data-timeout');
    	timeOut 		= (typeof(field_timeout) !== 'undefined') ? field_timeout : timeOut_Val;

    	ibwp_generate_shortcode_preview();
	});

	// On update of media item
	$(document).on('ibwp_media_item_added', function( event, ele ) {
		ibwp_generate_shortcode_preview();
	});

	// On remove of media item
	$(document).on('ibwp_media_item_removed', function( event, ele ) {
		ibwp_generate_shortcode_preview();
	});

    /* On Change of Customizer Shortcode */
	$(document).on('change', '.ibwp-cust-shrt-switcher', function() {
		redirect = $(this).find(":selected").attr('data-url');

		if( typeof(redirect) !== 'undefined' && redirect != '' ) {
			window.location = redirect;
		}
	});

	/* Tweak - An extra care that form should not be refresh */
	jQuery('#ibwp-customizer-shrt-form').submit(function( event ) {
    	var form_target = $(this).attr('target');

    	if( typeof(form_target) == 'undefined' || form_target == '' ) {
    		return false;
    	}
    });

	/* On Click of Preview Generate Button */
	$(document).on('click', '.ibwp-cust-shrt-generate', function() {
		
		var main_ele 	= '.ibwp-customizer-control';
		var shrt_val 	= $.trim( $('.ibwp-customizer-shrt').val() );
		var first_char 	= shrt_val.substr(0, 1);
		var last_char 	= shrt_val.substr(-1);

		/* Simply return if blank value */
		if( shrt_val == '' ) {
			return false;
		}

		if( first_char == '[' && last_char == ']' ) {
			shrt_val = $.trim( shrt_val.slice(1, -1) );

			first_char 	= shrt_val.substr(0, 1);
			last_char 	= shrt_val.substr(-1);
		}

		if( first_char != '[' ) {
			shrt_val = '[' + shrt_val;
		}
		if( last_char != ']' ) {
			shrt_val = shrt_val + ']';
		}
		
		$('.ibwp-customizer-shrt').val( shrt_val );

		temp_shrt_val = $.trim( shrt_val.slice(1, -1) );
		var data = wp.shortcode.attrs( temp_shrt_val );

		/* If wrong shortocde then simply return */
		if( data.numeric[0] && data.numeric[0] !== preview_shortcode ) {
			alert( IBWP_Shrt_Mapper.shortocde_err );
			return false;
		}

		if( data.named ) {
			$.each( data.named, function( shrt_param, shrt_param_val ) {
				if( shrt_param ) {
					$(main_ele+' .ibwp-'+shrt_param).val( shrt_param_val ).trigger('change').trigger('keyup');
				}
			});
		}
	});

	/* Shortcode Customizer Dependency */
	if( dependency ) {
		$.each( dependency, function( key, dependency_val ) {

			if( key ) {

				/* Dependency on page load */
				setTimeout(function() {
					if( $.inArray( key, checked_show_dep ) == -1 ) {
			        	$(dep_wrap+' .ibwp-'+key+'').trigger('change');
			    	}
			    }, 10);

				$(document).on('change keyup', dep_wrap+' .ibwp-'+key+'', function() {
			    	
			    	var input_val = $(this).val();

			    	/* Show Dependency */
			    	if( dependency_val.show ) {

			    		$.each( dependency_val.show, function( sub_key, sub_dep_val ) {
			    			$(dep_wrap+' .ibwp-'+sub_key+'').closest('.ibwp-customizer-row').hide();
			    			$(dep_wrap+' .ibwp-'+sub_key+'').addClass('ibwp-cust-hidden-field');

			    			/* If value is present then show */
			    			if( ( $.inArray( input_val, sub_dep_val ) !== -1 ) ) {
			    				$(dep_wrap+' .ibwp-'+sub_key+'').closest('.ibwp-customizer-row').show();
			    				$(dep_wrap+' .ibwp-'+sub_key+'').removeClass('ibwp-cust-hidden-field');
			    			}

			    			/* Check if reference dependency is there then hide it's element also */
			    			ibwp_check_ref_dependency( sub_key );
			    		});
			    	}

			    	/* Hide Dependency */
			    	if( dependency_val.hide ) {
			    		$.each( dependency_val.hide, function( sub_key, sub_dep_val ) {

			    			$(dep_wrap+' .ibwp-'+sub_key+'').closest('.ibwp-customizer-row').show();
			    			$(dep_wrap+' .ibwp-'+sub_key+'').removeClass('ibwp-cust-hidden-field');

			    			if( ( $.inArray( input_val, sub_dep_val ) !== -1 ) ) {
			    				$(dep_wrap+' .ibwp-'+sub_key+'').closest('.ibwp-customizer-row').hide();
			    				$(dep_wrap+' .ibwp-'+sub_key+'').addClass('ibwp-cust-hidden-field');
			    			}

			    			/* Check if reference dependency is there then hide it's element also */
			    			ibwp_check_hide_ref_dependency( sub_key );
			    		});
			    	}
				});
			}
		});
	} else {
		ibwp_generate_shortcode_preview();
	}
	/* Shortcode Customizer Dependency */
});

/* Function to generate shortocde preview */
function ibwp_generate_shortcode_preview() {

	/* Taking some variables */
    var shortcode_val   = '';
    var main_ele		= jQuery('.ibwp-customizer');
    var cls_ele         = jQuery('.ibwp-customizer-control');
    var shortcode_name  = preview_shortcode;

	clearTimeout(timer); /* if we pressed the key, it will clear the previous timer and wait again */
    timer = setTimeout(function() {

    	main_ele.find('.ibwp-customizer-loader').fadeIn();

        shortcode_val += '['+shortcode_name;

        /* Loop of form element */
        cls_ele.find('input[type="text"], input[type="checkbox"]:checked, input[type="radio"], input[type="number"], textarea, select').each(function(i, field){

        	if( jQuery(this).hasClass('ibwp-cust-hidden-field') ) {
        		return;
        	}

            var field_val	= jQuery(this).val();
            var field_name  = jQuery(this).attr('name');
            var default_val	= jQuery(this).attr('data-default');
            var allow_empty	= jQuery(this).attr('data-empty');            

            if( typeof(field_val) != 'undefined' && ( field_val != '' || allow_empty ) && field_val != default_val ) {
                shortcode_val += ' '+field_name+'='+'"'+field_val+'"';
            }
        });

        shortcode_val += ']';

        /* Append shortcode */
        main_ele.find('.ibwp-customizer-shrt').val(shortcode_val);

        jQuery('#ibwp-customizer-shrt-form').submit();        

		main_ele.find('.ibwp-customizer-preview-frame').load(function () {
		    main_ele.find('.ibwp-customizer-loader').fadeOut();
		});

    }, timeOut);
}

/* Function to check reference dependency */
function ibwp_check_ref_dependency( sub_key ) {

	ref_dep = sub_key in dependency;

	if( ref_dep ) {

		var ref_input_val = jQuery(dep_wrap+' .ibwp-'+sub_key+'').val();

		jQuery.each( dependency[sub_key]['show'], function( ref_key, ref_dep_val ) {
			
			jQuery(dep_wrap+' .ibwp-'+ref_key+'').closest('.ibwp-customizer-row').hide();
			jQuery(dep_wrap+' .ibwp-'+ref_key+'').addClass('ibwp-cust-hidden-field');

			if( jQuery.inArray( ref_input_val, ref_dep_val ) !== -1 && (!jQuery(dep_wrap+' .ibwp-'+sub_key+'').hasClass('ibwp-cust-hidden-field')) ) {
				jQuery(dep_wrap+' .ibwp-'+ref_key+'').closest('.ibwp-customizer-row').show();
				jQuery(dep_wrap+' .ibwp-'+ref_key+'').removeClass('ibwp-cust-hidden-field');
			}

			/* Check if reference dependency is there then hide it's element also */
			ibwp_check_ref_dependency( ref_key );
		});

		checked_show_dep.push( sub_key ); /* Log checked show dependency */
	}
}

/* Function to check hide reference dependency */
function ibwp_check_hide_ref_dependency( sub_key ) {

	ref_dep = sub_key in dependency;

	if( ref_dep ) {

		var ref_input_val = jQuery(dep_wrap+' .ibwp-'+sub_key+'').val();

		jQuery.each( dependency[sub_key]['hide'], function( ref_key, ref_dep_val ) {

			jQuery(dep_wrap+' .ibwp-'+ref_key+'').closest('.ibwp-customizer-row').hide();
			jQuery(dep_wrap+' .ibwp-'+ref_key+'').addClass('ibwp-cust-hidden-field');

			if( jQuery.inArray( ref_input_val, ref_dep_val ) == -1 && (!jQuery(dep_wrap+' .ibwp-'+sub_key+'').hasClass('ibwp-cust-hidden-field')) ) {
				jQuery(dep_wrap+' .ibwp-'+ref_key+'').closest('.ibwp-customizer-row').show();
				jQuery(dep_wrap+' .ibwp-'+ref_key+'').removeClass('ibwp-cust-hidden-field');
			}

			/* Check if reference dependency is there then hide it's element also */
			ibwp_check_hide_ref_dependency( ref_key );
		});
	}
}