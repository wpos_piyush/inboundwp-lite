jQuery( document ).ready(function( $ ) {

    // Media Uploader
    $( document ).on( 'click', '.ibwp-image-upload', function() {        
        
        var imgfield,showfield;
        imgfield = jQuery(this).prev('input').attr('id');
        showfield = jQuery(this).parents('td').find('.ibwp-wbg-preview-img');
        
        if(typeof wp == "undefined" || IBWPAdmin.new_ui != '1' ){ // Check for media uploader
          
            tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true');
            
            window.original_send_to_editor = window.send_to_editor;
            window.send_to_editor = function(html) {
                
                if(imgfield) {
                    
                    var mediaurl = $('img',html).attr('src');
                    $('#'+imgfield).val(mediaurl);
                    showfield.html('<img src="'+mediaurl+'" />');
                    tb_remove();
                    imgfield = '';
                    
                } else {
                    
                    window.original_send_to_editor(html);
                    
                }
            };
            return false;
            
              
        } else {
            
            var file_frame;
            
            //new media uploader
            var button = jQuery(this);        
            // If the media frame already exists, reopen it.
            if ( file_frame ) {
                file_frame.open();
              return;
            }
    
            // Create the media frame.
            file_frame = wp.media.frames.file_frame = wp.media({
                frame: 'post',
                state: 'insert',
                title: button.data( 'uploader-title' ),
                button: {
                    text: button.data( 'uploader-button-text' ),
                },
                multiple: false  // Set to true to allow multiple files to be selected
            });
    
            file_frame.on( 'menu:render:default', function(view) {
                // Store our views in an object.
                var views = {};
    
                // Unset default menu items
                view.unset('library-separator');
                view.unset('gallery');
                view.unset('featured-image');
                view.unset('embed');
    
                // Initialize the views in our view object.
                view.set(views);
            });
    
            // When an image is selected, run a callback.
            file_frame.on( 'insert', function() {
    
                // Get selected size from media uploader
                var selected_size = $('.attachment-display-settings .size').val();
                
                var selection = file_frame.state().get('selection');
                selection.each( function( attachment, index ) {
                    attachment = attachment.toJSON();
                    
                    // Selected attachment url from media uploader
                    var attachment_url = attachment.sizes[selected_size].url;
                    
                    if(index == 0){
                        // place first attachment in field
                        $('#'+imgfield).val(attachment_url);
                        showfield.html('<img src="'+attachment_url+'" />');
                        
                    } else{
                        $('#'+imgfield).val(attachment_url);
                        showfield.html('<img src="'+attachment_url+'" />');
                    }
                });
            });
    
            // Finally, open the modal
            file_frame.open();
        }
    });
    
    // Clear Media
    $( document ).on( 'click', '.ibwp-image-clear', function() {
        $(this).parent().find('.ibwp-img-upload-input').val('');
        $(this).parent().find('.ibwp-wbg-preview-img').html('');
    });

    /* Show/Hide advance row setting fields */
    $(document).on('click', '.ibwp-toggle-setting', function(){
        
        $(this).parent().parent().parent().children('.ibwp-row-standard-fields').slideToggle();
        var txt = $(this).text()=='Show settings' ? 'Hide settings' : 'Show settings';
        $(this).text(txt);

    });

    $(".ibwp-dashboard-wrap .ibwp-module-check").click( function() {

        var cls_ele = $(this).closest('.ibwp-site-module-wrap');

        if( $(this).is(':checked') ) {
            cls_ele.addClass('ibwp-site-module-active');
        } else {
            cls_ele.removeClass('ibwp-site-module-active');
        }
        cls_ele.find('.ibwp-site-module-conf-wrap').slideToggle();
    });

    /* Show save notice */
    $('.ibwp-dashboard-wrap .ibwp-module-cnt-wrp').find('input:not(.ibwp-no-chage), select:not(.ibwp-no-chage), textarea:not(.ibwp-no-chage), checkbox:not(.ibwp-no-chage)').bind('keydown change', function(){

        var anim_bottom = '50px';
        if( IBWPAdmin.is_mobile == 1 ) {
            anim_bottom = 0;
        }

        $('.ibwp-save-info-wrap').show().animate({bottom: anim_bottom}, 500);
    });

    /* Hide save notice */
    $(document).on('click', '.ibwp-save-info-wrap .ibwp-save-info-close', function(){
        $(this).closest('.ibwp-save-info-wrap').fadeOut( "slow", function() {
            $(this).css({bottom:''});
        });
    });

    /* Search Toggle */
    $(document).on('click', '.ibwp-dashboard-search-icon', function(e) {
        e.preventDefault();
        var cls_ele = $(this).closest('.ibwp-dashboard-header');

        cls_ele.find('.ibwp-dashboard-search-wrap').stop().slideToggle();
        cls_ele.find('.ibwp-dashboard-search').val('').focus().trigger('keyup');

        $('.ibwp-no-module-search').hide();
    });

    /* Esc key press */
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {

            var sort_cat_val = $('.ibwp-module-sort-cat').val();

            $('.ibwp-no-module-search').hide();
            $('.ibwp-dashboard-header .ibwp-dashboard-search-wrap').slideUp();

            if( sort_cat_val ) {
                $('.ibwp-site-modules-wrap .ibwp-site-module-'+sort_cat_val).fadeIn();
            } else {
                $('.ibwp-site-modules-wrap .ibwp-site-module-wrap').fadeIn();
            }
        }
    });

    var timer;
    var timeOut = 300; /* delay after last keypress to execute filter */

    /* Module Search */
    $('.ibwp-dashboard-search').keyup(function(event) {

        /* If element is focused and esc key is pressed */
        if (event.keyCode == 27) {
            return true;
        }

        clearTimeout(timer); /* if we pressed the key, it will clear the previous timer and wait again */
        timer = setTimeout(function() {

            var search_value    = $('.ibwp-dashboard-search').val().toLowerCase();
            var sort_cat_val    = $('.ibwp-module-sort-cat').val();
            var loop_part       = $('.ibwp-site-modules-wrap .ibwp-site-module-wrap');
            var zebra = 'odd';

            if( sort_cat_val ) {
                loop_part = $('.ibwp-site-modules-wrap .ibwp-site-module-'+sort_cat_val);
            }

            loop_part.each(function(index) {

                var contents = $(this).find('.ibwp-site-module-title span').html().toLowerCase();
                var desc_cnt = $(this).find('.ibwp-site-module-desc').html().toLowerCase();

                if (contents.indexOf(search_value) !== -1 || desc_cnt.indexOf(search_value) !== -1) {
                    $(this).fadeIn('slow');

                    if (zebra == 'odd') {
                        zebra = 'even';
                    } else {
                        zebra = 'odd';
                    }
                } else {
                    $(this).hide();
                }
            });

            if( $('.ibwp-site-modules-wrap .ibwp-site-module-wrap:visible').length <= 0 ) {
                $('.ibwp-no-module-search').fadeIn();
            } else {
                $('.ibwp-no-module-search').hide();
            }

        }, timeOut);
    });

    /* Vertical Tab */
    $( document ).on( "click", ".ibwp-stabs-nav a", function() {
        
        $(".ibwp-stabs-nav").removeClass('ibwp-active-stab');
        $(this).parent('.ibwp-stabs-nav ').addClass("ibwp-active-stab");
        var selected_tab = $(this).attr("href");
        $('.ibwp-stabs-cnt').hide();
        
        /* Show the selected tab content */
        $(selected_tab).show();
        
        /* Pass selected tab */
        $('.ibwp-selected-tab').val(selected_tab);
        return false;

    });

    /* Remain selected tab for user */
    if( $('.ibwp-selected-tab').length > 0 ) {
        
        var sel_tab = $('.ibwp-selected-tab').val();
        
        if( typeof(sel_tab) !== 'undefined' && sel_tab != ''  ) {
            $('.ibwp-stabs-nav [href="'+sel_tab+'"]').click();
        } else {
            $('.ibwp-stabs-nav:first-child a').click();
        }

    }

    /* Horizontal Tab (Work For WordPress Tab Also) */
    $( document ).on( "click", ".ibwp-htab-nav a", function() {

        var tab_type        = $(this).closest('.ibwp-htab-nav').attr('data-tab');
        var tab_active_cls  = ( tab_type == 'wp' ) ? 'nav-tab-active' : 'ibwp-htab-active';

        $(".ibwp-htab-nav").removeClass( tab_active_cls );
        $(".ibwp-htab-nav a").removeClass( tab_active_cls );

        if( tab_type == 'wp' ) {
             $(this).addClass( tab_active_cls );
        } else {
            $(this).parent('.ibwp-htab-nav').addClass( tab_active_cls );
        }

        $(".ibwp-htab-cnt").hide();

        var selected_tab = $(this).attr("href");
        $(selected_tab).show();

        $('.ibwp-htab-selected-tab').val(selected_tab);
        return false;
    });

    /* Remain selected horizontal tab for user */
    if( $('.ibwp-htab-selected-tab').length > 0 ) {
        var sel_tab = $('.ibwp-htab-selected-tab').val();

        if( typeof(sel_tab) !== 'undefined' && sel_tab != ''  ) {
            $('.ibwp-htab-nav [href="'+sel_tab+'"]').click();
        }
    }

    /* Module category filter */
    $( document ).on( 'change', '.ibwp-module-sort-cat', function() {
        var ele_val = $(this).val();

        $('.ibwp-dashboard-search').val('');
        $('.ibwp-no-module-search').hide();

        /* MixItUp plugin */
        var filter_val = ele_val ? '.ibwp-site-module-'+ele_val : '.ibwp-site-module-wrap';
        if( $(filter_val).length > 0 ) {
            $('.ibwp-site-modules-inr-wrap').mixItUp('filter', filter_val);
        }
    });

    /* MixItUp plugin */
    if( $('.ibwp-site-modules-inr-wrap').length > 0 ) {
        $('.ibwp-site-modules-inr-wrap').mixItUp({
            selectors: {
                target: '.ibwp-site-module-wrap',
            }
        });
    }

    /* Tooltip */
    if( $('.ibwp-tooltip').length > 0 ) {

        $('.ibwp-tooltip').each( function( attachment, index ) {

            var tooltip_cnt = $(this).attr('data-tooltip-content');
            tooltip_cnt = ( $(tooltip_cnt).length > 0 ) ? $(tooltip_cnt) : null;

            $(this).tooltipster({
                maxWidth: 500,
                content: tooltip_cnt,
                contentCloning: true,
                animation: 'grow',
                theme: 'ibwp-tooltipster tooltipster-punk',
                interactive: true,
                repositionOnScroll: true,
            });
        });
    }

    /* Reset Settings Button */
    $( document ).on( 'click', '.ibwp-reset-sett', function() {
        var ans;
        ans = confirm(IBWPAdmin.reset_msg);

        if(ans) {
            return true;
        } else {
            return false;
        }
    });
});

(function($) {
    /* When Page is fully loaded */
    $( window ).load(function() {
        var module_search = ibwp_get_url_parameter('search');
        if( typeof(module_search) !== 'undefined' && module_search != '' ) {
            $('.ibwp-dashboard-search-icon').trigger('click');
            $('.ibwp-dashboard-search').val(module_search).trigger('keyup');
        }
    });
})(jQuery);

function ibwp_get_url_parameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        result,
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            result = sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            return result.replace(/\+/g, ' ');
        }
    }
}