<?php
/**
 * Better Heading Module
 *
 * @package InboundWP
 * @subpackage Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wpdb;

// define constants
IBWP_Lite()->define( 'IBWP_BH_VERSION', '1.0' );	// version
IBWP_Lite()->define( 'IBWP_BH_DIR', dirname( __FILE__ ) );	//  dir
IBWP_Lite()->define( 'IBWP_BH_URL', plugin_dir_url( __FILE__ ) );	//  url
IBWP_Lite()->define( 'IBWP_BH_META_PREFIX', '_ibwp_bh_' );	//  meta prefix
IBWP_Lite()->define( 'IBWP_BH_TBL', $wpdb->prefix.'ibwp_bh_log' );	//  table name 


/**
 * Plugin Module Activation Function
 * Does the initial setup, sets the default values for the plugin module options
 * 
 * @subpackage Better Heading
 * @since 1.0
 */
function ibwp_bh_install(){
    
    // create table if not exist 
    ibwp_bh_create_tbl();

    // Update plugin version to option
	update_option( 'ibwp_bh_plugin_version', '1.0' );

}
add_action( 'ibwp_module_activation_hook_better-heading', 'ibwp_bh_install' );

// Function  File
require_once( IBWP_BH_DIR . '/includes/ibwp-bh-functions.php' );

// Admin Class File
require_once( IBWP_BH_DIR . '/includes/admin/class-ibwp-bh-admin.php' );

// Script Class File
require_once( IBWP_BH_DIR . '/includes/class-ibwp-bh-script.php' );

// Public Class File
require_once( IBWP_BH_DIR . '/includes/class-ibwp-bh-public.php' );