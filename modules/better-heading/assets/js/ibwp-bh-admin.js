// Admin js
jQuery( document ).ready(function($) {
    //console.log("ibwp-bh-admin.js");

    // Initialize Select 2 JS
    if( typeof $("#ibwp-bh-post-select").select2 === 'function' ){
        $("#ibwp-bh-post-select").select2();
    }

    //ib-wp-bh-clear-reset-all
    $(document).on('click', '#ib-wp-bh-clear-reset-all', function(){
        
        var post_id = $(this).data('post-id');
        if( confirm( ibwpbhAdmin.remove_all_reset ) ){
        
            var post_data = {
                'action'    : 'ibwp_bh_reset_all',
                'post_id'   : post_id,
            };

            // get data ajax 
            $.ajax({
                type: 'POST',
                //dataType: 'json',
                url: ajaxurl,
                data: post_data,
                success: function(response){
                    if(response == 'done'){
                        window.location.reload();
                    }
                }
            });
        }
    });
    
    // display daily report for view click count
    $('.ibwp-bh-show-result-btn').on('click',function(){
        var post_id = $('#ibwp-bh-post-select').val(); // ibwp-bh-post-select
        var s_date  = $('#ibwp-bh-start-date').val();  // ibwp-bh-start-date
        var n_date  = $('#ibwp-bh-end-date').val();  // ibwp-bh-start-date
        var display = $('#ibwp-bh-display-chart');
        
        var post_data = {
			'action'    : 'ibwp_bh_get_report',
			'post_id'   : post_id,
            'start_date': s_date,
            'end_date'  : n_date,
		};

        // get data ajax 
        $.ajax({
			type: 'POST',
            //dataType: 'json',
			url: ajaxurl,
			data: post_data,
			success: function(response){
                display.html( response );
			}
		});
    });

    //

    $(document).on('input propertychange', '#post-title-0', function() {
        var phead =  $(this).val();
        //console.log(phead);
        $('#ibwp-bh-heading').val(phead);
    })

    $(document).on('click', '#ib-wp-add-title', function(){
            
        if ( $(".ibwp-bh-title-row").length === 0 ) {
            var data_id_plus = 1;
            var unicid = createUUID();
        }else if ( $(".ibwp-bh-title-row").length >= 3 ) {
            $(".ibwp-bh-add-btn th").html('<div class="ibwp-pro-notice">'+ibwpbhAdmin.upgrade_message+'</div>');
            return;
        } else {
            var data_id      = $('.ibwp-bh-title-row:last').attr('data-id');
            var data_id_plus = parseInt(data_id) + 1;
            var unicid = createUUID();
        }

       var row_ =
    +  '<tr valign="top">'
    +    '<th scope="row">'
    +        '<label for="ibwp-bh-heading-'+ data_id_plus +'">Heading <span class="ibwp-bh-title-id">'+ data_id_plus +'</span></label>'
    +    '</th>'
    +    '<td>'
    +        '<input type="text" name="_ibwp_bh_headings['+ unicid +']" id="ibwp-bh-heading-'+ data_id_plus +'" class="ibwp-bh-heading" value="" placeholder >'
    +        '<span class="ibwp-bh-remove-title"><i class="dashicons dashicons-dismiss"></i></span><br/>'
    +        '<div class="ibwp-bh-clicks-views"><span class="ibwp-bh-view"><strong>Impression:</strong> 0</span> <span class="ibwp-bh-click"><strong>Click:</strong> 0</span><div class="ibwp-bh-cv-none"> <div class="ibwp-bh-cv" style="width:0%;"></div></div></div>'
    +    '</td>'
    +  '</tr>'
       $('.ibwp-post-bh-table tbody').find('.ibwp-bh-add-btn').before("<tr valign='top' class='ibwp-bh-title-row' data-id="+data_id_plus+">"+row_+"</tr>");
    });

    // Remove Title Row on click close icon
    $(document).on('click','.ibwp-bh-remove-title',function(){
        var remove;
        remove = confirm(ibwpbhAdmin.remove_title_row);
        if(remove) {
            $(this).parent().parent().remove();
            ibwp_bh_index_value();
            ibwp_bh_row_id();
            if( $(".ibwp-bh-title-row").length < 3 ) {
                var post_id = $(".ibwp-bh-add-btn input").val();
                $(".ibwp-bh-add-btn th").html('<button id="ib-wp-add-title" class="button button-primary">'+ibwpbhAdmin.add_title+'</button><button id="ib-wp-bh-clear-reset-all" data-post-id="'+post_id+'" class="button button-secondary ">'+ibwpbhAdmin.reset_title+'</button>');
            }
        }
    });
});

// Change Better Heading Index Value
function ibwp_bh_index_value() {
    var coupon_index_val    = document.getElementsByClassName('ibwp-bh-title-row');

    for (var i = 0; i < coupon_index_val.length; i++) {
        $('.ibwp-bh-title-id')[i].innerHTML = (i + 1);
    }
}

ibwp_bh_index_value();

// Reset row data-id on remove row function
function ibwp_bh_row_id() {
    var coupon_row_id    = document.getElementsByClassName('ibwp-bh-title-row');
    for (var i = 1; i < coupon_row_id.length; i++) {
        coupon_row_id[i].setAttribute('data-id', (parseInt(i)+1));
    }
}
ibwp_bh_row_id();

function createUUID() {
    // http://www.ietf.org/rfc/rfc4122.txt
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 7/*36*/; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    var uuid = s.join("");
    return uuid;
}

