<?php
/**
 * Function File
 *
 * @package InboundWP
 * @subpackage Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Function to create table for title click and views
 * 
 * @subpackage Better Heading
 * @since 1.0
 */
function ibwp_bh_create_tbl() {

    global $wpdb, $charset_collate;

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    $table_name = IBWP_BH_TBL;

    $sql = "CREATE TABLE {$table_name} (
        id int(10) NOT NULL AUTO_INCREMENT,
        post_id int(10) NOT NULL,
        meta_key varchar(25) NOT NULL,
        meta_value int(10) NOT NULL DEFAULT 0,
        save_date DATE NOT NULL,
        created_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
        modified_date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY  (id)
      ) $charset_collate;";
    
    dbDelta( $sql );  
}

/**
 * Function to get total data count 
 * 
 * @package Better Heading
 * @since 1.0
 */
function ibwp_bh_get_post_data_sum_between_date( $post_id, $key_id, $start_date, $end_date ){

    global $wpdb;
    $table_name = IBWP_BH_TBL;

	$row = $wpdb->get_results( "SELECT SUM(meta_value) AS meta_value FROM {$table_name} WHERE post_id = {$post_id} AND meta_key = '{$key_id}' AND save_date BETWEEN '{$start_date}' AND '{$end_date}'", 'ARRAY_A' );
    
    if( ! empty( $row[0]['meta_value'] ) ) {
        return $row[0]['meta_value'];
    } else {
        return 0;
    }
}

/**
 * Function to get total data count 
 * 
 * @package Better Heading
 * @since 1.0
 */
function ibwp_bh_get_post_data_sum( $post_id, $key_id ){

    global $wpdb;
    $table_name = IBWP_BH_TBL;

    $row = $wpdb->get_results( "SELECT SUM(meta_value) AS meta_value FROM {$table_name} WHERE post_id = {$post_id} AND meta_key = '{$key_id}'", 'ARRAY_A' );
    
    if( ! empty( $row[0]['meta_value'] ) ) {
        return $row[0]['meta_value'];
    } else {
        return 0;
    }
}

/**
 * Function to get data 
 * 
 * @package Better Heading
 * @since 1.0
 */
function ibwp_bh_get_post_data( $post_id, $key_id ){

    global $wpdb;
    $table_name = IBWP_BH_TBL;
    $date = date('Y-m-d');

	$row = $wpdb->get_results( "SELECT meta_value FROM {$table_name} WHERE post_id = {$post_id} AND meta_key = '{$key_id}' AND save_date = '{$date}' ORDER BY id DESC LIMIT 1", 'ARRAY_A' );
        
    if( !empty( $row[0]['meta_value'] ) ) {
        return $row[0]['meta_value'];
    }else {
        return 0;
    }
}

/**
 * Function to insert data
 * 
 * @package Better Heading
 * @since 1.0
 */
function ibwp_bh_insert_post_data( $post_id, $key_id, $value ){

    global $wpdb;
    $table_name = IBWP_BH_TBL;
    $date = date('Y-m-d');
    
	$result =  $wpdb->insert( 
        $table_name, 
        array(
        'save_date' => $date,
		'post_id' => $post_id,
		'meta_key' => $key_id,
		'meta_value' => $value,
		)
    );
    
    return $result;
}

/**
 * Function to update data else insert 
 * 
 * @package Better Heading
 * @since 1.0
 */
function ibwp_bh_update_post_data( $post_id, $key_id, $value ){

    global $wpdb;
    $table_name = IBWP_BH_TBL;
    $date = date('Y-m-d');

    $is_row  = ibwp_bh_get_post_data( $post_id, $key_id );
    if( $is_row  == 0 ){
        //insert
        $result = ibwp_bh_insert_post_data( $post_id, $key_id, $value );
    }else{
        //update
        $result =  $wpdb->update( $table_name, array(
            'post_id'       => $post_id,
		    'meta_value'    => $value,
            ),
            array( 
                'save_date' => $date,
                'meta_key'  => $key_id,
            )
        );
    }
}

/**
 * Function to reset delete post data 
 * 
 * @package Better Heading
 * @since 1.0
 */
function ibwp_bh_delete_post_data( $post_id ){

    global $wpdb;
    $table_name = IBWP_BH_TBL;
    
    // delete
    $result =  $wpdb->delete( $table_name, array( 'post_id'   => $post_id ) );

    return $result;
}