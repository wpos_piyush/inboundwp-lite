<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class IBWP_Bh_Script {
	
	function __construct() {
		
		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_bh_admin_style') );

		// Action to add script in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_bh_admin_script') );
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_admin_style( $hook ) {
		
		global $post_type;

		// If page is plugin setting page then enqueue script
		if( 'post' == $post_type ) {
			wp_register_style( 'ibwp-bh-admin-css', IBWP_BH_URL.'assets/css/ibwp-bh-admin.css', null, IBWP_BH_VERSION );
			wp_enqueue_style( 'ibwp-bh-admin-css' );
		}

		if( isset( $_GET['page'] ) && $_GET['page'] == 'ibwp-bh-report' ) {
			wp_enqueue_style( 'ibwp-select2-style' );
		}

	}

	/**
	 * Enqueue admin script
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_admin_script( $hook ) {

		global $post_type;

		// If page is plugin setting page then enqueue script
		if( 'post' == $post_type  || ( isset( $_GET['page'] ) && $_GET['page'] == 'ibwp-bh-report' ) ) {
			wp_register_script( 'ibwp-bh-admin-js', IBWP_BH_URL.'assets/js/ibwp-bh-admin.js', array('jquery'), IBWP_BH_VERSION, true );
			wp_localize_script( 'ibwp-bh-admin-js', 'ibwpbhAdmin', array(
				'remove_title_row'	=> __('Click OK to remove title row.', 'inboundwp-lite'),
				'remove_all_reset'	=> __('Click OK to remove All Data.', 'inboundwp-lite'),
				'upgrade_message'	=> sprintf( __('In lite version, you can add only 3 headings. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to add more headings.', 'inboundwp-lite'), IBWPL_PRO_LINK ),
				'add_title'			=> __('Add New Title', 'inboundwp-lite'),
				'reset_title'		=> __('Reset all', 'inboundwp-lite'),
	    	));
			wp_enqueue_script( 'ibwp-bh-admin-js' );
			wp_enqueue_script( 'ibwp-select2-script' );
		}
	}
}

$ibwp_bh_script = new IBWP_Bh_Script();