<?php
/**
 * Public Class
 *
 * Handles the Public side functionality of plugin
 *
 * @package Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class IBWP_Bh_Public {

	function __construct() {

		add_action( 'init', array( $this, 'ibwp_bh_start_session' ), 1 );

		add_action( 'wp', array( $this, 'ibwp_bh_single_post_track' ) );
		
		add_filter( 'the_title', array( $this, 'ibwp_bh_title_change' ), 10, 2 );
	}

	/**
	 * Function to start php core session store 
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_start_session() {
	    if( ! session_id() ){
	        session_start();
	    } 
	}

	/**
	 * Function to track per session title is click 
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_single_post_track() {

	    global $post,$post_type;

	    if( is_singular('post') ){

	        $post_id 	= $post->ID;
	        $prefix 	= IBWP_BH_META_PREFIX; // Metabox prefix
	        $enable 	= get_post_meta( $post_id, $prefix.'enable', true );
	        
	        if( ! $enable ) { 
	        	return; 
	        }

	        // if session set
	        if( isset( $_SESSION[ "ibwp_bh_".$post_id] ) ) {
	            $n = $_SESSION["ibwp_bh_".$post_id] ;
	        }else{
	            return; // on direct single page
	        }

	        // Storing title view and update click count
	        if( ! isset($_SESSION["ibwp_bh_view_".$post_id] ) ) { 

	            $headings	= get_post_meta( $post_id, $prefix.'headings', true );
	            if( $n >= 0 ){
	                $key = array_keys($headings)[$n];;
	            }else{
	                $key = '0';
	            }
	            
	            $click_id = "post-click-".$key."-".$n;
	            $click = ibwp_bh_get_post_data( $post_id, $click_id );
	            $click = ($click) ? ($click+1) : 1;
	            
	            ibwp_bh_update_post_data( $post_id, $click_id, $click );
	            $_SESSION["ibwp_bh_view_".$post_id] = true;
	        }
	    }
	}

	/**
	 * Function to change title 
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_title_change( $title, $id = null ) {

		// Check id and admin page
	    if( empty( $id ) || is_admin() ) { 
	    	return $title; 
	    }

	    // Check post type
	    $post_type =  get_post_type( $id );
	    if( $post_type != 'post' ){ 
	    	return $title; 
	    }

	    $prefix = IBWP_BH_META_PREFIX; // Metabox prefix
	    $enable = get_post_meta( $id, $prefix.'enable', true );
	    
	    // Check enable multiple headings
	    if( ! $enable ) { 
	    	return $title; 
	    }
	    
	    $headings = get_post_meta( $id, $prefix.'headings', true );
	    if( count($headings) <= 0 ){ 
	    	return $title; 
	    }

	    if( isset( $_SESSION[ "ibwp_bh_".$id] ) ){
	        $n = $_SESSION["ibwp_bh_".$id] ;
	    }else{
	        $n = $_SESSION["ibwp_bh_".$id] = rand(-1,count($headings)-1);
	        if( $n >= 0 ){
	        	$key = array_keys($headings)[$n];
	        }else{
	            $key = '0';
	        }
	        
	        $view_id = "post-view-".$key."-".$n;
	        $view = ibwp_bh_get_post_data( $id, $view_id );
	        $view = ($view) ? ($view+1) : 1;
	        
	        update_post_meta( $id, $view_id, $view );
	        ibwp_bh_update_post_data( $id, $view_id, $view );
	    }

	    if( $n >= 0 ){
	        $title = array_values($headings)[$n];
	        return $title; 
	    }else{
	        return $title;
	    }
	}
}

$ibwp_bh_public = new IBWP_Bh_Public();