<?php
/**
 * How it works file
 *
 * @package Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<h1><?php _e('How to use Better Heading','inboundwp-lite'); ?></h1>
<style type="text/css">
        .ibwp-mrgin-right-20{ margin-right: 20px; }
</style>

<div class="post-box-container">
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2- ibwp-mrgin-right-20">
			<!--How to Use HTML -->
			<div id="post-body-content">
				<div class="metabox-holder">
					<div class="meta-box-sortables ui-sortable">    
						<div class="postbox">
							
							<h3 class="hndle">
								<span><?php _e( 'How It Works', 'inboundwp-lite' ); ?></span>
							</h3>
							
							<div class="inside">
								<table class="form-table">
									<tbody>
										<tr>
											<th>
												<label><?php _e('Short Description', 'inboundwp-lite'); ?>:</label>
											</th>
											<td>
												<p><?php _e('Better Heading is one easy tools to choose best title after anlyzing 3 title for single blog or article. For this, you just have to add 3 heading for single post and it will be displayed randomly to visitor. As per the visistos views and clicks on post, you can able to check reports monthly, weekly and daily basis.', 'inboundwp-lite'); ?></p>
											</td>
										</tr>

										<tr>
											<th>
												<label><?php _e('Getting Started with Better Heading', 'inboundwp-lite'); ?>:</label>
											</th>
											<td>
												<ul>
													<li><?php _e('Step-1: Go to any Post or Create new post.', 'inboundwp-lite'); ?></li>
													<li><?php _e('Step-2: Create new Titles form metabox save post.', 'inboundwp-lite'); ?></li>
													<li><?php _e('Step-3: Enable Better Heading in metabox.', 'inboundwp-lite'); ?></li>
												</ul>
											</td>
										</tr>
									</tbody>
								</table>
							</div><!-- .inside -->
						</div><!-- #general -->
					</div><!-- .meta-box-sortables ui-sortable -->
				</div><!-- .metabox-holder -->
			</div><!-- #post-body-content -->			
		</div><!-- #post-body -->
	</div><!-- #poststuff -->
</div><!-- #post-box-container -->