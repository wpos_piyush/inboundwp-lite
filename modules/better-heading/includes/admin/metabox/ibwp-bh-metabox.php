<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @package Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$prefix = IBWP_BH_META_PREFIX; // Metabox prefix

$post_title = $post->post_title;
$post_id = $post->ID;

$enable = get_post_meta( $post_id, $prefix.'enable', true );

// Getting saved values
$headings = get_post_meta( $post_id, $prefix.'headings', true );
if(! is_array($headings) ) { $headings = array(); }
?>

<table class="form-table ibwp-post-bh-table">
	
	<tbody>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-bh-enable"><?php _e('Enable', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="checkbox" id="ibwp-bh-enable" class="ibwp-bh-enable" name="<?php echo $prefix; ?>enable" <?php checked( $enable, true ); ?> value="1" width="100%"/>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-bh-heading"><?php _e('Primary Heading', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" id="ibwp-bh-heading" class="ibwp-bh-heading" value="<?php echo ibwpl_esc_attr($post_title); ?>" width="100%" disabled="true" />
				<span class="ibwp-bh-remove-title"></span><br/>
				<?php
					$n = '-1'; $key = '0';
					$view_id = "post-view-".$key."-".$n;
					$view = ibwp_bh_get_post_data_sum( $post_id, $view_id );
					$click_id = "post-click-".$key."-".$n;
					$clicks = ibwp_bh_get_post_data_sum( $post_id, $click_id );
					$view 	= ($view)  ? $view   : 0;
					$clicks = ($clicks)? $clicks : 0;
					if( $view == 0 ) { 
						$prsnt = 0;
					} else {
						$prsnt  = $clicks/$view*100;
					}
				?>
				<div class="ibwp-bh-clicks-views">
					<span class="ibwp-bh-view"><strong><?php _e('Impression: ','inboundwp-lite');?></strong> <?php echo $view; ?></span>
					<span class="ibwp-bh-click"><strong><?php _e('Click: ','inboundwp-lite'); ?></strong> <?php echo $clicks; ?></span>
					<div class="ibwp-bh-cv-none">
		  				<div class="ibwp-bh-cv" style="width:<?=$prsnt;?>%"></div>
					</div>
				</div>
			</td>
		</tr>
				
		<?php
		$count = 1;
		foreach($headings as $key => $value) { ?>

			<tr valign="top" class="ibwp-bh-title-row" data-id="<?php echo ibwpl_esc_attr($count); ?>">
				<th scope="row">
					<label for="ibwp-bh-heading-<?php echo $count; ?>"><?php _e('Heading ', 'inboundwp-lite'); ?><span class="ibwp-bh-title-id"><?php echo $count; ?></span></label>
				</th>
				<td>
					<input type="text" id="ibwp-bh-heading-<?php echo $count; ?>" class="ibwp-bh-heading" name="<?php echo $prefix; ?>headings[<?php echo $key;?>]" value="<?php echo ibwpl_esc_attr($value); ?>"  width="100%"/>
					<span class="ibwp-bh-remove-title"><i class="dashicons dashicons-dismiss"></i></span><br/>
					<?php
						$n = $count-1;
						$view_id = "post-view-".$key."-".$n;
						$view = ibwp_bh_get_post_data_sum( $post_id, $view_id );
						$click_id = "post-click-".$key."-".$n;
						$clicks = ibwp_bh_get_post_data_sum( $post_id, $click_id );
						$view 	= ($view)  ? $view   : 0;
						$clicks = ($clicks)? $clicks : 0;	
						if( $view == 0 ){
							$prsnt = 0;
						} else {
							$prsnt  = $clicks/$view*100;
						}
					?>
					<div class="ibwp-bh-clicks-views">
						<span class="ibwp-bh-view"><strong><?php _e('Impression: ','inboundwp-lite');?></strong> <?php echo $view; ?></span>
						<span class="ibwp-bh-click"><strong><?php _e('Click: ','inboundwp-lite'); ?></strong> <?php echo $clicks; ?></span>
						<div class="ibwp-bh-cv-none">
			  				<div class="ibwp-bh-cv" style="width:<?=$prsnt;?>%"></div>
						</div>
					</div>
				</td>
			</tr>
		<?php
		$count++;
		}
		$head_count = count($headings);
		?>
		<tr valign="top" class="ibwp-bh-add-btn">
			<th scope="row"  colspan="2">
				<input type="hidden" name="ibwp_post_id" value="<?php echo $post_id; ?>" />
				<?php if( count($headings) < 3) { ?>
					<button id="ib-wp-add-title" class="button button-primary"><?php _e('Add New Title','inboundwp-lite');  ?></button>
					<button id="ib-wp-bh-clear-reset-all" data-post-id="<?php echo $post_id; ?>" class="button button-secondary "><?php _e('Reset all','inboundwp-lite');  ?></button>
				<?php } else { ?>
					<div class="ibwp-pro-notice">
						<?php echo sprintf( __('In lite version, you can add only 3 headings. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to add more headings.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
							
						</div>
				<?php } ?>
			</th>
		</tr>

	</tbody>
</table><!-- end .ibwp-post-sett-table -->