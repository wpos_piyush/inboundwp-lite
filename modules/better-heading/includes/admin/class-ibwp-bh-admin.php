<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package Better Heading
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class IBWP_Bh_Admin {
	
	function __construct() {

		// Action to register admin menu
		add_action( 'admin_menu', array( $this, 'ibwp_bh_register_menu' ) );
		
		// Action to add metabox
		add_action( 'add_meta_boxes', array( $this, 'ibwp_bh_post_sett_metabox' ) );

		// Action to save metabox
		add_action( 'save_post', array( $this, 'ibwp_bh_save_metabox_value' ) );

		// Action to reset heading view and click count
		add_action( 'wp_ajax_ibwp_bh_reset_all', array( $this, 'ibwp_bh_reset_all_post_report_cb_fn' ) );

		// Action to create resport of headings
		add_action( 'wp_ajax_ibwp_bh_get_report', array( $this, 'ibwp_bh_get_post_report_cb_fn' ) );
	}

	/**
	 * Admin Menu 
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_register_menu() {

		add_menu_page( __('Better Heading', 'inboundwp-lite'), __('Better Heading - IBWP', 'inboundwp-lite'), 'manage_options', 'ibwp-bh-how-it-work', array($this, 'ibwp_bh_how_it_work'),'dashicons-editor-bold' ,5 );
		add_submenu_page( 'ibwp-bh-how-it-work', __('Report', 'inboundwp-lite'), __('Report', 'inboundwp-lite'), 'manage_options', 'ibwp-bh-report', array($this, 'ibwp_bh_display_data') );
	}

	/**
	 * Html file of how to us
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_how_it_work(){
		include_once( IBWP_BH_DIR . '/includes/admin/ibwp-bh-how-it-work.php' );
	}

	/**
	 * Html file of report
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_display_data(){
		include_once( IBWP_BH_DIR . '/includes/admin/ibwp-bh-report.php' );
	}

	/**
	 * Post Settings Metabox
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_post_sett_metabox() {
		
		add_meta_box( 'ibwp-post-sett', __( 'Better Heading - IBWP', 'inboundwp-lite' ), array($this, 'ibwp_bh_post_sett_mb_content'), 'post', 'normal', 'high' );
	}

	/**
	 * Post Settings Metabox HTML
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_post_sett_mb_content() {
		include_once( IBWP_BH_DIR .'/includes/admin/metabox/ibwp-bh-metabox.php');
	}

	/**
	 * Function to save metabox values
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_save_metabox_value( $post_id ) {

		global $post_type;

		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                	// Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )  	// Check Revision
		|| ( $post_type !=  'post' ) )              					// Check if current post type is supported.
		{
		  return $post_id;
		}
		
		$prefix = IBWP_BH_META_PREFIX; // Taking metabox prefix
		
		$enable		= isset($_POST[$prefix.'enable']) 	? 1 : 0;
		$headings	= isset($_POST[$prefix.'headings']) ? array_map('ibwpl_clean_html' ,$_POST[$prefix.'headings']) : array();
		$headings 	= array_filter($headings);
		
		update_post_meta($post_id, $prefix.'enable', $enable);
		update_post_meta($post_id, $prefix.'headings', $headings);
	}

	/**
	 * Function to reset post headings view and clicks
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_reset_all_post_report_cb_fn() {
	    //senitize
	    $post_id = sanitize_text_field( $_POST['post_id'] );
	    //validate
	    if(empty( $post_id )){
	        _e('Please Select Post.','inboundwp-lite'); die();
	    }
	    //
	    $result = ibwp_bh_delete_post_data( $post_id );
	    if( $result ){
	        echo 'done';
	    }else{
	        echo 0;
	    }
	    wp_die();
	}

	/**
	 * Function to create repost of heading with some date filters range selection
	 * 
	 * @package Better Heading
	 * @since 1.0
	 */
	function ibwp_bh_get_post_report_cb_fn() {
    
	    global $wpdb;

	    //senitize
	    $post_id 	= sanitize_text_field( $_POST['post_id'] );
	    $start_date = sanitize_text_field( $_POST['start_date'] );
	    $end_date 	= sanitize_text_field( $_POST['end_date'] );
	    //validate
	    if(empty( $post_id )){
	        _e('Please Select Post.','inboundwp-lite'); die();
	    }
	    if(empty( $start_date )){
	        _e('Please Select Start Date.','inboundwp-lite'); die();
	    }
	    if(empty( $end_date )){
	        _e('Please Select End Date.','inboundwp-lite'); die();
	    }
	    
	    //get data form tbl
	    $prefix = IBWP_BH_META_PREFIX; // Metabox prefix
	    $title = get_the_title($post_id);
	    $enable = get_post_meta( $post_id, $prefix.'enable',true);
	    // Getting saved values
	    $headings	= get_post_meta( $post_id, $prefix.'headings', true );
	    
	    //display data 
	    ?>
	    <table class="form-table wp-list-table widefat fixed striped ">
		    <tbody>

	        <tr>
	            <td><strong><?php _e('Index','inboundwp-lite'); ?></strong></td>
	            <td><strong><?php _e('Title','inboundwp-lite'); ?></strong></td>
	            <td><strong><?php _e('Impression','inboundwp-lite'); ?></strong></td>
	            <td><strong><?php _e('Clicks','inboundwp-lite'); ?></strong></td>
	            <td><strong><?php _e('Percentage','inboundwp-lite'); ?></strong></td>
	        </tr>
	        

	        <tr valign="top">
	            <?php
	                $id 		= $post_id; $n = '-1';
	                $key 		= '0';
	                $trans_id 	= "post-view-".$key."-".$n;
	                $view 		= ibwp_bh_get_post_data_sum_between_date( $id, $trans_id ,$start_date ,$end_date);
	                $click_id 	= "post-click-".$key."-".$n;
	                $clicks 	= ibwp_bh_get_post_data_sum_between_date( $id, $click_id ,$start_date ,$end_date);
	                $view 		= ($view)  ? $view   : 0;
	                $clicks 	= ($clicks)? $clicks : 0;
	                
	                if( $view == 0 ) {
	                	$prsnt 	= 0;
	                } else {
	                	$prsnt  = $clicks/$view*100;
	                }
				?>
				<td scope="row">
					<?php _e('Primary Heading', 'inboundwp-lite'); ?>
				</td>
				<td><?php echo $title; ?></td>
	            <td><?php echo $view; ?></td>
	            <td><?php echo $clicks; ?></td>
	            <td><?php echo $prsnt;?>%</td>
			</tr>

	        <?php
			$count = 1;
			foreach($headings as $key => $value){
			?>
			<tr valign="top" class="ibwp-bh-title-row" data-id="<?php echo ibwpl_esc_attr($count); ?>">
	            <?php
	                $id 		= $post_id;
	                $n 			= $count-1;
	                $trans_id 	= "post-view-".$key."-".$n;
	                $view 		= ibwp_bh_get_post_data_sum_between_date( $id, $trans_id ,$start_date ,$end_date);
	                $click_id 	= "post-click-".$key."-".$n;
	                $clicks 	= ibwp_bh_get_post_data_sum_between_date( $id, $click_id ,$start_date ,$end_date);
	                $view 		= ($view)  ? $view   : 0;
	                $clicks 	= ($clicks)? $clicks : 0;	
	                
	                if( $view == 0 ) {
	                	$prsnt 	= 0;
	                } else {
	                	$prsnt  = $clicks/$view*100;
	                }
				?>
				<td scope="row">
					<label for="ibwp-bh-headings"><?php _e('Heading ', 'inboundwp-lite'); ?><span class="ibwp-bh-title-id"><?php echo $count; ?></span></label>
				</td>
				<td><?php echo $value; ?></td>
				<td><?php echo $view; ?></td>
				<td><?php echo $clicks; ?></td>
				<td><?php echo $prsnt;?>%</td>
			</tr>
			<?php
			$count++;
			}
			?>
	        </tbody>
	    </table>
	    <?php
		wp_die();
	}
}

$ibwp_bh_admin = new IBWP_Bh_Admin();