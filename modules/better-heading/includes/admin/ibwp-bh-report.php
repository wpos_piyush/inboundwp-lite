<?php
/**
 * File to display report of better headings with html
 *
 * @package Better Heading
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Getting Spin Wheel Post Type
$ibwp_bh_post_arg = array(
	'post_type'		=> 'post',
	'orderby'		=> 'title',
	'order'			=> 'ASC',
	'post_status'	=> 'publish',
	'posts_per_page'=> -1
);

$ibwp_bh_post 	= get_posts( $ibwp_bh_post_arg );
$current_date 	= date_i18n( 'Y-m-d', current_time( 'timestamp' ) ); 
?>

<div class="wrap wp-ibwp-bh-settings">
	<h2><?php _e( 'Report', 'inboundwp-lite' ); ?></h2><br/>
	<div class="wrap ibwp-tools-main">
		<div id="post-body-content">
			<div class="metabox-holder">
				<div class="postbox ibwp-bh-postbox">
					<h3 class="ibwp-title-border">
						<span><?php _e( 'Generate Report', 'inboundwp-lite' ); ?></span>
					</h3>
					<div class="inside">
						<p><?php _e('Please select post and date...', 'inboundwp-lite'); ?></p>
						<form id="ibwp-bh-data-form" class="ibwp-bh-display-data-form " method="post">
							<select name="ibwp_bh_post_title" id="ibwp-bh-post-select" class="ibwp-export-fields">
								<option value=""><?php _e('Select Post','inboundwp-lite'); ?></option>
								<?php
									if( !empty($ibwp_bh_post) ) {
										foreach ( $ibwp_bh_post as $post_key => $post_val ) {
											$post_title = !empty($post_val->post_title) ? $post_val->post_title : sprintf( __('Email - %d', 'inboundwp-lite'), $post_val->ID );
											
											echo '<option value="'.$post_val->ID.'" ' . selected( isset( $_GET['ibwp_bh_post_title'] ) ? $_GET['ibwp_bh_post_title'] : '', $post_val->ID, false ) . '>' . $post_title . '</option>';
										}
									}
								?>
							</select>
							<input id='ibwp-bh-start-date' type="date" name="start_date" value="" class="ibwp-export-fields" placeholder="<?php _e('Choose Start Date', 'inboundwp-lite'); ?>" max="<?php echo ibwpl_esc_attr($current_date); ?>" title="Choose Start Date" />
							<input id='ibwp-bh-end-date' type="date" name="end_date" value="" class="ibwp-export-fields" placeholder="<?php _e('Choose End Date', 'inboundwp-lite'); ?>" max="<?php echo ibwpl_esc_attr($current_date); ?>" title="Choose End Date" />
							<span class="ibwp-bh-show-result-btn button"><?php _e('Show Result','inboundwp-lite'); ?></span>
							
						</form><br>
						<div class="ibwp-chart-wrap">
							<div id='ibwp-bh-display-chart' class="ibwp-chart" >
                                <h1> <?php _e('No data found.','inboundwp-lite'); ?> </h1>
                            </div>
						</div>
					</div><!-- .inside -->
				</div><!-- #general -->
			</div><!-- .metabox-holder -->
		</div><!-- #post-body-content -->
	</div>
</div>