<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package Spin Wheel
 * @since 1.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Ibwp_Spw_Script {
	
	function __construct() {
		
		// Action to add style at front side
		add_action( 'wp_enqueue_scripts', array( $this, 'ibwp_front_style' ) );
		
		// Action to add script at front side
		add_action( 'wp_enqueue_scripts', array( $this, 'ibwp_front_script' ) );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array( $this, 'ibwp_spw_admin_style' ) );

		// Action to add script in backend
		add_action( 'admin_enqueue_scripts', array( $this, 'ibwp_spw_admin_script' ) );
	}

	/**
	 * Function to add style at front side
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_front_style() {	

		wp_register_style( 'ibwp-spw-public', IBWP_SPW_URL.'assets/css/ibwp-spw-public.css', null, IBWP_SPW_VERSION );
		wp_enqueue_style( 'ibwp-spw-public' );

	}
	
	/**
	 * Function to add script at front side
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	
	function ibwp_front_script() {
		global $post, $spw_active_id;

		if( empty( $spw_active_id ) ) {
			return;
		}

		$prefix 	= IBWP_SPW_META_PREFIX;

		foreach ( $spw_active_id as $key => $id ) {
			// Check notification enable or not
            $enable = get_post_meta( $id, $prefix. 'enable_spin_wheel', true );
            if( ! $enable ) {
            	continue;
            }

   			$icon_position				= get_post_meta( $id, $prefix. 'icon_position', true );
			$email_store				= get_post_meta( $id, $prefix. 'email_store', true );
			$wheel_bg_image 			= get_post_meta( $id, $prefix. 'wheel_bg_image', true );
			$wheel_bg_color				= get_post_meta( $id, $prefix. 'wheel_bg_color', true );
			$enable_terms_conditions	= get_post_meta( $id, $prefix. 'enable_terms_conditions', true );
			$spining_duration			= get_post_meta( $id, $prefix. 'spining_duration', true );
			$button_text				= get_post_meta( $id, $prefix. 'button_text', true );
			$coupon_type				= get_post_meta( $id, $prefix. 'coupon_type', true );
			$spin_wheel_coupons			= get_post_meta( $id, $prefix. 'spin_wheel_coupons', true );
			$email_subject				= get_post_meta( $id, $prefix. 'email_subject', true );
			$email_heading				= get_post_meta( $id, $prefix. 'email_heading', true );
			$email_message				= get_post_meta( $id, $prefix. 'email_message', true );
			$win_message				= get_post_meta( $id, $prefix. 'win_message', true );
			$lost_message				= get_post_meta( $id, $prefix. 'lost_message', true );
			$coupon_code_count 			= count($spin_wheel_coupons);
			
			$get_url = IBWP_SPW_URL;
			
			if( !empty( $spin_wheel_coupons ) ) {
				foreach ($spin_wheel_coupons as $file_key => $file_data) {
					$file_data['coupon_code']			= ( $file_data['coupon_code'] != '' ) 			? $file_data['coupon_code'] 		: '';
					$file_data['coupon_lbl']			= ( $file_data['coupon_lbl'] != '' ) 			? $file_data['coupon_lbl'] 			: '';
					$file_data['coupon_probability']	= ( $file_data['coupon_probability'] != '' ) 	? $file_data['coupon_probability']	: '';
					$file_data['coupon_color']			= ( $file_data['coupon_color'] != '' ) 			? $file_data['coupon_color'] 		: '';
					$coupon_code[ $file_key ] 			= $file_data['coupon_code'];
					$coupon_lbl[ $file_key ] 			= $file_data['coupon_lbl'];
					$coupon_probability[ $file_key ] 	= $file_data['coupon_probability'];
					$coupon_color[ $file_key ] 			= $file_data['coupon_color'];
				}
			}

			foreach ($coupon_lbl as $key => $value) {
				$coupon_probability_val[] 	= $coupon_probability[$key];
				$coupon_color_val[] 		= $coupon_color[$key];
				$coupon_lbl_val[] 			= $coupon_lbl[$key];
			}

			// Registring Wheel script
			wp_register_script( 'ibwp-spw-wheel-js', IBWP_SPW_URL.'assets/js/ibwp-spw-public.js', array('jquery'), IBWP_SPW_VERSION, true );
			wp_localize_script('ibwp-spw-wheel-js', 'ibwpspwPublic', array(
				'id'						=> $id,
				'enable_spin_wheel'			=> $enable,
				'icon_position'				=> $icon_position,
				'email_store'				=> $email_store,
				'wheel_bg_color'			=> $wheel_bg_color,
				'enable_terms_conditions'	=> $enable_terms_conditions,
				'spining_duration'			=> $spining_duration,
				'button_text'				=> $button_text,
				'coupon_type'				=> $coupon_type,
				'spin_wheel_coupons'		=> $spin_wheel_coupons,
				'coupon_code_count'			=> $coupon_code_count,
				'coupon_lbl'				=> $coupon_lbl_val,
				'coupon_color'				=> $coupon_color_val,
				'email_subject'				=> $email_subject,
				'email_heading'				=> $email_heading,
				'email_message'				=> $email_message,
				'win_message'				=> $win_message,
				'lost_message'				=> $lost_message,
				'get_url'					=> $get_url,
				'please_wait_msg'			=> __('Page is redirecting. Please wait...','inboundwp-lite'),
				'enter_email_msg'			=> __('Please enter your email address.','inboundwp-lite'),
            	'valid_email_msg'			=> __('Please enter valid email address.','inboundwp-lite'),
            	'terms_condition_msg'		=> __('Please agree with our term and condition.','inboundwp-lite'),
			));

        }

		wp_enqueue_script('ibwp-spw-wheel-js');
		
		// Load Google Analytics JS
		wp_register_script( 'ibwp-spw-analytics-js', IBWP_SPW_URL.'assets/js/ibwp-spw-analytics.js', array('jquery'), IBWP_SPW_VERSION, true );
		wp_enqueue_script('ibwp-spw-analytics-js');

	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_admin_style() {

		global $post_type;
		
		// If page is plugin setting page then enqueue script
		wp_enqueue_style( 'ibwp-select2-style' );
		
		// Colorpicker Styles
        wp_enqueue_style( 'wp-color-picker' );
		
		// If page is plugin setting page then enqueue script
		wp_register_style( 'ibwp-spw-admin-css', IBWP_SPW_URL.'assets/css/ibwp-spw-admin.css', null, IBWP_SPW_VERSION );
		wp_enqueue_style( 'ibwp-spw-admin-css' );

		// Registering main admin style 
		wp_enqueue_style( 'ibwp-admin-style' );
	}

	/**
	 * Enqueue admin script
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_admin_script() {

		global $wp_version, $post_type, $typenow;

		$new_ui = $wp_version >= '3.5' ? '1' : '0'; // Check wordpress version for older scripts
		
		// Enqueue Select 2 JS
		wp_enqueue_script('ibwp-select2-script');
		
		// If page is plugin post type then enqueue script
		if( IBWP_SPW_POST_TYPE == $post_type || IBWP_SPW_POST_TYPE == $typenow ) {
			// Enquie WP Editor Script
			wp_enqueue_editor();
		
			// Enquie Media Script
			wp_enqueue_media();
			
			// Colorpicker Scripts
	        wp_enqueue_script( 'wp-color-picker' );
			
			// If page is plugin setting page then enqueue script
			wp_register_script( 'ibwp-spw-admin-js', IBWP_SPW_URL.'assets/js/ibwp-spw-admin.js', array('jquery'), IBWP_SPW_VERSION, true );
			wp_localize_script( 'ibwp-spw-admin-js', 'ibwpspwAdmin', array(
				'error_msg' 				=> __('Sorry, Something Happened Wrong.', 'inboundwp-lite'),
				'new_ui'                    =>  $new_ui,
	            'remove_coupon_row'         => __('Click OK to remove coupon row.', 'inboundwp-lite'),
	            'remove_coupon_err'         => __('Must have at least 3 columns!', 'inboundwp-lite'),
	            'coupon_type_err'         	=> __('You must need to select coupon part.', 'inboundwp-lite'),
	            'probability_err'         	=> __('The coupons total probability must be 100%.', 'inboundwp-lite'),
	    	));
			wp_enqueue_script( 'ibwp-spw-admin-js' );

			// Registering main admin script 
			wp_enqueue_script( 'ibwp-admin-js' );
		}

	}
}
$ibwp_spw_script = new Ibwp_Spw_Script();