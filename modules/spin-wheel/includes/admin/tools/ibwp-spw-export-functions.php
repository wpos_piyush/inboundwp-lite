<?php
/**
 * Email Export Functions
 *
 * @package Spin Wheel
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Function to get file if exist else create the new
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_get_file( $file = false ) {

	$file_data = '';
	if ( $file && @file_exists( $file ) ) {
		$file_data = @file_get_contents( $file );
	} else {
		@file_put_contents( $file, '' );
		@chmod( $file, 0664 );
	}
	return $file_data;

}

/**
 * Append data to export file
 *
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_file_add_data( $file, $data = '' ) {

	$file_data = ibwp_spw_get_file( $file );
	$file_data .= $data;
	@file_put_contents( $file, $file_data );

}

/**
 * Emails Export File
 *
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_export_emails_file() {

	$upload_dir 	= wp_upload_dir();
	$filename   	= 'spin-wheel-emails.csv';
	$file 			= trailingslashit( $upload_dir['basedir'] ) . $filename;
	return $file;

}

/**
 * Function to export emails data
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_export_emails( $args = array() ) {

	global $wpdb;
	
	// Taking some variables
	$result 				= array( 'status' => 0 );
	$col_data 				= '';
	$row_data 				= '';
	$limit					= 10;
	$start_date				= isset( $args['start_date'] ) 				? $args['start_date'] 			: '';
	$end_date				= isset( $args['end_date'] ) 				? $args['end_date'] 			: '';
	$wheel_id				= !empty( $args['ibwp_spw_wheel_title'] ) 	? $args['ibwp_spw_wheel_title'] : 0;
	$page 					= !empty( $args['page'] ) 					? $args['page'] 				: 1;
    $data_process			= isset( $args['data_process'] ) 			? $args['data_process'] 		: 0;
    $total_count			= isset( $args['total_count'] ) 			? $args['total_count'] 			: 0;
	$file 					= ibwp_spw_export_emails_file();
	
	// CSV Columns
	$cols = array(
			'id'			=> __( 'ID',   'inboundwp-lite' ),
			'name'			=> __( 'Name', 'inboundwp-lite' ),
			'email'			=> __( 'Email Address', 'inboundwp-lite' ),
			'coupon_code'	=> __( 'Coupon Code', 'inboundwp-lite' ),
			'post_title'	=> __( 'Wheel Name', 'inboundwp-lite' ),
			'created_date'	=> __( 'Registered Date', 'inboundwp-lite' ),
		);

	$total_cols = count( $cols );
	$Spw_Email_Modal 		= new Spw_Email_Modal();
	
	// If process is newly started - Step 1
	if( $page < 2 ) {
		
		// Make sure we start with a fresh file on step 1
		@unlink( $file );
		$i = 1;
		
		foreach( $cols as $col_id => $column ) {
			$col_data .= '"' . addslashes( $column ) . '"';
			$col_data .= ( $i == $total_cols ) ? '' : ',';
			$i++;
		}

		$col_data .= "\r\n";
		ibwp_spw_file_add_data( $file, $col_data ); // Add columns to file
		
		// Taking total counts of data at first time
		$total_count = $Spw_Email_Modal->ibwp_spw_export_emails_count( array(
															'wheel_id' 		=> $wheel_id,
															'start_date'	=> $start_date,
															'end_date'		=> $end_date,
														) );
	}

	// Getting data of emails
	$users_data = $Spw_Email_Modal->ibwp_spw_export_emails_data( array(
															'wheel_id' 		=> $wheel_id,
															'start_date'	=> $start_date,
															'end_date'		=> $end_date,
															'limit'			=> $limit,
															'page'			=> $page,
														) );
	
	// If user data found
	if( $users_data ) {
		
		// Output each row
		foreach ( $users_data as $row ) {
			$j = 1;
			$count++;
			$row_data .= '"' . ($count+( ($page-1) *$limit) ) . '",';
			
			foreach ( $cols as $col_id => $column ) {
				// Make sure the column is valid
				if ( isset( $row->$col_id ) ) {
					if($j == 1){
						$j++;
						continue;
					}
					$row_data .= '"' . addslashes( preg_replace( "/\"/","'", $row->$col_id ) ) . '"';
					$row_data .= $j == count( $cols ) ? '' : ',';
					$j++;
				}
			}
			$row_data .= "\r\n";

		}
		
		// Record total process data
		$data_process = ( $data_process + count($users_data) );
		ibwp_spw_file_add_data( $file, $row_data ); // Add rows to file
		
		// Calculate percentage
		$percentage = 100;
		
		if( $total_count > 0 ) {
			$percentage = ( ( $limit * $page ) / $total_count ) * 100;
		}
		
		if( $percentage > 100 ) {
			$percentage = 100;
		}
		
		// If process is done
		if( $data_process >= $total_count ) {
			
			$args = array(
						'post_type'			=> IBWP_SPW_POST_TYPE,
						'page' 				=> 'ibwp-spw-tools',
						'nonce'      		=> wp_create_nonce( 'ibwp_spw_export' ),
						'ibwp_spw_action' 	=> 'ibwp_spw_export',
					);
			$result['url'] = add_query_arg( $args, admin_url('edit.php') );

		}
		
		$result['status'] 		= 1;
		$result['page']			= ( $page + 1 );
		$result['total_count'] 	= $total_count;
		$result['percentage'] 	= $percentage;
		$result['data_process'] = $data_process;

	} else {
		$result['msg']	= __('Sorry, no data found.', 'inboundwp-lite');
	}
	return $result;

}