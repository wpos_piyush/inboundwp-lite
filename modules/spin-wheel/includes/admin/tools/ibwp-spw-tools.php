<?php
/**
 * Tools Page
 *
 * @package Spin Wheel
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Getting Spin Wheel Post Type
$ibwp_spw_emails_arg = array(
	'post_type'		=> IBWP_SPW_POST_TYPE,
	'orderby'		=> 'title',
	'order'			=> 'ASC',
	'post_status'	=> 'publish',
	'posts_per_page'=> -1
);

$ibwp_spw_emails 	= get_posts( $ibwp_spw_emails_arg );
$current_date 		= date_i18n( 'Y-m-d', current_time( 'timestamp' ) ); ?>

<div class="wrap wp-ibwp-spw-settings">
	<h2><?php _e( 'Export Tools', 'inboundwp-lite' ); ?></h2><br/>
	<div class="wrap ibwp-tools-main">
		<div id="post-body-content">
			<div class="metabox-holder">
				<div class="postbox ibwp-spw-postbox">
					<h3 class="ibwp-title-border">
						<span><?php _e( 'Export - Filter & Export', 'inboundwp-lite' ); ?></span>
					</h3>
					<div class="inside">
						<p><?php _e('Download a CSV of all emails recorded.', 'inboundwp-lite'); ?></p>
						<form id="ibwp-spw-export-emails-form" class="ibwp-spw-export-form ibwp-spw-export-emails-form" method="post">
							<select name="ibwp_spw_wheel_title" class="ibwp-spw-filter-wheelname ibwp-export-fields ibwp-select2">
								<option value=""><?php _e('Select Spin Wheel','inboundwp-lite'); ?></option>
								<?php
									if( !empty($ibwp_spw_emails) ) {
										foreach ( $ibwp_spw_emails as $post_key => $post_val ) {
											$wheel_title = !empty($post_val->post_title) ? $post_val->post_title : sprintf( __('Email - %d', 'epiec'), $post_val->ID );
											
											echo '<option value="'.$post_val->ID.'" ' . selected( isset( $_GET['ibwp_spw_wheel_title'] ) ? $_GET['ibwp_spw_wheel_title'] : '', $post_val->ID, false ) . '>' . $wheel_title . '</option>';
										}
									}
								?>
							</select>
							<input type="date" name="start_date" value="" class="ibwp-export-fields" placeholder="<?php _e('Choose Start Date', 'inboundwp-lite'); ?>" max="<?php echo ibwpl_esc_attr($current_date); ?>" title="Choose Start Date" />
							<input type="date" name="end_date" value="" class="ibwp-export-fields" placeholder="<?php _e('Choose End Date', 'inboundwp-lite'); ?>" max="<?php echo ibwpl_esc_attr($current_date); ?>" title="Choose End Date" />
							<span class="ibwp-export-emails-btn button"><?php _e('Generate CSV','inboundwp-lite'); ?></span>
							<span class="spinner ibwp-spw-spinner"></span>
						</form><br>
						<div class="ibwp-progress-wrap">
							<div class="ibwp-progress"><div class="ibwp-progress-strip"></div></div>
						</div>
					</div><!-- .inside -->
				</div><!-- #general -->
			</div><!-- .metabox-holder -->
		</div><!-- #post-body-content -->
	</div>
</div>