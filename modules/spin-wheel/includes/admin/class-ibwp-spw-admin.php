<?php

/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package Spin Wheel
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Ibwp_Spw_Admin {	

	function __construct() {

		// Action to register admin menu
		add_action( 'admin_menu', array( $this, 'ibwp_spw_register_menu' ), 9 );

		// Action to register plugin settings
		add_action( 'admin_init', array( $this, 'ibwp_spw_register_settings' ) );

		// Action to export download process
		add_action( 'admin_init', array( $this, 'ibwp_spw_process_batch_export_download' ) );

		// Action to export emails data
		add_action( 'wp_ajax_ibwp_spw_do_ajax_export', array($this, 'ibwp_spw_do_ajax_export') );

		// Action to add metabox
		add_action( 'add_meta_boxes', array( $this, 'ibwp_spw_posts_metabox' ) );

		// Action to save metabox
		add_action( 'save_post', array( $this, 'ibwp_spw_pro_save_metabox_value' ), 10, 3 );

		// Section Type Get
		add_action ( 'wp_ajax_ibwp_spw_section_type', array( $this,'ibwp_spw_section_type' ) );
		add_action ( 'wp_ajax_nopriv_ibwp_spw_section_type', array( $this,'ibwp_spw_section_type' ) );

	}

	/**
	 * Function to register admin menus
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_register_menu() {

		// Add emails page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_SPW_POST_TYPE, __('Emails', 'inboundwp-lite'), __('Emails', 'inboundwp-lite'), 'manage_options', 'ibwp-spw-emails', array($this, 'ibwp_spw_emails_page') );

		// Add tools page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_SPW_POST_TYPE, __('Tools', 'inboundwp-lite'), __('Tools', 'inboundwp-lite'), 'manage_options', 'ibwp-spw-tools', array($this, 'ibwp_spw_tools_page') );
		
		// Add settings page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_SPW_POST_TYPE, __('Settings', 'inboundwp-lite'), __('Settings', 'inboundwp-lite'), 'manage_options', 'ibwp-spw-settings', array($this, 'ibwp_spw_settings_page') );
	}
	
	/**
	 * Function to handle the emails page html
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_emails_page() {
		include_once( IBWP_SPW_DIR . '/includes/admin/emails/ibwp-spw-email-list.php' );
	}


	/**
	 * Function to handle the tools page html
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_tools_page() {
		include_once( IBWP_SPW_DIR . '/includes/admin/tools/ibwp-spw-tools.php' );
	}

	/**
	 * Function to handle the setting page html
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_settings_page() {
		include_once( IBWP_SPW_DIR . '/includes/admin/settings/ibwp-spw-settings.php' );
	}

	/**
	 * Function register setings
	 * 
	 * @subpackage Spin Wheel
 	 * @since 1.0
	 */
	function ibwp_spw_register_settings() {
		global $wpdb;

		// Reset default settings
		if( !empty( $_POST['ibwp_spw_reset_settings'] ) ) {
			ibwp_spw_default_settings();
		}

		register_setting( 'ibwp_spw_plugin_options', 'ibwp_spw_options', array($this, 'ibwp_spw_validate_options') );

		// Email Delete action
		if( (isset( $_GET['action'] ) && $_GET['action'] == 'delete' ) || (isset( $_GET['action2'] ) && $_GET['action2'] == 'delete' ) && isset($_GET['page']) && $_GET['page'] == 'ibwp-spw-emails' && ( isset( $_GET['ibwp_spw_nonce'] ) && wp_verify_nonce( $_GET['ibwp_spw_nonce'], 'ibwp-spw-delete-email' ) ) ) {
			$email_id = !empty( $_GET['ibwp_spw_emails'] ) ? $_GET['ibwp_spw_emails'] : '';

			if( !empty($email_id) && is_array( $email_id ) ){
				
				foreach ($email_id as $key => $id) {
					$status = $wpdb->delete( IBWP_SPW_EMAIL_TABLE, array( 'id' => $id ) );
				}

				if($status){
					$page_url 	  = add_query_arg( array( 'post_type' => IBWP_SPW_POST_TYPE, 'page' => 'ibwp-spw-emails','message' => '1' ), admin_url('edit.php') );
					wp_redirect( $page_url );
					exit;
				}
			}
		}
	}

	/**
	 * Validate Settings Options
	 * 
	 * @subpackage Spin Wheel
 	 * @since 1.0
	 */
	function ibwp_spw_validate_options( $input ) {

		$input['ga_enable'] 		= isset($input['ga_enable']) 		? 1 										: 0;
		$input['ga_track_id'] 		= isset($input['ga_track_id']) 		? ibwpl_clean($input['ga_track_id'])		: '';
		$input['post_types']		= isset($input['post_types']) 		? ibwpl_clean($input['post_types'])			: array();
		$input['select_wheel']		= isset($input['select_wheel']) 	? ibwpl_clean($input['select_wheel'])		: '';
		$input['wheel_glob_locs']	= isset($input['wheel_glob_locs']) 	? ibwpl_clean($input['wheel_glob_locs'])	: array();
		return $input;
	}

	/**
	 * Process the download file generated by a batch export
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_process_batch_export_download() {

		if( isset( $_GET['ibwp_spw_action'] ) && $_GET['ibwp_spw_action'] == 'ibwp_spw_export' ) {
			
			if( ! wp_verify_nonce( $_GET['nonce'], 'ibwp_spw_export' ) ) {
				wp_die( __( 'Nonce verification failed', 'inboundwp-lite' ), __( 'Error', 'inboundwp-lite' ), array( 'response' => 403 ) );
			}

			// Set headers
			ignore_user_abort( true );
			set_time_limit( 0 );
			nocache_headers();
			
			header( 'Content-Type: text/csv; charset=utf-8' );
			header( 'Content-Disposition: attachment; filename=spin-wheel-emails-export-' . date( 'm-d-Y' ) . '.csv' );
			header( "Expires: 0" );
			
			$file_path 	= ibwp_spw_export_emails_file();
			$file 		= ibwp_spw_get_file( $file_path );
			
			@unlink( $file_path );
			echo $file;
			exit();
		}

	}

	/**
	* Email Filter Data
	* 
	* @package Spin Wheel
	* @since 1.0
	*/
	function ibwp_spw_do_ajax_export() {

		// Taking some variables
		$result = array();

		// Taking passed data
		parse_str( $_POST['form_data'], $form_data );

		$ibwp_spw_action 	= isset($_POST['ibwp_spw_action']) 	? ibwpl_clean( $_POST['ibwp_spw_action'] )	: '';
		$page 				= isset($_POST['page']) 			? $_POST['page'] 							: 1;
		$total_count 		= isset($_POST['total_count']) 		? $_POST['total_count']						: 0;
		$data_process 		= isset($_POST['data_process']) 	? $_POST['data_process'] 					: 0;

		if( empty( $ibwp_spw_action ) ||  ! wp_verify_nonce( $form_data['ibwp_spw_export'], 'ibwp_spw_export' ) ) {
			$result['status'] 	= 0;
			$result['msg'] 		= __('Sorry, Something happened wrong.', 'inboundwp-lite');
		}

		// Gathering all data
		$form_data 					= (array) $form_data;
		$form_data['page'] 			= $page;
		$form_data['total_count'] 	= $total_count;
		$form_data['data_process'] 	= $data_process;
		$form_data['is_ajax'] 		= true;

		if ( function_exists( 'ibwp_spw_'.$ibwp_spw_action ) ) {
			$result = call_user_func( 'ibwp_spw_'.$ibwp_spw_action, $form_data );
		}

		echo json_encode( $result );
		exit;
	}


	/**
	* Wheel Settings Metabox
	* 
	* @package Spin Wheel
	* @since 1.0
	*/
	function ibwp_spw_posts_metabox() {

		// Add metabox on  page  and posts for spin wheel
		$ibwp_spw_support_post_type = ibwp_spw_get_option('post_types', array());

		if(!empty($ibwp_spw_support_post_type)){

			add_meta_box( 'ibwp-spw-support-post-type-sett', __( 'Spin Wheel - IBWP', 'inboundwp-lite' ), array($this, 'ibwp_spw_support_post_type_sett'), $ibwp_spw_support_post_type, 'normal', 'high' );

		}

		// Add Spen Wheel Setting Metabox  
		add_meta_box( 'ibwp-spw-post-sett', __( 'Spin Wheel Settings', 'inboundwp-lite' ), array($this, 'ibwp_spw_post_sett') , IBWP_SPW_POST_TYPE, 'normal', 'high' );

		// Quick - Side Meta Box
		add_meta_box( 'ibwp-spw-short-desc', __( 'How to Use', 'inboundwp-lite' ), array($this, 'ibwp_spw_shrt_prev_mb_content'), IBWP_SPW_POST_TYPE, 'side' );
	}

	/**
	* Get Wheel on page, post, custom post type metabox HTML
	* 
	* @package Spin Wheel
	* @since 1.0
	*/
	function ibwp_spw_support_post_type_sett() {
		require IBWP_SPW_DIR .'/includes/admin/metabox/post-type-sett.php';
	}

	/**
	* Spin Wheel Post Settings Metabox HTML
	*
	* @package Spin Wheel
	* @since 1.0
	*/
	function ibwp_spw_post_sett() {
		require IBWP_SPW_DIR .'/includes/admin/metabox/spw-post-sett.php';
	}	

	/**
	 * Spin Wheel Post Quick Settings Metabox HTML
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_shrt_prev_mb_content() {
		include_once( IBWP_SPW_DIR .'/includes/admin/metabox/spw-post-prev.php');
	}

	/**
	* Save metabox value
	* 
	* @package Spin Wheel
	* @since 1.0
	*/
	function ibwp_spw_pro_save_metabox_value( $post_id, $post, $update ) {

		global $post_type;
       	$prefix = IBWP_SPW_META_PREFIX;

       	// Taking popup id from page, post and custom post type
       	if(isset($_POST[$prefix.'wheel_post'])) {
			$wheel_post_id	= $_POST[$prefix.'wheel_post'] ? ibwpl_clean( $_POST[$prefix.'wheel_post'] ) : '';
			update_post_meta($post_id, $prefix.'wheel_post', $wheel_post_id);
		}

       if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )               // Check Autosave
       || ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] ) // Check Revision
       || ( $post_type !=  IBWP_SPW_POST_TYPE ) ) { // Check if current post type is supported.
			return $post_id;
       }

		// Taking metabox prefix
		$data['tab'] 						= isset($_POST[$prefix.'tab']) 						?  ibwpl_clean($_POST[$prefix.'tab']) 							: '';
       	$data['enable_spin_wheel'] 			= isset($_POST[$prefix.'enable_spin_wheel']) 		?  1 															: 0;
       	$data['icon_position'] 				= isset($_POST[$prefix.'icon_position']) 			?  ibwpl_clean($_POST[$prefix.'icon_position']) 				: 'bottom-right';
       	$data['email_store'] 				= !empty($_POST[$prefix.'email_store']) 			?  1 															: 0;
       	$data['wheel_bg_image'] 			= isset($_POST[$prefix.'wheel_bg_image']) 			?  ibwpl_clean_url($_POST[$prefix.'wheel_bg_image']) 			: '';
       	$data['wheel_bg_color'] 			= !empty($_POST[$prefix.'wheel_bg_color']) 			?  ibwpl_clean_color($_POST[$prefix.'wheel_bg_color']) 			: '#8c8c8c';
       	$data['wheel_title'] 				= !empty($_POST[$prefix.'wheel_title']) 			?  htmlspecialchars($_POST[$prefix.'wheel_title']) 				: __('Spin Now To Get <b style="color:#ff0000;">Discount</b> Coupon Code','inboundwp-lite');
       	$data['wheel_title_color'] 			= !empty($_POST[$prefix.'wheel_title_color']) 		?  ibwpl_clean_color($_POST[$prefix.'wheel_title_color']) 		: '#000000';
       	$data['wheel_description'] 			= !empty($_POST[$prefix.'wheel_description']) 		?  ibwpl_clean_html($_POST[$prefix.'wheel_description'])		: '';
       	$data['pointer_icon_color'] 		= !empty($_POST[$prefix.'pointer_icon_color']) 		?  ibwpl_clean_color($_POST[$prefix.'pointer_icon_color']) 		: '#000000';
       	$data['icon_tooltip_text'] 			= !empty($_POST[$prefix.'icon_tooltip_text']) 		?  ibwpl_clean($_POST[$prefix.'icon_tooltip_text']) 			: __('Try Your Luck.','inboundwp-lite');
       	$data['name_field_show'] 			= isset($_POST[$prefix.'name_field_show']) 			?  1 															: 0;
       	$data['button_text'] 				= !empty($_POST[$prefix.'button_text']) 			?  ibwpl_clean($_POST[$prefix.'button_text']) 					: __('Click Here','inboundwp-lite');
       	$data['button_text_color'] 			= !empty($_POST[$prefix.'button_text_color']) 		?  ibwpl_clean_color($_POST[$prefix.'button_text_color']) 		: '#ffffff';
       	$data['button_bg_color'] 			= !empty($_POST[$prefix.'button_bg_color']) 		?  ibwpl_clean_color($_POST[$prefix.'button_bg_color']) 		: '#000000';
       	$data['enable_terms_conditions'] 	= isset($_POST[$prefix.'enable_terms_conditions']) 	?  1															: 0;
       	$data['terms_conditions_desc'] 		= !empty($_POST[$prefix.'terms_conditions_desc']) 	?  ibwpl_clean($_POST[$prefix.'terms_conditions_desc'])			: __('I agree with the <a href="#">term and condition</a>','inboundwp-lite');
       	$data['spining_duration'] 			= !empty($_POST[$prefix.'spining_duration']) 		?  ibwpl_clean_number($_POST[$prefix.'spining_duration']) 		: 5;
       	$data['coupon_type'] 				= !empty($_POST[$prefix.'coupon_type']) 			?  ibwpl_clean($_POST[$prefix.'coupon_type']) 					: __('other','inboundwp-lite');
       	$data['email_subject'] 				= !empty($_POST[$prefix.'email_subject']) 			?  ibwpl_clean($_POST[$prefix.'email_subject']) 				: __('Spin Wheel Coupon Award.','inboundwp-lite');
       	$data['email_heading'] 				= !empty($_POST[$prefix.'email_heading']) 			?  ibwpl_clean($_POST[$prefix.'email_heading']) 				: __('Congratulations!!!','inboundwp-lite');
       	$data['email_message'] 				= !empty($_POST[$prefix.'email_message']) 			?  ibwpl_clean_html($_POST[$prefix.'email_message'])			: __('Dear <strong>{customer_name}</strong>, <br>Congratulation. You have won a discount coupon by spinning spin wheel. <br>Please apply the coupon when shopping with us. <br>Coupon Label: <strong>{coupon_label}</strong> <br>Coupon Code: <strong>{coupon_code}</strong> <br>Expiry date: <strong>{date_expires}</strong> <br>Your Sincerely','inboundwp-lite');
       	$data['win_message'] 				= !empty($_POST[$prefix.'win_message']) 			?  ibwpl_clean_html($_POST[$prefix.'win_message']) 				: __('<h3><strong>Congratulations! You Won a Free Coupon.</strong></h3><span>Your coupon has been sent to you via email. You can use the coupon code on the checkout page.</span>','inboundwp-lite');
       	$data['lost_message'] 				= !empty($_POST[$prefix.'lost_message']) 			?  ibwpl_clean_html($_POST[$prefix.'lost_message'])				: __('<h3>OOPS! You are not lucky today. Sorry.</h3>','inboundwp-lite');

	  	$spin_wheel_coupons 				= !empty($_POST[$prefix.'spin_wheel_coupons']) 		?  ibwpl_clean($_POST[$prefix.'spin_wheel_coupons'])			: '';

		if( !empty( $spin_wheel_coupons ) ) {
			
			foreach ($spin_wheel_coupons as $ckey => $cdata) {

				$cdata['coupon_part']              	= !empty( $cdata['coupon_part'] ) 					? $cdata['coupon_part'] 				: '';

				if( ($data['coupon_type'] == 'woocommerce' || $data['coupon_type'] == 'edd') && $cdata['coupon_code'] ) {
					$cdata['coupon_id']           	= !empty( $cdata['coupon_code']  ) 					? $cdata['coupon_code'] 				: 0;
					if($data['coupon_type'] == 'woocommerce') {
						$cdata['coupon_code']           = !empty( $cdata['coupon_code']  ) 				? wc_get_coupon_code_by_id($cdata[coupon_id]) 	: 0;
					} else {
						$discount_coupon 	= get_post_meta($cdata['coupon_code'], '_edd_discount_code', true);
						$cdata['coupon_code']           = !empty( $cdata['coupon_code']  ) 				? $discount_coupon 								: 0;
					}
				}
				if( $data['coupon_type'] == 'other' ) {
					$cdata['coupon_id'] 			= isset( $cdata['coupon_code'] )					? 0 						: '';
					$cdata['coupon_code']           = !empty( $cdata['coupon_code']  ) 					? $cdata['coupon_code'] 	: 0;
				}
				
				$cdata['coupon_lbl']              	= !empty( $cdata['coupon_lbl']  ) 					? $cdata['coupon_lbl'] 					: '';
				$cdata['coupon_probability']        = !empty( $cdata['coupon_probability']  ) 			? $cdata['coupon_probability']			: '';
				$cdata['coupon_color']             	= !empty( $cdata['coupon_color']  ) 				? $cdata['coupon_color'] 				: '';
				$cdata['coupon_redirect']      		=  isset( $cdata['coupon_redirect']) 				? 1											: 0;
				$cdata['coupon_redirect_link'] 		= !empty( $cdata['coupon_redirect_link']  ) 		? $cdata['coupon_redirect_link'] 		: '';
				$cdata['coupon_win_message']        = !empty( $cdata['coupon_win_message']  ) 			? $cdata['coupon_win_message'] 			: '';
				$cdata['coupon_email_message']      = !empty( $cdata['coupon_email_message']  ) 		? $cdata['coupon_email_message'] 		: '';
				$cdata['coupon_lost_message']       = !empty( $cdata['coupon_lost_message']  ) 			? $cdata['coupon_lost_message'] 		: '';

				$spin_wheel_coupons[ $ckey ] = $cdata;
			}

			$data['spin_wheel_coupons'] = $spin_wheel_coupons;
		}

		if ( ! $update ) {
			$data['email_store'] = 1;
		}
		
		if( !empty( $data ) ){
			foreach ($data as $mkey => $mval) {
				update_post_meta( $post_id, $prefix.$mkey, $mval );
			}
		}
	}

	/**
	 * Get Coupon Section Type Ajax Function
	 *
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_section_type() {
		$coupon_type 		= $_POST['coupon_type'];
		$section_type 		= $_POST['section_type'];
		$prefix 			= IBWP_SPW_META_PREFIX;
		$spw_coupons_key 	= $_POST['spw_coupons_key'];

		if($section_type == 'none') { ?>

			<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
			<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" value="0" class="large-text ibwp-spw-coupon-code" readonly>
			<span class="description"><?php _e('Coupon part "none" coupon code is 0.','inboundwp-pro'); ?></span>

		<?php } else {

			if($coupon_type == 'woocommerce' || $coupon_type == 'edd') { ?>
				<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
				<select name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" class="ibwp-select ibwp-spw-coupon-code ibwp-select2" id="ibwp-spw-coupon-code" style="width:100%">
					<?php if($coupon_type == 'woocommerce') { ?>
						<option value=""><?php _e('Select WooCommerce Coupon','inboundwp-pro'); ?></option>
					<?php } if($coupon_type == 'edd') { ?>
						<option value=""><?php _e('Select EDD Coupon','inboundwp-pro'); ?></option>
					<?php }
					$post_type		= ($coupon_type == 'woocommerce') 	? 'shop_coupon' : 'edd_discount';
					$post_status	= ($coupon_type == 'woocommerce')	? 'publish'		: 'active';
						$coupon_args = array(
							'posts_per_page'   => -1,
							'orderby'          => 'title',
							'order'            => 'asc',
							'post_type'        => $post_type,
							'post_status'      => $post_status,
						);

						if( $coupon_type == 'woocommerce' ) {
							$coupon_args['meta_query'][] = array(
								'relation' => 'OR',
								array(
									'key' => 'expiry_date', 
									'value' => date("Y-m-d"), 
									'compare' => '>=', 
									'type' => 'DATE',
								),
								array(
									'key' => 'expiry_date', 
									'value' => '', 
									'compare' => '=', 
								),
							);
						}
						
						$coupons = get_posts( $coupon_args );

						foreach($coupons as $coupon) {
							echo '<option value="'.$coupon->ID.'" class="coupon-id" coupon-code-type="'.$coupon_type.'" coupon-id="'.$coupon->ID.'" '.selected( $coupon_code, $coupon->ID ).'>'.$coupon->post_title.'</option>';
						} ?>
				</select>
				<span class="description"><?php _e('Select coupon code.','inboundwp-pro'); ?></span>

			<?php } else { ?>

			<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
			<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" class="large-text ibwp-spw-other-coupon-code" value="">
			<span class="description"><?php _e('Enter coupon code.','inboundwp-pro'); ?></span>

		<?php }
		}
		exit;
	}
}

$ibwp_spw_admin = new Ibwp_Spw_Admin();