<?php
/**
 * MetaBox HTML File
 *
 * @package Spin Wheel
 * @since 1.0
 */
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;
$prefix = IBWP_SPW_META_PREFIX; // Metabox prefix

// Getting stored metabox values
$selected_tab				= get_post_meta( $post->ID, $prefix. 'tab', true );
$enable_spin_wheel			= get_post_meta( $post->ID, $prefix. 'enable_spin_wheel', true );
$icon_position				= get_post_meta( $post->ID, $prefix. 'icon_position', true );
$email_store				= get_post_meta( $post->ID, $prefix. 'email_store', true );
$wheel_bg_image 			= get_post_meta( $post->ID, $prefix. 'wheel_bg_image', true );
$wheel_bg_color				= get_post_meta( $post->ID, $prefix. 'wheel_bg_color', true );
$wheel_title				= get_post_meta( $post->ID, $prefix. 'wheel_title', true );
$wheel_title_color			= get_post_meta( $post->ID, $prefix. 'wheel_title_color', true );
$wheel_description			= get_post_meta( $post->ID, $prefix. 'wheel_description', true );
$pointer_icon_color			= get_post_meta( $post->ID, $prefix. 'pointer_icon_color', true );
$icon_tooltip_text			= get_post_meta( $post->ID, $prefix. 'icon_tooltip_text', true );
$name_field_show			= get_post_meta( $post->ID, $prefix. 'name_field_show', true );
$button_text				= get_post_meta( $post->ID, $prefix. 'button_text', true );
$button_text_color			= get_post_meta( $post->ID, $prefix. 'button_text_color', true );
$button_bg_color			= get_post_meta( $post->ID, $prefix. 'button_bg_color', true );
$enable_terms_conditions	= get_post_meta( $post->ID, $prefix. 'enable_terms_conditions', true );
$terms_conditions_desc		= get_post_meta( $post->ID, $prefix. 'terms_conditions_desc', true );
$spining_duration			= get_post_meta( $post->ID, $prefix. 'spining_duration', true );
$coupon_type				= get_post_meta( $post->ID, $prefix. 'coupon_type', true );
$spin_wheel_coupons			= get_post_meta( $post->ID, $prefix. 'spin_wheel_coupons', true );
$spin_wheel_coupons			= !empty( $spin_wheel_coupons ) ? $spin_wheel_coupons : array('1' => '','2' => '','3' => '','4' => '');
$email_subject				= get_post_meta( $post->ID, $prefix. 'email_subject', true );
$email_heading				= get_post_meta( $post->ID, $prefix. 'email_heading', true );
$email_message				= get_post_meta( $post->ID, $prefix. 'email_message', true );
$win_message				= get_post_meta( $post->ID, $prefix. 'win_message', true );
$lost_message				= get_post_meta( $post->ID, $prefix. 'lost_message', true );
?>

<div id="ibwp-spw-wheel-settings" class="post-box-container ibwp-spw-wheel-settings">
	<div class="meta-box-sortables ui-sortable">
		<div id="general">
			<div class="ibwp-stabs-wrap ibwp-clearfix">
				
				<ul class="ibwp-stabs-nav-wrap">
					<li class="ibwp-stabs-nav ibwp-active-stab">
						<a href="#ibwp_spw_general_options"><?php _e('General', 'inboundwp-lite'); ?></a>
					</li>					
					<li class="ibwp-stabs-nav">
						<a href="#ibwp_spw_wheel_design_options"><?php _e('Wheel Design', 'inboundwp-lite'); ?></a>
					</li>
					<li class="ibwp-stabs-nav">
						<a href="#ibwp_spw_wheel_setting_options"><?php _e('Wheel Coupons', 'inboundwp-lite'); ?></a>
					</li>
					<li class="ibwp-stabs-nav">
						<a href="#ibwp_spw_wheel__coupon_result_options"><?php _e('Wheel Notification', 'inboundwp-lite'); ?></a>
					</li>
				</ul>

				<div class="ibwp-cont-wrap">
					
					<!-- Start General Settings HTML -->
					<div id="ibwp_spw_general_options" class="ibwp-stabs-cnt ibwp-clearfix"  style="display:block;">
						<div class="ibwp-info-wrap">
							<div class="ibwp-title"><?php _e('General', 'inboundwp-lite'); ?></div>
							<span class="ibwp-spw-desc description"><?php _e('General settings.', 'inboundwp-lite'); ?></span>
						</div>
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-enable-popup"><?php _e('Active', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="checkbox" name="<?php echo $prefix; ?>enable_spin_wheel" value="1" class="ibwp-checkbox ibwp-spw-enable-popup" id="ibwp-spw-enable-popup" <?php checked( $enable_spin_wheel, 1 ); ?> /><br/>
										<span class="description"><?php _e('Check this box if you want to display on site.', 'inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-popup-icon-position"><?php _e('Popup Icon Position', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<select name="<?php echo $prefix; ?>icon_position" style="width: 100%;" class="ibwp-select ibwp-select2" id="ibwp-spw-popup-icon-position">
											<option <?php selected( $icon_position, 'top-left' ) ?> value="top-left"> <?php esc_html_e( 'Top Left', 'inboundwp-lite' ); ?></option>
					                        <option <?php selected( $icon_position, 'top-right' ) ?> value="top-right"><?php esc_html_e( 'Top Right', 'inboundwp-lite' ); ?></option>
					                        <option <?php selected( $icon_position, 'middle-left' ) ?> value="middle-left"><?php esc_html_e( 'Middle Left', 'inboundwp-lite' ); ?></option>
					                        <option <?php selected( $icon_position, 'middle-right' ) ?> value="middle-right"><?php esc_html_e( 'Middle Right', 'inboundwp-lite' ); ?></option>
					                        <option  <?php selected( $icon_position, 'bottom-left' ) ?> value="bottom-left"><?php esc_html_e( 'Bottom Left', 'inboundwp-lite' ); ?></option>
					                        <option  <?php selected( $icon_position, 'bottom-right' ) ?> value="bottom-right"><?php esc_html_e( 'Bottom Right', 'inboundwp-lite' ); ?></option>
										</select>
										<span class="description"><?php _e( 'Select Popup icon position.', 'inboundwp-lite' ); ?></span>
									</td>
								</tr>
								<tr>
									<th>
										<label for="ibwp-spw-store-email"><?php _e('Enable Email Store', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="checkbox" value="1" name="<?php echo $prefix; ?>email_store" <?php checked( $email_store, 1 ); ?> class="ibwp-checkbox ibwp-spw-store-email" id="ibwp-spw-store-email" /><br/>
										<span class="description"><?php _e( 'Check this checkbox to store email in your database.', 'inboundwp-lite' ); ?></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- End General Settings HTML -->

					<!-- Start Wheel Design Settings HTML -->
					<div id="ibwp_spw_wheel_design_options" class="ibwp-stabs-cnt ibwp-clearfix"  style="display:none;">
						<div class="ibwp-info-wrap">
							<div class="ibwp-title"><?php _e('Wheel Design', 'inboundwp-lite'); ?></div>
							<span class="ibwp-spw-desc description"><?php _e('Wheel design settings.', 'inboundwp-lite'); ?></span>
						</div>
						<table class="form-table">
							<tbody>
								<tr>
									<th>
										<label for="ibwp-wbg-url"><?php _e('Background Image', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>wheel_bg_image" value="<?php echo ibwpl_esc_attr($wheel_bg_image); ?>" id="ibwp-wbg-url" class="large-text ibwp-url ibwp-img-upload-input" />
										<input type="button" name="ibwp_spw_image_btn" id="ibwp_spw_url" class="button-secondary ibwp-image-upload" value="<?php _e( 'Upload Image', 'inboundwp-lite'); ?>" data-pdt-preview="1" /> 
										<input type="button" name="ibwp_spw_image_clear" id="ibwp-spw-url-clear" class="button button-secondary ibwp-image-clear" value="<?php _e( 'Clear', 'inboundwp-lite'); ?>"  data-pdt-preview="1" /> <br />
										<span class="description"><?php _e( 'Upload a background image for your spin wheel box, or specify an image URL directly. Recommended Image size: 850 X 650', 'inboundwp-lite' ); ?></span>
										<?php
											$wheel_bg_image_url = '';
											if( $wheel_bg_image ) { 
												$wheel_bg_image_url = '<img src="'.esc_url($wheel_bg_image).'" alt="" />';
											}
										?>
										<div class="ibwp-wbg-preview-img ibwp-spw-img-preview"><?php echo $wheel_bg_image_url;?></div>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-bg-colorpicker"><?php _e('Background Color', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input name="<?php echo $prefix; ?>wheel_bg_color" value="<?php echo ibwpl_esc_attr($wheel_bg_color); ?>" class="ibwp-colorpicker ibwp-spw-bg-colorpicker" id="ibwp-spw-bg-colorpicker" />
										<span class="description"><?php _e('Select spin wheel box background color.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-wheel-title"><?php _e('Wheel Title', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>wheel_title" value="<?php echo ibwpl_esc_attr($wheel_title); ?>" class="large-text ibwp-text ibwp-spw-wheel-title" id="ibwp-spw-wheel-title">
										<span class="description"><?php _e('Enter spin wheel box top title with html tag.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-wheel-title-color"><?php _e('Wheel Title Color', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>wheel_title_color" value="<?php echo ibwpl_esc_attr($wheel_title_color); ?>" class="ibwp-colorpicker ibwp-spw-wheel-title-color" id="ibwp-spw-wheel-title-color">
										<span class="description"><?php _e('Select spin wheel box top title color.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-wheel-description"><?php _e('Wheel Description', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										 <?php wp_editor( $wheel_description, 'ibwp-spw-wheel-description', array('textarea_name' => $prefix.'wheel_description', 'textarea_rows' => 5, 'media_buttons' => false, 'class' => 'ibwp-spw-width') ); ?>
										 <span class="description"><?php _e('Description on mobile will display maximum 4 rows.', 'inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th>
										<label for="ibwp-spw-wpc-colorpicker"><?php _e('Pointer Icon Color','inboundwp-lite'); ?></label>
									</th>
									<td>
										<input name="<?php echo $prefix; ?>pointer_icon_color" value="<?php echo ibwpl_esc_attr($pointer_icon_color); ?>" class="ibwp-colorpicker ibwp-spw-wpc-colorpicker" id="ibwp-spw-wpc-colorpicker">
										<span class="description"><?php _e('Select pointer icon color.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th><label for="ibwp-spw-icon-tooltip-text"><?php _e('Icon Tooltip Text','inboundwp-lite'); ?></label></th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>icon_tooltip_text" value="<?php echo ibwpl_esc_attr($icon_tooltip_text); ?>" class="ibwp-spw-icon-tooltip-text large-text" id="ibwp-spw-icon-tooltip-text">
										<span class="description"><?php _e('Set wheel icon tooltip.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th><label for="ibwp-spw-name-field"><?php _e('Name Fields','inboundwp-lite'); ?></label></th>
									<td>
										<input type="checkbox" name="<?php echo $prefix; ?>name_field_show" value="1" class="ibwp-spw-name-field" id="ibwp-spw-name-field" <?php checked( $name_field_show, 1 ); ?>><br>
										<span class="description"><?php _e('Check this checkbox to show name field.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th><label for="ibwp-spw-wheel-button-text"><?php _e('Button Text','inboundwp-lite'); ?></label></th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>button_text" value="<?php echo ibwpl_esc_attr($button_text); ?>" class="large-text ibwp-text ibwp-spw-wheel-button-text" id="ibwp-spw-wheel-button-text">
										<span class="description"><?php _e('Enter form button text.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th><label for="ibwp-spw-btc-colorpicker"><?php _e('Button Text Color','inboundwp-lite'); ?></label></th>
									<td>
										<input name="<?php echo $prefix; ?>button_text_color" value="<?php echo ibwpl_esc_attr($button_text_color); ?>" class="ibwp-colorpicker ibwp-spw-btc-colorpicker" id="ibwp-spw-btc-colorpicker" />
										<span class="description"><?php _e('Select button text color.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th><label for="ibwp-spw-btn-bgc-colorpicker"><?php _e('Button Background Color','inboundwp-lite'); ?></label></th>
									<td>
										<input name="<?php echo $prefix; ?>button_bg_color" value="<?php echo ibwpl_esc_attr($button_bg_color); ?>" class="ibwp-colorpicker ibwp-spw-btn-bgc-colorpicker" id="ibwp-spw-btn-bgc-colorpicker" />
										<span class="description"><?php _e('Select button background color.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-terms-cond"><?php _e('Enable Terms & Conditions', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="checkbox" name="<?php echo $prefix; ?>enable_terms_conditions" value="1" class="ibwp-checkbox ibwp-spw-terms-cond" id="ibwp-spw-terms-cond" <?php checked( $enable_terms_conditions, 1 ); ?> /><br/>
										<span class="description"><?php _e('Check this box if you want to enable terms and condition option in form.', 'inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-terms-conditions-desc"><?php _e('Terms & Conditions Description', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										 <?php wp_editor( $terms_conditions_desc, 'ibwp-spw-terms-conditions-desc', array('textarea_name' => $prefix.'terms_conditions_desc', 'textarea_rows' => 5, 'media_buttons' => false, 'class' => 'ibwp-spw-width') ); ?>
										 <span class="description"><?php _e('Wirte Description for Terms & Conditions.', 'inboundwp-lite'); ?></span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- End Wheel Design Settings HTML -->
					
					<!-- Start Wheel Coupon Settings HTML -->
					<div id="ibwp_spw_wheel_setting_options" class="ibwp-stabs-cnt ibwp-clearfix"  style="display:none;">
						<div class="ibwp-info-wrap">
							<div class="ibwp-title"><?php _e('Wheel Coupons', 'inboundwp-lite'); ?></div>
							<span class="ibwp-spw-desc description"><?php _e('Wheel coupons settings.', 'inboundwp-lite'); ?></span>
						</div>
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-spin-duration"><?php _e('Wheel Spining Duration', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<select name="<?php echo $prefix; ?>spining_duration" class="ibwp-select ibwp-select2" id="ibwp-spw-wheel-spin-duration" style="width:100%">
											<option value=""><?php esc_html_e('Select Spining Duration','inboundwp-lite'); ?></option>
											<option value="1" <?php selected( $spining_duration, '1' ) ?>><?php esc_html_e( '1', 'inboundwp-lite' ); ?></option>
											<option value="2" <?php selected( $spining_duration, '2' ) ?>><?php esc_html_e( '2', 'inboundwp-lite' ); ?></option>
											<option value="3" <?php selected( $spining_duration, '3' ) ?>><?php esc_html_e( '3', 'inboundwp-lite' ); ?></option>
											<option value="4" <?php selected( $spining_duration, '4' ) ?>><?php esc_html_e( '4', 'inboundwp-lite' ); ?></option>
											<option value="5" <?php selected( $spining_duration, '5' ) ?>><?php esc_html_e( '5', 'inboundwp-lite' ); ?></option>
											<option value="6" <?php selected( $spining_duration, '6' ) ?>><?php esc_html_e( '6', 'inboundwp-lite' ); ?></option>
											<option value="7" <?php selected( $spining_duration, '7' ) ?>><?php esc_html_e( '7', 'inboundwp-lite' ); ?></option>
											<option value="8" <?php selected( $spining_duration, '8' ) ?>><?php esc_html_e( '8', 'inboundwp-lite' ); ?></option>
											<option value="9" <?php selected( $spining_duration, '9' ) ?>><?php esc_html_e( '9', 'inboundwp-lite' ); ?></option>
											<option value="10" <?php selected( $spining_duration, '10' ) ?>><?php esc_html_e( '10', 'inboundwp-lite' ); ?></option>
										</select>
										<span class="description"><?php _e( 'Select wheel spining duration in second.', 'inboundwp-lite' ); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-coupons-type"><?php _e('Coupon Type', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<select name="<?php echo $prefix; ?>coupon_type" id="ibwp-spw-coupons-type" class="ibwp-select2 ibwp-spw-coupon-type" style="width: 100%;">
											<option value=""><?php _e('Select Coupon Type','inboundwp-lite'); ?></option>
											<option <?php selected( $coupon_type, 'other' ) ?> value="other"><?php _e('Other','inboundwp-lite'); ?></option>
											<?php if ( class_exists( 'WooCommerce' ) ) { ?>
												<option <?php selected( $coupon_type, 'woocommerce' ) ?> value="woocommerce"><?php _e('Woocommerce','inboundwp-lite'); ?></option>
											<?php } if( function_exists( 'EDD' ) ) { ?>
												<option <?php selected( $coupon_type, 'edd' ) ?> value="edd"><?php _e('EDD','inboundwp-lite'); ?></option>
											<?php } ?>
										</select>
										<span class="description"><?php _e('Select spin wheel coupon type.','inboundwp-lite'); ?></span>
									</td>
								</tr>
							</tbody>
						</table>

						<table class="form-table ibwp-tbl-main">
							<tbody>
								<tr>
									<th scope="row" class="no-padding">
										<label for="ibwp-spw-coupons"><strong><?php _e('Wheel Segments', 'inboundwp-lite'); ?></strong></label>
									</th>
								</tr>
								<tr>
									<td class="ibwp-no-padding">
										<div class="ibwp-spw-coupons-wrap ibwp-spw-file-fields ui-sortable">
											<div class="ibwp-spw-coupons-table ui-sortable">
												<?php
													foreach ($spin_wheel_coupons as $spw_coupons_key => $spw_coupons_data) {
														$coupon_part 			= isset( $spw_coupons_data['coupon_part'] )				? $spw_coupons_data['coupon_part']				: '';
														
														if( $coupon_type == 'woocommerce' || $coupon_type == 'edd' ) {
															$coupon_id 			= isset( $spw_coupons_data['coupon_id'] )				? $spw_coupons_data['coupon_id']				: '';
														}
														if( $coupon_type == 'other' ) {
															$coupon_code 		= isset( $spw_coupons_data['coupon_code'] )				? $spw_coupons_data['coupon_code']				: '';
														}

														$coupon_lbl 			= isset( $spw_coupons_data['coupon_lbl'] )				? $spw_coupons_data['coupon_lbl']				: '';
														$coupon_probability 	= isset( $spw_coupons_data['coupon_probability'] )		? $spw_coupons_data['coupon_probability']		: '';
														$coupon_color 			= isset( $spw_coupons_data['coupon_color'] )			? $spw_coupons_data['coupon_color']				: '';
														$coupon_redirect 		= isset( $spw_coupons_data['coupon_redirect'] )			? $spw_coupons_data['coupon_redirect']			: '';
														$coupon_redirect_link	= isset( $spw_coupons_data['coupon_redirect_link'] )	? $spw_coupons_data['coupon_redirect_link']		: '';
														$coupon_win_message 	= isset( $spw_coupons_data['coupon_win_message'] )		? $spw_coupons_data['coupon_win_message']		: '';
														$coupon_email_message 	= isset( $spw_coupons_data['coupon_email_message'] )	? $spw_coupons_data['coupon_email_message']		: '';
														$coupon_lost_message 	= isset( $spw_coupons_data['coupon_lost_message'] )		? $spw_coupons_data['coupon_lost_message']		: '';
													?>
														<div class="ibwp-tbl-row" id="wheel_coupon_row_<?php echo $spw_coupons_key; ?>" data-key="<?php echo $spw_coupons_key; ?>">
															<div class="ibwp-tbl-row-header">
																<span class="ibwp-tbl-row-index"><strong><?php _e('Wheel Coupon '.$spw_coupons_key.': ','inboundwp-lite'); ?></strong><span></span></span>
																<span class="ibwp-tbl-row-actions">
																	<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
																</span>
															</div>
															<div class="ibwp-row-standard-fields">
																<table class="form-table spw-coupons-tbl">
																	<tr class="ibwp-spw-field-blank">
																		<td colspan="2" class="ibwp-column ibwp-medium-6">
																			<label for="ibwp-spw-section-type-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Part', 'inboundwp-lite'); ?> </label>
																			<select name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_part]" class="ibwp-select ibwp-spw-section-type ibwp-select2" id="ibwp-spw-section-type" style="width: 100%;">
																				<option value=""><?php _e('Select Coupon Part','inboundwp-lite'); ?></option>
																				<option <?php selected( $coupon_part, 'coupons' ) ?> value="coupons"><?php _e('Coupons ','inboundwp-lite'); ?></option>
																				<option <?php selected( $coupon_part, 'none' ) ?> value="none"><?php _e('None ','inboundwp-lite'); ?></option>
																			</select>
																			<span class="description"><?php _e('Select coupon part.','inboundwp-lite'); ?></span>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-field-blank">																		
																		<td class="wheel-coupon-label ibwp-column ibwp-medium-6">
																			<label for="ibwp-spw-coupon-label-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Label', 'inboundwp-lite'); ?> </label>
																			<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_lbl]" class="ibwp-text large-text" id="ibwp-spw-coupon-label-<?php echo $spw_coupons_key; ?>" value="<?php echo ibwpl_esc_attr($coupon_lbl); ?>">
																			<span class="description"><?php _e('Enter coupon label to display on spin wheel.','inboundwp-lite'); ?></span>
																		</td>
																		<td class="wheel-coupon-code ibwp-column ibwp-medium-6">
																			<?php if($coupon_type == 'woocommerce' || $coupon_type == 'edd') { 
																				if($coupon_part == 'coupons') { ?>
																				<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
																				<select name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" class="ibwp-select ibwp-spw-coupon-code ibwp-select2" id="ibwp-spw-coupon-code" style="width:100%">
																					<?php if($coupon_type == 'woocommerce') { ?>
																						<option value=""><?php _e('Select WooCommerce Coupon','inboundwp-pro'); ?></option>
																					<?php } if($coupon_type == 'edd') { ?>
																						<option value=""><?php _e('Select EDD Coupon','inboundwp-pro'); ?></option>
																					<?php }
																					$post_type		= ($coupon_type == 'woocommerce') 	? 'shop_coupon' : 'edd_discount';
																					$post_status	= ($coupon_type == 'woocommerce')	? 'publish'		: 'active';
																						$coupon_args = array(
																						    'posts_per_page'   => -1,
																						    'orderby'          => 'title',
																						    'order'            => 'asc',
																						    'post_type'        => $post_type,
																						    'post_status'      => $post_status,
																						);

																						if( $coupon_type == 'woocommerce' ) {
																							$coupon_args['meta_query'][] = array(
																							    'relation' => 'OR',
																							    array(
																							    	'key' => 'expiry_date', 
																								    'value' => date("Y-m-d"), 
																								    'compare' => '>=', 
																								    'type' => 'DATE',
																								),
																								array(
																									'key' => 'expiry_date', 
																								    'value' => '', 
																								    'compare' => '=', 
																								),
																							);
																						}
																						
																						$coupons = get_posts( $coupon_args );

																						foreach($coupons as $coupon) {
																							echo '<option value="'.$coupon->ID.'" class="coupon-id" coupon-code-type="'.$coupon_type.'" coupon-id="'.$coupon->ID.'" '.selected( $coupon_id, $coupon->ID ).'>'.$coupon->post_title.'</option>';
																						} ?>
																				</select>
																				<span class="description"><?php _e('Select coupon code.','inboundwp-pro'); ?></span>
																			<?php } else { ?>
																				<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
																				<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" value="0" class="ibwp-text large-text ibwp-spw-coupon-code" id="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>" readonly>
																				<span class="description"><?php _e('Coupon part "none" coupon code is 0.','inboundwp-pro'); ?></span>
																			<?php } } else { if($coupon_part == 'coupons') { ?>
																				<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
																				<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" class="ibwp-text large-text ibwp-spw-other-coupon-code" id="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>" value="<?php echo ibwp_esc_attr($coupon_code); ?>">
																				<span class="description"><?php _e('Enter coupon code.','inboundwp-pro'); ?></span>
																			<?php } else { ?>
																				<label for="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Value', 'inboundwp-pro'); ?> </label>
																				<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_code]" value="0" class="ibwp-text large-text ibwp-spw-coupon-code" id="ibwp-spw-select-coupon-<?php echo $spw_coupons_key; ?>" readonly>
																				<span class="description"><?php _e('Coupon part "none" coupon code is 0.','inboundwp-pro'); ?></span>
																			<?php } } ?>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-field-blank">
																		<td class="ibwp-column ibwp-medium-6">
																			<label for="ibwp-spw-coupon-probability-<?php echo $spw_coupons_key; ?>"><?php _e('Probability', 'inboundwp-lite'); ?> </label>
																			<input type="number" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_probability]" class="ibwp-number large-text ibwp-spw-coupon-probability" min="0" max="100" value="<?php echo ibwpl_esc_attr($coupon_probability); ?>" id="ibwp-spw-coupon-probability-<?php echo $spw_coupons_key; ?>">
																			<span class="description"><?php _e('Enter coupon probability(%).','inboundwp-lite'); ?></span>
																		</td>
																		<td class="ibwp-column ibwp-medium-6">
																			<label for="ibwp-spw-coupon-color-<?php echo $spw_coupons_key; ?>"><?php _e('Background Color', 'inboundwp-lite'); ?> </label>
																			<input name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_color]" value="<?php echo ibwpl_esc_attr($coupon_color); ?>" class="ibwp-colorpicker" id="ibwp-spw-wcc-colorpicker" />
																			<span class="description"><?php _e('Select coupon background color.','inboundwp-lite'); ?></span>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-field-blank">
																		<td colspan="2" class="ibwp-column ibwp-medium-6">
																			<label for="ibwp-spw-page-redirect-<?php echo $spw_coupons_key; ?>"><?php _e('Page Redirect', 'inboundwp-lite'); ?> </label>
																			<input type="checkbox" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_redirect]" value="1" <?php checked( $coupon_redirect, 1 ); ?> class="ibwp-text ibwp-spw-page-redirect" id="ibwp-spw-page-redirect-<?php echo $spw_coupons_key; ?>"><br>
																			<span class="description"><?php _e('Check this box to add page redirect url textbox.','inboundwp-lite'); ?></span>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-page-redirect-url" style="<?php if($coupon_redirect == '1') { ?> display: table-row; <?php } else { ?> display: none; <?php } ?>">
																		<td colspan="2" class="ibwp-column ibwp-medium-12">
																			<label for="ibwp-spw-page-redirect-link-<?php echo $spw_coupons_key; ?>"><?php _e('Page Redirect URL', 'inboundwp-lite'); ?> </label>
																			<input type="text" name="<?php echo $prefix; ?>spin_wheel_coupons[<?php echo $spw_coupons_key; ?>][coupon_redirect_link]" value="<?php echo esc_url($coupon_redirect_link); ?>" class="ibwp-text large-text ibwp-spw-page-redirect-link" id="ibwp-spw-page-redirect-link-<?php echo $spw_coupons_key; ?>">
																			<span class="description"><?php _e('Enter page redirect url.','inboundwp-lite'); ?></span>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-coupons-msg ibwp-spw-coupons-win-msg" style="<?php if($coupon_part == 'coupons' ) { echo 'display:table-row;'; } else { echo 'display: none;'; } ?>">
																		<td colspan="2" class="ibwp-column ibwp-medium-12">
																			<label for="ibwp-spw-coupon-win-message-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Win Message', 'inboundwp-lite'); ?> </label>
																			<?php wp_editor( $coupon_win_message, 'ibwp-spw-coupon-win-message-'.$spw_coupons_key, array('textarea_name' => $prefix.'spin_wheel_coupons['.$spw_coupons_key.'][coupon_win_message]', 'textarea_rows' => 5, 'media_buttons' => false) ); ?><br>
																			<span class="description"><?php _e("{coupon_label} - Label of coupon that customers win<br> {checkout} - 'Checkout' with link to checkout page<strong>(Only For WooCommerce)</strong><br> {customer_name} - Customers'name if they enter<br> {customer_email} - Email that customers enter to spin<br> {coupon_code} - Coupon code/custom value will be sent to customer. <br><b>Note: </b>Coupon win message not work with page redirect.","inboundwp-pro"); ?></span>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-coupons-msg" style="<?php if($coupon_part == 'coupons') { echo 'display:table-row'; } else { echo 'display: none;'; } ?>">
																		<td colspan="2" class="ibwp-column ibwp-medium-12">
																			<label for="ibwp-spw-coupon-email-message-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Email Message', 'inboundwp-lite'); ?> </label>
																			<?php wp_editor( $coupon_email_message, 'ibwp-spw-coupon-email-message-'.$spw_coupons_key, array('textarea_name' => $prefix.'spin_wheel_coupons['.$spw_coupons_key.'][coupon_email_message]', 'textarea_rows' => 5, 'media_buttons' => false) ); ?><br>
																			<span class="description"><?php _e("{customer_name} - Customer's name.<br> {coupon_code} - Coupon code/custom value will be sent to customer.<br> {coupon_label} - Coupon label/custom label that customers win<br> {date_expires} - Expiry date of the coupon<b>(Work with WooCommerce & EDD)</b>.","inboundwp-pro"); ?> </span>
																		</td>
																	</tr>
																	<tr class="ibwp-spw-coupons-lost-msg" style="<?php if($coupon_part == 'none') { echo 'display:table-row'; } else { echo 'display: none;'; } ?>">
																		<td colspan="2" class="ibwp-column ibwp-medium-12">
																			<label for="ibwp-spw-coupon-lost-message-<?php echo $spw_coupons_key; ?>"><?php _e('Coupon Lost Message', 'inboundwp-lite'); ?> </label>
																			<?php wp_editor( $coupon_lost_message, 'ibwp-spw-coupon-lost-message-'.$spw_coupons_key, array('textarea_name' => $prefix.'spin_wheel_coupons['.$spw_coupons_key.'][coupon_lost_message]', 'textarea_rows' => 5, 'media_buttons' => false) ); ?><br>
																		</td>
																	</tr>
																</table>
															</div>
														</div>
													<?php } ?>
											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="ibwp-pro-notice">
											<?php echo sprintf( __('In lite version, you can only add 4 wheel segments. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, To add more wheel segments of spin wheel.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
										</div><br>
										<div class="ibwp-info-note">
											<?php _e('Note: The coupons total probability must be <strong>100%</strong>.','inboundwp-lite'); ?>
										</div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- End Wheel Coupon Settings HTML -->

					<!-- Start Wheel Coupon Results HTML -->
					<div id="ibwp_spw_wheel__coupon_result_options" class="ibwp-stabs-cnt ibwp-clearfix"  style="display:none;">
						<div class="ibwp-info-wrap">
							<div class="ibwp-title"><?php _e('Wheel Coupon Result', 'inboundwp-lite'); ?></div>
							<span class="ibwp-spw-desc description"><?php _e('Wheel coupon result settings.', 'inboundwp-lite'); ?></span>
						</div>
						<table class="form-table">
							<tbody>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-email-subject"><?php _e('Email Subject', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>email_subject" value="<?php echo ibwpl_esc_attr($email_subject); ?>" class="ibwp-text large-text ibwp-spw-email-subject" id="ibwp-spw-email-subject">
										<span class="description"><?php _e('Enter spin wheel email subject.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-email-heading"><?php _e('Email Heading', 'inboundwp-lite'); ?></label>
									</th>
									<td>
										<input type="text" name="<?php echo $prefix; ?>email_heading" value="<?php echo ibwpl_esc_attr($email_heading); ?>" class="ibwp-text large-text ibwp-spw-email-heading" id="ibwp-spw-email-heading">
										<span class="description"><?php _e('Enter spin wheel email heading.','inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-email-description"><?php _e('Email Message', 'inboundwp-lite'); ?></label><br><br>
										<span class="info-msg"><?php _e('The content of email sending to inform customers about the coupon code they receive','inboundwp-lite'); ?></span>
									</th>
									<td>
										 <?php wp_editor( $email_message, 'ibwp-spw-email-message', array('textarea_name' => $prefix.'email_message', 'textarea_rows' => 10, 'media_buttons' => true) ); ?><br>
										 <span class="description"><?php _e("{customer_name} - Customer's name.<br>{coupon_code} - Coupon code/custom value will be sent to customer.<br>{coupon_label} - Coupon label/custom label that customers win<br>{date_expires} - Expiry date of the coupon <b>(Work with WooCommerce & EDD)</b>.", 'inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-frontend-win-msg"><?php _e('Frontend Win Message', 'inboundwp-lite'); ?></label><br><br>
									</th>
									<td>
										<?php wp_editor( $win_message, 'ibwp-spw-frontend-win-message', array('textarea_name' => $prefix.'win_message', 'textarea_rows' => 10, 'media_buttons' => true) ); ?><br>
										<span class="description"><?php _e("{coupon_label} - Label of coupon that customers win<br>{checkout} - 'Checkout' with link to checkout page<strong>(Only For WooCommerce)</strong><br>{customer_name} - Customers'name if they enter<br>{customer_email} - Email that customers enter to spin<br>{coupon_code} - Coupon code/custom value will be sent to customer. <br><b>Note: </b>Win Message not work with page redirect.", 'inboundwp-lite'); ?></span>
									</td>
								</tr>
								<tr>
									<th scope="row">
										<label for="ibwp-spw-frontend-lost-msg"><?php _e('Frontend Lost Message', 'inboundwp-lite'); ?></label><br><br>
									</th>
									<td>
										<?php wp_editor( $lost_message, 'ibwp-spw-frontend-lost-message', array('textarea_name' => $prefix.'lost_message', 'textarea_rows' => 10, 'media_buttons' => true) ); ?>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- End Wheel Coupon Results HTML -->
				</div>
				<input type="hidden" class="ibwp-selected-tab" value="<?php echo ibwpl_esc_attr($selected_tab); ?>" name="<?php echo $prefix; ?>tab" />
			</div>
		</div>
	</div>
</div>