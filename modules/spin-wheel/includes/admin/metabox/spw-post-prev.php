<?php
/**
 * Handles shortcode preview metabox HTML
 *
 * @package Spin Wheel
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<p><?php _e('Kindly goto: ','inboundwp-lite'); ?><a href="<?php echo admin_url(); ?>/edit.php?post_type=<?php echo IBWP_SPW_POST_TYPE; ?>&page=ibwp-spw-settings">Settings</a> then you can checked wheel display settings.</p>
<p><?php _e('To display spin wheel, select spin wheel on your page, post or etc.', 'inboundwp-lite'); ?></p>