<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @package Spin Wheel
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$prefix 			= IBWP_SPW_META_PREFIX; // Metabox prefix
$get_wheel_post 	= get_post_meta( $post->ID, $prefix.'wheel_post', true );
$get_wheel_post		= !empty($get_wheel_post) ? $get_wheel_post : '';
$spw_posts 			= ibwp_spw_post_by_type();
?>

<table class="form-table ibwp-spw-page-sett-table">
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="ibwp-spw-select-wheel"><?php _e('Select Spin Wheel', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>wheel_post" class="large-text" id="ibwp_spw_wheel_post">
					<option value=""><?php _e('Select Spin Wheel','inboundwp-lite'); ?></option>
					<option value="disable" <?php selected('disable',$get_wheel_post);?>><?php _e('Disable','inboundwp-lite'); ?></option>
					<?php
					// Get the 'Spin Wheel' post type
					$wheel_posts = get_posts(
						array(
							'post_type'			=> IBWP_SPW_POST_TYPE, 
							'post_status'		=> 'publish', 
							'posts_per_page'	=>-1,
							'order'				=> 'DESC',
							'suppress_filters' 	=> false, 
						)
					);
					foreach ($spw_posts as $spw_post) {
						echo '<option value="'. $spw_post->ID .'" '.selected( $get_wheel_post, $spw_post->ID ).'>'. $spw_post->post_title .'</option>';
					} 
					wp_reset_postdata();
					?>
				</select>
			</td>
		</tr>
	</tbody>
</table>