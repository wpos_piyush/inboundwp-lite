<?php
/**
 * Pro Designs and Plugins Feed
 *
 * @package Spin Wheel
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Action to add menu
add_action('admin_menu', 'ibwp_spw_register_design_page');

/**
 * Register plugin design page in admin menu
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_register_design_page() {
	add_submenu_page( 'edit.php?post_type='.IBWP_SPW_POST_TYPE, __('How it works - Spin Wheel', 'inboundwp-lite'), __('How It Works', 'inboundwp-lite'), 'manage_options', 'ibwp-spw-how-it-work', 'ibwp_spw_designs_page' );
}

/**
 * Function to display plugin design HTML
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_designs_page() { ?>
	<div class="wrap ibwp-spw-wrap ibwp-hwit-wrap">

        <h2><?php _e('How It Works - Spin Wheel', 'inboundwp-lite'); ?></h2>

        <div class="ibwp-spw-tab-cnt-wrp">
            <div class="post-box-container">
                <div id="poststuff">
                    <div id="post-body" class="metabox-holder">

                        <!--How it workd HTML -->
                        <div id="post-body-content">
                            <div class="metabox-holder">
                                <div class="meta-box-sortables ui-sortable">
                                    <div class="postbox">
                                    	<h3 class="hndle">
											<span><?php _e( 'How It Works - Display', 'inboundwp-lite' ); ?></span>
										</h3>
										
										<div class="inside">
											<table class="form-table">
												<tbody>
													<tr>
														<th>
															<label><?php _e('Getting Started', 'inboundwp-lite'); ?>:</label>
														</th>
														<td>
															<ul>
																<li><?php _e('Step-1: Go to "Spin Wheel - IBWP --> Add Spin Wheel".', 'inboundwp-lite'); ?></li>
																<li><?php _e('Step-2: Enable Spin Wheel, Set wheel icon position, Set wheel design, Set wheel coupons, Set wheel result.','inboundwp-lite'); ?></li>
																<li><?php _e('Step-3: You can set on any pages or posts.','inboundwp-lite'); ?></li>
															</ul>
														</td>
													</tr>

													<tr>
														<th>
															<label><?php _e('How Spin Wheel Works', 'inboundwp-lite'); ?>:</label>
														</th>
														<td>
															<ul>
																<li><?php _e('Step-1: Go Spin Wheel - IBWP -->  Settings', 'inboundwp-lite'); ?></li>
																<li><?php _e('Step-2: Under Wheel Display Settings, Check any post type to register metabox on these post type.', 'inboundwp-lite'); ?></li>
																<li><?php _e('Step-3: Go to Any page or post select any spin wheel.',''); ?></li>
															</ul>
														</td>
													</tr>
												</tbody>
											</table>
										</div><!-- .inside -->
                                    </div><!-- #general -->
                                </div><!-- .meta-box-sortables ui-sortable -->
                            </div><!-- .metabox-holder -->
                        </div><!-- #post-body-content -->
                    </div><!-- #post-body -->
                </div><!-- #poststuff -->
            </div><!-- #post-box-container -->
        </div><!-- end .ibwp-spw-tab-cnt-wrp -->
    </div><!-- end .ibwp-spw-wrap -->
<?php }