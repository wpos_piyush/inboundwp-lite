<?php
/**
 * Emails List Page
 *
 * @package Spin Wheel
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Ibwp_spw_email_lists extends WP_List_Table {

	var $prefix, $per_page, $email_count;
	function __construct() {
		
        // Set parent defaults
        parent::__construct( array(
								'singular'  => 'ibwp_spw_emails',	// singular name of the listed records
								'plural'    => 'ibwp_spw_emails', 	// plural name of the listed records
								'ajax'      => true        	// does this table support ajax?
							));
		$this->per_page		= 15; // Per page
		$this->prefix 		= IBWP_SPW_META_PREFIX;
		$Spw_Email_Modal 	= new Spw_Email_Modal();
		$this->email_count  = $Spw_Email_Modal->ibwp_spw_email_count();

    }

    /**
	 * Displaying emails data
	 * 
	 * Does prepare the data for displaying the emails in the table.
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 */
    function ibwp_spw_display_emails() {

    	// Taking parameter
		$args 		= $data = array();
		$orderby 	= isset( $_GET['orderby'] )	? urldecode( $_GET['orderby'] )	: 'created_date';
		$order		= isset( $_GET['order'] )	? $_GET['order']                : 'DESC';
		
		$args = array(
						'limit'		=> $this->per_page,
						'orderby'	=> $orderby,
						'order'		=> $order,
						'search'	=> isset( $_GET['s'] ) ? $_GET['s'] : '',
					);

		// Get Emails Data
		$Spw_Email_Modal 		= new Spw_Email_Modal();
		$ibwp_spw_email_data 	= $Spw_Email_Modal->ibwp_spw_email_data( $args );
		$prefix 				= IBWP_SPW_META_PREFIX;

		if( !empty($ibwp_spw_email_data) ) {
			foreach ( $ibwp_spw_email_data as $key => $value ) {

				$coupon_type 					= get_post_meta($value->wheel_id, $prefix.'coupon_type', true);
				$data[$key]['id'] 				= $value->id;
				$data[$key]['email'] 			= $value->email;
				$data[$key]['name'] 			= $value->name;
				$data[$key]['post_title']		= $value->post_title;

				if($value->refer_page) {
					$data[$key]['refer_page'] 	= '<a href="'.esc_url($value->refer_page).'" target="__blank">'.__('Visit Link','inboundwp-lite').'</a>';
				} else {
					$data[$key]['refer_page'] 	= '-';
				}

				if($value->coupon_code == '0') {
					$data[$key]['coupon_code'] 	= '-';
				} else {
					$data[$key]['coupon_code']	= $value->coupon_code;
				}

				$data[$key]['coupon_type']		= $coupon_type;
				
				$data[$key]['created_date']		= $value->created_date;

				if($value->order_id == '0') {
					$data[$key]['order_id'] = '-';
				} else {
					if( $coupon_type == 'woocommerce' ) {
						$order 					= wc_get_order( $value->order_id );
						$order_url 				= $order->get_edit_order_url();
						$data[$key]['order_id']	= '<a href="'.esc_url($order_url).'" target="__blank">'.$value->order_id.'</a>';
					} else if( $coupon_type == 'edd' ) {
						$order_url 				= add_query_arg( 'id', $value->order_id, admin_url( 'edit.php?post_type=download&page=edd-payment-history&view=view-order-details' ) );
						$data[$key]['order_id']	= '<a href="'.esc_url($order_url).'" target="__blank">'.$value->order_id.'</a>';
					}
				}
			}
		}
		return $data;

	}

	/**
	 * Mange column data
	 * 
	 * Default Column for listing table
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 */
	function column_default($item, $column_name){

        switch( $column_name ) {
            default:
				$default_val = $item[ $column_name ];
				break;
        }
        return $default_val;

    }

    /**
	 * Handles checkbox HTML
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 **/
    function column_cb($item){

        return sprintf(
            '<input type="checkbox" name="%1$s[]" value="%2$s" />',
            $this->_args['singular'],  //Let's simply repurpose the table's singular label ("ibwp-spw-emails")
            $item['id']                //The value of the checkbox should be the record's id
        );

    }

    /**
     * Manage Post Title Column
     *
     * @package Spin Wheel
     * @since 1.0
     */
    function column_email($item){

    	$paged 				= isset($_GET['paged']) ? $_GET['paged'] : false;
    	$page_url			= add_query_arg( array('post_type' => IBWP_SPW_POST_TYPE, 'page' => 'ibwp-spw-emails', 'paged' => $paged), admin_url('edit.php') );
    	$delete_email 		= add_query_arg( array('action' => 'delete', 'ibwp_spw_emails[]' => $item['id'], 'ibwp_spw_nonce' => wp_create_nonce('ibwp-spw-email-delete') ), $page_url );
    	$email 				= !empty($item['email']) ? ucfirst( $item['email'] ) : '';
    	$actions['delete'] 	= sprintf('<a class="email-list-view email-action-link" href="%s">'.__('Delete', 'inboundwp-lite').'</a>', $delete_email );
        //Return the email contents
        return sprintf('%1$s %2$s',
           /*%1$s*/ $email,
           /*%2$s*/ $this->row_actions($actions)
        );

    }

    /**
     * Display Columns
     * 
     * Handles which columns to show in table
     * 
	 * @package Spin Wheel
 	 * @since 1.0
     */
    function get_columns(){

        $columns = array(
            'cb'        	=> '<input type="checkbox" />', //Render a checkbox instead of text
            'email'     	=> __('Email','inboundwp-lite'),
            'name'    		=> __('Name','inboundwp-lite'),
            'post_title'  	=> __('Wheel Name','inboundwp-lite'),
            'coupon_code'	=> __('Coupon Code','inboundwp-lite'),
            'coupon_type'	=> __('Coupon Type','inboundwp-lite'),
            'refer_page'	=> __('Referral Page','inboundwp-lite'),
            'created_date'	=> __('Date','inboundwp-lite'),
            'order_id'		=> __('Converted Order ID','inboundwp-lite')
        );
        return $columns;

    }

    /**
     * Sortable Columns
     *
     * Handles soratable columns of the table
     * 
	 * @package Spin Wheel
 	 * @since 1.0
     */
    function get_sortable_columns() {

        $sortable_columns = array(
            'email'     	=> array('email',false),     //true means it's already sorted
            'name'    		=> array('name',false),
            'created_date'  => array('created_date',false)
        );
        return $sortable_columns;

    }

    /**
	 * Message to show when no records in database table
	 *
	 * @package Spin Wheel
 	 * @since 1.0
	 */
	function no_items() {

		echo __('No Emails Record Found', 'inboundwp-lite');

	}

	/**
	 * Show the search field
	 *
	 * @param string $text Label for the search box
	 * @param string $input_id id of the search box
	 *
	 * @package Spin Wheel
 	 * @since 1.0
	 */
	public function search_box( $text, $input_id ) {
		
		$input_id = $input_id . '-search-input';
		if ( ! empty( $_REQUEST['post_type'] ) ) {
			echo '<input type="hidden" name="post_type" value="'.IBWP_SPW_POST_TYPE.'" />';
		}
		if ( ! empty( $_REQUEST['orderby'] ) ) {
			echo '<input type="hidden" name="orderby" value="' . esc_attr( $_REQUEST['orderby'] ) . '" />';
		}
		if ( ! empty( $_REQUEST['order'] ) ) {
			echo '<input type="hidden" name="order" value="' . esc_attr( $_REQUEST['order'] ) . '" />';
		}
	?>
		<div id="spw-user-search-filters" class="spw-user-search-filters">
			<p class="search-box">
				<label class="screen-reader-text" for="<?php echo $input_id ?>"><?php echo $text; ?>:</label>
				<input type="search" id="<?php echo $input_id ?>" name="s" value="<?php _admin_search_query(); ?>" />
				<?php submit_button( $text, 'button', false, false, array('id' => 'search-submit') ); ?>
			</p>
		</div>
	<?php
	}

    /**
     * Bulk actions field
     *
     * Handles Bulk Action combo box values
     * 
	 * @package Spin Wheel
 	 * @since 1.0
     */
	function get_bulk_actions() {
        $actions = array(
						'delete' => __('Delete', 'inboundwp-lite')
					);
        return $actions;
	}
	
    /**
	 * Prepare Items for emails listing
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 **/
    function prepare_items() {
        
        // Get how many records per page to show
        $per_page	= $this->per_page;
        
        // Get All, Hidden, Sortable columns
        $columns	= $this->get_columns();
        $hidden 	= array();
        $sortable 	= $this->get_sortable_columns();
        
        // Get final column header
        $this->_column_headers = array($columns, $hidden, $sortable);
   		
		// Get current page number
        $current_page = $this->get_pagenum();
        
		// Get total count
        $total_items  = $this->email_count;
        // Get page items
        $this->items = $this->ibwp_spw_display_emails();
        // Register pagination options and calculations.
        $this->set_pagination_args( array(
							            'total_items' => $total_items,                  // Calculate the total number of items
							            'per_page'    => $per_page,                     // Determine how many items to show on a page
							            'total_pages' => ceil($total_items / $per_page)	// Calculate the total number of pages
							        ));
    }
}

$email_lists = new Ibwp_spw_email_lists();
$email_lists->prepare_items();

?>
<div class="wrap spw-users-list-wrp">
	<h2><?php _e( 'Spin Wheel Emails', 'inboundwp-lite' ); ?></h2>
    
    <?php
    if( !empty($_GET['message']) && $_GET['message'] == 1 ) {
    	echo '<div class="updated notice notice-success is-dismissible">
				<p>'.__('Email deleted successfully.', 'inboundwp-lite').'</p>
		      </div>';
    } ?>

    <!-- Forms are NOT created automatically, so you need to wrap the table in one to use features like bulk actions -->
    <form id="spw-site-list-frm" class="spw-site-list-frm" method="get" action="">
    	<!-- For plugins, we also need to ensure that the form posts back to our current page -->
        <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
		
        <!-- Search Title -->
        <?php $email_lists->search_box( __( 'Search', 'inboundwp-lite' ), 'inboundwp-lite' ); ?>
        
		<?php $email_lists->views(); // Showing sorting links on the top of the list ?>
        
        <!-- Now we can render the completed list table -->
        <?php $email_lists->display(); ?>
    </form><!-- end .spw-site-list-frm -->
</div><!-- end .wrap -->