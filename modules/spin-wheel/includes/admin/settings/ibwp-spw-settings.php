<?php
/**
 * General Settings Page
 *
 * @package Spin Wheel
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

$post_types 			= ibwpl_display_locations('global',false);//ibwp_spw_pro_get_post_types();
$ibwp_spw_post_types 	= ibwp_spw_get_option( 'post_types', array() );
$wheel_glob_locs 		= ibwp_spw_get_option( 'wheel_glob_locs', array() );
$spw_posts 				= ibwp_spw_post_by_type();

?>

<div class="wrap ibwp-sett-wrap ibwp-analy-settings">
	<h2><?php _e( 'Spin Wheel Settings', 'inboundwp-lite' ); ?></h2>

	<?php
	// Reset message
	if( !empty( $_POST['ibwp_spw_reset_settings'] ) ) {
		ibwpl_display_message( 'reset' );
	}

	// Success message
	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {
		ibwpl_display_message( 'update' );
	}
	?>

	<!-- Plugin reset settings form -->
	<form action="" method="post" id="ibwp-spw-reset-sett-form" class="ibwp-right ibwp-spw-reset-sett-form">
		<input type="submit" class="button button-primary right ibwp-btn ibwp-reset-sett ibwp-resett-sett-btn ibwp-spw-reset-sett" name="ibwp_spw_reset_settings" id="ibwp-spw-reset-sett" value="<?php _e( 'Reset All Settings', 'inboundwp-lite' ); ?>" />
	</form>

	<form action="options.php" method="POST" id="ibwp-analy-settings-form" class="ibwp-analy-settings-form">
	
		<?php
			settings_fields( 'ibwp_spw_plugin_options' );
		?>
		<div id="ibwp-analy-mc-sett" class="post-box-container ibwp-analy-mc-sett">

			<div class="textright ibwp-clearfix">
				<input type="submit" name="ibwp_spw_sett_submit" class="button button-primary right ibwp-btn ibwp-spin-wheel-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
			</div>
			<div class="metabox-holder">
				<!-- Start Google Analytics Settings HTML -->
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">
						<h3 class="hndle">
							<span><?php _e( 'General Settings', 'inboundwp-lite' ); ?></span>
						</h3>
						<div class="inside">
							<table class="form-table ibwp-analy-mc-sett-tbl">
								<tbody>
									<tr>
										<th>
											<label for="ibwp-spw-enable-analy"><?php _e('Enable','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="checkbox" name="ibwp_spw_options[ga_enable]" id="ibwp-spw-enable-analy" value="1" <?php checked( ibwp_spw_get_option('ga_enable'), 1 ); ?>><br>
											<span class="description"><?php _e('Check this box if you want to enable Google analytics event tracking.','inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th>
											<label for="ibwp-spw-track-id"><?php _e('Google Analytics Identifier','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="text" name="ibwp_spw_options[ga_track_id]" id="ibwp-spw-track-id" value="<?php echo ibwpl_esc_attr(ibwp_spw_get_option('ga_track_id')); ?>"><br>
											<span class="description"><?php _e('Enter google analytics tracking ID. Ex: UA-123456789-1','inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th>
											<label for="ibwp-spw-enable-setting-opts"><?php _e('Setting Display On','inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php foreach ($post_types as $post_key => $post_type) {
												
												$checked_cls = '';
												if(in_array($post_key,$ibwp_spw_post_types)) {
													$checked_cls = 'checked = "checked"';
												} ?>
												<label for="type-<?php echo $post_type;?>">
													<input type="checkbox" id="type-<?php echo $post_type;?>" name="ibwp_spw_options[post_types][]" value="<?php echo ibwpl_esc_attr($post_key);?>" <?php echo $checked_cls;?>><?php echo $post_type;?>
												</label><br>
											<?php } ?>
											<span class="description"><?php _e('Check post type to display spin wheel setting metabox on these post types.','inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th>
											<label for="ibwp-spw-select-sett"><?php _e('Select Spin Wheel','inboundwp-lite'); ?></label>
										</th>
										<td>
											<select name="ibwp_spw_options[select_wheel]" class="ibwp-spw-select-sett ibwp-select2" id="ibwp-spw-select-sett">
												<option value=""><?php _e('Select Spin Wheel','inboundwp-lite'); ?></option>
												<?php
													foreach ($spw_posts as $spw_post) {
														echo '<option value="'.$spw_post->ID.'" '.selected($spw_post->ID,ibwp_spw_get_option('select_wheel')).'>'.$spw_post->post_title.'</option>';
													}
												?>
											</select>
										</td>
									</tr>
									<tr>
										<th>
											<label for="ibwp-spw-global-locs-all"><?php _e('Display in whole site','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="checkbox" name="ibwp_spw_options[wheel_glob_locs][all]" id="ibwp-spw-global-locs-all" class="ibwp-checkbox ibwp-spw-global-locs" value="1" <?php if( array_key_exists('all', $wheel_glob_locs ) ) { checked( true ); } ?> /><br/>
											<span class="description"><?php _e('Check this box to enable spin wheel globally to whole site.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End Google Analytics Settings HTML -->
				<div class="textright ibwp-clearfix">
					<input type="submit" name="ibwp_spw_sett_submit" class="button button-primary right ibwp-btn ibwp-spin-wheel-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
				</div>
				<!-- End Wheel display Settings HTML -->
			</div>
		</div>
	</form>
</div>