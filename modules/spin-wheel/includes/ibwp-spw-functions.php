<?php
/**
 * Functions File
 *
 * @package Spin Wheel
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Update default settings
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_default_settings() {

	global $ibwp_spw_options;

	$ibwp_spw_options = array(
							'ga_enable'			=> 0,
							'ga_track_id'		=> '',
							'post_types'		=> array(),
							'select_wheel'		=> '',
							'wheel_glob_locs'	=> array(),
						);

    $default_options = apply_filters('ibwp_spw_options_default_values', $ibwp_spw_options );

	// Update default options
	update_option( 'ibwp_spw_options', $default_options );

	// Overwrite global variable when option is update	
	$ibwp_spw_options = ibwpl_get_settings( 'ibwp_spw_options' );
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_get_option( $key = '', $default = false ) {
	global $ibwp_spw_options;
	$value 	= ! empty( $ibwp_spw_options[ $key ] ) ? $ibwp_spw_options[ $key ] : $default;
	return $value;
}

/**
 * Create Email List store table
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_create_email_table() {
	global $wpdb;

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE `".IBWP_SPW_EMAIL_TABLE."` (
			id INT(20) unsigned NOT NULL AUTO_INCREMENT,
			email varchar(100) NOT NULL,
			name varchar(250) NOT NULL,
			order_id INT(20) unsigned NOT NULL,
			wheel_id INT(20) unsigned NOT NULL,
			coupon_id INT(20) unsigned NOT NULL,
			coupon_code varchar(250) NOT NULL,
			refer_page 	varchar(250) NOT NULL,
			created_date datetime NOT NULL,
			modification_date datetime NOT NULL,
			PRIMARY KEY  (id)
		) $charset_collate";
	dbDelta( $sql );
}

/**
 * Get popup post type list
 * 
 * @package Notification Popup Pro
 * @since 1.0
 */
function ibwp_spw_post_by_type() {

    $args = array(
        'post_type'         => IBWP_SPW_POST_TYPE, 
        'post_status'       => 'publish', 
        'posts_per_page'    => -1,
        'order'             => 'DESC',
        'suppress_filters'  => false,
    );

    $spw_posts = get_posts( $args );
    return $spw_posts;
}

/**
 * Check and update order id for email table
 * 
 * @subpackage Spin Wheel
 * @since 1.0
 */
function ibwp_spw_check_coupon_code( $order_id, $email_id, $coupon_code, $status ) {

	global $wpdb;

	$email_table 	= IBWP_SPW_EMAIL_TABLE;

	$coupon_result = $wpdb->get_results( "SELECT * FROM $email_table WHERE email='$email_id' AND coupon_code='$coupon_code'" );
		
	if( $wpdb->num_rows > 0 ) {

		foreach ($coupon_result as $wkey => $wval) {

			$coupon_type = get_post_meta($wval->wheel_id, IBWP_SPW_META_PREFIX.'coupon_type', true);
	
			if( $status == $coupon_type ) {
	
				$wpdb->query("UPDATE $email_table SET order_id='$order_id' WHERE email='$email_id' AND wheel_id='$wval->wheel_id'");
				unset($_COOKIE['ibwp_spw_place_order']);
				setcookie('ibwp_spw_place_order', null, '-1', '/');
			}
		}

	} else if( isset($_COOKIE['ibwp_spw_place_order']) ) {
		
		$cookie_value 		= $_COOKIE['ibwp_spw_place_order'];
		$cookie_value 		= explode("__", $cookie_value);
		$email_data_id 		= $cookie_value[0];

		$email_data = $wpdb->get_results( "SELECT * FROM $email_table WHERE id='$email_data_id' AND coupon_code='$coupon_code'" );

		if( $wpdb->num_rows > 0 ) {

			$wpdb->query("UPDATE $email_table SET order_id='$order_id' WHERE id='$email_data_id' AND coupon_code='$coupon_code'");
			
			unset($_COOKIE['ibwp_spw_place_order']);
			setcookie('ibwp_spw_place_order', null, '-1', '/');

			return true;
		} else {
			echo 'Else';
			return false;
		}
	} else {
		return false;
	}
}