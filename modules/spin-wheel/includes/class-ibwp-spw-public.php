<?php

/**
 * Public Class
 *
 * Handles the public side functionality of plugin
 *
 * @package Spin Wheel
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Ibwp_Spw_Public {

	function __construct() {
		
		add_action( 'wp', array( $this, 'ibwp_spw_check_active_ids' ) );

		// Action to Google analytics Event Header Script
		add_action( 'wp_head', array( $this, 'ibwp_spw_ga_events_header' ) );

		// Action For Spin Wheel
		add_action( 'wp_footer', array( $this, 'ibwp_spw_add_spin_wheel' ) );

		// Get Email Ajax
		add_action( 'wp_ajax_ibwp_spw_get_email', array( $this, 'ibwp_spw_get_email' ) );
		add_action( 'wp_ajax_nopriv_ibwp_spw_get_email', array( $this, 'ibwp_spw_get_email' ) );		

		// Action to check spin wheel code at checkout
		add_action( 'woocommerce_new_order', array( $this, 'ibwp_spw_woo_check_coupon_code' ),  1, 1  );
		add_action( 'edd_complete_purchase', array( $this, 'ibwp_spw_edd_check_coupon_code' ) );
	}

	/**
	 * Function to get active spin wheel ids
	 * 
	 * @package Spin Wheel
	 * @since 1.0
	 */
	function ibwp_spw_check_active_ids() {

		global $ibwp_spw_options, $post, $pagenow, $spw_active_id;

		$prefix = IBWP_SPW_META_PREFIX;

		if( is_admin() ) {
			return;
		}

		$spw_post_ids = array();
		$wheel_glob_locs = $ibwp_spw_options['wheel_glob_locs'];
		$wheel_glob_id = $ibwp_spw_options['select_wheel'];
			
		if ( is_404() || is_search() || is_archive() ) {

			$sp_post_ids[] = ibwpl_post_status( $wheel_glob_id );
			$spw_active_id = $sp_post_ids;
			return;
		}

		$wheel_id = get_post_meta( $post->ID, $prefix. 'wheel_post', true );
		
		if( $wheel_id == 'disable' ) {
			return;
		}

		if( $wheel_id ) {
			
			$spw_post_ids[] = $wheel_id;
		
		} elseif ( ! empty( $wheel_glob_locs ) && isset( $wheel_glob_locs['all'] ) ) {
			
			$spw_post_ids[] = $wheel_glob_id;
		}
		
		$spw_post_ids = ibwpl_post_status( $spw_post_ids );
		$spw_active_id = $spw_post_ids;
	}

	/**
	 * Function to Add GA event tracking header script
	 * 
	 * @package Spin Wheel
	 * @since 1.0 
	 **/
	function ibwp_spw_ga_events_header() {

	    $ga_enable 		= ibwp_spw_get_option('ga_enable');
		$ga_track_id 	= ibwp_spw_get_option('ga_track_id');

  		if($ga_enable == '1' && !empty($ga_track_id)) { 
			
			$domain 		= $_SERVER['SERVER_NAME']; ?>

   			<script>
   				if (typeof ga === 'undefined') {
	                  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	                  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	                  <?php echo "ga('create','$ga_track_id', '$domain');"; ?>
	                  
	                  ga('send', 'pageview');
                }
   			</script>
   		<?php }
	}

	/**
	 * Function to add spin wheel to site
	 * 
	 * @package Spin Wheel
	 * @since 1.0 
	 **/
	function ibwp_spw_add_spin_wheel() {

		global $post, $spw_active_id;

		$ga_enable 		= ibwp_spw_get_option('ga_enable');
		$ga_track_id 	= ibwp_spw_get_option('ga_track_id');
		$prefix 		= IBWP_SPW_META_PREFIX;

		if( empty( $spw_active_id ) ) {
			return;
		}

		$wheel_id = $spw_active_id[0];

		/*Add GA event tracking footer script*/
		if( $ga_enable == '1' && !empty( $ga_track_id ) ) {

			$page_title = get_the_title();

			echo "
				<!-- BEGIN: Spin wheel ga events array -->
	    		<script>
	    			jQuery(document).ready(function() {
	    				if(typeof scroll_events == 'undefined') {
	                    	return;
	                    }
	                    scroll_events.bind_events( {
	                    	universal		: 1,
	                        gtm 			: 0,
	                        gst 			: 0,
	                    	click_elements	: [{'select':'.ga_icon_click_".$wheel_id."','category':'".$page_title."(Wheel Icon)','action':'click','label':'Spin Wheel Icon','bounce':'false','evalue':''},
	                    					  {'select':'.ibwp-spw-form-submit-event','category':'".$page_title."(wheel Form)','action':'submit','label':'Spin Wheel Icon','bounce':'false','evalue':''}]
	                    });
	    			});
	    		</script>
			";
		}

		/* Speen Wheel Html Create */		
		// Check notification enable or not
        $enable = get_post_meta( $wheel_id, $prefix. 'enable_spin_wheel', true );
        if( ! $enable ) {
        	return;
        }

		$post_type 				= get_post_type();
		$ibwp_spw_post_types 	= ibwp_spw_get_option( 'post_types', array() );

		echo $this->ibwp_spw_wheel_css( $wheel_id ); // Popup custom css
	
		$icon_position				= get_post_meta( $wheel_id, $prefix. 'icon_position', true );
		$wheel_title				= get_post_meta( $wheel_id, $prefix. 'wheel_title', true );		
		$wheel_description			= get_post_meta( $wheel_id, $prefix. 'wheel_description', true );
		$button_text				= get_post_meta( $wheel_id, $prefix. 'button_text', true );
		$enable_terms_conditions	= get_post_meta( $wheel_id, $prefix. 'enable_terms_conditions', true );
		$terms_conditions_desc		= get_post_meta( $wheel_id, $prefix. 'terms_conditions_desc', true );
		$name_field_show			= get_post_meta( $wheel_id, $prefix. 'name_field_show', true );
		$icon_tooltip_text			= get_post_meta( $wheel_id, $prefix. 'icon_tooltip_text', true );

		$id		 					= isset($wheel_id) 					? $wheel_id 				: '';
		$enable_spin_wheel 			= isset($enable_spin_wheel) 		? $enable_spin_wheel 		: '';
		$wheel_description 			= isset($wheel_description) 		? $wheel_description 		: '';
		$enable_terms_conditions 	= isset($enable_terms_conditions) 	? $enable_terms_conditions 	: '';
		$name_field_show 			= isset($name_field_show) 			? $name_field_show 			: '';

		// Include Design File
		include_once( IBWP_SPW_DIR . "/templates/design-1.php" );
	}

	/**
	 * Function for Spin Wheel css to site
	 * 
	 * @package Spin Wheel
	 * @since 1.0 
	 **/
	function ibwp_spw_wheel_css( $id ) {

		$prefix 	= IBWP_SPW_META_PREFIX;

		$icon_position			= get_post_meta( $id, $prefix. 'icon_position', true );
		$wheel_title_color		= get_post_meta( $id, $prefix. 'wheel_title_color', true );

		$pointer_icon_color		= get_post_meta( $id, $prefix. 'pointer_icon_color', true );
		$pointer_icon_color 	= !empty($pointer_icon_color) 	? $pointer_icon_color 	: '#000000';

		$button_text_color		= get_post_meta( $id, $prefix. 'button_text_color', true );
		$button_text_color 		= !empty($button_text_color) 	? $button_text_color 	: '#ffffff';

		$button_bg_color		= get_post_meta( $id, $prefix. 'button_bg_color', true );
		$button_bg_color 		= !empty($button_bg_color) 		? $button_bg_color 		: '#000000';

		$wheel_bg_image 		= get_post_meta( $id, $prefix. 'wheel_bg_image', true );
		$wheel_bg_color			= get_post_meta( $id, $prefix. 'wheel_bg_color', true );

		$html = '<style type="text/css">';

		$html .= '.ibwp-spw-inboundwp-pro-main { background:'.$wheel_bg_color.' url('.$wheel_bg_image.'); }';

		$html .= '.ibwp-spw-content-wrap .ibwp-spw-inboundwp-pro-right .ibwp-spw-wheel-desc .ibwp-spw-wheel-title { color: '.$wheel_title_color.'; }';
		
		$html .= '.ibwp-spw-content-wrap .ibwp-spw-pointer:before { color: '.$pointer_icon_color.'; }';

		$html .= '.ibwp-spw-lucky-user .ibwp-spw-spin-button { background:'.$button_bg_color.'; color: '.$button_text_color.'; }';

		$html .= '.ibwp-spw-lucky-user .ibwp-spw-spin-button:hover { background:'.$button_bg_color.'; color: '.$button_text_color.'; }';

		$html .='</style>';

		return $html;
	}

	/**
	 * Get Email Function
	 *
	 * @package Spin Wheel
	 * @since 1.0
	 */
	public function ibwp_spw_get_email() {

		global $wpdb;

		$wheel_id				= isset( $_POST['wheel_id'] ) 				? $_POST['wheel_id'] 									: '';
		$email       			= isset( $_POST["user_email"] ) 			? sanitize_email( strtolower( $_POST["user_email"] ) ) 	: '';
		$name        			= !empty( $_POST["user_name"] ) 			? sanitize_text_field( $_POST["user_name"] ) 			: 'Guest';
		$email_store			= isset( $_POST['email_store'] )  			? $_POST['email_store'] 								: 0;
		$coupon_type 			= $_POST['coupon_type']						? $_POST['coupon_type'] 								: '';
		$lost_message 			= stripcslashes($_POST['lost_message']);
		$enable_spin_wheel 		= isset( $_POST['enable_spin_wheel'] )		? $_POST['enable_spin_wheel']							: '';
		$spin_duration 			= $_POST['spining_duration']				? $_POST['spining_duration']							: 5;
		$allow       			= __('no','inboundwp-pro');
		$stop_position 			= -1;
		$spin_num				= 1;
		$spin_wheel_coupons 	= isset($_POST['spin_wheel_coupons'])		? $_POST['spin_wheel_coupons']							: array('1' => '','2' => '','3' => '','4' => '');
		$win_message 			= wpautop($_POST['win_message']);
		$win_message 			= stripcslashes($win_message);
		$table_name 			= IBWP_SPW_EMAIL_TABLE;
		$refer_page 			= isset($_POST['refer_page'])				? $_POST['refer_page']									: '';

		if ( $enable_spin_wheel != 1 ) {
			$allow = __('Wrong email.','inboundwp-pro');
			$data  = [ 'allow_spin' => $allow ];
			wp_send_json( $data );
			die;
		}

		$wpdb->get_results( "SELECT email FROM $table_name WHERE email='$email' AND wheel_id='$wheel_id'" );

		if($wpdb->num_rows > 0) {
			
			$allow = __('This email has reach the maximum spins.','inboundwp-pro');

		} else {
			
			$allow 					= __('yes','inboundwp-pro');
			$spin_wheel_coupons 	= isset($_POST['spin_wheel_coupons'])	? $_POST['spin_wheel_coupons']		: '';
			$created_date 			= current_time( 'mysql' );

			if( !empty( $spin_wheel_coupons ) ) {

				foreach ($spin_wheel_coupons as $file_key => $file_data) {

					$file_data['coupon_code']				= ( $file_data['coupon_code'] != '' ) 				? $file_data['coupon_code'] 			: '';
					$file_data['coupon_id']					= ( $file_data['coupon_id'] != '' ) 				? $file_data['coupon_id'] 				: '';
					$file_data['coupon_lbl']				= ( $file_data['coupon_lbl'] != '' ) 				? $file_data['coupon_lbl'] 				: '';
					$file_data['coupon_probability']		= ( $file_data['coupon_probability'] != '' ) 		? $file_data['coupon_probability'] 		: '';
					$file_data['coupon_redirect']			= ( $file_data['coupon_redirect'] != '' ) 			? $file_data['coupon_redirect'] 		: '';
					$file_data['coupon_redirect_link']		= ( $file_data['coupon_redirect_link'] != '' ) 		? $file_data['coupon_redirect_link'] 	: '';
					$file_data['coupon_win_message']		= ( $file_data['coupon_win_message'] != '' ) 		? $file_data['coupon_win_message'] 		: '';
					$file_data['coupon_email_message']		= ( $file_data['coupon_email_message'] != '' ) 		? $file_data['coupon_email_message'] 	: '';
					$file_data['coupon_lost_message']		= ( $file_data['coupon_lost_message'] != '' ) 		? $file_data['coupon_lost_message'] 	: '';
		
					$coupon_code[] 				= $file_data['coupon_code'];
					$coupon_id[] 				= $file_data['coupon_id'];
					$coupon_lbl[] 				= $file_data['coupon_lbl'];
					$coupon_probability[] 		= $file_data['coupon_probability'];
					$coupon_redirect[] 			= $file_data['coupon_redirect'];
					$coupon_redirect_link[] 	= $file_data['coupon_redirect_link'];
					$coupon_win_message[] 		= $file_data['coupon_win_message'];
					$coupon_email_message[] 	= $file_data['coupon_email_message'];
					$coupon_lost_message[] 		= $file_data['coupon_lost_message'];
				}
			}

			//Get stop position
			for ( $i = 1; $i < sizeof( $coupon_probability ); $i ++ ) {
				$coupon_probability[ $i ] += $coupon_probability[ $i - 1 ];
			}

			for ( $i = 0; $i < sizeof( $coupon_probability ); $i ++ ) {
				if ( $coupon_probability == 0 ) {
					$coupon_probability[ $i ] = 0;
				}
			}

			$random 		= rand( 1, 100 );
			$stop_position  = 0;

			foreach ( $coupon_probability as $probability ) {
				if ( $random <= $probability ) {
					break;
				}
				$stop_position ++;
			}

			//Page Redirect Link Condition
			if($coupon_redirect[$stop_position] == '1') {

				$page_redirect_link = $coupon_redirect_link[$stop_position];

			}

			$lost_message 			= !empty($coupon_lost_message[$stop_position]) 		? $coupon_lost_message[$stop_position] 			: $lost_message;
			$coupon_code 			= $coupon_code[$stop_position];
			$coupon_id 				= $coupon_id[$stop_position];

			$coupon_email_message 	= wpautop($coupon_email_message[$stop_position]);

			// Insert Data into in your database
			if($email_store == '1') {

				$insert_data = array(
								'wheel_id'			=> $wheel_id,
								'coupon_id'			=> $coupon_id,
								'email'				=> $email,
								'name'				=> $name,
								'coupon_code'		=> $coupon_code,
								'refer_page'		=> $refer_page,
								'created_date'		=> $created_date,
								'modification_date'	=> $created_date,
							);

				$wpdb->insert( $table_name, $insert_data );
				$last_insert 	= $wpdb->insert_id;
			}

			if( $coupon_code == '0' ) {

				$result_notification 	= wpautop($lost_message);

			} else {

				$coupon_lbl 			= $coupon_lbl[$stop_position];

				if( $coupon_type == 'woocommerce' ) {
					$coupons_data_get 	= new WC_Coupon($coupon_id);
					$expire_date 		= $coupons_data_get->get_date_expires();
				} if( $coupon_type == 'edd' ) {
					$expire_date 		= get_post_meta( $coupon_id, '_edd_discount_expiration', true );
				} if( $coupon_type == 'other' ) {
					$expire_date 		= __('No Expire','inboundwp-pro');
				}

				if($expire_date == 'No Expire') {
					$coupon_days 	= 30;
				} else {
					$now 			= time(); // or your date as well
					$your_date 		= strtotime($expire_date);
					$datediff 		= $your_date - $now;

					$coupon_days   	= round($datediff / (60 * 60 * 24));
				}

				$coupon_expire_date 	= !empty($expire_date) ? $expire_date : __('No Expire');
				$coupon_win_message 	= wpautop($coupon_win_message[$stop_position]);
				$coupon_win_message 	= stripcslashes($coupon_win_message);
				$result_notification 	= !empty($coupon_win_message) ? $coupon_win_message : $win_message;
				
				self::ibwp_spw_send_email( $email, $name, $coupon_code, $coupon_email_message, $coupon_expire_date, $coupon_lbl );

				$result_notification = str_replace( '{coupon_code}', $coupon_code , $result_notification );
				$result_notification = str_replace( '{coupon_label}', $coupon_lbl , $result_notification );
				$result_notification = str_replace( '{customer_name}', $name , $result_notification );
				$result_notification = str_replace( '{customer_email}', $email , $result_notification );

				if(class_exists( 'WooCommerce' ) || $coupon_type == 'woocommerce') { 

					$result_notification = str_replace( '{checkout}', '<a href="' . wc_get_checkout_url() . '">' . esc_html__( 'Checkout', 'inboundwp-pro' ) . '</a>', $result_notification );

				}
				setcookie('ibwp_spw_place_order', $last_insert.'__'.$coupon_code.'__'.$wheel_id, time() + (86400 * 30), "/"); // 86400 = 1 day
			}
		}

		$data = [
			'allow_spin'          	=> $allow,
			'stop_position'       	=> $stop_position,
			'spin_duration'		  	=> $spin_duration,
			'page_redirect_link'  	=> $page_redirect_link,
			'result_notification' 	=> $result_notification,
		];

		wp_send_json( $data );
		exit;

	}

	/**
	 * Send email function
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 **/
	function ibwp_spw_send_email( $user_email, $customer_name, $coupon_code, $coupon_email_message, $coupon_expire_date, $coupon_label = '' ) {

		$email_subject = isset( $_POST['email_subject'] ) ? $_POST['email_subject'] : '';

		$email_message = isset( $_POST['email_message'] ) ? $_POST['email_message'] : '';
		$email_message = !empty($coupon_email_message) ? $coupon_email_message : $email_message;
		$email_message = stripcslashes($email_message);
		$email_message = str_replace( '{message}', wpautop($email_message), $email_message );
		$email_message = str_replace( '{coupon_label}', $coupon_label, $email_message );
		$email_message = str_replace( '{customer_name}', $customer_name, $email_message );
		$email_message = str_replace( '{coupon_code}', '<span class="spw-email-coupon-code">' . $coupon_code . '</span>', $email_message );
		$email_message = str_replace('{date_expires}', $coupon_expire_date, $email_message);
		
		$atts['heading'] = isset( $_POST['email_heading'] ) 		? $_POST['email_heading']			: '';
		$atts['message'] = wpautop( $email_message );

		$content = ibwpl_get_template_html( 'spin-wheel', 'email-template.php', $atts );

		$headers = 'Content-Type: text/html';	   
		$subject = stripslashes( $email_subject );

		wp_mail($user_email, $subject, $content, $headers, '');
	}

	/**
	 * Check coupon code at checkout for WooCommerce
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 **/
	function ibwp_spw_woo_check_coupon_code( $order_id ) {

		global $woocommerce;

		$order 			= wc_get_order( $order_id );
		$order_email 	= $order->get_billing_email();
		$email_table 	= IBWP_SPW_EMAIL_TABLE;
		$coupon_code 	= $woocommerce->session->applied_coupons[0];
		
		$coupon_code_status = ibwp_spw_check_coupon_code( $order_id, $order_email, $coupon_code, 'woocommerce' );

		return $coupon_code_status;
	}

	/**
	 * Check coupon code at checkout for WooCommerce
	 * 
	 * @package Spin Wheel
 	 * @since 1.0
	 **/
	function ibwp_spw_edd_check_coupon_code( $payment_id ) {

		// Basic payment meta
		$payment_meta 	= edd_get_payment_meta( $payment_id );
		$email_id 		= $payment_meta['user_info']['email'];
		$discount_code 	= $payment_meta['user_info']['discount'];

		$coupon_code_status = ibwp_spw_check_coupon_code( $payment_id, $email_id, $discount_code, 'edd' );

		return $coupon_code_status;
	}

}

$ibwp_spw_public = new Ibwp_Spw_Public();