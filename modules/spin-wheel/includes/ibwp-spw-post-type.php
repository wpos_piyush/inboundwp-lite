<?php
/**
 * Register Post type functionality
 *
 * @package Spin Wheel
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Function to register post type
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_wheel_post_type() {

	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	if ( post_type_exists( IBWP_SPW_POST_TYPE ) ) {
		return;
	}

	$ibwp_spw_lables = array(
		'name'					=> __('Spin Wheel', 'inboundwp-lite'),
		'singular_name'			=> __('Spin Wheel', 'inboundwp-lite'),
		'all_items'				=> __('All Spin Wheels', 'inboundwp-lite'),
		'add_new'				=> __('Add Spin Wheel', 'inboundwp-lite'),
		'add_new_item'			=> __('Add New Spin Wheel', 'inboundwp-lite'),
		'edit_item'				=> __('Edit Spin Wheel', 'inboundwp-lite'),
		'new_item'				=> __('New Spin Wheel', 'inboundwp-lite'),
		'view_item'				=> __('View Spin Wheel', 'inboundwp-lite'),
		'search_items'			=> __('Search Spin Wheel', 'inboundwp-lite'),
		'not_found'				=>  __('No Spin Wheel Items found', 'inboundwp-lite'),
		'not_found_in_trash'	=> __('No Spin Wheel Items found in Trash', 'inboundwp-lite'),
		'parent_item_colon'		=> '',
		'menu_name'				=> __('Spin Wheel - IBWP', 'inboundwp-lite'),
		'featured_image'		=> __('Spin Wheel Image', 'inboundwp-lite'),
		'set_featured_image'	=> __('Set Spin Wheel Image', 'inboundwp-lite'),
		'remove_featured_image'	=> __('Remove blog image', 'inboundwp-lite'),
		'use_featured_image'	=> __('Use as blog image', 'inboundwp-lite'),
	);

	$ibwp_spw_args = array(
		'labels'				=> $ibwp_spw_lables,
		'public'				=> false,
		'exclude_from_search'	=> false,
		'show_ui'				=> true,
		'show_in_menu'			=> true, 
		'query_var'				=> true,
		'rewrite'				=> false,
		'capability_type'		=> 'post',
		'has_archive'			=> true,
		'hierarchical'			=> false,
		'menu_position'			=> 8,
		'menu_icon'				=> 'dashicons-sos',
		'supports'				=> apply_filters( 'ibwp_spw_wheel_post_supports', array('title', 'author') ),
	);

	// Register Spin Wheel post type
	register_post_type( IBWP_SPW_POST_TYPE, apply_filters( 'ibwp_spw_post_type_spin_wheel', $ibwp_spw_args ) );
}

// Action to register plugin post type
add_action( 'init', 'ibwp_spw_wheel_post_type' );

/**
 * Function to update post message for spin wheel
 * 
 * @subpackage Spin Wheel
 * @since 1.0
 */
function ibwp_spw_post_updated_messages( $messages ) {

	global $post, $post_ID;

	$messages[IBWP_SPW_POST_TYPE] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Spin Wheel updated.', 'inboundwp-lite' ),
		2 => __( 'Custom field updated.', 'inboundwp-lite' ),
		3 => __( 'Custom field deleted.', 'inboundwp-lite' ),
		4 => __( 'Spin Wheel updated.', 'inboundwp-lite' ),
		5 => isset( $_GET['revision'] ) ? sprintf( __( 'Spin Wheel restored to revision from %s', 'inboundwp-lite' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __( 'Spin Wheel published.', 'inboundwp-lite' ),
		7 => __( 'Spin Wheel saved.', 'inboundwp-lite' ),
		8 => __( 'Spin Wheel submitted.', 'inboundwp-lite' ),
		9 => __( 'Spin Wheel scheduled for: <strong>%1$s</strong>.', 'inboundwp-lite' ),
		  date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ),
		10 => __( 'Spin Wheel draft updated.', 'inboundwp-lite' ),
	);

	return $messages;
}

// Filter to update spin-wheel post message
add_filter( 'post_updated_messages', 'ibwp_spw_post_updated_messages' );