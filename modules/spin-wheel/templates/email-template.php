<?php
/**
 * Email Template Html
 * 
 * @package InboundWP
 * @subpackage Spin Wheel
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>InboundWP Email Template</title>
</head>
<body>
<div style="background: #f5f5f5;">
	<h1 style="background: rgb(130,110,180); color: rgb(255, 255, 255); margin: 0; padding: 15px;"><?php echo $heading; ?></h1>
	<div style="padding: 15px; border: 1px solid rgb(130,110,180);"><?php echo $message; ?></div>
</div>
</body>
</html>