<?php
/**
 * Spin Wheel Html
 * 
 * @package InboundWP
 * @subpackage Spin Wheel
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="ibwp-spw-overlay"></div>
<div class="ibwp-spw-inboundwp-pro-main">
	<div class="ibwp-spw-content-wrap">		
		<div class="ibwp-spw-inboundwp-pro-left">
			<div class="ibwp-spw-wheel-spin">
				<canvas id="ibwp-spw-wheel"></canvas>
				<canvas id="ibwp-spw-wheel-1"></canvas>
				<canvas id="ibwp-spw-wheel-2"></canvas>
				<div class="ibwp-spw-wheel-container">
					<div class="ibwp-spw-pointer-before"></div>
					<div class="ibwp-spw-pointer-content">
						<i class="fa fa-map-marker ibwp-spw-location ibwp-spw-pointer"></i>
					</div>
				</div>
			</div>
		</div>
		<div class="ibwp-spw-inboundwp-pro-right">
			<div class="ibwp-spw-wheel-desc">
				<h2 class="ibwp-spw-wheel-title">
					<?php echo htmlspecialchars_decode($wheel_title); ?>
				</h2>
				<div class="ibwp-spw-wheel-short-desc">
					<?php echo wpautop($wheel_description); ?>
				</div>
			</div>			
				<div class="ibwp-spw-lucky-user">
					<?php if($name_field_show == '1') { ?>
						<label class="ibwp-spw-wheel-name">
							<input type="text" class="ibwp_spw_input_field ibwp-spw-field-name" name="ibwp_spw_player_name" placeholder="Please enter your name" id="ibwp_spw_player_name">
						</label>
					<?php } ?>
					<label class="ibwp-spw-wheel-email">
						<input type="email" class="ibwp_spw_input_field ibwp-spw-field-email" name="ibwp_spw_player_email" placeholder="Please enter your email" id="ibwp_spw_player_email">
					</label>
					<button class="ibwp-spw-spin-button" id="ibwp-spw-spin-button" data-refer-page="<?php echo ibwpl_get_current_page_url(); ?>"><?php echo $button_text; ?></button>
					<input type="hidden" class="ibwp-spw-form-submit-event">
					<?php if($enable_terms_conditions == '1') { ?>
						<div class="ibwp-spw-term-condition">
							<input type="checkbox" id="spw-terms-con"><span><?php echo $terms_conditions_desc; ?></span>
						</div>
					<?php } ?>
				</div>			
		</div>
	</div>
	<div class="ibwp-spw-close-wheel">
		<div class="ibwp-spw-close">X</div>
	</div>
	<div class="ibwp-spw-hide-after-spin">
		<span class="ibwp-spw-close">X</span>
	</div>	
</div>
<?php
if(!empty($id)) { ?>
	<div class="ibwp-spw-<?php echo $icon_position; ?>">
		<span class="ibwp-spw-icon-tooltip icon-show"><?php echo $icon_tooltip_text; ?></span>
		<canvas id="ibwp_spw_popup_canvas" class="ibwp_spw_wheel_icon icon-show ga_icon_click_<?php echo $id; ?> ibwp-spw-img" width="64" height="64"></canvas>
	</div>
<?php }