<?php
/**
 * Spin Wheel Module
 * 
 * @package InboundWP
 * @subpackage Spin Wheel
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $wpdb;

IBWP_Lite()->define( 'IBWP_SPW_DIR', dirname( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_SPW_PLUGIN_FILE', __FILE__ );
IBWP_Lite()->define( 'IBWP_SPW_URL', plugin_dir_url( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_SPW_POST_TYPE', 'ibwp_spw_spin_wheel' ); 	// Plugin post type
IBWP_Lite()->define( 'IBWP_SPW_META_PREFIX', '_ibwp_spw_' ); 		// Plugin meta prefix
IBWP_Lite()->define( 'IBWP_SPW_VERSION', '1.0' ); // Plugin version
IBWP_Lite()->define( 'IBWP_SPW_EMAIL_TABLE', $wpdb->base_prefix.'ibwp_spw_email_list' );

/**
 * Plugin Activation Function
 * Does the initial setup, sets the default values for the plugin options
 * 
 * @package Spin Wheel
 * @since 1.0
 */
function ibwp_spw_install(){

	ibwp_spw_wheel_post_type();

	// IMP need to flush rules for custom registered post type
	flush_rewrite_rules();

	// Get settings for the plugin
	$ibwp_spw_options = get_option( 'ibwp_spw_options' );

	if( empty( $ibwp_spw_options ) ) { // Check plugin version option

		// Set default settings
		ibwp_spw_default_settings();

		// Update plugin version to option
		update_option( 'ibwp_spw_plugin_version', '1.0' );
	}

	// Creating table to store email ids
	ibwp_spw_create_email_table();
}

add_action( 'ibwp_module_activation_hook_spin-wheel', 'ibwp_spw_install' );

//Taking some globals
global $ibwp_spw_options;

// Function File
require_once ( IBWP_SPW_DIR . '/includes/ibwp-spw-functions.php' );
$ibwp_spw_options = ibwpl_get_settings( 'ibwp_spw_options' );

// Plugin Post Type File
require_once( IBWP_SPW_DIR . '/includes/ibwp-spw-post-type.php' );

// Email Exports Function File
require_once( IBWP_SPW_DIR . '/includes/admin/tools/ibwp-spw-export-functions.php' );

// Script Class File
require_once ( IBWP_SPW_DIR . '/includes/class-ibwp-spw-script.php' );

// Admin Class File
require_once ( IBWP_SPW_DIR . '/includes/admin/class-ibwp-spw-admin.php' );

// Public Class
require_once ( IBWP_SPW_DIR . '/includes/class-ibwp-spw-public.php' );

// Email modal class
require_once ( IBWP_SPW_DIR . '/includes/admin/class-email-list-modal.php' );

// Load admin files
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
	require_once( IBWP_SPW_DIR . '/includes/admin/ibwp-spw-how-it-work.php' );
}