jQuery( document ).ready(function($) {
    
    // Initialize Select 2 JS
    $(".ibwp-select2").select2();
    
    // Initialize Color Picker JS
    $( '.ibwp-colorpicker' ).wpColorPicker();
        
    // Wheel Coupon Type JS
    $( document ).on('change', '.ibwp-spw-coupon-type', function(){

        // Destroy Select 2 JS
        $('.ibwp-select2').select2("destroy");
        $(".ibwp-spw-section-type").val($(".ibwp-spw-section-type option:first").val());
        $(".ibwp-spw-coupon-code").val($(".ibwp-spw-coupon-code option:first").val());
        $('.ibwp-select2').select2();

        $('.wheel-coupon-label').find('input').val('');
        $('.wheel-coupon-code').find('input').val('');
        $('.ibwp-spw-coupon-probability').val('');

    });
   
    // Coupon Code Dropdown based on Section Type
    $( document ).on('change', '.ibwp-spw-section-type', function(){

        var section_type        = $(this).find('option:selected').attr('value');
        var parent_div          = $(this).closest('.ibwp-tbl-row').attr('id');
        var spw_coupons_key     = $(this).closest('.ibwp-tbl-row').data('key');
        var coupon_type   = $('.ibwp-spw-coupon-type').find('option:selected').attr('value');
        
        if(section_type == 'coupons') {
            $('#' +parent_div).find('.ibwp-spw-coupons-msg').css('display','table-row');
            $('#' +parent_div).find('.ibwp-spw-coupons-lost-msg').css('display','none');
        } else {
            $('#' +parent_div).find('.ibwp-spw-coupons-msg').css('display','none');
            $('#' +parent_div).find('.ibwp-spw-coupons-lost-msg').css('display','table-row');
        }

        jQuery.post(ibwp_ajaxurl, {
            action              : 'ibwp_spw_section_type',
            section_type        : section_type,
            coupon_type         : coupon_type,
            spw_coupons_key     : spw_coupons_key,
        }, function(data) {
            var html_data = $('#'+parent_div).find('.wheel-coupon-code');
            html_data.html(data);
            $(".ibwp-select2").select2();
        });

    });
    
    // Initialize WP_Editor Function
    function ibwp_spw_wp_editor_init( field_ele = '' ) {
        if( field_ele != '' ) {
             wp.editor.initialize( field_ele, {
                    tinymce: {
                        wpautop     : true,
                        plugins     : 'charmap colorpicker compat3x directionality fullscreen hr image lists media paste tabfocus textcolor wordpress wpautoresize wpdialogs wpeditimage wpemoji wpgallery wplink wptextpattern wpview fullscreen', //dfw
                        toolbar1    : 'formatselect bold italic bullist numlist blockquote alignleft aligncenter alignright link unlink wp_more',
                        toolbar2    : 'strikethrough hr forecolor pastetext removeformat charmap outdent indent undo redo wp_help'
                    },
                    quicktags: {
                        buttons: 'strong,em,link,block,del,ins,img,ul,ol,li,code,more,close'
                    },
                    mediaButtons: false
            });
        }
    }

    // Wheel Page Redirect JS
    $( document ).on('click', '.ibwp-spw-page-redirect', function(){
        
        var parent      = $(this).closest('.ibwp-tbl-row');
        
        if( $(this).is(":checked") ){
            //var page_redirect       = parent.find('.ibwp-spw-page-redirect').val();
            parent.find('.ibwp-spw-page-redirect-url').show();
        } else {
            parent.find('.ibwp-spw-page-redirect-url').hide();
        }

    });

    // Change Coupon Probability Function
    function ibwp_spw_change_probability() {
        
        var arr = document.getElementsByClassName('ibwp-spw-coupon-probability');
        var tot=0;
        for(var i=0;i<arr.length;i++){
            if(parseInt(arr[i].value))
                tot += parseInt(arr[i].value);
        }
        document.getElementsByClassName('ibwp-spw-coupon-probability').value = tot;
        return tot;
    }
    
    // On Save Check Probability
    $('.post-type-ibwp_spw_spin_wheel #publish').on('click', function () {
        
        var empty = true;
        $('.ibwp-tbl-row').each(function( index ) {
            var coupon_part = $(this).find('.ibwp-spw-section-type').val();
            if( coupon_part == '' ) {
                empty = false;
            }
        });

        if( empty == false ) {
            alert(ibwpspwAdmin.coupon_type_err);
            return false;
        } 

        var tong    = ibwp_spw_change_probability();
        if( tong != 100 ) {

            alert(ibwpspwAdmin.probability_err);
            return false;
        }
    });
    
    // On click Call Email Export Ajax
    $(document).on('click', '.ibwp-export-emails-btn', function() {
        
        var current_obj = $(this);
        var cls_form    = current_obj.closest('form');
        var cls_ele     = current_obj.closest('.ibwp-spw-postbox');
        current_obj.attr('disabled', 'disabled');
        cls_form.find('.ibwp-spw-email-spinner').css('visibility', 'visible');
        cls_ele.find('.ibwp-progress-wrap').show();
        cls_ele.find('.ibwp-progress-wrap .ibwp-progress-strip').css('width', 0);
        ibwp_spw_export_emails( null, current_obj );

    });
});

// Generate CSV Function
function ibwp_spw_export_emails( data, obj ) {

    var cls_form    = obj.closest('form');
    var cls_ele     = obj.closest('.ibwp-spw-postbox');
    if( ! data ) {
        var data = {
                        action          : 'ibwp_spw_do_ajax_export',
                        form_data       : cls_form.serialize(),
                        ibwp_spw_action : 'export_emails',
                        page            : 1,
                    };
    }

    jQuery.post(ibwp_ajaxurl, data, function(response) {
        
        var result = jQuery.parseJSON(response);
        
        if( result.status == 0 ) {
            alert( result.msg );
            obj.removeAttr('disabled', 'disabled');
            cls_form.find('.ibwp-spw-spinner').css('visibility', 'hidden');
            cls_ele.find('.ibwp-progress-wrap').hide();
        } else {
            cls_ele.find('.ibwp-progress-wrap .ibwp-progress-strip').css('width', result.percentage+'%');
            
            /* If data is there then process again */
            if( result.data_process != 0 && (result.data_process < result.total_count) ) {
                data['page']            = result.page;
                data['total_count']     = result.total_count;
                data['data_process']    = result.data_process;
                ibwp_spw_export_emails( data, obj );
            }

            /* If process is done */
            if( result.data_process >= result.total_count ) {
                obj.removeAttr('disabled', 'disabled');
                cls_form.find('.ibwp-spw-spinner').css('visibility', 'hidden');
                cls_ele.find('.ibwp-progress-wrap').delay(500).hide(1);
                if( result.url ) {
                    window.location = result.url;
                }
            }
        }

    });
}