"use strict";
jQuery( document ).ready(function($) {

    // General Variable Define
    var wheel_id                    = ibwpspwPublic.id;
    var enable_spin_wheel           = ibwpspwPublic.enable_spin_wheel;
    var email_store                 = ibwpspwPublic.email_store;
    var spin_wheel_coupons          = ibwpspwPublic.spin_wheel_coupons;
    var email_subject               = ibwpspwPublic.email_subject;
    var email_heading               = ibwpspwPublic.email_heading;
    var email_message               = ibwpspwPublic.email_message;
    var win_message                 = ibwpspwPublic.win_message;
    var lost_message                = ibwpspwPublic.lost_message;
    var icon_position               = ibwpspwPublic.icon_position;
    var current_page_id             = ibwpspwPublic.current_page_id;
    var wheel_bg_color              = ibwpspwPublic.wheel_bg_color;
    var coupon_color                = ibwpspwPublic.coupon_color;
    var wheel_coupon_text_color     = '#ffffff';
    var coupon_code_count           = ibwpspwPublic.coupon_code_count;
    var coupon_lbl                  = ibwpspwPublic.coupon_lbl;
    var coupon_type                 = ibwpspwPublic.coupon_type;
    var enable_terms_conditions     = ibwpspwPublic.enable_terms_conditions;
    var spining_duration            = ibwpspwPublic.spining_duration;
    var get_url                     = ibwpspwPublic.get_url;
    var ibwp_spw_wheel_center_color = '#ffffff'
    var button_text                 = ibwpspwPublic.button_text;
    var initial_time                = 1;
    var ibwp_spw_wheel_border_color = '#f5f5f5';
    var ibwp_spw_wheel_dot_color    = '#000000';
    var ibwp_spw_auto_close         = 0;
    var coupons_degree              = 360 / coupon_code_count;
    var degree                      = -(coupons_degree / 2);
    var wheel_pointer_position      = 'center';

    // Canvas 1 Width & Hight Define
    var canvas          = document.getElementById('ibwp-spw-wheel');
    var canvas_text     = canvas.getContext('2d');
    var canvas_width;
    var wd_width, wd_height;
    var device_width    = screen.width;
    
    wd_width            = window.innerWidth;
    wd_height           = window.innerHeight;
    
    if (wd_width > wd_height) {
        canvas_width = wd_height;
    } else {
        canvas_width = wd_width;
    }
    
    if(device_width <= 767) {
        canvas.width = canvas_width * 0.70 + 20;
        canvas.height = canvas.width;
    } else if(device_width <= 1023) {
        canvas.width = canvas_width * 0.60 + 20;
        canvas.height = canvas.width;
    } else {
        canvas.width = canvas_width * 0.70 + 20;
        canvas.height = canvas.width;
    }
    
    var width   = canvas.width; //Canvas Width
    var height  = canvas.height; //Canvas Width
    var center  = (width) / 2; // center
    
    // Canvas 2 Width & Height Define
    canvas          = document.getElementById('ibwp-spw-wheel-1');
    canvas_text     = canvas.getContext('2d');
    canvas.width    = width;
    canvas.height   = canvas.width;

    // Canvas 3 Width & Height Define
    canvas          = document.getElementById('ibwp-spw-wheel-2');
    canvas_text     = canvas.getContext('2d');
    canvas.width    = width;
    canvas.height   = canvas.width;

    // Wheel Box Width Define
    $('.ibwp-spw-wheel-spin').css({'width': width + 'px', 'height': height + 'px'});
    
    var spw_wheel_text_size;
    
    if(device_width <= 767) {
        spw_wheel_text_size = parseInt(width / 24);
    } else {
        spw_wheel_text_size = parseInt(width / 27);
    }
    
    if(device_width <= 767) {

        $('.ibwp-spw-inboundwp-pro-right').css({'width': '100%', 'max-width': '100%'});
        $('.ibwp-spw-content-wrap .ibwp-spw-inboundwp-pro-left').css({'width': '100%','margin':0});
        $('.ibwp-spw-content-wrap .ibwp-spw-inboundwp-pro-left .ibwp-spw-wheel-spin').css({'margin': '0 auto'});
   
    } else if(device_width <= 1023) {
        
        jQuery('.ibwp-spw-inboundwp-pro-main').css({'width': '100%'});
        var col_right_width = jQuery('.ibwp-spw-inboundwp-pro-main').width();
        var right_div_width = col_right_width - width * 0.60 - 65;
        
        $('.ibwp-spw-inboundwp-pro-right').css({
            'width': right_div_width + 'px', 
            'max-width': right_div_width + 'px',
            'margin-left': parseInt(width / 10)+'px'
        });
        
        $('.ibwp-spw-content-wrap .ibwp-spw-inboundwp-pro-left').css({'margin-left': -canvas_width * 0.60 * 0.45 + 'px'});

    } else {

        var col_right_width = jQuery('.ibwp-spw-inboundwp-pro-main').width();

        if (wd_width > 1.8 * wd_height) {
            jQuery('.ibwp-spw-inboundwp-pro-main').css({'width': '60%'});
            right_div_width = col_right_width * 0.33;
        }
        $('.ibwp-spw-inboundwp-pro-right').css({
            'width': right_div_width + 'px', 
            'max-width': right_div_width + 'px',
            'margin-left': parseInt(width / 10)+'px'
        });
        
        $('.ibwp-spw-content-wrap .ibwp-spw-inboundwp-pro-left').css({'margin-left': -canvas_width * 0.65 * 0.45 + 'px'});

    }

    function degree2rad(degree) {
        return degree * Math.PI / 180;
    }

    // Draw Wheel Coupons Part Function
    function ibwp_spw_draw_coupons_part(degree, color) {

        canvas_text.beginPath();
        canvas_text.fillStyle = color;
        canvas_text.moveTo(center, center);
        var r;
        
        if (width <= 480) {
            r = width / 2 - 10;
        } else {
            r = width / 2 - 14;
        }

        canvas_text.arc(center, center, r, degree2rad(degree), degree2rad(degree + coupons_degree));
        canvas_text.lineTo(center, center);
        canvas_text.fill();
        canvas_text.strokeStyle = '#ffffff';
        canvas_text.lineWidth = 3;
        canvas_text.stroke();

    }

    // Wheel Center Circle Function
    function ibwp_spw_draw_point(deg, color) {

        var centerX = canvas.width / 2;
        var centerY = canvas.height / 2;
       
        if(device_width <= 767) {
            var radius = 30;
        } else if(device_width <= 1023) {
            var radius = 50;
        } else {
            var radius = 55;
        }

        canvas_text.beginPath();
        canvas_text.arc(centerX, centerY, radius, 0, 2 * Math.PI, false);
        canvas_text.fillStyle = color;
        canvas_text.fill();

    }

    // Wheel Border Draw Function
    function ibwp_spw_draw_wheel_border(borderC, dotC, lineW, dotR, des) {

        canvas_text.beginPath();
        canvas_text.strokeStyle = borderC;
        canvas_text.lineWidth = lineW;
        canvas_text.arc(center, center, center, 0, 2 * Math.PI);
        canvas_text.stroke();
        var x_val, y_val, degree;
        degree = coupons_degree / 2;
        var center_icon_pointer = center - des;
        
        for (var i = 0; i < coupon_code_count; i++) {
            canvas_text.beginPath();
            canvas_text.fillStyle = dotC;
            x_val = center + center_icon_pointer * Math.cos(degree * Math.PI / 180);
            y_val = center - center_icon_pointer * Math.sin(degree * Math.PI / 180);
            canvas_text.arc(x_val, y_val, dotR, 0, 2 * Math.PI);
            canvas_text.fill();
            degree += coupons_degree;
        }

    }

    // Draw Coupons Text Function
    function ibwp_spw_draw_coupon_lbl(degree, text,color) {

        canvas_text.save();
        canvas_text.translate(center, center);
        canvas_text.rotate(degree2rad(degree));
        canvas_text.textAlign   = "right";
        canvas_text.fillStyle   = color;
        canvas_text.font        = '700 ' + spw_wheel_text_size + 'px Arial';
        text = text.replace(/&#(\d{1,4});/g, function (fullStr, code) {
            return String.fromCharCode(code);
        });

        canvas_text.fillText(text, 4.5 * center / 6, spw_wheel_text_size / 2 - 2);
        canvas_text.restore();

    }

    // Trim Function
    function ibwp_spw_trim(x) {
        return x.replace(/^\s+|\s+$/gm, '');
    }

    // Overlay Click Event Function
    function ibwp_spw_overlay_function() {

        $('.ibwp-spw-overlay').on('click',function(){
            $('.ibwp-spw-overlay').css('display', 'none');
            $('html').removeClass('ibwp-spw-html');
            $('.ibwp-spw-img').removeClass('icon-hide');
            $('.ibwp-spw-img').addClass('icon-show');
            $('.ibwp-spw-icon-tooltip').removeClass('icon-hide');
            $('.ibwp-spw-icon-tooltip').addClass('icon-show');
            $('.ibwp-spw-inboundwp-pro-main').css({"margin-left":"-110%","opacity":"0"});
            
            var ibwp_spw_delay = setTimeout(function () {
                $('.ibwp-spw-inboundwp-pro-main').hide();
            }, 1000);
            
            clearTimeout(ibwp_spw_delay);

        });

    }

    // Spin Wheel Function
    function ibwp_spw_spins_wheel(stop_position, result) {

        var angle = 0;
        var wheel_stop = (360 - (coupons_degree * stop_position)) + 720 * spining_duration;
       
        var my_spin = setInterval(function () {

            $('#ibwp-spw-wheel').css({
                "-moz-transform": "rotate(" + angle + "deg)",
                "-webkit-transform": "rotate(" + angle + "deg)",
                "-o-transform": "rotate(" + angle + "deg)",
                "-ms-transform": "rotate(" + angle + "deg)"
            });

            $('#ibwp-spw-wheel-2').css({
                "-moz-transform": "rotate(" + angle + "deg)",
                "-webkit-transform": "rotate(" + angle + "deg)",
                "-o-transform": "rotate(" + angle + "deg)",
                "-ms-transform": "rotate(" + angle + "deg)"
            });

            if (angle < 360 * 2 || angle > (wheel_stop - (360 * 3))) {
                if (angle < 360 || angle > (wheel_stop - (360 * 2))) {
                    if (angle < 360 / 2 || angle > (wheel_stop - (360))) {
                        if (angle < 360 / 4 || angle > wheel_stop - (360 / 2)) {
                            if (angle > wheel_stop - (360 / 4)) {
                                if (angle > wheel_stop - (360 / 6)) {
                                    if (angle > wheel_stop - (360 / 8)) {
                                        angle += 0.5;
                                    } else {
                                        angle += 1;
                                    }
                                } else {
                                    angle += 1.5;
                                }
                            } else {
                                angle += 2;
                            }
                        } else {
                            angle += 2.5;
                        }
                    } else {
                        angle += 3;
                    }
                } else {
                    angle += 3.5;
                }
            } else {
                angle += 4;
            }

            if (angle >= wheel_stop) {

                $('#ibwp-spw-wheel').css({
                    "-moz-transform": "rotate(" + wheel_stop + "deg)",
                    "-webkit-transform": "rotate(" + wheel_stop + "deg)",
                    "-o-transform": "rotate(" + wheel_stop + "deg)",
                    "-ms-transform": "rotate(" + wheel_stop + "deg)"
                });
                
                $('#ibwp-spw-wheel-2').css({
                    "-moz-transform": "rotate(" + wheel_stop + "deg)",
                    "-webkit-transform": "rotate(" + wheel_stop + "deg)",
                    "-o-transform": "rotate(" + wheel_stop + "deg)",
                    "-ms-transform": "rotate(" + wheel_stop + "deg)"
                });
                
                $('.ibwp-spw-inboundwp-pro-right').html('<div class="ibwp-spw-frontend-result">' + result + '</div>');
                
                clearInterval(my_spin);

            }

        }, 0.01);

    }

    // Email Validation Function
    function ibwp_spw_isValidEmailAddress(emailAddress) {

        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
        return pattern.test(emailAddress);

    }

    // Check Email Function
    $('#ibwp_spw_player_email').keypress(function (e) {
        if (e.keyCode === 13) {
            $('#ibwp-spw-spin-button').click();
        }
    });
    function ibwp_spw_check_email() {

        // Form Submit Button Click Event
        $('#ibwp-spw-spin-button').on('click', function () {
            var current_obj         = $(this);
            var enter_email_msg     = ibwpspwPublic.enter_email_msg;
            var valid_email_msg     = ibwpspwPublic.valid_email_msg;
            var terms_condition_msg = ibwpspwPublic.terms_condition_msg;
            var refer_page          = $(this).attr('data-refer-page');

            current_obj.attr("disabled", "disabled");
            $('#ibwp_spw_player_email').attr("disabled", "disabled");
            current_obj.css('cursor','no-drop');

            $(this).unbind();
            $('.ibwp-spw-overlay').unbind();
            var check_mail_text = $(this).html();
                
                if ($('.ibwp-spw-field-email').val() != '') {

                    if (ibwp_spw_isValidEmailAddress($('.ibwp-spw-field-email').val())) {
                        
                        current_obj.empty();
                        current_obj.append('Please wait...');
                        
                        $('#ibwp-spw-error-email').html('');
                        $('.ibwp-spw-spin-button').addClass('ibwp-spw-adding');
                        
                        var ibwp_spw_email      = $('.ibwp-spw-field-email').val();
                        var ibwp_spw_name       = $('#ibwp_spw_player_name').val();

                        if( ($('#spw-terms-con').prop("checked") == true && enable_terms_conditions == 1) || (!$('#spw-terms-con').prop("checked") == true && enable_terms_conditions == 0)){

                            jQuery.post(ibwp_ajaxurl, {

                                action                  : 'ibwp_spw_get_email',
                                wheel_id                : wheel_id,
                                enable_spin_wheel       : enable_spin_wheel,
                                email_store             : email_store,
                                spining_duration        : spining_duration,
                                coupon_type             : coupon_type,
                                spin_wheel_coupons      : spin_wheel_coupons,
                                email_subject           : email_subject,
                                email_heading           : email_heading,
                                email_message           : email_message,
                                win_message             : win_message,
                                lost_message            : lost_message,
                                user_email              : ibwp_spw_email,
                                user_name               : ibwp_spw_name,
                                refer_page              : refer_page,

                            }, function(data) {
                                if (data.allow_spin === 'yes') {

                                    var result          = data.result_notification;
                                    var please_wait_msg = ibwpspwPublic.please_wait_msg;
                                    var email           = data.email;
                                    var coupon_code     = data.coupon_code;
                                    var expire_days     = data.expire_days;
                                    var email_data_id   = data.email_data_id;

                                    $('.wlwl-show-again-option').hide();
                                    $('.ibwp-spw-close-wheel').hide();
                                    $('.ibwp-spw-hide-after-spin').show();
                                    $('.ibwp-spw-form-submit-event').click();

                                    // Page Redirect Condition
                                    if(data.page_redirect_link != null) {
                                        ibwp_spw_spins_wheel(data.stop_position, '<div class="ibwp-spw-please-wait-msg">'+ please_wait_msg +'</div>');
                                        setTimeout(function(){
                                            window.location.replace(data.page_redirect_link);
                                        }, (data.spin_duration*1000) + 3000);
                                    } else {
                                        // Get Result Function
                                        ibwp_spw_spins_wheel(data.stop_position, result.replace('\"','"'));
                                    }

                                    ibwp_spw_overlay_function();

                                } else {

                                    $('.ibwp-spw-field-email').after('<span class="ibwp-spw-error-email">'+ data.allow_spin +'</span>');
                                    current_obj.empty();
                                    current_obj.append(button_text);
                                    $('.ibwp-spw-spin-button').removeClass('wlwl-adding');
                                
                                    setTimeout(function(){
                                        $('.ibwp-spw-error-email').remove();
                                        $('#ibwp_spw_player_email').removeAttr("disabled");
                                        current_obj.removeAttr("disabled");
                                        current_obj.css('cursor','pointer');

                                    }, 2000);

                                    ibwp_spw_check_email();
                                    ibwp_spw_overlay_function();

                                }

                            });
                        } else {
                            current_obj.empty();
                            current_obj.append(button_text);
                            $('.ibwp-spw-field-email').after('<span class="ibwp-spw-error-email">'+ terms_condition_msg +'</span>');
                            setTimeout(function(){
                                $('.ibwp-spw-error-email').remove();
                                current_obj.removeAttr("disabled");
                                $('#ibwp_spw_player_email').removeAttr("disabled");
                                current_obj.css('cursor','pointer');
                            }, 2000);

                            ibwp_spw_check_email();
                            ibwp_spw_overlay_function();
                        }
                        
                    } else {
                        
                        ibwp_spw_check_email();
                        ibwp_spw_overlay_function();

                        $('.ibwp-spw-field-email').after('<span class="ibwp-spw-error-email">'+ valid_email_msg +'</span>');
                        $('.ibwp-spw-field-email').focus();

                        setTimeout(function(){
                            $('.ibwp-spw-error-email').remove();
                            $('#ibwp_spw_player_email').removeAttr("disabled");
                            current_obj.removeAttr("disabled");
                            current_obj.css('cursor','pointer');
                        }, 2000);

                    }

                } else {

                    ibwp_spw_check_email();
                    ibwp_spw_overlay_function();

                    $('#ibwp-spw-error-email').css({'background-color':'#f2dede', 'border':'1px solid #ebccd1', 'padding':'7px', 'margin-top':'10px'});
                    $('.ibwp-spw-field-email').after('<span class="ibwp-spw-error-email">'+ enter_email_msg +'</span>');
                    $('.ibwp-spw-field-email').focus();

                    setTimeout(function(){
                        $('.ibwp-spw-error-email').remove();
                        $('#ibwp_spw_player_email').removeAttr("disabled");
                        current_obj.removeAttr("disabled");
                        current_obj.css('cursor','pointer');
                    }, 2000);

                }
        });

    }

    ibwp_spw_overlay_function();
    ibwp_spw_check_email();
    var center_icon_pointer = 32;

    // Wheel Icon Click Event
    $('.ibwp-spw-img').on('click',function(){

        $('.ibwp-spw-overlay').css('display', 'block');
        $('html').addClass('ibwp-spw-html');
        $(this).removeClass('icon-show');
        $(this).addClass('icon-hide');
        $('.ibwp-spw-icon-tooltip').removeClass('icon-show');
        $('.ibwp-spw-icon-tooltip').addClass('icon-hide');
        $('.ibwp-spw-inboundwp-pro-main').css({"margin-left":"0","opacity":"1"});

    });

    // Overlay Close Button Click Event
    $('.ibwp-spw-close').on('click',function(){

        $('.ibwp-spw-overlay').css('display', 'none');
        $('html').removeClass('ibwp-spw-html');
        $('.ibwp-spw-img').removeClass('icon-hide');
        $('.ibwp-spw-img').addClass('icon-show');
        $('.ibwp-spw-icon-tooltip').removeClass('icon-hide');
        $('.ibwp-spw-icon-tooltip').addClass('icon-show');
        $('.ibwp-spw-inboundwp-pro-main').css({"margin-left":"-110%","opacity":"0"});

    });

    // Wheel Draw For Loop
    for (var i = 0; i < coupon_code_count; i++) {
        ibwp_spw_draw_coupons_part(degree, coupon_color[i]);
        ibwp_spw_draw_coupon_lbl(degree + coupons_degree / 2, coupon_lbl[i],wheel_coupon_text_color);
        degree += coupons_degree;
    }

    ibwp_spw_draw_point(degree, ibwp_spw_wheel_center_color);

    if (width <= 480) {
        ibwp_spw_draw_wheel_border(ibwp_spw_wheel_border_color, 'rgba(0,0,0,0)', 20, 4, 5);
        ibwp_spw_draw_wheel_border('rgba(0,0,0,0)', ibwp_spw_wheel_dot_color, 20, 4, 5);
    } else {
        ibwp_spw_draw_wheel_border(ibwp_spw_wheel_border_color, 'rgba(0,0,0,0)', 30, 6, 7);
        ibwp_spw_draw_wheel_border('rgba(0,0,0,0)', ibwp_spw_wheel_dot_color, 30, 6, 7);
    }

    // Draw Wheel Icon Function
    function ibwp_spw_draw_popup_icon() {

        canvas = document.getElementById('ibwp_spw_popup_canvas');
        canvas_text = canvas.getContext('2d');

        for (var k = 0; k < coupon_code_count; k++) {
            ibwp_spw_draw_wheel_icon(degree, coupon_color[k]);
            degree += coupons_degree;
        }

        ibwp_spw_draw_wheel_icon_pointer(ibwp_spw_wheel_center_color);
        ibwp_spw_draw_wheel_icon_border(ibwp_spw_wheel_border_color, ibwp_spw_wheel_dot_color, 4, 1, 0);
   
    }
    
    // Call Draw Wheel Icon Function
    if(enable_spin_wheel == '1'){
        if(wheel_id != '') { 
            ibwp_spw_draw_popup_icon();
        }
    }

    // Draw Coupons Wheel Icon Function
    function ibwp_spw_draw_wheel_icon(degree, color) {
        
        canvas_text.beginPath();
        canvas_text.fillStyle = color;
        canvas_text.moveTo(center_icon_pointer, center_icon_pointer);
        canvas_text.arc(center_icon_pointer, center_icon_pointer, 32, degree2rad(degree), degree2rad(degree + coupons_degree));
        canvas_text.lineTo(center_icon_pointer, center_icon_pointer);
        canvas_text.fill();
        canvas_text.strokeStyle = '#ffffff';
        canvas_text.lineWidth = 1;
        canvas_text.stroke();

    }
    
    // Draw Coupons Wheel Icon Pointer Function
    function ibwp_spw_draw_wheel_icon_pointer(color) {
        
        canvas_text.save();
        canvas_text.beginPath();
        canvas_text.fillStyle = color;
        canvas_text.arc(center_icon_pointer, center_icon_pointer, 8, 0, 2 * Math.PI);
        canvas_text.fill();
        canvas_text.restore();

    }

    // Draw Coupons Wheel Border Function
    function ibwp_spw_draw_wheel_icon_border(borderC, dotC, lineW, dotR, des) {
       
        canvas_text.beginPath();
        canvas_text.strokeStyle = borderC;
        canvas_text.lineWidth = lineW;
        canvas_text.arc(center_icon_pointer, center_icon_pointer, center_icon_pointer, 0, 2 * Math.PI);
        canvas_text.stroke();

        var x_val, y_val, degree;
        degree = coupons_degree / 2;
        var center2 = center_icon_pointer - des;
        
        for (var i = 0; i < coupon_code_count; i++) {
            canvas_text.beginPath();
            canvas_text.fillStyle = dotC;
            x_val = center_icon_pointer + center2 * Math.cos(degree * Math.PI / 180);
            y_val = center_icon_pointer - center2 * Math.sin(degree * Math.PI / 180);
            canvas_text.arc(x_val, y_val, dotR, 0, 2 * Math.PI);
            canvas_text.fill();
            degree += coupons_degree;
        }
    }    
});