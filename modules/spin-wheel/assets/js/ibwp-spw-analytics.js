var scroll_events = (function ($) {
    
    "use strict";
    var click_elements  = [];
    var universal       = 0;
    var gtm             = 0;
    var gst             = 0;
    var ga_element;

    var track_event = function (category, action, label, universal, bounce, evalue ){
        var event_category  = !category         ? ''    : category;
        var event_action    = !action           ? ''    : action;
        var event_label     = !label            ? ''    : label;
        var event_bounce    = bounce === "true" ? true  : false;
        var event_value     = !evalue           ? false : evalue;
        
        if( typeof ga_element === "undefined" ){
           if( typeof ga !== 'undefined' ){
               ga_element = ga;
            } else if (!gtm && !gst){
               return;
            }
        }

        if (gtm) {
            dataLayer.push({
                'event': 'WPGAE',
                'eventCategory'     : category,
                'eventAction'       : action,
                'eventLabel'        : label,
                'eventValue'        : event_value,
                'nonInteraction'    : event_bounce
            });
        } else if (gst) {
            gtag('event', action, {
                // Event parameters
                'event_category'    : category,
                'event_label'       : label,
                'value'             : event_value,
                'non_interaction'   : event_bounce
            });
        } else if( universal ){
            if (event_value) {
                ga_element('send','event', category, action, label, event_value,{'nonInteraction': event_bounce});
            }else{
                ga_element('send','event', category, action, label, {'nonInteraction': event_bounce});
            }
        } else {
            ga_element.push( ['_trackEvent',category, action, label, event_value, event_bounce] );
        }

    };

    var click_event = function( event ){
        track_event(event.data.category, event.data.action, event.data.label, event.data.universal, event.data.bounce, event.data.evalue);
    };

    var unescapeChars = function (text) {
        var map = {
            '&amp;': '&',
            '&lt;': '<',
            '&gt;': '>',
            '&quot;': '"',
            '\"': '"',
            '&#039;': "'"
        };

        if (typeof text != 'string') {
            
            var cleanObj;
            // if param text is non-string (assuming JSON object), we convert it first to string
            cleanObj = JSON.stringify(text);
            // we replace other chars
            cleanObj.replace(/&lt;|&gt;|&quot;|'&#039;/gi, function (m) {
                return map[m];
            });
            // convert it back to JSON obj
            cleanObj = JSON.parse(cleanObj);
            return cleanObj;
        }else{
            return text.replace(/&lt;|&gt;|&quot;|'&#039;/gi, function (m) {
                return map[m];
            });
        }        
        return ''; //fallback
    };

    return {
        bind_events : function (settings) {
            click_elements  = settings.click_elements;
            universal       = settings.universal;
            gtm             = settings.gtm;
            gst             = settings.gst;
            var i;
            for (i = 0; i < click_elements.length; i++) {
                var clicked         = click_elements[i];
                var selector        = unescapeChars(clicked.select);
                clicked.universal   = universal;
                $('body').on('click', selector, clicked, click_event);
            }            
        }
    };
    
}(jQuery));