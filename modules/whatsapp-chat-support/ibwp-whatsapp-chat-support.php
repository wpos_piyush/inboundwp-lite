<?php
/**
 * WhatsApp Chat Support Module
 * 
 * @package InboundWP Lite
 * @subpackage WhatsApp Chat Support
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

IBWP_Lite()->define( 'IBWP_WTCS_DIR', dirname( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_WTCS_URL', plugin_dir_url( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_WTCS_POST_TYPE', 'ibwp_wtacs' );
IBWP_Lite()->define( 'IBWP_WTCS_META_PREFIX', '_ibwp_wtacs_' );
IBWP_Lite()->define( 'IBWP_WTCS_API', 'https://web.whatsapp.com/send' );


/**
 * Plugin Setup (On Activation)
 * 
 * Does the initial setup, set default values for the plugin options.
 *
 * @subpackage WhatsApp Chat Support
 * @since 1.0
 */
function ibwp_wtcs_install() {

    ibwp_wtcs_register_post_type();
    ibwp_wtcs_register_taxonomies();

    // IMP need to flush rules for custom registered post type
    flush_rewrite_rules();

   // Get settings for the plugin
    $ibwp_wtcs_options = get_option( 'ibwp_wtcs_options' );

    if( empty( $ibwp_wtcs_options ) ) { // Check plugin version option

        // Set default settings
        ibwp_wtcs_default_settings();

        // Update plugin version to option
        update_option( 'ibwp_wtcs_plugin_version', '1.0' );
    }

   
}
add_action( 'ibwp_module_activation_hook_wtcs', 'ibwp_wtcs_install' );

//Taking some globals
global $ibwp_wtcs_options;

// Function File
require_once( IBWP_WTCS_DIR . '/includes/ibwp-wtcs-functions.php' );

$ibwp_wtcs_options = ibwpl_get_settings( 'ibwp_wtcs_options' );

// Post Type File
require_once( IBWP_WTCS_DIR . '/includes/ibwp-wtcs-post-types.php' );

// Script Class File
require_once( IBWP_WTCS_DIR . '/includes/class-ibwp-wtcs-script.php' );

// public Class File
require_once( IBWP_WTCS_DIR . '/includes/class-ibwp-wtcs-public.php' );

// Admin Class File
require_once( IBWP_WTCS_DIR . '/includes/admin/class-ibwp-wtcs-admin.php' );
