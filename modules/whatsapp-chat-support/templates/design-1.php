 <?php 
/*
*Chatbox design template
*/
 ?>
 <div class="ibwp_wtcs_popup_content_item ">

     <span data-href="<?php echo $Wt_url; ?>" class="ibwp_wtcs_stt <?php echo $statusClass; ?> wtcs_open_on">
       <div class="ibwp_wtcs_popup_avatar">
          <div class="ibwp_wtcs_cs_img_wrap" style="background: url(<?php echo $featured_img_url; ?>) center center no-repeat; background-size: cover;"></div>
       </div>
       <div class="ibwp_wtcs_popup_txt">
          <?php if( !empty( $agent_name ) ){ ?>
          <div class="ibwp_wtcs_member_name"><?php echo $agent_name;?> </div>
          <?php } ?>

          <?php if( !empty( $designation ) ){ ?>
          <div class="ibwp_wtcs_member_duty"><?php echo $designation;?></div>
          
          <?php } ?>

          <?php if( !empty( $statusMsg ) ){ ?>
          <div class="ibwp_wtcs_member_status <?php echo $statustag;?>">
             <?php echo $statusMsg;?>                           
          </div>
        <?php } ?>
       </div>
    </span>

 </div>