(function($) {
    var wa_time_out, wa_time_in;
    $(document).ready(function() {

        
        $(".wtcs_open_on").on("click", function() {
            var url = $(this).attr('data-href');
            if(url!=''){
                window.open(url, '_blank');    
            }
        });


        $(".ibwp_wtcs_btn_popup").on("click", function() {
            if ($(".ibwp_wtcs_popup_chat_box").hasClass("ibwp_wtcs_active")) {
                $(".ibwp_wtcs_popup_chat_box").removeClass("ibwp_wtcs_active");
                $(".ibwp_wtcs_btn_popup").removeClass("ibwp_wtcs_active");
                clearTimeout(wa_time_in);
                if ($(".ibwp_wtcs_popup_chat_box").hasClass("ibwp_wtcs_lauch")) {
                    wa_time_out = setTimeout(function() {
                        $(".ibwp_wtcs_popup_chat_box").removeClass("ibwp_wtcs_pending");
                        $(".ibwp_wtcs_popup_chat_box").removeClass("ibwp_wtcs_lauch");
                    }, 400);
                }
            } else {
                $(".ibwp_wtcs_popup_chat_box").addClass("ibwp_wtcs_pending");
                $(".ibwp_wtcs_popup_chat_box").addClass("ibwp_wtcs_active");
                $(".ibwp_wtcs_btn_popup").addClass("ibwp_wtcs_active");
                clearTimeout(wa_time_out);
                if (!$(".ibwp_wtcs_popup_chat_box").hasClass("ibwp_wtcs_lauch")) {
                    wa_time_in = setTimeout(function() {
                        $(".ibwp_wtcs_popup_chat_box").addClass("ibwp_wtcs_lauch");
                    }, 100);
                }
            }
        });

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
            var expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(";");
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == " ") {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
        $("#nta-wa-gdpr").change(function() {
            if (this.checked) {
                setCookie("nta-wa-gdpr", "accept", 30);
                if (getCookie("nta-wa-gdpr") != "") {
                    $('.nta-wa-gdpr').hide(500);
                    $('.ibwp_wtcs_popup_content_item').each(function() {
                        $(this).removeClass('pointer-disable');
                        $('.ibwp_wtcs_popup_content_list').off('click');
                    })
                }
            }
        });
        if (getCookie("nta-wa-gdpr") != "") {
            $('.ibwp_wtcs_popup_content_list').off('click');
        } else {
            $('.ibwp_wtcs_popup_content_list').click(function() {
                $('.nta-wa-gdpr').delay(500).css({
                    "background": "red",
                    "color": "#fff"
                });
            });
        }
    });
})(jQuery);