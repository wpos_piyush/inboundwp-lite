jQuery( document ).ready(function( $ ) {
   
   $(document).on('change', '#ibwp-wtcs-status', function(){ 
            var vals = $(this).val();
            if( vals == 'Online' ){
                $('.Online').show();
                $('.Offline').hide();
            }else{
                $('.Online').hide();
                $('.Offline').show();
            }
   });


   $('.wtcs-color-box').wpColorPicker();

    
});

