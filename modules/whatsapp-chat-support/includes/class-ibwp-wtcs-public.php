<?php

/**
 * Public Class
 *
 * Handles the public side functionality of plugin
 *
 * @package WhatsApp Chat Support
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Ibwp_Wtcs_Public {

	function __construct() {
		
		// Action For chatbox add
		add_action( 'wp_footer', array( $this, 'ibwp_wtcs_add_chatbox' ) );
		
	}

	/**
	 * Function to add spin wheel to site
	 * 
	 * @package WhatsApp Chat Support
	 * @since 1.0 
	 **/
	function ibwp_wtcs_add_chatbox() {

		global $post;

		$prefix = IBWP_WTCS_META_PREFIX; // Metabox prefix
		
		$enable 				= ibwp_wtcs_get_option('enable');

		if( ! $enable ) {
        	return false;
        }

		$main_title 			= ibwp_wtcs_get_option('main_title');
		$sub_title 				= ibwp_wtcs_get_option('sub_title');
		$notice 				= ibwp_wtcs_get_option('notice');
		$toggle_text 			= ibwp_wtcs_get_option('toggle_text');

		$charbox_bg 			= ibwp_wtcs_get_option('charbox_bg');
		$charbox_header_bg 		= ibwp_wtcs_get_option('charbox_header_bg');
		$online_border_color 	= ibwp_wtcs_get_option('online_border_color');
		$offline_border_color 	= ibwp_wtcs_get_option('offline_border_color');
		$toggle_bg 				= ibwp_wtcs_get_option('toggle_bg');

	    $order 	 = 'DESC';    
	    $orderby = 'date';

	    $args = array (
			'post_type' 			=> IBWP_WTCS_POST_TYPE,
			'post_status'			=> array( 'publish' ),
			'order' 				=> $order,
			'orderby'				=> $orderby,
			'posts_per_page'		=> 1,
		);

		// WP Query
		$query = new WP_Query($args);
		
		$max_num_pages = $query->max_num_pages;

		// If post is there
		if ( $query->have_posts() ) { ?>

			<div class="ibwp_wtcs_btn_popup ">
	           <?php if( !empty( $toggle_text) ){ ?>	
			   <div class="ibwp_wtcs_btn_popup_txt"><?php echo $toggle_text; ?></div>
			   <?php } ?>
			   <div class="ibwp_wtcs_btn_popup_icon"></div>
			</div>
			<div class="ibwp_wtcs_popup_chat_box">
			   <div class="ibwp_wtcs_popup_heading">
			   	  
			   	  <?php if( !empty( $main_title ) ){ ?>
			      <div class="ibwp_wtcs_popup_title"> <?php echo $main_title; ?> </div>
			      <?php } ?>

			      <?php if( !empty( $sub_title ) ){ ?>
			      <div class="ibwp_wtcs_popup_intro">
			         <?php echo $sub_title; ?>
			      </div>
			  	  <?php } ?>
			   </div>
			   <!-- /.ibwp_wtcs_popup_heading -->
			   <div class="ibwp_wtcs_popup_content ibwp_wtcs_popup_content_left">
			   	  <?php if( !empty( $notice ) ){ ?>
			      <div class="ibwp_wtcs_popup_notice"><?php echo $notice; ?></div>
			      <?php } ?>
			      <div class="ibwp_wtcs_popup_content_list">

			        <?php

			        while ( $query->have_posts() ) : $query->the_post();

			        	$agent_name 			= get_post_meta( $post->ID, $prefix.'agent_name', true );
						$whatapp_number 		= get_post_meta( $post->ID, $prefix.'whatapp_number', true );
						$designation 			= get_post_meta( $post->ID, $prefix.'designation', true );
						$custom_message 		= get_post_meta( $post->ID, $prefix.'custom_message', true );
						$status 				= get_post_meta( $post->ID, $prefix.'status', true );
						$online_message 		= get_post_meta( $post->ID, $prefix.'online_message', true );
						$offline_message 		= get_post_meta( $post->ID, $prefix.'offline_message', true );

						if($status == 'Online'){

							$statusClass= 'ibwp_wtcs_stt_online';
							$statusMsg  = $online_message;
							$statustag  = 'on';
							$Wt_url 	= add_query_arg(
											            array(
											                'phone' => $whatapp_number,
											                'text'  => $custom_message
											            ),
											            IBWP_WTCS_API
				        							);
						} else {

							$statusClass	= 'ibwp_wtcs_stt_offline';
							$statusMsg  	= $offline_message;
							$statustag      = 'off';
							$Wt_url 		= "";
							

						}

						$featured_img_url = get_the_post_thumbnail_url( $post->ID,'thumbnail'); 
						$featured_img_url = !empty($featured_img_url) 	? $featured_img_url 	: IBWP_WTCS_URL.'assets/images/person-placeholder.png';

					// Include Design File
					include( IBWP_WTCS_DIR . "/templates/design-1.php" );

					endwhile;

					wp_reset_postdata(); // Reset WP Query
					
					?>
				</div>
	      <!-- /.ibwp_wtcs_popup_content_list -->
	   		</div>
	   <!-- /.ibwp_wtcs_popup_content -->
		</div>
<?php 	} } }

$ibwp_wtcs_public = new Ibwp_Wtcs_Public();