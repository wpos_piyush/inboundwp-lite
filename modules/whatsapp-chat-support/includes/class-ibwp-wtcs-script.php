<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package InboundWP Lite
 * @subpackage WhatsApp Chat Support
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Wtcs_Script {

	function __construct(){

		// Action to add style on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_wtcs_front_end_style') );

		// Action to add script on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_wtcs_front_end_script') );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_wtcs_admin_style') );
		
		// Action to add script at admin side
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_wtcs_admin_script') ); 

	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_wtcs_admin_style( $hook ) {
			wp_register_style( 'ibwp-wtcs-admin-style', IBWP_WTCS_URL.'assets/css/ibwp-wtcs-admin.css', array(), IBWPL_VERSION );
			wp_enqueue_style( 'ibwp-wtcs-admin-style');

			//color picker
			wp_enqueue_style( 'wp-color-picker' );

	}

	/**
	 * Function to add script at admin side
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_wtcs_admin_script( $hook ) {
		
			wp_register_script( 'ibwp-wtcs-admin-script', IBWP_WTCS_URL.'assets/js/ibwp-wtcs-admin.js', array('jquery','wp-util' ), IBWPL_VERSION, true );
			wp_localize_script( 'ibwp-wtcs-admin-script', 'ibwpwtcsAdmin', array(
				'ajaxurl'           		=> admin_url( 'admin-ajax.php' ),
				'error_msg' 				=> __('Sorry, Something Happened Wrong.', 'inboundwp-lite'),
	    	));
			wp_enqueue_script('ibwp-wtcs-admin-script');

			//color picker
			wp_enqueue_script( 'wp-color-picker' );


	}

	/**
	 * Function to add style at front side
	 * 
	 * @subpackage WhatsApp Chat Support
 	 * @since 1.0
 	 */
	function ibwp_wtcs_front_end_style() {

		wp_enqueue_style( 'ibwp-font-awesome' );

		// Registring testimonials style
		wp_register_style( 'ibwp-wtcs-public-style', IBWP_WTCS_URL.'assets/css/ibwp-wtcs-public.css', null, IBWPL_VERSION );
		wp_enqueue_style( 'ibwp-wtcs-public-style' );

		
	}

	/**
	 * Function to add script at front side
	 * 
	 * @subpackage WhatsApp Chat Support
	 * @since 1.0
	 */
	function ibwp_wtcs_front_end_script() {
			
		// Registring public script
		wp_register_script( 'ibwp-wtcs-public-script', IBWP_WTCS_URL.'assets/js/ibwp-wtcs-public.js', array('jquery'), IBWPL_VERSION, true );

		wp_localize_script( 'ibwp-wtcs-public-script', 
			'IBWP_WTCS', array(
							'is_mobile' 		=> (wp_is_mobile()) ? 	1 	: 0,
							'is_rtl' 			=> (is_rtl()) 		? 	1 	: 0,
		));
		wp_enqueue_script( 'ibwp-wtcs-public-script' );
		
	}
}

$ibwp_wtcs_script = new IBWP_Wtcs_Script();