<?php
/**
 * Register Post type functionality
 *
 * @package InboundWP Lite
 * @subpackage Agents
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Function to register post type
 * 
 * @subpackage Agents
 * @since 1.0
 */
function ibwp_wtcs_register_post_type () {

	$ibwp_wtcs_post_labels = apply_filters( 'ibwp_wtcs_post_labels', array(
							'name' 					=> __( 'Agents', 'inboundwp-lite' ),
							'singular_name' 		=> __( 'Agent', 'inboundwp-lite' ),
							'add_new' 				=> __( 'Add New', 'inboundwp-lite' ),
							'add_new_item' 			=> __( 'Add New Agent', 'inboundwp-lite' ),
							'edit_item' 			=> __( 'Edit Agent', 'inboundwp-lite' ),
							'new_item' 				=> __( 'New Agent', 'inboundwp-lite' ),
							'all_items' 			=> __( 'All Agents', 'inboundwp-lite' ),
							'view_item' 			=> __( 'View Agents', 'inboundwp-lite' ),
							'search_items' 			=> __( 'Search Agents', 'inboundwp-lite' ),
							'not_found' 			=> __( 'No Agents Found', 'inboundwp-lite' ),
							'not_found_in_trash'	=> __( 'No Agents Found in Trash', 'inboundwp-lite' ),
							'parent_item_colon' 	=> '',
							'featured_image'        => __( 'Profile Image', 'inboundwp-lite' ),
							'set_featured_image'    => __( 'Set Profile image', 'inboundwp-lite' ),
							'remove_featured_image' => __( 'Remove Profile image', 'inboundwp-lite' ),
							'use_featured_image'    => __( 'Use as profile image', 'inboundwp-lite' ),
							'insert_into_item'      => __( 'Insert into profile', 'inboundwp-lite' ),
							'uploaded_to_this_item' => __( 'Uploaded to this profile', 'inboundwp-lite' ),
							'menu_name' 			=> __( 'WhatsApp Chat Support - IBWP', 'inboundwp-lite' ),
						));

	$testimonial_args = array(
								'labels' 				=> $ibwp_wtcs_post_labels,
								'public' 				=> false,
								'show_ui' 				=> true,
								'show_in_menu' 			=> true,
								'query_var' 			=> false,
								'exclude_from_search'	=> false,
								'rewrite' 				=> false,
								'capability_type' 		=> 'post',
								'hierarchical' 			=> false,
								'supports' 				=> apply_filters('ibwp_wtcs_post_supports', array('title', 'author', 'thumbnail')),
								'menu_icon' 			=> 'dashicons-format-chat',
							);

	// Register testimonial post type
	register_post_type( IBWP_WTCS_POST_TYPE, apply_filters('ibwp_wtcs_post_type_args', $testimonial_args) );
}

// Action to register post type
add_action( 'init', 'ibwp_wtcs_register_post_type');


/**
 * Function to update post message for testimonial post type
 * 
 * @subpackage Agents
 * @since 1.0
 */
function ibwp_wtcs_post_updated_messages( $messages ) {

	global $post, $post_ID;

	$messages[IBWP_WTCS_POST_TYPE] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __( 'Agent updated. <a href="%s">View Agent</a>', 'inboundwp-lite' ), esc_url( get_permalink( $post_ID ) ) ),
		2 => __( 'Custom field updated.', 'inboundwp-lite' ),
		3 => __( 'Custom field deleted.', 'inboundwp-lite' ),
		4 => __( 'Agent updated.', 'inboundwp-lite' ),
		5 => isset( $_GET['revision'] ) ? sprintf( __( 'Agent restored to revision from %s', 'inboundwp-lite' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __( 'Agent published. <a href="%s">View Agent</a>', 'inboundwp-lite' ), esc_url( get_permalink( $post_ID ) ) ),
		7 => __( 'Agent saved.', 'inboundwp-lite' ),
		8 => sprintf( __( 'Agent submitted. <a target="_blank" href="%s">Preview Agent</a>', 'inboundwp-lite' ), esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
		9 => sprintf( __( 'Agent scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Agent</a>', 'inboundwp-lite' ),
		  date_i18n( 'M j, Y @ G:i', strtotime($post->post_date) ), esc_url(get_permalink($post_ID)) ),
		10 => sprintf( __( 'Agent draft updated. <a target="_blank" href="%s">Preview Agent</a>', 'inboundwp-lite' ), esc_url( add_query_arg( 'preview', 'true', get_permalink( $post_ID ) ) ) ),
	);

	return $messages;
}

// Filter to update testimonial post message
add_filter( 'post_updated_messages', 'ibwp_wtcs_post_updated_messages' );