<?php
/**
 * Functions File
 *
 * @package WhatsApp Chat Support
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Update default settings
 * 
 * @package WhatsApp Chat Support
 * @since 1.0
 */
function ibwp_wtcs_default_settings() {

	global $ibwp_wtcs_options;

	$ibwp_wtcs_options = array(
							'enable'				=> 0,
							'main_title'			=> __('Start a Conversation','inboundwp-lite'),
							'sub_title'				=> __('Hi! Click one of our members below to chat on WhatsApp','inboundwp-lite'),
							'notice'				=> __('The team typically replies in a few minutes.','inboundwp-lite'),
							'toggle_text'			=> __('Need Help? Chat with us','inboundwp-lite'),
							'charbox_bg'			=> '#ffffff',
							'charbox_header_bg'		=> '#2db742',
							'online_border_color'	=> '#2db742',
							'offline_border_color'	=> '#c0c5ca',
							'toggle_bg'				=> '#2db742', 	
						);

    $default_options = apply_filters('ibwp_wtcs_options_default_values', $ibwp_wtcs_options );

	// Update default options
	update_option( 'ibwp_wtcs_options', $default_options );

	// Overwrite global variable when option is update	
	$ibwp_wtcs_options = ibwpl_get_settings( 'ibwp_wtcs_options' );
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @package WhatsApp Chat Support
 * @since 1.0
 */
function ibwp_wtcs_get_option( $key = '', $default = false ) {
	global $ibwp_wtcs_options;
	$value 	= ! empty( $ibwp_wtcs_options[ $key ] ) ? $ibwp_wtcs_options[ $key ] : $default;
	return $value;
}