<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package InboundWP Lite
 * @subpackage WhatsApp Chat Support
 * @since 1.0
 */
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Wtcs_Admin {
	
	function __construct() {

		// Action to register plugin settings
		add_action ( 'admin_init', array($this, 'ibwp_wtcs_register_settings') );

		// Action to add metabox
		add_action( 'add_meta_boxes', array($this, 'ibwp_wtcs_add_agent_metabox') );

		// Action to save metabox
		add_action( 'save_post', array($this,'ibwp_wtcs_save_metabox_value') );

		// Action to add custom column to Testimonials listing
		add_filter( 'manage_posts_columns', array($this, 'ibwp_wtcs_posts_columns'), 10, 2 );

		// Action to add custom column data to Testimonials listing
		add_action('manage_'.IBWP_WTCS_POST_TYPE.'_posts_custom_column', array($this, 'ibwp_wtcs_post_columns_data'), 10, 2);

		// Action to add admin menu
		add_action( 'admin_menu', array($this, 'ibwp_wtcs_register_menu'), 12 );

	}

	/**
	 * Function to add menu
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_wtcs_register_menu() {
		// Register plugin Setting page
		add_submenu_page( 'edit.php?post_type='.IBWP_WTCS_POST_TYPE, __('Settings', 'inboundwp-lite'), __('Settings', 'inboundwp-lite'), 'manage_options', 'ibwp-wtacs-settings', array($this, 'ibwp_wtacs_settings_page') );
		
	}


	/**
	 * Getting Started Page Html
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_wtacs_settings_page() {		
		include_once( IBWP_WTCS_DIR . '/includes/admin/settings/ibwp-wtcs-settings.php' );
	}

	
	/**
	 * Function register setings
	 * 
	 * @subpackage WhatsApp Chat Support
 	 * @since 1.0
	 */

	function ibwp_wtcs_register_settings() {
		global $wpdb;

		// Reset default settings
		if( !empty( $_POST['ibwp_wtcs_reset_settings'] ) ) {
			ibwp_wtcs_default_settings();
		}

		register_setting( 'ibwp_wtcs_plugin_options', 'ibwp_wtcs_options', array($this, 'ibwp_wtcs_validate_options') );

		
	}

	/**
	 * Validate Settings Options
	 * 
	 * @subpackage WhatsApp Chat Support
 	 * @since 1.0
	 */
	function ibwp_wtcs_validate_options( $input ) {

		$input['enable'] 				= isset($input['enable']) 				? 1 											: 0;
		$input['main_title'] 			= !empty($input['main_title']) 			? ibwpl_clean($input['main_title'])				: __('Start a Conversation','inboundwp-lite');
		$input['sub_title'] 			= !empty($input['sub_title']) 			? ibwpl_clean($input['sub_title'])				: __('Hi! Click one of our members below to chat on WhatsApp','inboundwp-lite');
		$input['notice'] 				= !empty($input['notice']) 				? ibwpl_clean($input['notice'])					: __('The team typically replies in a few minutes.','inboundwp-lite');
		$input['toggle_text'] 			= !empty($input['toggle_text']) 		? ibwpl_clean($input['toggle_text'])				: __('Need Help? Chat with us','inboundwp-lite');
		$input['charbox_bg'] 			= isset($input['charbox_bg']) 			? ibwpl_clean($input['charbox_bg'])				: '#ffffff';
		$input['charbox_header_bg'] 	= isset($input['charbox_header_bg']) 	? ibwpl_clean($input['charbox_header_bg'])		: '#2db742';
		$input['online_border_color'] 	= isset($input['online_border_color']) 	? ibwpl_clean($input['online_border_color'])		: '#2db742';
		$input['offline_border_color'] 	= isset($input['offline_border_color']) ? ibwpl_clean($input['offline_border_color'])	: '#c0c5ca';
		$input['toggle_bg'] 			= isset($input['toggle_bg']) 			? ibwpl_clean($input['toggle_bg'])				: '#2db742';

		return $input;
	}

	/**
	 * Function to register metabox
	 * 
	 * @subpackage WhatsApp Chat Support
	 * @since 1.0
	 */
	function ibwp_wtcs_add_agent_metabox() {
		add_meta_box( 'wtcs-details', __( 'Agent Details - IBWP', 'inboundwp-lite' ), array($this, 'ibwp_wtcs_meta_box_content'), IBWP_WTCS_POST_TYPE, 'normal', 'high' );
	}

	/**
	 * Function to handle metabox content
	 * 
	 * @subpackage WhatsApp Chat Support
	 * @since 1.0
	 */
	function ibwp_wtcs_meta_box_content() {
		include_once( IBWP_WTCS_DIR .'/includes/admin/metabox/wtcs-post-sett.php');
	}

	/**
	 * Function to save metabox values
	 * 
	 * @subpackage WhatsApp Chat Support
	 * @since 1.0
	 */
	function ibwp_wtcs_save_metabox_value( $post_id ){

		global $post_type;
		
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                	// Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )  	// Check Revision
		|| ( $post_type !=  IBWP_WTCS_POST_TYPE ) )              					// Check if current post type is supported.
		{
			return $post_id;
		}

		$prefix = IBWP_WTCS_META_PREFIX; // Taking metabox prefix

		// Getting saved values
		$agent_name 			= isset($_POST[$prefix.'agent_name']) 		? ibwpl_clean( $_POST[$prefix.'agent_name'] ) 		: '';
		$whatapp_number 		= isset($_POST[$prefix.'whatapp_number'])	? ibwpl_clean( $_POST[$prefix.'whatapp_number'] ) 	: '';
		$designation 			= isset($_POST[$prefix.'designation']) 		? ibwpl_clean( $_POST[$prefix.'designation'] ) 		: '';
		$status 				= isset($_POST[$prefix.'status']) 			? ibwpl_clean( $_POST[$prefix.'status'] ) 			: '';
		$online_message 		= isset($_POST[$prefix.'online_message']) 	? ibwpl_clean( $_POST[$prefix.'online_message'] ) 	: '';
		$offline_message 		= isset($_POST[$prefix.'offline_message']) 	? ibwpl_clean( $_POST[$prefix.'offline_message'] ) 	: '';
		$custom_message 		= isset($_POST[$prefix.'custom_message'])   ? ibwpl_clean( $_POST[$prefix.'custom_message'] ) 	: '';
		
		update_post_meta($post_id, $prefix.'agent_name', $agent_name);
		update_post_meta($post_id, $prefix.'whatapp_number', $whatapp_number);
		update_post_meta($post_id, $prefix.'custom_message', $custom_message);
		update_post_meta($post_id, $prefix.'designation', $designation);
		update_post_meta($post_id, $prefix.'status', $status);
		update_post_meta($post_id, $prefix.'online_message', $online_message);
		update_post_meta($post_id, $prefix.'offline_message', $offline_message);
		
	}

	/**
	 * Add custom column to Testimonials listing page
	 * 
	 * @subpackage WhatsApp Chat Support
	 * @since 1.0
	 */
	function ibwp_wtcs_posts_columns( $columns, $post_type ) {

		if( $post_type == IBWP_WTCS_POST_TYPE ) {

			$new_columns['ibwp_wtcs_status'] 	= __('Status', 'inboundwp-lite');
			$new_columns['ibwp_wtcs_image'] 	= __('Profile Image', 'inboundwp-lite');
			

			$columns = ibwpl_add_array( $columns, $new_columns, 1, true);
		}
		return $columns;
	}

	/**
	 * Add custom column data to Testimonials listing page
	 * 
	 * @subpackage WhatsApp Chat Support
 	 * @since 1.0
	 */
	function ibwp_wtcs_post_columns_data( $column, $post_id ) {
		$prefix = IBWP_WTCS_META_PREFIX; // Metabox prefix
		if($column == 'ibwp_wtcs_image') {
			$value = ibwpl_get_post_featured_image($post_id, 40 ,'square');
			if($value != '') {
				echo '<img class="wp-post-image ibwp-wtcs-avatar-image" height="40" width="40" src="'.$value.'" alt="" />';
			}
		}

		if($column == 'ibwp_wtcs_status') {
			$status = get_post_meta( $post_id, $prefix.'status', true );
			if($status != '') {
				echo $status;
			}
		}
	}
}

$ibwp_wtcs_admin = new IBWP_Wtcs_Admin();