<?php
/**
 * Handles testimonial metabox HTML
 *
 * @package InboundWP Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $post;

$prefix = IBWP_WTCS_META_PREFIX; // Metabox prefix

// Getting saved values
$agent_name 				= get_post_meta( $post->ID, $prefix.'agent_name', true );
$whatapp_number 			= get_post_meta( $post->ID, $prefix.'whatapp_number', true );
$designation 				= get_post_meta( $post->ID, $prefix.'designation', true );
$custom_message 			= get_post_meta( $post->ID, $prefix.'custom_message', true );
$status 					= get_post_meta( $post->ID, $prefix.'status', true );
$online_message 			= get_post_meta( $post->ID, $prefix.'online_message', true );
$offline_message 			= get_post_meta( $post->ID, $prefix.'offline_message', true );
?>

<table class="form-table ibwp-tstmnl-table">
	<tbody>
		
		<tr valign="top">
			<th scope="row">
				<label for="ibwp-agent-name"><?php _e('Agent Name', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($agent_name); ?>" class="large-text" id="ibwp-agent-name" name="<?php echo $prefix; ?>agent_name" /> <br>
				<span class="description"><?php _e('Eg:- John Deo','inboundwp-lite')?></span>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-whatapp-number"><?php _e('WhatsApp Number', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($whatapp_number); ?>" class="large-text" id="ibwp-whatapp-number" name="<?php echo $prefix; ?>whatapp_number" /><br>
				<span class="description"><?php _e('Enter WhatsApp number with country code Eg:- +529999999999','inboundwp-lite')?></span>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-job-title"><?php _e('Designation', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($designation); ?>" class="large-text" id="ibwp-job-title" name="<?php echo $prefix; ?>designation" /><br>
				<span class="description"><?php _e('Enter designation Eg:- Customer Support','inboundwp-lite')?></span>
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-wtcs-status"><?php _e('Status', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>status" class="ibwp-status regular-text" id="ibwp-wtcs-status">
					<option value="Online" <?php selected( $status, 'Online' ); ?> ><?php 	_e('Online', 'inboundwp-lite'); ?></option>
					<option value="Offline" <?php selected( $status, 'Offline' ); ?> ><?php 	_e('Offline', 'inboundwp-lite'); ?></option>
				</select><br/>
				<span class="description"><?php _e( 'Select Status.', 'inboundwp-lite' ); ?></span>
			</td>
		</tr>

		<tr valign="top" style="<?php echo ($status=='Online' || $status == '')?'display: table-row;':'display:none'; ?>" class="Online">
			<th scope="row">
				<label for="ibwp-online-message"><?php _e('Online Message', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($online_message); ?>" class="large-text" id="ibwp-online-message" name="<?php echo $prefix; ?>online_message" /><br>
				<span class="description"><?php _e( 'E.g:- I am Online', 'inboundwp-lite' ); ?></span>
				
			</td>
		</tr>

		<tr valign="top" style="<?php echo ($status=='Offline')?'display: table-row;':'display: none;'; ?>" class="Offline">
			<th scope="row">
				<label for="ibwp-offline-message"><?php _e('Offline Message', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($offline_message); ?>" class="large-text" id="ibwp-offline-message" name="<?php echo $prefix; ?>offline_message" /><br>
				<span class="description"><?php _e( 'E.g:- I am not available today', 'inboundwp-lite' ); ?></span>
			</td>
		</tr>


		<tr valign="top">
			<th scope="row">
				<label for="ibwp-custom-message"><?php _e('Custom Message', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<textarea class="large-text" id="ibwp-custom-message" name="<?php echo $prefix; ?>custom_message"><?php echo ibwpl_esc_attr($custom_message); ?></textarea><br>
				<span class="description"><?php _e( 'Enter WhatsApp predefined message E.g:- Hello, I have visit  and I need help from you.', 'inboundwp-lite' ); ?></span>
			</td>
		</tr>

	</tbody>
</table><!-- end .ibwp-tstmnl-table -->