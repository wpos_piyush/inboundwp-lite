<?php
/**
 * General Settings Page
 *
 * @package WhatsApp Chat Support
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;



?>

<div class="wrap ibwp-sett-wrap ibwp-wtcs-settings">
	<h2><?php _e( 'Chatbox Settings', 'inboundwp-lite' ); ?></h2>

	<?php
	// Reset message
	if( !empty( $_POST['ibwp_wtcs_reset_settings'] ) ) {
		ibwpl_display_message( 'reset' );
	}

	// Success message
	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {
		ibwpl_display_message( 'update' );
	}
	?>

	<!-- Plugin reset settings form -->
	<form action="" method="post" id="ibwp-spw-reset-sett-form" class="ibwp-right ibwp-spw-reset-sett-form">
		<input type="submit" class="button button-primary right ibwp-btn ibwp-reset-sett ibwp-resett-sett-btn ibwp-spw-reset-sett" name="ibwp_wtcs_reset_settings" id="ibwp-spw-reset-sett" value="<?php _e( 'Reset All Settings', 'inboundwp-lite' ); ?>" />
	</form>

	<form action="options.php" method="POST" id="ibwp-wtcs-settings-form" class="ibwp-wtcs-settings-form">
	
		<?php
			settings_fields( 'ibwp_wtcs_plugin_options' );
		?>
		<div id="ibwp-wtcs-mc-sett" class="post-box-container ibwp-wtcs-mc-sett">

			<div class="textright ibwp-clearfix">
				<input type="submit" name="ibwp_wtcs_sett_submit" class="button button-primary right ibwp-btn ibwp-spin-wheel-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
			</div>
			<div class="metabox-holder">
				<!-- Start Settings HTML -->
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">
						<h3 class="hndle">
							<span><?php _e( 'General Settings', 'inboundwp-lite' ); ?></span>
						</h3>
						<div class="inside">
							<table class="form-table ibwp-wtcs-mc-sett-tbl">
								<tbody>
									<tr>
										<th colspan="2">
											<span class="ibwp-pro-notice ">
												<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use multiple agents with custom chatbox design options. Right now, Only one latest agent will be display.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>	
											</span>
										</th>
									</tr>
									<tr>
										<th>
											<label for="ibwp-wtcs-enable"><?php _e('Enable','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="checkbox" name="ibwp_wtcs_options[enable]" id="ibwp-wtcs-enable" value="1" <?php checked( ibwp_wtcs_get_option('enable'), 1 ); ?>><br>
											<span class="description"><?php _e('Check this checkbox if you want to enable chatbox on front end.','inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr>
										<th>
											<label for="ibwp-wtcs-main-title"><?php _e('Main Title','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="text" class="large-text" name="ibwp_wtcs_options[main_title]" id="ibwp-wtcs-main-title" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('main_title')); ?>" ><br>
											<span class="description"><?php _e('Enter chatbox heading main title E.g:- Start a Conversation','inboundwp-lite'); ?></span>
										</td>
									</tr>


									<tr>
										<th>
											<label for="ibwp-wtcs-sub-title"><?php _e('Sub Title','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="text" class="large-text" name="ibwp_wtcs_options[sub_title]" id="ibwp-wtcs-sub-title" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('sub_title')); ?>" ><br>
											<span class="description"><?php _e('Enter chatbox heading sub title E.g:- Hi! Click one of our members below to chat on WhatsApp.','inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr>
										<th>
											<label for="ibwp-wtcs-notice"><?php _e('Notice Message','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="text" class="large-text" name="ibwp_wtcs_options[notice]" id="ibwp-wtcs-notice" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('notice')); ?>" ><br>
											<span class="description"><?php _e('Enter chatbox notice message E.g:- The team typically replies in a few minutes.','inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr>
										<th>
											<label for="ibwp-wtcs-toggle-text"><?php _e('Toggle Button Text','inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="text" class="large-text" name="ibwp_wtcs_options[toggle_text]" id="ibwp-wtcs-toggle-text" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('toggle_text')); ?>" ><br>
											<span class="description"><?php _e('Enter chatbox toggle button text E.g:- Need Help? Chat with us.','inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr class="ibwp-pro-disable">
										<th>
											<label for="ibwp-wtcs-charbox-bg"><?php _e('Chatbox Background Color', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
										</th>
										<td>
											<input type="text" class="wtcs-color-box" name="ibwp_wtcs_options[charbox_bg]" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('charbox_bg')); ?>" id="ibwp-wtcs-charbox-bg">
											<span class="description"><?php _e('Select Background color for chatbox.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr class="ibwp-pro-disable">
										<th>
											<label for="ibwp-wtcs-charbox-header-bg"><?php _e('Header Background Color', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
										</th>
										<td>
											<input type="text" class="wtcs-color-box" name="ibwp_wtcs_options[charbox_header_bg]" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('charbox_header_bg')); ?>" id="ibwp-wtcs-charbox-header-bg">
											<span class="description"><?php _e('Select Background color for chatbox header.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr class="ibwp-pro-disable">
										<th>
											<label for="ibwp-wtcs-online-border-color"><?php _e('Online Agent Border Color', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
										</th>
										<td>
											<input type="text" class="wtcs-color-box" name="ibwp_wtcs_options[online_border_color]" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('online_border_color')); ?>" id="ibwp-wtcs-online-border-color">
											<span class="description"><?php _e('Select color for online agent list left side border Color.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr class="ibwp-pro-disable">
										<th>
											<label for="ibwp-wtcs-offline-border-color"><?php _e('Offline Agent Border Color', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
										</th>
										<td>
											<input type="text" class="wtcs-color-box" name="ibwp_wtcs_options[offline_border_color]" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('offline_border_color')); ?>" id="ibwp-wtcs-offline-border-color">
											<span class="description"><?php _e('Select color for online agent list left side border Color.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>

									<tr class="ibwp-pro-disable">
										<th>
											<label for="ibwp-wtcs-toggle-bg"><?php _e('Toggle Button Background Color', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
										</th>
										<td>
											<input type="text" class="wtcs-color-box" name="ibwp_wtcs_options[toggle_bg]" value="<?php echo ibwpl_esc_attr(ibwp_wtcs_get_option('toggle_bg')); ?>" id="ibwp-wtcs-toggle-bg">
											<span class="description"><?php _e('Select toggle button background color.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End Settings HTML -->
				<div class="textright ibwp-clearfix">
					<input type="submit" name="ibwp_wtcs_sett_submit" class="button button-primary right ibwp-btn ibwp-spin-wheel-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
				</div>
				<!-- End Wheel display Settings HTML -->
			</div>
		</div>
	</form>
</div>