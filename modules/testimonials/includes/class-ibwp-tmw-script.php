<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package InboundWP Lite
 * @subpackage Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Tmw_Script {

	function __construct(){

		// Action to add style on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_tmw_front_end_style') );

		// Action to add script on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_tmw_front_end_script') );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_tmw_admin_style') );
		
		// Action to add script at admin side
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_tmw_admin_script') ); 

	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_admin_style( $hook ) {

		$pages_array = array(	
						IBWP_TMW_POST_TYPE . '_page_ibwp-tmw-settings',
						IBWP_TMW_POST_TYPE . '_page_ibwp-shrt-mapper',
					);
		if( in_array($hook, $pages_array) ) {
			
			wp_register_style( 'ibwp-tmw-admin-style', IBWP_TMW_URL.'assets/css/ibwp-tmw-admin.css', array(), IBWPL_VERSION );
			wp_enqueue_style( 'ibwp-tmw-admin-style');
		}
	
	}

	/**
	 * Function to add script at admin side
	 * 
	 * @package Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_admin_script( $hook ) {

		$sett_page = IBWP_TMW_POST_TYPE . '_page_ibwp-tmw-settings';
		if( $sett_page == $hook ) {
			wp_register_script( 'ibwp-tmw-admin-script', IBWP_TMW_URL.'assets/js/ibwp-tmw-admin.js', array('jquery','wp-util' ), IBWPL_VERSION, true );
			wp_localize_script( 'ibwp-tmw-admin-script', 'ibwptmwAdmin', array(
				'ajaxurl'           		=> admin_url( 'admin-ajax.php' ),
				'error_msg' 				=> __('Sorry, Something Happened Wrong.', 'inboundwp-lite'),
	    	));
			wp_enqueue_script('ibwp-tmw-admin-script');
		}
	}

	/**
	 * Function to add style at front side
	 * 
	 * @subpackage Testimonials
 	 * @since 1.0
 	 */
	function ibwp_tmw_front_end_style() {

		wp_enqueue_style( 'ibwp-font-awesome' );

		// Registring testimonials style
		wp_register_style( 'ibwp-tmw-public-style', IBWP_TMW_URL.'assets/css/ibwp-tmw-public.css', null, IBWPL_VERSION );
		wp_enqueue_style( 'ibwp-tmw-public-style' );

		wp_enqueue_style( 'ibwp-slick-style' );
	}

	/**
	 * Function to add script at front side
	 * 
	 * @subpackage Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_front_end_script() {
			
		// Registring public script
		wp_register_script( 'ibwp-tmw-public-script', IBWP_TMW_URL.'assets/js/ibwp-tmw-public.js', array('jquery'), IBWPL_VERSION, true );

		wp_localize_script( 'ibwp-tmw-public-script', 
			'IBWP_TMW', array(
							'is_mobile' 		=> (wp_is_mobile()) ? 	1 	: 0,
							'is_rtl' 			=> (is_rtl()) 		? 	1 	: 0,
			));
	}
}

$ibwp_tmw_script = new IBWP_Tmw_Script();