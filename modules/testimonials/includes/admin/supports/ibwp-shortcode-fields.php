<?php
/**
 * Shortcode Fields for Shortcode Preview
 *
 * @package Testimonials
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Generate 'sp_testimonials' shortcode fields for preview
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_grid_shortcode_fields( $shortcode ) {

	$fields = array(
			// General fields
			'general' => array(
					'title'		=> __('General Parameters', 'inboundwp-lite'),
					'params'	=> array(
									// General settings
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Design', 'inboundwp-lite' ),
										'name' 		=> 'design',
										'value' 	=> array(
															'design-1'	=> __('Grid Design 1', 'inboundwp-lite'),
															'design-2'	=> __('Grid Design 2', 'inboundwp-lite'),
															'design-3'	=> __('Grid Design 3', 'inboundwp-lite'),
															'design-4'	=> __('Grid Design 4', 'inboundwp-lite'),
														),
										'desc' 		=> __( 'Choose design.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Grid', 'inboundwp-lite' ),
										'name' 		=> 'per_row',
										'value' 	=> array(
															1	=> __( 'Grid 1', 'inboundwp-lite' ),
															2	=> __( 'Grid 2', 'inboundwp-lite' ),
															3	=> __( 'Grid 3', 'inboundwp-lite' ),
															4	=> __( 'Grid 4', 'inboundwp-lite' ),
															6	=> __( 'Grid 6', 'inboundwp-lite' ),
														),
										'default'	=> 3,
										'desc' 		=> __( 'Enter number to be displayed testimonial per row.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Show Title', 'inboundwp-lite' ),
										'name' 		=> 'show_title',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display testimonial title.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Client Name', 'inboundwp-lite' ),
										'name' 		=> 'display_client',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display client name.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Job Name', 'inboundwp-lite' ),
										'name' 		=> 'display_job',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display job name.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Company Name', 'inboundwp-lite' ),
										'name' 		=> 'display_company',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display company name.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Show Ratings', 'inboundwp-lite' ),
										'name' 		=> 'show_rating',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display ratings.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Avatar Image', 'inboundwp-lite' ),
										'name' 		=> 'display_avatar',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display avatar image.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Image Style', 'inboundwp-lite' ),
										'name' 		=> 'image_style',
										'value' 	=> array(
															'circle'	=> __( 'Circle', 'inboundwp-lite' ),
															'square'	=> __( 'Square', 'inboundwp-lite' ),
														),
										'dependency'=> array(
															'element' 	=> 'display_avatar',
															'value' 	=> array( 'true' ),
														),
										'desc' 		=> __( 'Choose image style.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Content', 'inboundwp-lite' ),
										'name' 		=> 'show_content',
										'value' 	=> array(
															'true' 	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
											),
										'desc' 		=> __( 'Display team member content.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'number',
										'heading'	=> __( 'Content Words limit', 'inboundwp-lite' ),
										'name' 		=> 'words_limit',
										'value' 	=> 30,
										'min'		=> 1,
										'desc' 		=> __( 'Enter words limit for team member content.', 'inboundwp-lite' ),
										'dependency'=> array(
														'element' 	=> 'show_content',
														'value' 	=> array( 'true' ),
														),
									),
									array(
										'type' 			=> 'text',
										'heading' 		=> __( 'Extra Class', 'inboundwp-lite' ),
										'name' 			=> 'extra_class',
										'value' 		=> '',
										'refresh_time'	=> 1000,
										'desc' 			=> __( 'Enter extra CSS class for design customization.', 'inboundwp-lite' ) . '<label title="'.__('Note: Extra class added as parent so using extra class you customize your design.', 'inboundwp-lite').'"> [?]</label>',
									),
								),
			),
			// Query Fields
			'query' => array(
					'title'		=> __('Query Parameters', 'inboundwp-lite'),
					'params'	=> array(
									// Query Settings
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Total Items', 'inboundwp-lite' ),
										'name' 		=> 'limit',
										'value' 	=> 20,
										'min'		=> -1,
										'desc' 		=> __( 'Enter number to be displayed. Enter -1 to display all.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Post Order By', 'inboundwp-lite' ),
										'name' 		=> 'orderby',
										'value' 	=> array(
															'date'		=> __( 'Post Date', 'inboundwp-lite' ),
															'ID'		=> __( 'Post ID', 'inboundwp-lite' ),
															'author'	=> __( 'Post Author', 'inboundwp-lite' ),
															'title'		=> __( 'Post Title', 'inboundwp-lite' ),
															'modified'	=> __( 'Post Modified Date', 'inboundwp-lite' ),
															'rand'		=> __( 'Random', 'inboundwp-lite' ),
															'menu_order'=> __( 'Menu Order', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Select order type.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Post Order', 'inboundwp-lite' ),
										'name' 		=> 'order',
										'value' 	=> array(
															'desc'	=> __( 'Descending', 'inboundwp-lite' ),
															'asc'	=> __( 'Ascending', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Select sorting order.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Display Specific Category', 'inboundwp-lite' ),
										'name' 		=> 'category',
										'value' 	=> '',
										'desc' 		=> __( 'Enter category id to display categories wise.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant category listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Include Child Category', 'inboundwp-lite' ),
										'name' 		=> 'include_cat_child',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'If you are using parent category then whether to display child category or not.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Exclude Category', 'inboundwp-lite' ),
										'name' 		=> 'exclude_cat',
										'value' 	=> '',
										'desc' 		=> __( 'Exclude post category. Works only if `Category` field is empty.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant category listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Display Specific Post', 'inboundwp-lite' ),
										'name' 		=> 'posts',
										'value' 	=> '',
										'desc' 		=> __('Enter id of the post which you want to display.', 'inboundwp-lite') . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant post listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Exclude Post', 'inboundwp-lite' ),
										'name' 		=> 'exclude_post',
										'value' 	=> '',
										'desc' 		=> __('Enter id of the post which you do not want to display.', 'inboundwp-lite') . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant post listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Include Author', 'inboundwp-lite' ),
										'name' 		=> 'include_author',
										'value' 	=> '',
										'desc' 		=> __( 'Enter author id to display posts of particular author.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at users listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Exclude Author', 'inboundwp-lite' ),
										'name' 		=> 'exclude_author',
										'value' 	=> '',
										'desc' 		=> __( 'Enter author id to hide post of particular author. Works only if `Include Author` field is empty.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant users listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Pagination', 'inboundwp-lite' ),
										'name' 		=> 'pagination',
										'value' 	=> 	array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),	
										'dependency' => 	array(
															'element' 				=> 'limit',
															'value_not_equal_to' 	=> '-1',
														),
										'desc' 		=> __( 'Display pagination.', 'inboundwp-lite'),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Pagination Type', 'inboundwp-lite' ),
										'name' 		=> 'pagination_type',
										'value' 	=> 	array(
															'numeric'	=> __( 'Numeric', 'inboundwp-lite' ),
															'prev-next'	=> __( 'Next - Prev', 'inboundwp-lite' ),
														),
										'dependency' => array(
															'element' 				=> 'pagination',
															'value_not_equal_to' 	=> array( 'false' ),
														),
										'desc' 		=> __( 'Choose pagination type.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Query Offset', 'inboundwp-lite' ),
										'name' 		=> 'query_offset',
										'value' 	=> '',
										'desc' 		=> __( 'Exclude number of posts from starting.', 'inboundwp-lite' ) . '<label title="'.__('e.g if you pass 5 then it will skip first five post. Note: Do not use limit=-1 and pagination=true with this.', 'inboundwp-lite').'"> [?]</label>',
									),
								)
			),
	);
	return $fields;
}
/**
 * Generate 'sp_testimonials_slider' shortcode fields for preview
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_slider_shortcode_fields( $shortcode ) {

	$fields = array(
			// General fields
			'general' => array(
					'title'		=> __('General Parameters', 'inboundwp-lite'),
					'params'	=>  array(
									// General settings
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Design', 'inboundwp-lite' ),
										'name' 		=> 'design',
										'value' 	=> array(
															'design-1'	=> __('Slider Design 1', 'inboundwp-lite'),
															'design-2'	=> __('Slider Design 2', 'inboundwp-lite'),
															'design-3'	=> __('Slider Design 3', 'inboundwp-lite'),
															'design-4'	=> __('Slider Design 4', 'inboundwp-lite'),
															),
										'desc' 		=> __( 'Choose slider design.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Show Title', 'inboundwp-lite' ),
										'name' 		=> 'show_title',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display testimonial title.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Client Name', 'inboundwp-lite' ),
										'name' 		=> 'display_client',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display client name.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Job Name', 'inboundwp-lite' ),
										'name' 		=> 'display_job',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display job name.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Company Name', 'inboundwp-lite' ),
										'name' 		=> 'display_company',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display company name.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Show Ratings', 'inboundwp-lite' ),
										'name' 		=> 'show_rating',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ) ,
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display ratings.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Avatar Image', 'inboundwp-lite' ),
										'name' 		=> 'display_avatar',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Display avatar image.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Display Image Style', 'inboundwp-lite' ),
										'name' 		=> 'image_style',
										'value' 	=> array(
															'circle'	=> __( 'Circle', 'inboundwp-lite' ),
															'square'	=> __( 'Square', 'inboundwp-lite' ),
														),
										'dependency'=> array(
															'element' 	=> 'display_avatar',
															'value' 	=> array( 'true' ),
														),
										'desc' 		=> __( 'Choose image style.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Content', 'inboundwp-lite' ),
										'name' 		=> 'show_content',
										'value' 	=> array(
															'true' 	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
											),
										'desc' 		=> __( 'Display team member content.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'number',
										'heading'	=> __( 'Content Words limit', 'inboundwp-lite' ),
										'name' 		=> 'words_limit',
										'value' 	=> 30,
										'min'		=> 1,
										'desc' 		=> __( 'Enter words limit for team member content.', 'inboundwp-lite' ),
										'dependency'=> array(
														'element' 	=> 'show_content',
														'value' 	=> array( 'true' ),
														),
									),
									array(
										'type' 			=> 'text',
										'heading' 		=> __( 'Extra Class', 'inboundwp-lite' ),
										'name' 			=> 'extra_class',
										'value' 		=> '',
										'refresh_time'	=> 1000,
										'desc' 			=> __( 'Enter extra CSS class for design customization.', 'inboundwp-lite' ) . '<label title="'.__('Note: Extra class added as parent so using extra class you customize your design.', 'inboundwp-lite').'"> [?]</label>',
									),
								)
			),
			
			//Slider Fields
			'slider' => array(
					'title'		=> __('Slider Parameters', 'inboundwp-lite'),
					'params'	=> array(
									//Slider Setting
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Show Dots', 'inboundwp-lite' ),
										'name' 		=> 'dots',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Show pagination dots.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Show Arrows', 'inboundwp-lite' ),
										'name' 		=> 'arrows',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ) ,
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Show Prev - Next arrows.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Slide To Show', 'inboundwp-lite' ),
										'name' 		=> 'slides_column',
										'value' 	=> 2,
										'desc' 		=> __( 'Enter number of slide you want to show in slider.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Slide To Scroll', 'inboundwp-lite' ),
										'name' 		=> 'slides_scroll',
										'value' 	=> 1,
										'desc' 		=> __( 'Enter number of slides you want to scroll at a time.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Autoplay', 'inboundwp-lite' ),
										'name' 		=> 'autoplay',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ) ,
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Enable autoplay.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Autoplay Interval', 'inboundwp-lite' ),
										'name' 		=> 'autoplay_interval',
										'value' 	=> 3000,
										'desc' 		=> __( 'Enter autoplay speed.', 'inboundwp-lite' ),
										'dependency'=> array(
															'element' 	=> 'autoplay',
															'value' 	=> array( 'true' ),
														),
									),
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Speed', 'inboundwp-lite' ),
										'name' 		=> 'speed',
										'value' 	=> 300,
										'desc' 		=> __( 'Enter slide speed.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Fade Effect', 'inboundwp-lite' ),
										'name' 		=> 'effect',
										'value' 	=> array(
															'slide'	=> __( 'False', 'inboundwp-lite' ),
															'fade'	=> __( 'True', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Enable fade effect. Note: fade effect parameter will work with slides_column="1".', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Loop', 'inboundwp-lite' ),
										'name' 		=> 'loop',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Run slider continuously.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Centermode', 'inboundwp-lite' ),
										'name' 		=> 'center_mode',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'default'	=> 'false',
										'desc' 		=> __( 'Enable slider center mode effect.Note: effect parameter will work with slides_column="3".', 'inboundwp-lite' ),
									)
								)
			),
			
			// Query Fields
			'query' => array(
					'title'		=> __('Query Parameters', 'inboundwp-lite'),
					'params'    => array(
									// Query Setting
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Total items', 'inboundwp-lite' ),
										'name' 		=> 'limit',
										'value' 	=> 20,
										'min'		=> -1,
										'desc' 		=> __( 'Enter number to be displayed. Enter -1 to display all.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Post Order By', 'inboundwp-lite' ),
										'name' 		=> 'orderby',
										'value' 	=> array(
															'date'		=> __( 'Post Date', 'inboundwp-lite' ),
															'ID'		=> __( 'Post ID', 'inboundwp-lite' ),
															'author'	=> __( 'Post Author', 'inboundwp-lite' ),
															'title'		=> __( 'Post Title', 'inboundwp-lite' ),
															'modified'	=> __( 'Post Modified Date', 'inboundwp-lite' ),
															'rand'		=> __( 'Random', 'inboundwp-lite' ),
															'menu_order'=> __( 'Menu Order', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Select order type.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Post Order', 'inboundwp-lite' ),
										'name' 		=> 'order',
										'value' 	=> array(
															'desc'	=> __( 'Descending', 'inboundwp-lite' ),
															'asc'	=> __( 'Ascending', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'Select sorting order.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Display Specific Category', 'inboundwp-lite' ),
										'name' 		=> 'category',
										'value' 	=> '',
										'desc' 		=> __( 'Enter category id to display categories wise.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant category listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'dropdown',
										'heading' 	=> __( 'Include Child Category', 'inboundwp-lite' ),
										'name' 		=> 'include_cat_child',
										'value' 	=> array(
															'true'	=> __( 'True', 'inboundwp-lite' ),
															'false'	=> __( 'False', 'inboundwp-lite' ),
														),
										'desc' 		=> __( 'If you are using parent category then whether to display child category or not.', 'inboundwp-lite' ),
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Exclude Category', 'inboundwp-lite' ),
										'name' 		=> 'exclude_cat',
										'value' 	=> '',
										'desc' 		=> __( 'Exclude post category. Works only if `Category` field is empty.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant category listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Display Specific Post', 'inboundwp-lite' ),
										'name' 		=> 'posts',
										'value' 	=> '',
										'desc' 		=> __('Enter id of the post which you want to display.', 'inboundwp-lite') . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant post listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Exclude Post', 'inboundwp-lite' ),
										'name' 		=> 'exclude_post',
										'value' 	=> '',
										'desc' 		=> __('Enter id of the post which you do not want to display.', 'inboundwp-lite') . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant post listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Include Author', 'inboundwp-lite' ),
										'name' 		=> 'include_author',
										'value' 	=> '',
										'desc' 		=> __( 'Enter author id to display posts of particular author.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at users listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'text',
										'heading' 	=> __( 'Exclude Author', 'inboundwp-lite' ),
										'name' 		=> 'exclude_author',
										'value' 	=> '',
										'desc' 		=> __( 'Enter author id to hide post of particular author. Works only if `Include Author` field is empty.', 'inboundwp-lite' ) . '<label title="'.__('You can pass multiple ids with comma seperated. You can find id at relevant users listing page.', 'inboundwp-lite').'"> [?]</label>',
									),
									array(
										'type' 		=> 'number',
										'heading' 	=> __( 'Query Offset', 'inboundwp-lite' ),
										'name' 		=> 'query_offset',
										'value' 	=> '',
										'desc' 		=> __( 'Exclude number of posts from starting.', 'inboundwp-lite' ) . '<label title="'.__('e.g if you pass 5 then it will skip first five post. Note: Do not use limit=-1 and pagination=true with this.', 'inboundwp-lite').'"> [?]</label>',
									),
								)
			),
	);
	return $fields;
}