<?php
/**
 * Handles testimonial metabox HTML
 *
 * @package Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $post;

$prefix = IBWP_TMW_META_PREFIX; // Metabox prefix

// Getting saved values
$client 	= get_post_meta( $post->ID, $prefix.'client', true );
$job 		= get_post_meta( $post->ID, $prefix.'job', true );
$company 	= get_post_meta( $post->ID, $prefix.'company', true );
$url 		= get_post_meta( $post->ID, $prefix.'url', true );
$rating 	= get_post_meta( $post->ID, $prefix.'rating', true );
?>

<table class="form-table ibwp-tmw-table">
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="ibwp-tmw-client-name"><?php _e('Client Name', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($client); ?>" class="ibwp-text regular-text" id="ibwp-tmw-client-name" name="<?php echo $prefix; ?>client" />
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-tmw-job-title"><?php _e('Job Title', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($job); ?>" class="ibwp-text regular-text" id="ibwp-tmw-job-title" name="<?php echo $prefix; ?>job" />
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-tmw-company"><?php _e('Company', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo ibwpl_esc_attr($company); ?>" class="ibwp-text regular-text" id="ibwp-tmw-company" name="<?php echo $prefix; ?>company" />
			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="ibwp-tmw-url"><?php _e('URL', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<input type="text" value="<?php echo esc_url($url); ?>" class="ibwp-text regular-text" id="ibwp-tmw-url" name="<?php echo $prefix; ?>url" />
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="ibwp-tmw-rating"><?php _e('Rating', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>rating" class="ibwp-select ibwp-tmw-rating" id="ibwp-tmw-rating">
					<option value=""><?php _e('None', 'inboundwp-lite'); ?></option>
					<?php for( $i=1; $i<=5; $i++ ) { ?>
					<option value="<?php echo $i; ?>" <?php selected( $rating, $i ); ?>><?php echo $i; ?></option>
					<?php } ?>
				</select><br/>
				<span class="description"><?php _e( 'Select testimonial rating.', 'inboundwp-lite' ); ?></span>
			</td>
		</tr>
	</tbody>
</table><!-- end .ibwp-tstmnl-table -->