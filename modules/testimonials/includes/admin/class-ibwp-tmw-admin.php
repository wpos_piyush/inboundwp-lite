<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package InboundWP Lite
 * @subpackage Testimonials
 * @since 1.0
 */
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Tmw_Admin {
	
	function __construct() {

		// Action to add admin menu
		add_action( 'admin_menu', array($this, 'ibwp_tmw_register_menu'), 12 );

		// Action to register plugin settings
		add_action ( 'admin_init', array($this, 'ibwp_tmw_register_settings') );

		// Filter to add screen id
		add_filter( 'ibwp_screen_ids', array( $this, 'ibwp_tmw_add_screen_id') );
		
		// Shortocde Preview
		add_action( 'current_screen', array($this, 'ibwp_tmw_generate_preview_screen') );

		// Action to add metabox
		add_action( 'add_meta_boxes', array($this, 'ibwp_tmw_add_metabox') );

		// Action to save metabox
		add_action( 'save_post', array($this,'ibwp_tmw_save_metabox_value') );

		// Action to add custom column to Testimonials listing
		add_filter( 'manage_posts_columns', array($this, 'ibwp_tmw_posts_columns'), 10, 2 );

		// Action to add custom column data to Testimonials listing
		add_action( 'manage_'.IBWP_TMW_POST_TYPE.'_posts_custom_column', array($this, 'ibwp_tmw_post_columns_data'), 10, 2);

		// Add some support to taxonomy like shortcode column and etc
		add_filter( 'ibwp_taxonomy_supports', array($this, 'ibwp_tmw_taxonomy_supports') );
	}

	/**
	 * Function to add menu
	 * 
	 * @package Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_register_menu() {

		// Register Setting page
		add_submenu_page( 'edit.php?post_type='.IBWP_TMW_POST_TYPE, __('Settings', 'inboundwp-lite'), __('Settings', 'inboundwp-lite'), 'manage_options', 'ibwp-tmw-settings', array($this, 'ibwp_tmw_settings_page') );
	
		// Shortocde Mapper
		add_submenu_page( 'edit.php?post_type='.IBWP_TMW_POST_TYPE, __('Testimonial Shortcode Builder', 'inboundwp-lite'), __('Shortcode Builder', 'inboundwp-lite'), 'edit_posts', 'tmw-shrt-mapper', array($this, 'ibwp_tmw_shortcode_mapper_page') );

		// Shortocde Preview
		add_submenu_page( null, __('Shortcode Preview', 'inboundwp-lite'), __('Shortcode Preview', 'wp-testimonial-with-widget'), 'edit_posts', 'tmw-preview', array($this, 'ibwp_tmw_shortcode_preview_page') );
	}

	/**
	 * Getting Started Page Html
	 * 
	 * @package Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_settings_page() {		
		include_once( IBWP_TMW_DIR . '/includes/admin/settings/ibwp-tmw-settings.php' );
	}

	/**
	 * Function register setings
	 * 
	 * @package Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_register_settings() {

		// Reset default settings
		if( !empty( $_POST['ibwp_tmw_reset_sett'] ) ) {
			ibwp_tmw_default_settings();
		}

		register_setting( 'ibwp_tmw_plugin_options', 'ibwp_tmw_options', array($this, 'ibwp_tmw_validate_options') );
	}
	
	/**
	 * Validate Settings Options
	 * 
	 * @package Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_validate_options( $input ) {

		// Pro version fetures
		return $input;
	}


	/**
	 * Function to add screen id
	 * 
	 * @subpackage Testimonials
 	 * @since 1.0
	 */
	function ibwp_tmw_add_screen_id( $screen_ids ) {

		$screen_ids['main'][] 				= IBWP_TMW_POST_TYPE;
		$screen_ids['shortcode_mapper'][] 	= IBWP_TMW_POST_TYPE.'_page_tmw-shrt-mapper';
		return $screen_ids;
	}

	/**
	 * Function to handle plugin shoercode preview
	 * 
	 * @package Testimonials 
	 * @since 1.1
	 */
	function ibwp_tmw_generate_preview_screen( $screen ) {

		if( $screen->id == 'admin_page_tmw-preview' ) {

			$config_array = array(							
							'registered_shortcodes' => ibwp_tmw_registered_shortcodes(),
							'referral_page'			=> 'tmw-shrt-mapper',
							'localize_var'			=> array( 'handler' => 'IBWP_TMW', 'value' => ibwp_tmw_script_local_vars() ),
							'module_styles'			=> array(
														 	IBWPL_URL.'assets/css/slick.css',
														 	IBWPL_URL.'assets/css/font-awesome.min.css',
															IBWP_TMW_URL.'assets/css/ibwp-tmw-public.css'
														),
							'module_scripts'		=> array(
															IBWPL_URL.'assets/js/slick.min.js',
															IBWP_TMW_URL.'assets/js/ibwp-tmw-public.js',
															
														),
						);

			include_once( IBWPL_DIR . '/includes/admin/shortcode-mapper/shortcode-preview.php' );
			exit;
		}

	}

	/**
	 * Function to handle plugin shoercode preview
	 * 
	 * @package Testimonials 
	 * @since 1.1
	 */
	function ibwp_tmw_shortcode_mapper_page() {

		$preview_shortcode = !empty($_GET['shortcode']) ? $_GET['shortcode'] : apply_filters('ibwp_default_shortcode_preview', 'ibwp_tmw_grid', 'testimonials_grid' );

		$config_array = array(
							'registered_shortcodes' 	=> ibwp_tmw_registered_shortcodes(),
							'page_title'				=> __( 'Testimonial - Shortcode Builder', 'inboundwp-lite' ),
							'pro_notice'				=> sprintf( __('In lite version, you can use only 4 designs. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use more than 4 designs.', 'inboundwp-lite'), IBWPL_PRO_LINK ),
							'preview_shortcode'			=> $preview_shortcode,
							'preview_url' 				=> add_query_arg( array( 'page' => 'tmw-preview', 'shortcode' => $preview_shortcode), admin_url('admin.php') ),
							'shortcode_generator_url' 	=> add_query_arg( array('post_type' => IBWP_TMW_POST_TYPE, 'page' => 'tmw-shrt-mapper'), admin_url('edit.php') ),
						);

		include_once( IBWPL_DIR . '/includes/admin/shortcode-mapper/ibwp-shortcode-mapper.php' );

	}


	/**
	 * Function to handle plugin shoercode preview
	 * 
	 * @package Testimonials 
	 * @since 1.1
	 */
	function ibwp_tmw_shortcode_preview_page(){
	}

	/**
	 * Function to register metabox
	 * 
	 * @subpackage Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_add_metabox() {
		add_meta_box( 'testimonial-details', __( 'Testimonial Details - IBWP', 'inboundwp-lite' ), array($this, 'ibwp_tmw_meta_box_content'), IBWP_TMW_POST_TYPE, 'normal', 'high' );
	}

	/**
	 * Function to handle metabox content
	 * 
	 * @subpackage Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_meta_box_content() {
		include_once( IBWP_TMW_DIR .'/includes/admin/metabox/tmw-post-sett.php');
	}

	/**
	 * Function to save metabox values
	 * 
	 * @subpackage Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_save_metabox_value( $post_id ){

		global $post_type;
		
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                	// Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )  	// Check Revision
		|| ( $post_type !=  IBWP_TMW_POST_TYPE ) )              					// Check if current post type is supported.
		{
			return $post_id;
		}

		$prefix = IBWP_TMW_META_PREFIX; // Taking metabox prefix

		// Getting saved values
		$client 	= isset($_POST[$prefix.'client']) 	? ibwpl_clean( $_POST[$prefix.'client'] ) 	: '';
		$job 		= isset($_POST[$prefix.'job'])		? ibwpl_clean( $_POST[$prefix.'job'] ) 		: '';
		$company 	= isset($_POST[$prefix.'company']) 	? ibwpl_clean( $_POST[$prefix.'company'] ) 	: '';
		$url 		= isset($_POST[$prefix.'url']) 		? ibwpl_clean_url( $_POST[$prefix.'url'] ) 	: '';
		$rating 	= isset($_POST[$prefix.'rating']) 	? ibwpl_clean( $_POST[$prefix.'rating'] ) 	: '';

		update_post_meta($post_id, $prefix.'client', $client);
		update_post_meta($post_id, $prefix.'job', $job);
		update_post_meta($post_id, $prefix.'company', $company);
		update_post_meta($post_id, $prefix.'url', $url);
		update_post_meta($post_id, $prefix.'rating', $rating);
		
	}

	/**
	 * Add custom column to Testimonials listing page
	 * 
	 * @subpackage Testimonials
	 * @since 1.0
	 */
	function ibwp_tmw_posts_columns( $columns, $post_type ) {

		if( $post_type == IBWP_TMW_POST_TYPE ) {

			$new_columns['ibwp_tmw_image'] = __('Image', 'inboundwp-lite');

			$columns = ibwpl_add_array( $columns, $new_columns, 1, true);
		}
		return $columns;
	}

	/**
	 * Add custom column data to Testimonials listing page
	 * 
	 * @subpackage Testimonials
 	 * @since 1.0
	 */
	function ibwp_tmw_post_columns_data( $column, $post_id ) {

		if($column == 'ibwp_tmw_image') {
			$value = ibwpl_get_post_featured_image($post_id, 40 ,'square');
			if($value != '') {
				echo '<img class="wp-post-image ibwp-tmw-avatar-image" height="40" width="40" src="'.$value.'" alt="" />';
			}
		}
	}

	/**
	 * Function to add support to taxonomy like shortcode column etc
	 * 
	 * @subpackage Testimonials
 	 * @since 1.0
	 */
	function ibwp_tmw_taxonomy_supports( $supports ) {
		$supports[IBWP_TMW_CAT] = array(
										'row_data_id' => true
									);
		return $supports;
	}

}

$ibwp_tmw_admin = new IBWP_Tmw_Admin();