<?php
/**
 * Settings Page
 *
 * @package Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Plugin tabs
$sett_tabs = array(
				'ibwp-tmw-google-review-sett'	=> __('Google Review', 'inboundwp-lite'),
				'ibwp-tmw-form-sett' 			=> __('Form', 'inboundwp-lite')
			);

$tab = isset($_GET['tab']) ? $_GET['tab'] : 'ibwp-tmw-google-review-sett';
?>
<div class="wrap">

	<h2><?php _e( 'Testimonials', 'inboundwp-lite' ); ?></h2>

	<?php
	// Reset message
	if( !empty( $_POST['ibwp_tmw_reset_sett']) ) {
		ibwpl_display_message( 'reset' );
	}

	// Success message
	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {
		ibwpl_display_message( 'update' );
	}
	?>

	<h2 class="nav-tab-wrapper">
		<?php foreach ($sett_tabs as $tab_key => $tab_val) { 
			$tab_url 		= add_query_arg( array( 'post_type' => 'ibwp_testimonial', 'page' => 'ibwp-tmw-settings', 'tab' => $tab_key ), admin_url('edit.php') );
			$active_tab_cls = ($tab == $tab_key) ? 'nav-tab-active' : '';
		?>
			<a class="nav-tab <?php echo $active_tab_cls; ?>" href="<?php echo $tab_url; ?>"><?php echo $tab_val; ?></a>
		<?php } ?>
	</h2>

	<div class="ibwp-sett-wrap ibwp-tmw-settings ibwp-pad-top-20">

		<!-- Reset settings form -->
		<form action="" method="post" id="ibwp-tmw-reset-sett-form" class="ibwp-right ibwp-tmw-reset-sett-form">
			<input type="submit" class="button button-primary ibwp-btn ibwp-reset-sett ibwp-resett-sett-btn ibwp-tmw-reset-sett" name="ibwp_tmw_reset_sett" id="ibwp-tmw-reset-sett" value="<?php _e( 'Reset All Settings', 'inboundwp-lite' ); ?>" />
		</form>

		<form action="options.php" method="POST" id="ibwp-tmw-settings-form" class="ibwp-tmw-settings-form">

			<?php settings_fields( 'ibwp_tmw_plugin_options' ); ?>

			<div class="textright ibwp-clearfix">
				<input type="submit" name="ibwp_tmw_settings_submit" class="button button-primary right ibwp-btn ibwp-tmw-sett-submit" value="<?php _e('Save Changes', 'inboundwp-lite'); ?>" />
			</div>

			<?php
				// Setting files
				if( $tab == 'ibwp-tmw-form-sett' ) {
					include_once( IBWP_TMW_DIR . '/includes/admin/settings/form-sett.php' );
				} else {
					include_once( IBWP_TMW_DIR . '/includes/admin/settings/google-review-sett.php' );
				}
			?>	
		</form>

	</div><!-- .ibwp-sett-wrap -->
</div><!-- .wrap -->