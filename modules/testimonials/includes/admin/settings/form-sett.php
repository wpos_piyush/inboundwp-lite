<?php
/**
 * Form Settings Page
 *
 * @package Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
?>

<div id="ibwp-testimonial-google-general-sett" class="post-box-container ibwp-testimonial-general-sett">
	<div class="metabox-holder">
		<div class="meta-box-sortables ui-sortable">
			<div id="general" class="postbox">
				<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>
					<!-- Settings box title -->
					<h3 class="hndle">
						<span><?php _e( 'Testimonial Form Settings', 'inboundwp-lite' ); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></span>
					</h3>
					<div class="inside">
						<div class="ibwp-pro-notice">
							<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want use front-end testimonial form shortcode.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
						</div>
					</div><!-- .inside -->
			</div><!-- #general -->
		</div><!-- .meta-box-sortables ui-sortable -->
	</div><!-- .metabox-holder -->
</div><!-- #ibwp-testimonial-general-sett -->