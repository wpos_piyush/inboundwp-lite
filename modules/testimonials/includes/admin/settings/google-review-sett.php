<?php
/**
 * Google Review Settings Page
 *
 * @package Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
?>

<div id="ibwp-testimonial-google-general-sett" class="post-box-container ibwp-testimonial-general-sett">
	<div class="metabox-holder">
		<div class="meta-box-sortables ui-sortable">
			<div id="general" class="postbox">
				<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>
					<!-- Settings box title -->
					<h3 class="hndle">
						<span><?php _e( 'Google Review Settings', 'inboundwp-lite' ); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></span>
					</h3>
					<div class="inside">
					<div class="ibwp-pro-notice">
						<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want Google Review features.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
					</div>
					<table id="ibwp-tmw-cache-place" class="ibwp-tmw-cache-place ibwp-pro-disable form-table ibwp-testimonial-general-sett-tbl">
						<tbody>
							
							<tr>
								<th><label for="ibwp-google-api-key"><?php _e('Google API Key', 'inboundwp-lite'); ?>:</label></th>
								<td>
									<input type="text" name="<?php echo $prefix; ?>google_api_key" disabled="disabled" class="regular-text">
								</td>
							</tr>
							 <tr>
                                <th><label><?php _e('Shortcodes', 'inboundwp-lite'); ?>:</label></th>
                                <td>
                                    <span class="ibwp-shortcode-preview">[ibwp_tmw_google_review_grid place_id="google place id"]</span> – <?php _e('Google Review Testimonials Grid', 'inboundwp-lite'); ?>
                                    <span class="ibwp-shortcode-preview">[ibwp_tmw_google_review_slider place_id="google place id"]</span> – <?php _e('Google Review Testimonials Slider', 'inboundwp-lite'); ?>
                                </td>
                            </tr>
                    	</tbody>
					 </table>
					 <div class="ibwp-tmw-msg-wrap"></div>
				</div><!-- .inside -->
			</div><!-- #general -->
		</div><!-- .meta-box-sortables ui-sortable -->
	</div><!-- .metabox-holder -->
</div><!-- #ibwp-testimonial-general-sett -->