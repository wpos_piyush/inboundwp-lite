<?php
/**
 * Functions File
 *
 * @package InboundWP Lite
 * @subpackage Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Update default settings
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_default_settings() {
    
	global $ibwp_tmw_options;

	$ibwp_tmw_options = array(
							'google_api_key'	=> '',
							'form_opts'			=> array(),							
						);
    
    $default_options = apply_filters('ibwp_tmw_options_default_values', $ibwp_tmw_options );
    
    // Update default options
    update_option( 'ibwp_tmw_options', $default_options );

    // Overwrite global variable when option is update
	$ibwp_tmw_options = ibwpl_get_settings('ibwp_tmw_options');
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_get_option( $key = '', $default = false ) {
	global $ibwp_tmw_options;
	$value 	= ! empty( $ibwp_tmw_options[ $key ] ) ? $ibwp_tmw_options[ $key ] : $default;
	return $value;
}

/**
 * Function to echo form options
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_field_options(  $value ) {
	?>
	<option value="title" <?php selected( $value,'title'); ?> ><?php _e('Title','inboundwp-lite'); ?></option>
	<option value="category" <?php selected( $value ,'category'); ?> ><?php _e('Category','inboundwp-lite'); ?></option>
	<option value="client" <?php selected( $value,'client'); ?> ><?php _e('Client','inboundwp-lite'); ?></option>
	<option value="job" <?php selected( $value,'job'); ?> ><?php _e('Job','inboundwp-lite'); ?></option>
	<option value="company" <?php selected( $value,'company'); ?> ><?php _e('Company','inboundwp-lite'); ?></option>
	<option value="url" <?php selected( $value,'url'); ?> ><?php _e('Url','inboundwp-lite'); ?></option>
	<option value="rating" <?php selected( $value,'rating'); ?> ><?php _e('Rating','inboundwp-lite'); ?></option>
	<option value="image" <?php selected( $value,'image'); ?> ><?php _e('Image','inboundwp-lite'); ?></option>
	<option value="content" <?php selected( $value,'content'); ?> ><?php _e('Content','inboundwp-lite'); ?></option>
	<option value="captcha" <?php selected( $value,'captcha'); ?> ><?php _e('Captcha','inboundwp-lite'); ?></option>
	<?php
}

/**
 * Function to get testimonial shortcode designs
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_testimonials_designs() {

	$design_arr = array(
		'design-1'	=> __('Design 1', 'inboundwp-lite'),
		'design-2'	=> __('Design 2', 'inboundwp-lite'),
		'design-3'	=> __('Design 3', 'inboundwp-lite'),
		'design-4'	=> __('Design 4', 'inboundwp-lite'),
		'design-5'	=> __('Design 5', 'inboundwp-lite'),
		'design-6'	=> __('Design 6', 'inboundwp-lite'),
		'design-7'	=> __('Design 7', 'inboundwp-lite'),
		'design-8'	=> __('Design 8', 'inboundwp-lite'),
		'design-9'	=> __('Design 9', 'inboundwp-lite'),
		'design-10'	=> __('Design 10', 'inboundwp-lite'),
		'design-11'	=> __('Design 11', 'inboundwp-lite'),
		'design-12'	=> __('Design 12', 'inboundwp-lite'),
		'design-13'	=> __('Design 13', 'inboundwp-lite'),
		'design-14'	=> __('Design 14', 'inboundwp-lite'),
		'design-15'	=> __('Design 15', 'inboundwp-lite'),
		'design-16'	=> __('Design 16', 'inboundwp-lite'),
		'design-17'	=> __('Design 17', 'inboundwp-lite'),
		'design-18'	=> __('Design 18', 'inboundwp-lite'),
		'design-19'	=> __('Design 19', 'inboundwp-lite'),
		'design-20'	=> __('Design 20', 'inboundwp-lite'),
	);
	return apply_filters('ibwp_tmw_testimonials_designs', $design_arr );
}

/**
 * Function to get shortocdes registered in plugin
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_registered_shortcodes() {

	$shortcodes = array(
					'ibwp_tmw_grid'    				=> __('Testimonials Grid', 'inboundwp-lite'),
					'ibwp_tmw_slider'				=> __('Testimonials Slider', 'inboundwp-lite'),
				);
	return apply_filters('ibwp_tmw_registered_shortcodes', (array)$shortcodes );
}

/**
 * Pagination function for grid
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_pagination($args = array()) {

	$big = 999999999; // need an unlikely integer

	$paging = apply_filters('ibwp_tmw_paging_args', array(
					'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format'    => '?paged=%#%',
					'current'   => max( 1, $args['paged'] ),
					'total'     => $args['total'],
					'prev_next' => true,
					'prev_text' => '&laquo; '.__('Previous', 'inboundwp-lite'),
					'next_text' => __('Next', 'inboundwp-lite').' &raquo;',
				));

	return paginate_links($paging);
}

/**
 * Function to get script local variables
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_script_local_vars() {
	return $local_var = array(
							'is_mobile' 		=> (wp_is_mobile()) ? 	1 	: 0,
							'is_rtl' 			=> (is_rtl()) 		? 	1 	: 0,
						);
}

/**
 * Function to get user image
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_get_image( $id, $size, $style = "ibwp-tmw-circle" ) {

	$response = '';

	if ( has_post_thumbnail( $id ) ) {
		// If not a string or an array, and not an integer, default to 150x9999.
		if ( ( is_int( $size ) || ( 0 < intval( $size ) ) ) && ! is_array( $size ) ) {
			$size = array( intval( $size ), intval( $size ) );
		} elseif ( ! is_string( $size ) && ! is_array( $size ) ) {
			$size = array( 100, 100 );
		}

		$response = get_the_post_thumbnail( intval( $id ), $size, array('class' => $style) );

	} else {

		$testimonial_email = get_post_meta( $id, '_testimonial_email', true );

		if ( $testimonial_email != '' && is_email( $testimonial_email ) ) {
			$response = get_avatar( $testimonial_email, $size );
		}
	}

	if($response == '') {

		$default_image = IBWP_TMW_URL ."assets/images/placeholder.png";
		
		if(!empty($default_image)) {
			$response = '<img class="'.$style.'" src="'.$default_image.'" alt="" />';
		}
	}
	return $response;
}

/**
 * Function to get grid column based on grid
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_grid_column( $grid = '' ) {

	if($grid == '2') {
		$grid_clmn = '6';
	} else if($grid == '3') {
		$grid_clmn = '4';
	}  else if($grid == '4') {
		$grid_clmn = '3';
	} else if($grid == '6') {
		$grid_clmn = '2';
	} else {
		$grid_clmn = '12';
	}

	return $grid_clmn;
}

/**
 * Function to set description words limit
 * 
 * @package Testimonial
 * @since 1.0
 */
function tmw_content_words_limit($string, $word_limit) {
  $words = explode(' ', $string, ($word_limit + 1));
  if(count($words) > $word_limit)
  array_pop($words);
  return implode(' ', $words);
}