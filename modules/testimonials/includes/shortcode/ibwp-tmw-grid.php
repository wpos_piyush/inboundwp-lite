<?php
/**
 * `ibwp_tmw_testimonials` Shortcode
 * 
 * @package Testimonials
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * Function to handle testimonial shortcode
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_grid ( $atts, $content ) {

	$atts = shortcode_atts(array(
		'limit' 				=> 20,
		'design'				=> 'design-1',
		'per_row' 				=> 3,
		'orderby' 				=> 'date',
		'order' 				=> 'DESC',
		'include_cat_child'		=> 'true',
		'display_client' 		=> 'true',
		'display_avatar' 		=> 'true',
		'display_job' 			=> 'true',
		'display_company' 		=> 'true',
		'image_style'			=> 'circle',
		'show_rating'			=> 'true',
		'show_title'			=> 'true',
		'words_limit'	=> 30,
		'show_content'			=> 'true',
		'posts'					=> array(),
		'exclude_post'			=> array(),
		'category' 				=> '',
		'exclude_cat'			=> array(),
		'include_author' 		=> array(),
		'exclude_author'		=> array(),
		'query_offset'			=> '',
		'pagination' 			=> 'true',
		'pagination_type'		=> 'numeric',
		'extra_class'			=> '',
	), $atts, 'ibwp_tmw_grid' );

	$shortcode_designs 			= ibwp_tmw_testimonials_designs();
	$atts['limit'] 				= !empty( $atts['limit'] ) 						? $atts['limit'] 							: 20;
	$atts['design'] 			= ( $atts['design'] && ( array_key_exists( trim( $atts['design'] ), $shortcode_designs ) ) ) ? trim( $atts['design'] ) 	: 'design-1';
	$atts['per_row']			= !empty( $atts['per_row'] ) 					? $atts['per_row'] 							: 1;
	$atts['orderby'] 			= !empty( $atts['orderby'] ) 					? $atts['orderby'] 							: 'date';
	$atts['order'] 				= ( strtolower( $atts['order'] ) == 'asc' ) 	? 'ASC' 									: 'DESC';
	$atts['cat'] 				= ( !empty($atts['category'] ) )				? explode( ',',$atts['category'] ) 			: '';
	$atts['include_cat_child']	= ( $atts['include_cat_child'] == 'false' ) 	? false 									: true;
	$atts['exclude_cat'] 		= !empty( $atts['exclude_cat'] )				? explode( ',', $atts['exclude_cat'] ) 		: array();
	$atts['display_client'] 	= ( $atts['display_client'] == 'true' ) 		? 1 										: 0;
	$atts['display_avatar'] 	= ( $atts['display_avatar'] == 'true' ) 		? 1 										: 0;
	$atts['display_job'] 		= ( $atts['display_job'] == 'true' ) 			? 1 										: 0;
	$atts['display_company']	= ( $atts['display_company'] == 'true' ) 		? 1 										: 0;
	$atts['show_rating'] 		= ( $atts['show_rating'] == 'true' ) 			? 1 										: 0;
	$atts['show_title'] 		= ( $atts['show_title'] == 'true' ) 			? 1 										: 0;
	$atts['show_content'] 		= ( $atts['show_content'] == 'true' ) 			? 1 										: 0;
	$atts['words_limit'] 		= !empty( $atts['words_limit'] ) 				? $atts['words_limit'] 						: 30;
	$atts['image_style'] 		= ( $atts['image_style'] == 'circle' ) 			? 'ibwp-tmw-circle' 						: 'ibwp-tmw-square';
	$atts['pagination'] 		= ( $atts['pagination'] == 'false' )			? false										: true;
	$atts['pagination_type'] 	= ( $atts['pagination_type'] == 'prev-next' )	? 'prev-next' 								: 'numeric';
	$atts['exclude_post'] 		= !empty( $atts['exclude_post'] )				? explode( ',', $atts['exclude_post'] ) 	: array();
	$atts['posts'] 				= !empty( $atts['posts'] )						? explode( ',', $atts['posts'] ) 			: array();
	$atts['include_author']		= !empty( $atts['include_author'] )				? explode( ',', $atts['include_author'] ) 	: array();
	$atts['exclude_author']		= !empty( $atts['exclude_author'] )				? explode( ',', $atts['exclude_author'] ) 	: array();
	$atts['query_offset']		= !empty( $atts['query_offset'] )				? $atts['query_offset'] 					: null;
	$atts['extra_class']		= ibwpl_sanitize_html_classes( $atts['extra_class'] );

	extract( $atts );

	// Taking some globals
	global $post;

	// Pagination Parameter
	if ( get_query_var( 'paged' ) ) {
		$paged = get_query_var('paged');
	} else if ( get_query_var( 'page' ) ) {
		$paged = get_query_var( 'page' );
	} else {
		$paged = 1;
	}

	// Taking some variables
	$count 			= 0;
	$prefix 		= IBWP_TMW_META_PREFIX;
	$type_row		= ibwp_tmw_grid_column( $per_row );

	// Query Parameter
	$args = array (
		'post_type' 			=> IBWP_TMW_POST_TYPE,
		'post_status'			=> array( 'publish' ),
		'order' 				=> $order,
		'orderby'				=> $orderby,
		'posts_per_page' 		=> $limit,
		'post__not_in'			=> $exclude_post,
		'post__in'				=> $posts,
		'author__in'			=> $include_author,
		'author__not_in' 		=> $exclude_author,
		'paged'					=> ($pagination) ? $paged : 1,
		'offset'				=> $query_offset,
		'ignore_sticky_posts'	=> true,
	);

	// Category Parameter
	if($cat != '') {

		$args['tax_query'] = array(
								array(
									'taxonomy' 			=> IBWP_TMW_CAT,
									'field' 			=> 'term_id',
									'terms' 			=> $cat,
									'include_children'	=> $include_cat_child,
							));

	} elseif( !empty( $exclude_cat ) ) {

		$args['tax_query'] = array(
								array(
									'taxonomy' 			=> IBWP_TMW_CAT,
									'field' 			=> 'term_id',
									'terms' 			=> $exclude_cat,
									'operator'			=> 'NOT IN',
									'include_children'	=> $include_cat_child,
							));
	}

	// WP Query
	$query = new WP_Query($args);

	// Template Variable
	$max_num_pages = $query->max_num_pages;

	ob_start();

	// If post is there
	if ( $query->have_posts() ) { ?>

		<div class="ibwp-tmw-testimonials-wrp ibwp-tmw-testimonials-list <?php echo 'ibwp-tmw-'.$design; ?> ibwp-tmw-clearfix <?php echo $extra_class; ?>">
			<div class="ibwp-tmw-tstmnl-wrp-inr ibwp-tmw-clearfix">
		<?php
		while ( $query->have_posts() ) : $query->the_post();

			$count++;
			$css_class 			= 'ibwp-tmw-quote ibwp-col-'.$type_row.' ibwp-columns';
			$css_class			.= ( $count % $per_row == 1 )	? ' ibwp-tmw-first' : '';
			$css_class			.= ( $count % $per_row == 0 )	? ' ibwp-tmw-last'	: '';

			$job_data			= array();
			$author_image 		= ibwp_tmw_get_image( $post->ID, $size, $image_style );
			$author 			= get_post_meta( $post->ID, $prefix.'client', true );
			$job_title			= get_post_meta( $post->ID, $prefix.'job', true );
			$company 			= get_post_meta( $post->ID, $prefix.'company', true );
			$url 				= get_post_meta( $post->ID, $prefix.'url', true );
			$rating 			= get_post_meta( $post->ID, $prefix.'rating', true );
			$testimonial_title	= get_the_title();
			$testimonial_cont	= get_the_content();

			// Add a CSS class if no image is available.
			if( empty( $author_image ) ) {
				$css_class 		.= ' ibwp-tmw-no-image';
			}

			// Testimonial Meta
			if( $display_job && $job_title ) {
				$job_data[] = $job_title;
			}
			if( $display_company && $company ) {
				$job_data[] = !empty( $url ) ? '<a href="'.esc_url( $url ).'" target="_blank">'.$company.'</a>' : $company;
			}
			$job_meta = join( ' / ', $job_data );

			// Include shortcode html file
			include( IBWP_TMW_DIR . "/templates/testimonial/{$design}.php" );

			endwhile; ?>
			</div>
			<?php if( $pagination ) { ?>
				<div class="ibwp-tmw-paging ibwp-tmw-clearfix">
					<?php if($pagination_type == "numeric") {
						echo ibwp_tmw_pagination( array( 'paged' => $paged , 'total' => $max_num_pages ) );
					} else { ?>
						<div class="ibwp-tmw-pagi-btn ibwp-tmw-next-btn"><?php next_posts_link( __('Next', 'featured-and-trending-post').' &raquo;', $max_num_pages ); ?></div>
						<div class="ibwp-tmw-pagi-btn ibwp-tmw-prev-btn"><?php previous_posts_link( '&laquo; '.__('Previous', 'featured-and-trending-post') ); ?></div>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		<?php

	} // end of have_post()

	wp_reset_postdata(); // Reset WP Query

	$content .= ob_get_clean();
	return $content;
}

// Testimonial Grid Shortcode
add_shortcode( 'ibwp_tmw_grid', 'ibwp_tmw_grid' );