<?php
/**
 * `ibwp_testimonials_slider` Shortcode
 * 
 * @package Testimonials
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Function to handle testimonial slider shortcode
 * 
 * @package Testimonials
 * @since 1.0
 */
function ibwp_tmw_slider( $atts, $content ) {

	// Shortcode Parameter
	$atts = shortcode_atts(array(
		'limit' 				=> 20,
		'design'				=> 'design-1',
		'orderby' 				=> 'date',
		'order' 				=> 'DESC',
		'slides_column' 		=> 2,
		'slides_scroll' 		=> 1,
		'include_cat_child'		=> 'true',
		'display_client' 		=> 'true',
		'display_avatar' 		=> 'true',
		'display_job' 			=> 'true',
		'display_company' 		=> 'true',
		'image_style' 			=> 'circle',
		'dots' 					=> 'true',
		'arrows' 				=> 'true',
		'autoplay' 				=> 'true',
		'autoplay_interval' 	=> 3000,
		'speed' 				=> 300,
		'show_rating'			=> 'true',
		'show_title'			=> 'true',
		'words_limit'	=> 30,
		'show_content'			=> 'true',
		'loop'					=> 'true',
		'effect'				=> 'slide',
		'center_mode'			=> 'false',
		'posts'					=> array(),
		'exclude_post'			=> array(),
		'category' 				=> '',
		'exclude_cat'			=> array(),
		'include_author' 		=> array(),
		'exclude_author'		=> array(),
		'query_offset'			=> '',
		'rtl'					=> false,
		'extra_class'			=> '',
	), $atts, 'ibwp_tmw_slider' );

	$shortcode_designs 			= ibwp_tmw_testimonials_designs();
	$atts['limit'] 				= !empty( $atts['limit'] ) 						? $atts['limit'] 							: 20;
	$atts['design'] 			= ( $atts['design'] && ( array_key_exists( trim( $atts['design'] ), $shortcode_designs ) ) ) ? trim( $atts['design'] ) 	: 'design-1';
	$atts['orderby'] 			= !empty( $atts['orderby'] ) 					? $atts['orderby'] 							: 'date';
	$atts['order'] 				= ( strtolower( $atts['order'] ) == 'asc' ) 	? 'ASC' 									: 'DESC';
	$atts['slides_column'] 		= !empty( $atts['slides_column'] ) 				? $atts['slides_column']					: 2;
	$atts['slides_scroll'] 		= !empty( $atts['slides_scroll'] ) 				? $atts['slides_scroll'] 					: 1;
	$atts['cat'] 				= ( !empty( $atts['category'] ) )				? explode(',',$atts['category']) 			: '';
	$atts['include_cat_child']	= ( $atts['include_cat_child'] == 'false' ) 	? false 									: true;
	$atts['exclude_cat'] 		= !empty( $atts['exclude_cat'] )				? explode(',', $atts['exclude_cat'])		: array();
	$atts['display_client'] 	= ( $atts['display_client'] == 'true' ) 		? 1 										: 0;
	$atts['display_avatar'] 	= ( $atts['display_avatar'] == 'true' ) 		? 1 										: 0;
	$atts['display_job'] 		= ( $atts['display_job'] == 'true' ) 			? 1 										: 0;
	$atts['display_company']	= ( $atts['display_company'] == 'true' ) 		? 1 										: 0;
	$atts['show_rating'] 		= ( $atts['show_rating'] == 'true' ) 			? 1 										: 0;
	$atts['show_title'] 		= ( $atts['show_title'] == 'true' ) 			? 1 										: 0;
	$atts['show_content'] 		= ( $atts['show_content'] == 'true' ) 			? 1 										: 0;
	$atts['words_limit'] 		= !empty( $atts['words_limit'] ) 		? $atts['words_limit'] 								: 30;
	$atts['image_style'] 		= ( $atts['image_style'] == 'circle' ) 			? 'ibwp-tmw-circle' 						: 'ibwp-tmw-square';
	$atts['dots'] 				= ( $atts['dots'] == 'true' ) 					? 'true' 									: 'false';
	$atts['arrows'] 			= ( $atts['arrows'] == 'true' ) 				? 'true' 									: 'false';
	$atts['loop'] 				= ( $atts['loop'] == 'true' ) 					? 'true' 									: 'false';
	$atts['autoplay'] 			= ( $atts['autoplay'] == 'true' ) 				? 'true' 									: 'false';
	$atts['effect'] 			= ( $atts['effect'] == 'fade' ) 				? 'true' 									: 'false';
	$atts['autoplay_interval'] 	= !empty( $atts['autoplay_interval'] ) 			? $atts['autoplay_interval'] 				: 2000;
	$atts['speed'] 				= !empty( $atts['speed'] ) 						? $atts['speed'] 							: 300;
	$atts['exclude_post'] 		= !empty( $atts['exclude_post'] )				? explode( ',', $atts['exclude_post'] ) 	: array();
	$atts['posts'] 				= !empty( $atts['posts'] )						? explode( ',', $atts['posts'] ) 			: array();
	$atts['include_author']		= !empty( $atts['include_author'] )				? explode( ',', $atts['include_author'] ) 	: array();
	$atts['exclude_author']		= !empty( $atts['exclude_author'] )				? explode( ',', $atts['exclude_author'] ) 	: array();
	$atts['query_offset']		= !empty( $atts['query_offset'] )				? $atts['query_offset'] 					: null;
	$atts['extra_class']		= ibwpl_sanitize_html_classes( $atts['extra_class'] );

	extract( $atts );

	// For RTL
	if( empty( $rtl ) && is_rtl() ) {
		$rtl = 'true';
	} elseif ( $rtl == 'true' ) {
		$rtl = 'true';
	} else {
		$rtl = 'false';
	}

	// Taking some globals
	global $post;

	// Taking some variables
	$prefix 		= IBWP_TMW_META_PREFIX;
	$unique			= ibwp_get_unique();

	// Enqueing required script
	wp_enqueue_script( 'ibwp-slick-jquery' );
	wp_enqueue_script( 'ibwp-tmw-public-script' );

	// Query Parameter
	$args = array (
		'post_type' 			=> IBWP_TMW_POST_TYPE,
		'post_status'			=> array( 'publish' ),
		'order' 				=> $order,
		'orderby' 				=> $orderby,
		'posts_per_page' 		=> $limit,
		'post__in'				=> $posts,
		'post__not_in'			=> $exclude_post,
		'author__in'			=> $include_author,
		'author__not_in' 		=> $exclude_author,
		'offset'				=> $query_offset,
		'ignore_sticky_posts'	=> true,
	);

	// Category Parameter
	if( !empty( $cat ) ) {

		$args['tax_query'] = array(
								array(
									'taxonomy' 			=> IBWP_TMW_CAT,
									'field' 			=> 'term_id',
									'terms' 			=> $cat,
									'include_children'	=> $include_cat_child,
								));

	} elseif( !empty( $exclude_cat ) ) {

		$args['tax_query'] = array(
								array(
									'taxonomy' 			=> IBWP_TMW_CAT,
									'field' 			=> 'term_id',
									'terms' 			=> $exclude_cat,
									'operator'			=> 'NOT IN',
									'include_children'	=> $include_cat_child,
								));
	}

	// WP Query
	$query		= new WP_Query($args);
	$post_count = $query->post_count;

	// Slider configuration and taken care of centermode
	$slides_column 				= (!empty($slides_column) && $slides_column <= $post_count) ? $slides_column : $post_count;
	$center_mode				= ($center_mode == 'true' && $slides_column % 2 != 0 && $slides_column != $post_count) ? 'true' : 'false';
	$center_mode_cls 			= ( $center_mode == 'true' ) ? 'ibwp-tmw-center' : '';
	$slides_column				= $slides_column;

	// Slider configuration
	$slider_conf = compact( 'slides_column', 'slides_scroll', 'dots', 'loop', 'arrows', 'autoplay', 'autoplay_interval', 'speed', 'center_mode', 'effect', 'rtl' );

	ob_start();

	// If post is there
	if ( $query->have_posts() ) { ?>

		<div class="ibwp-tmw-slider-wrp <?php echo $extra_class; ?>">
			<div class="ibwp-tmw-testimonials-slider ibwp-tmw-testimonials-slidelist <?php echo 'ibwp-tmw-'.$design.' '.$center_mode_cls; ?> <?php echo "ibwp-tmw-slider-clmn-".$slides_column; ?>" id="ibwp-tmw-testimonials-<?php echo $unique; ?>">

		<?php

		while ( $query->have_posts() ) : $query->the_post();

			$job_data			= array();
			$author_image		= ibwp_tmw_get_image( $post->ID, $size, $image_style );
			$author				= get_post_meta( $post->ID, $prefix.'client', true );
			$job_title			= get_post_meta( $post->ID, $prefix.'job', true );
			$company			= get_post_meta( $post->ID, $prefix.'company', true );
			$url 				= get_post_meta( $post->ID, $prefix.'url', true );
			$rating 			= get_post_meta( $post->ID, $prefix.'rating', true );
			$testimonial_title	= get_the_title();
			$testimonial_cont	= get_the_content();
			$css_class			= 'ibwp-tmw-quote';

			// Add a CSS class if no image is available.
			if( empty( $author_image ) ) {
				$css_class .= ' ibwp-tmw-no-image';
			}

			// Testimonial Meta
			if( $display_job && $job_title ) {
				$job_data[] = $job_title;
			}
			if( $display_company && $company ) {
				$job_data[] = !empty( $url ) ? '<a href="'.esc_url( $url ).'" target="_blank">'.$company.'</a>' : $company;
			}
			$job_meta = join( ' / ', $job_data );
			
			// Include shortcode html file
			include( IBWP_TMW_DIR . "/templates/testimonial/{$design}.php" );
	
		endwhile; ?>
			</div>
				<div class="ibwp-tmw-slider-conf" data-conf="<?php echo htmlspecialchars(json_encode( $slider_conf )); ?>"></div>
		</div>
		<?php
	} // End of have_post()

	wp_reset_postdata(); // Reset WP Query

	$content .= ob_get_clean();
	return $content;
}

// Testimonial Slider Shortcode
add_shortcode( 'ibwp_tmw_slider', 'ibwp_tmw_slider' );