<?php
/**
 * Testimonials Module
 * 
 * @package InboundWP Lite
 * @subpackage Testimonials
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

IBWP_Lite()->define( 'IBWP_TMW_DIR', dirname( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_TMW_URL', plugin_dir_url( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_TMW_POST_TYPE', 'ibwp_testimonial' );
IBWP_Lite()->define( 'IBWP_TMW_CAT', 'ibwp_tmw_cat' );
IBWP_Lite()->define( 'IBWP_TMW_META_PREFIX', '_ibwp_tmw_' );
IBWP_Lite()->define( 'IBWP_TEMPLATE_DEBUG_MODE', false );

/**
 * Plugin Setup (On Activation)
 * 
 * Does the initial setup, set default values for the plugin options.
 *
 * @subpackage Testimonials
 * @since 1.0
 */
function ibwp_tmw_install() {

    ibwp_tmw_register_post_type();
    ibwp_tmw_register_taxonomies();

    // IMP need to flush rules for custom registered post type
    flush_rewrite_rules();

    // Get settings for the plugin
    $ibwp_tmw_options = get_option( 'ibwp_tmw_options' );

    if( empty( $ibwp_tmw_options ) ) { // Check plugin version option
        
        // Set default settings
        ibwp_tmw_default_settings();

        // Update plugin version to option
        update_option( 'ibwp_tmw_plugin_version', '1.0' );
    }
}
add_action( 'ibwp_module_activation_hook_testimonials', 'ibwp_tmw_install' );

// Taking some globals
global $ibwp_tmw_options, $ibwp_tmw_form_error;

// Function File
require_once( IBWP_TMW_DIR . '/includes/ibwp-tmw-functions.php' );
$ibwp_tmw_options       = ibwpl_get_settings('ibwp_tmw_options');

// Post Type File
require_once( IBWP_TMW_DIR . '/includes/ibwp-tmw-post-types.php' );

// Script Class File
require_once( IBWP_TMW_DIR . '/includes/class-ibwp-tmw-script.php' );

// Admin Class File
require_once( IBWP_TMW_DIR . '/includes/admin/class-ibwp-tmw-admin.php' );

// Shortcode Builder
require_once( IBWP_TMW_DIR . '/includes/admin/supports/ibwp-shortcode-fields.php' );

// Shortcode File
require_once( IBWP_TMW_DIR . '/includes/shortcode/ibwp-tmw-slider.php' );
require_once( IBWP_TMW_DIR . '/includes/shortcode/ibwp-tmw-grid.php' );