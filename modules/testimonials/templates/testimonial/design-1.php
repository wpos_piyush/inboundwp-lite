<?php
/**
 * Template for Design 1
 *
 * This template can be overridden by copying it to yourtheme/wp-testimonial-with-widget-pro/designs/design-1.php
 *
 * If you want to override for grid only then put it into 'grid' folder and same for respective.
 *
 * @package Testimonials
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} 

global $post; ?>

<div id="ibwp-tmw-quote-<?php echo $post->ID; ?>" class="<?php echo $css_class; ?>">

	<?php if ( $author_image && $display_avatar ) { ?>
	<div class="ibwp-tmw-testimonial-left">
		<div class="ibwp-tmw-avtar-image"><?php echo $author_image; ?></div>
	</div>
	<?php } ?>

	<div class="ibwp-tmw-testimonial-content">
		<i class="fa fa-quote-left"></i>

		<?php if( $show_title && $testimonial_title ) { ?>
		<div class="ibwp-tmw-testimonial-title"><?php echo $testimonial_title; ?></div>
		<?php } ?>
		<?php if( $show_content ){ ?>
		<div class="ibwp-tmw-testimonials-text"><em><?php echo tmw_content_words_limit($testimonial_cont, $words_limit); ?></em></div>
		<?php } ?>
	</div>

	<?php if( $display_client && $author != '' ) { ?>
	<div class="ibwp-tmw-testimonial-author"><strong><?php echo $author; ?></strong></div>
	<?php }

	if( $job_meta ) { ?>
		<div class="ibwp-tmw-testimonial-job"><?php echo $job_meta; ?></div>
	<?php } ?>

	<?php if( !empty($rating) && $show_rating ) { ?>
	<div class="ibwp-tmw-testimonial-rating">
		<?php for ($i=0; $i<5; $i++) {
			if( $i < $rating ){
				echo '<i class="fa fa-star"></i>';
			} else {
				echo '<i class="fa fa-star-o"></i>';
			}
		} ?>
	</div>
	<?php } ?>
</div>