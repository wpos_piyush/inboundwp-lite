<?php 
/**
 * Template for Design 4
 *
 * This template can be overridden by copying it to yourtheme/wp-testimonial-with-widget-pro/designs/design-4.php
 *
 * If you want to override for grid only then put it into 'grid' folder and same for respective.
 *
 * @package Testimonials
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} 

global $post; ?>

<div id="ibwp-tmw-quote-<?php echo $post->ID; ?>" class="ibwp-tmw-testimonial-box <?php echo $css_class; ?>">

	<div class="ibwp-tmw-testimonial-inner">
		<div class="ibwp-tmw-testimonial-avatar">
			<?php if ( $author_image && $display_avatar ) { ?>
			<div class="ibwp-tmw-avtar-image"><?php echo $author_image; ?></div>
			<?php } ?>
		</div>

		<div class="ibwp-tmw-testimonial-author">
			<?php if($display_client && $author != '') { ?>
			<div class="ibwp-tmw-testimonial-author"><strong><?php echo $author; ?></strong></div>
			<?php }

			if( $job_meta ) { ?>
			<div class="ibwp-tmw-testimonial-cdec"><?php echo $job_meta; ?></div>
			<?php } ?>

			<?php if( !empty($rating) && $show_rating ) { ?>
			<div class="ibwp-tmw-testimonial-rating">
				<?php for ($i=0; $i<5; $i++) {
					if( $i < $rating ){
						echo '<i class="fa fa-star"></i>';
					} else {
						echo '<i class="fa fa-star-o"></i>';
					}
				} ?>
			</div>
			<?php } ?>
		</div>

		<div class="ibwp-tmw-testimonial-content">
			<?php if( $show_title && $testimonial_title) { ?>
			<div class="ibwp-tmw-testimonial-title"><?php echo $testimonial_title; ?></div>
			<?php } ?>
			<?php if( $show_content ){ ?>
			<div class="ibwp-tmw-testimonials-text"><em><?php echo tmw_content_words_limit($testimonial_cont, $words_limit); ?></em></div>
			<?php } ?>
			
		</div>
	</div>
</div>