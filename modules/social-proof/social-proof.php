<?php
/**
 * Social Proof Module
 * 
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Basic plugin definitions
 * 
 * @subpackage Social Proof
 * @since 1.0
 */
IBWP_Lite()->define( 'IBWP_SP_URL', plugin_dir_url( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_SP_VERSION', '1.0' );	// version
IBWP_Lite()->define( 'IBWP_SP_DIR', dirname( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_SP_DIR_NAME', 'social-proof' );
IBWP_Lite()->define( 'IBWP_SP_URL', plugin_dir_url( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_SP_POST_TYPE', 'ibwp_sp' ); // Plugin post type
IBWP_Lite()->define( 'IBWP_SP_META_PREFIX', '_ibwp_sp_' ); // Plugin meta prefix

/**
 * Plugin Module Activation Function
 * Does the initial setup, sets the default values for the plugin module options
 * 
 * @subpackage Social Proof 
 * @since 1.0
 */
function ibwp_sp_install(){
	
	ibwp_sp_post_type();
		
	// IMP need to flush rules for custom registered post type
	flush_rewrite_rules();

	// Get settings for the plugin
	$ibwp_sp_options = get_option( 'ibwp_sp_options' );

	if( empty( $ibwp_sp_options ) ) { // Check plugin version option

		// Set default settings
		ibwp_sp_default_settings();

		// Update plugin version to option
		update_option( 'ibwp_sp_plugin_version', '1.0' );
	}
}
add_action( 'ibwp_module_activation_hook_social-proof', 'ibwp_sp_install' );

global $ibwp_sp_options;

// Function File
require_once( IBWP_SP_DIR . '/includes/ibwp-sp-functions.php' );
$ibwp_sp_options = ibwpl_get_settings( 'ibwp_sp_options' );

// Post Type File
require_once( IBWP_SP_DIR . '/includes/ibwp-sp-post-type.php' );

// Script Class
require_once( IBWP_SP_DIR . '/includes/class-ibwp-sp-script.php' );

// Admin Class
require_once( IBWP_SP_DIR . '/includes/admin/class-ibwp-sp-admin.php' );

// Public Notfication 
require_once( IBWP_SP_DIR . '/includes/class-ibwp-sp-public.php' );