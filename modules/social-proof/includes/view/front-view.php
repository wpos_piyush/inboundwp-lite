<?php
/**
 * Front Page Notification Popup Html
 *
 * @package Social Proof
 * @since 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$prefix = IBWP_SP_META_PREFIX;

$hide_on_desktop    = get_post_meta( $id, $prefix. 'hide_on_desk', true );
$hide_on_mobile     = get_post_meta( $id, $prefix. 'hide_on_mob', true );
$show_close_btn     = get_post_meta( $id, $prefix. 'show_close_btn', true );
$display_img        = get_post_meta( $id, $prefix. 'display_img', true );
$target             = get_post_meta( $id, $prefix. 'target', true );
$target             = isset( $target ) ? "_blank" : "_self";
$content            = get_post_meta( $id, $prefix. 'content', true );
$type               = get_post_meta( $id, $prefix. 'type', true );
$source             = get_post_meta( $id, $prefix. 'source', true );
$classes            = '';    

if ( $hide_on_desktop ) {
    $classes .= ' ibwp-sp-notification-hide-desktop';
}
if ( $hide_on_mobile ) {
    $classes .= ' ibwp-sp-notification-hide-mobile';
}

$cdata = array();
if( $source == 'edd' ) {
    $cdata = ibwp_sp_get_edd_data( $type );
} elseif ($source == 'woo') {
    $cdata = ibwp_sp_get_woo_data( $type );
} elseif ( $source == 'custom' ) {
    $cdata = ibwp_sp_get_custom_data( $id, $type );
}

$count = 0;
if( !empty( $cdata ) ) {
    foreach ($cdata as $ckey => $cval) {
        
        $name       = ! empty( $cval['name'] )        ? $cval['name']         : '';
        $country    = ! empty( $cval['country'] )     ? $cval['country']      : '';
        $time       = ! empty( $cval['time'] )        ? $cval['time']         : '';
        $title      = ! empty( $cval['title'] )       ? $cval['title']        : '';
        $link       = ! empty( $cval['link'] )        ? $cval['link']         : '';
        $star       = ! empty( $cval['star'] )        ? $cval['star']         : 4;
        $image_url  = ! empty( $cval['image_url'] )   ? $cval['image_url']    : IBWPL_URL. "modules/social-proof/assets/images/placeholder-300x300.png";
        
        if( $type == "conversion" && $country ) {
            $name = $name." from ".$country;
        }
        
        $new_content        = str_replace("{{name}}", '<span class="ibwp-sp-name ibwp-sp-name-'.$id.'">'.ucwords($name).'</span>', $content);
        $display_content    = str_replace("{{title}}", '<span class="ibwp-sp-title ibwp-sp-title-'.$id.'">'.ucfirst($title).'</span>', $new_content);
        ?>
        <div class="ibwp-sp-main-wrap ibwp-sp-<?php echo $type; ?>-wrap ibwp-sp-notification-popup-<?php echo $id; ?> animated <?php echo $classes; ?>" id="ibwp-sp-notification-popup-<?php echo $id; ?>" data-popup-id="<?php echo $id;?>" data-sequence="<?php echo $count; ?>">

            <div class="ibwp-sp-inner-wrap">

                <?php if( $show_close_btn ) { ?> <p class="ibwp-sp-popup-close" title="Close">×</p> <?php } ?>

                <div class="ibwp-sp-content-wrap">

                    <?php if( $link ) { ?><a href="<?php echo $link; ?>" target="<?php echo $target; ?>" class="ibwp-sp-link"><?php } ?>
                        <?php if( $display_img ) { ?>
                        <div class="ibwp-sp-img ibwp-sp-img-<?php echo $id; ?>">
                            <img src="<?php echo $image_url; ?>" alt="">
                        </div>
                        <?php } ?>

                        <div class="ibwp-sp-content type-<?php echo $type; ?> ibwp-sp-content-<?php echo $id; ?>">
                            <?php if( $type == 'conversion' ) { ?>
                                <?php echo $display_content; ?>
                            <?php } ?>
                            <small><?php _e('About','inboundwp-lite'); ?> <?php echo $time; ?> <?php _e('ago','inboundwp-lite'); ?></small>
                        </div>

                    <?php if( $link ) { ?></a><?php } ?>
                </div>
            </div>
        </div>
    <?php $count++; }     
} ?>