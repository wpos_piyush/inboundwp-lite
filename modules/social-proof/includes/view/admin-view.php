<?php
/**
 * Admin Side Notification Popup Preview Html
 *
 * @package Social Proof
 * @since 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $post;
$prefix = IBWP_SP_META_PREFIX;

$type               = get_post_meta( $post->ID, $prefix. 'type', true );
$border_radius      = get_post_meta( $post->ID, $prefix. 'border_radius', true );
$border_radius      = isset($border_radius) ? $border_radius : "5px";
$img_border_radius  = get_post_meta( $post->ID, $prefix. 'img_border_radius', true );
$img_border_radius  = isset($img_border_radius) ? $img_border_radius : "25px";
$bg_color           = get_post_meta( $post->ID, $prefix. 'bg_color', true );
$bg_color           = isset($bg_color) ? $bg_color : "#ffffff";
$name_color         = get_post_meta( $post->ID, $prefix. 'name_color', true );
$name_color         = isset($name_color) ? $name_color : "#000000";
$title_color        = get_post_meta( $post->ID, $prefix. 'title_color', true );
$title_color        = isset($title_color) ? $title_color : "#000000";
$gen_txt_color      = get_post_meta( $post->ID, $prefix. 'gen_txt_color', true );
$gen_txt_color      = isset($gen_txt_color) ? $gen_txt_color : "#000000";
?>

<style>
.ibwp-sp-main-wrap { position: fixed; bottom: 40px; right: 20px; min-width: 278px; max-width: 100%; min-height: 90px; height: 100px; font-size: 13px; background: <?php echo $bg_color; ?>; padding: 10px; border-radius: <?php echo $border_radius; ?>px; box-sizing: border-box; z-index: 100001; border-width: 1px; border-style: solid; border-color: #cccccc; }
.ibwp-sp-main-wrap .ibwp-sp-preview-text { position: absolute; top: -24px; left: -1px; text-transform: uppercase; font-size: 12px; color: #000 !important; }
.ibwp-sp-inner-wrap { display: inline-block; height: 100%; width: 100%; }
.ibwp-sp-content { color: <?php echo $gen_txt_color; ?> }
.ibwp-sp-inner-wrap .ibwp-sp-popup-close { display: block; cursor: pointer; font-size: 19px; font-weight: 600; position: absolute; top: 2px; right: 3px; margin: 0; padding: 0px 6px; text-decoration: none!important; vertical-align: text-top; text-shadow: 0 0px 0 #fff; border-radius: 15px; opacity: 1; z-index: 9999;}
.ibwp-sp-inner-wrap .ibwp-sp-content-wrap { display: table; height: 100%; width: 100%; }
.ibwp-sp-inner-wrap .ibwp-sp-img { display: table-cell; height: 50px; width: 50px; vertical-align: middle; }
.ibwp-sp-inner-wrap .ibwp-sp-img img { border-radius:  <?php echo $img_border_radius; ?>px; height: 100%; width: 100%; vertical-align: middle; }
.ibwp-sp-inner-wrap .ibwp-sp-content { display: table-cell; padding: 0 15px; text-align: left; vertical-align: middle; font-size: 11px; line-height: 20px; }
.ibwp-sp-inner-wrap .ibwp-sp-title { display: block; font-size: 14px; font-weight: bold; line-height: 1; color:  <?php echo $title_color; ?>; }
.ibwp-sp-main-wrap small { font-size: 10px; }
.ibwp-sp-inner-wrap .ibwp-sp-name { font-size: 12px; color:  <?php echo $name_color; ?>; }
</style>

<div class="ibwp-sp-main-wrap ibwp-sp-wrap-conversion" <?php if( $type == 'reviews' ) { ?> style="display: none;" <?php } ?> >
    <span class="ibwp-sp-preview-text"><?php _e('Preview','inboundwp-lite'); ?></span>
    <div class="ibwp-sp-inner-wrap">
        <p class="ibwp-sp-popup-close" title="Close">×</p>
        <div class="ibwp-sp-content-wrap">
            <div class="ibwp-sp-img">
                <img src="<?php echo IBWPL_URL; ?>modules/social-proof/assets/images/placeholder-300x300.png" alt="">
            </div>
            <div class="ibwp-sp-content type-conversion content-text-color">
                <span class="ibwp-sp-name"><?php _e('John D.','inboundwp-lite'); ?></span> <?php _e('just purchased','inboundwp-lite'); ?>
                <span class="ibwp-sp-title"><?php _e('Your Product Title','inboundwp-lite'); ?></span>
                <small><?php _e('About 1 hour ago','inboundwp-lite'); ?></small>
            </div>
        </div>
    </div>
</div>