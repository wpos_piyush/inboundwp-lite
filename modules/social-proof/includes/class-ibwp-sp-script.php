<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package WP InBound Marketing Social Proof 
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Sp_Script {
	
	function __construct() {

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_sp_admin_style') );

		// Action to add script in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_sp_admin_script') );

		// Action to add style on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_sp_front_style_script') );
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package Social Proof 
	 * @since 1.0
	 */
	function ibwp_sp_admin_style() {

		global $wp_version, $post_type;

		wp_enqueue_style( 'ibwp-select2-style' );
		
		// If page is post type page than add style
		if( $post_type == IBWP_SP_POST_TYPE ) {

			// Enqueu built in style for color picker
			if( wp_style_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_style( 'wp-color-picker' );
			} else {
				wp_enqueue_style( 'farbtastic' );
			}
			// Registring Public Style
			wp_register_style( 'ibwp-sp-admin-style', IBWP_SP_URL.'assets/css/ibwp-sp-admin.css', null, IBWP_SP_VERSION );
			wp_enqueue_style('ibwp-sp-admin-style');

		}

		// Registering main admin style 
		wp_enqueue_style( 'ibwp-admin-style' );
	}

	/**
	 * Enqueue admin script
	 * 
	 * @package Social Proof 
	 * @since 1.0
	 */
	function ibwp_sp_admin_script() {

		global $wp_version, $post_type;

		$new_ui = $wp_version >= '3.5' ? '1' : '0'; // Check wordpress version for older scripts

		// Enqueue Select 2 JS
		wp_enqueue_script('ibwp-select2-script');

		// If page is post type page than add script
		if( $post_type == IBWP_SP_POST_TYPE || ( isset( $_GET['page'] ) && $_GET['page'] == 'ibwp-sp-settings' ) ) {

			// Enqueu built-in script for color picker
			if( wp_script_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_script( 'wp-color-picker' );
			} else {
				wp_enqueue_script( 'farbtastic' );
			}

			// Registring admin script
			wp_register_script( 'ibwp-sp-admin-js', IBWP_SP_URL.'assets/js/ibwp-sp-admin.js', array('jquery','jquery-ui-accordion'), IBWP_SP_VERSION, true );
			wp_localize_script( 'ibwp-sp-admin-js', 'ibwp_sp_admin', 
				array(
					'ajaxurl' => admin_url( 'admin-ajax.php' ),
					'new_ui' => $new_ui,
					'error_msg' => __('Sorry, Something Happened Wrong.', 'inboundwp-lite'),
					'remove_row' => __('Click OK to remove custom row.', 'inboundwp-lite'),
				)
			);

			wp_enqueue_script( 'ibwp-sp-admin-js' );

			// Registering main admin script 
			wp_enqueue_script( 'ibwp-admin-js' );

			// Registring imgae upload script
			wp_register_script( 'ibwp-sp-image-select-js', IBWP_SP_URL.'assets/js/ibwp-sp-image-select.js', array('jquery','media-upload'), IBWP_SP_VERSION, true );
			wp_enqueue_media();
			wp_enqueue_script( 'ibwp-sp-image-select-js' );
		}
	}

	/**
	 * Enqueue front end styles
	 * 
	 * @package WP InBound Marketing Social Proof 
	 * @since 1.0
	 */
	function ibwp_sp_front_style_script() {

		// Registring Public Style
		wp_register_style( 'ibwp-sp-public-style', IBWP_SP_URL.'assets/css/ibwp-sp-public.css', null, IBWP_SP_VERSION );
		wp_enqueue_style('ibwp-sp-public-style');
		wp_register_style( 'ibwp-sp-animate-style', IBWP_SP_URL.'assets/css/animate.min.css', null, IBWP_SP_VERSION );
		wp_enqueue_style('ibwp-sp-animate-style');

		// Registring Popup script
		wp_register_script( 'ibwp-sp-public-js', IBWP_SP_URL.'assets/js/ibwp-sp-public.js', array('heartbeat','jquery'), IBWP_SP_VERSION, true );
		wp_enqueue_script('ibwp-sp-public-js');
	}
}

$ibwp_sp_script = new IBWP_Sp_Script();