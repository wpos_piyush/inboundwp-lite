<?php
/**
 * Register Post type functionality
 *
 * @package InboundWP
 * @package Social Proof
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Function to register post type
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_post_type() {

	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}

	if ( post_type_exists( IBWP_SP_POST_TYPE ) ) {
		return;
	}

	$ibwp_sp_lbl = array(
		'name'					=> __('Social Proof', 'inboundwp-lite'),
		'singular_name'			=> __('Social Proof', 'inboundwp-lite'),
		'all_items'				=> __('All Social Proofs', 'inboundwp-lite'),
		'add_new'				=> __('Add Social Proof', 'inboundwp-lite'),
		'add_new_item'			=> __('Add New Social Proof', 'inboundwp-lite'),
		'edit_item'				=> __('Edit Social Proof', 'inboundwp-lite'),
		'new_item'				=> __('New Social Proof', 'inboundwp-lite'),
		'view_item'				=> __('View Social Proof', 'inboundwp-lite'),
		'search_items'			=> __('Search Social Proof', 'inboundwp-lite'),
		'not_found'				=>  __('No Social Proof Items found', 'inboundwp-lite'),
		'not_found_in_trash'	=> __('No Social Proof Items found in Trash', 'inboundwp-lite'),
		'parent_item_colon'		=> '',
		'menu_name'				=> __('Social Proof - IBWP', 'inboundwp-lite'),
	);

	$ibwp_sp_args = array(
		'labels'				=> $ibwp_sp_lbl,
		'public'				=> false,
		'exclude_from_search'	=> false,
		'show_ui'				=> true,
		'show_in_menu'			=> true, 
		'query_var'				=> true,
		'rewrite'				=> false,
		'capability_type'		=> 'post',
		'has_archive'			=> true,
		'hierarchical'			=> false,
		'menu_position'			=> 8,
		'menu_icon'				=> 'dashicons-image-filter',
		'supports'				=> apply_filters( 'ibwp_sp_post_supports', array('title', 'author') ),
	);

	// Register Social Proof post type
	register_post_type( IBWP_SP_POST_TYPE, apply_filters( 'ibwp_sp_post_type', $ibwp_sp_args ) );
}

// Action to register plugin post type
add_action( 'init', 'ibwp_sp_post_type' );

/**
 * Function to update post message
 * 
 * @subpackage Social Proof
 * @since 1.0
 */
function ibwp_sp_post_updated_messages( $messages ) {

	global $post, $post_ID;

	$messages[IBWP_SP_POST_TYPE] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Social Proof updated.', 'inboundwp-lite' ),
		2 => __( 'Custom field updated.', 'inboundwp-lite' ),
		3 => __( 'Custom field deleted.', 'inboundwp-lite' ),
		4 => __( 'Social Proof updated.', 'inboundwp-lite' ),
		5 => isset( $_GET['revision'] ) ? sprintf( __( 'Social Proof restored to revision from %s', 'inboundwp-lite' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __( 'Social Proof published.', 'inboundwp-lite' ),
		7 => __( 'Social Proof saved.', 'inboundwp-lite' ),
		8 => __( 'Social Proof submitted.', 'inboundwp-lite' ),
		9 => __( 'Social Proof scheduled for: <strong>%1$s</strong>.', 'inboundwp-lite' ),
		  date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ),
		10 => __( 'Social Proof draft updated.', 'inboundwp-lite' ),
	);

	return $messages;
}

// Filter to update post message
add_filter( 'post_updated_messages', 'ibwp_sp_post_updated_messages' );