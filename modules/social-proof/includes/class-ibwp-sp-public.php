<?php
/**
 * Public Class
 *
 * The Public side functionality of plugin
 *
 * @package WP Social Proof
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Sp_Public {
	
	function __construct() {
		
		// Action to get ids of active notifications
		add_action( 'wp', array( $this, 'ibwp_sp_check_active_ids' ) );

		// Action to create html of notifications
		add_action( 'wp_footer', array( $this, 'ibwp_sp_get_active_notifications' ) );
		
		// Action to display notifications
		add_action( 'wp_ajax_ibwp_sp_notifications_display', array( $this, 'ibwp_sp_notifications_display' ) );
		add_action( 'wp_ajax_nopriv_ibwp_sp_notifications_display', array( $this,'ibwp_sp_notifications_display') );
	}

	/**
	 * Function to get active notification ids
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_check_active_ids() {

		global $ibwp_sp_options, $post, $pagenow, $active_ids;

		$prefix = IBWP_SP_META_PREFIX;

		if( is_admin() ) {
			return;
		}

		$sp_post_ids = array();
		$sp_check = "";
		$conver_glob_locs = $ibwp_sp_options['conver_glob_locs'];
		
		if ( ! empty($ibwp_sp_options['conver_target_locs']) && $ibwp_sp_options['conversion'] ) {

			$result = ibwp_sp_post_type( $ibwp_sp_options['conver_target_locs'] );
			if( $result ) {
				$sp_post_ids[] = $ibwp_sp_options['conversion'];
			}
		}

		// Checking custom locations
		if( is_search() ) {
			$sp_check = "is_search";
		} elseif( is_404() ) {
			$sp_check = "is_404";
		} elseif( is_archive() ){
			$sp_check = "is_archive";
		}

		if( $sp_check ) {

			if( ! empty($conver_glob_locs) && array_key_exists($sp_check, $conver_glob_locs) || isset( $conver_glob_locs['all'] ) ) {
				$sp_post_ids[] = $ibwp_sp_options['conversion'];
			} 
						
			if( ! empty( $sp_post_ids ) ) {
				$sp_post_ids = ibwpl_post_status( $sp_post_ids );
				$active_ids = array_unique($sp_post_ids);
			}
			return;
		}
		
		$conver_id = get_post_meta( $post->ID, $prefix. 'conver_post', true );
		
		if ( isset( $conver_glob_locs['all'] ) && $conver_id != "disable" ) {
			$sp_post_ids[] = $ibwp_sp_options['conversion'];
		}

		// Post is selected by custom for conversion
		if( ! empty( $conver_id ) && $conver_id != "disable" ) {

			$sp_post_ids[] = $conver_id;

		} else if( ! empty($conver_glob_locs) && is_singular() && array_key_exists($post->post_type, $conver_glob_locs ) ) {

			$conver_id = $ibwp_sp_options['conversion'];
			$sp_post_ids[] = $conver_id;
		
		}

		if( $sp_post_ids ) {
			$sp_post_ids = ibwpl_post_status( $sp_post_ids );
			$active_ids = array_unique($sp_post_ids);
		}
	}

	/**
	 * Function to passing active notification data
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_get_active_notifications() {

		global $post, $active_ids;

		if( empty( $active_ids ) ) {
			return;
		}

		$prefix = IBWP_SP_META_PREFIX;

		$conversion_ids = array();
		
		foreach ( $active_ids as $key => $id ) {

			// Check notification enable or not
            $enable = get_post_meta( $id, $prefix. 'enable', true );
            if( ! $enable ) {
            	continue;
            }
            
            // Check user logged in status
            $display_for = get_post_meta( $id, $prefix. 'display_for', true );
            if ( ( is_user_logged_in() && $display_for == 'log-out' ) || ( ! is_user_logged_in() && $display_for == 'log-in' ) ) {
                continue;
            }

            $type = get_post_meta( $id, $prefix. 'type', true );

			if( $type == 'conversion') {
				$conversion_ids[] = $id;
			}
		}

		?>
		<script type="text/javascript">
            var ibwp_sp_conf = {
                sp_nonce: '<?php echo wp_create_nonce('ibwp_sp_nonce_front'); ?>',
                sp_conversions : <?php echo json_encode( $conversion_ids ); ?>,
                ajaxurl: '<?php echo admin_url('admin-ajax.php'); ?>',
            };
        </script>
        <?php 
	}

	/**
	 * Function to pass some headers defaults
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ajax_headers() {
        send_origin_headers();
		@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
		@header( 'X-Robots-Tag: noindex' );
		send_nosniff_header();
		nocache_headers();
		status_header( 200 );
    }

	/**
	 * Function to passing active notification data
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_notifications_display() {

		$prefix = IBWP_SP_META_PREFIX;

		$this->ajax_headers();

        if ( ! isset( $_POST['sp_nonce'] ) || ! wp_verify_nonce( $_POST['sp_nonce'], 'ibwp_sp_nonce_front' ) ) {
            return;
        }
        if ( ! isset( $_POST['sp_ids'] ) || empty( $_POST['sp_ids'] ) || ! is_array( $_POST['sp_ids'] ) ) {
            return;
        }

        $ids = $_POST['sp_ids'];
        $data = array();

        foreach ( $ids as $id ) {
            
            $source 		= get_post_meta( $id, $prefix. 'source', true );
            $int_delay 		= get_post_meta( $id, $prefix. 'int_delay', true );
            $hide_after		= get_post_meta( $id, $prefix. 'hide_after', true );
            $delay_between	= get_post_meta( $id, $prefix. 'delay_between', true );
            $loop_repeat	= get_post_meta( $id, $prefix. 'loop_repeat', true );
            $show_animation = get_post_meta( $id, $prefix. 'show_animation', true );
			$hide_animation = get_post_meta( $id, $prefix. 'hide_animation', true );
            $max_per_page	= get_post_meta( $id, $prefix. 'max_per_page', true );

            $data['config'] = array(
                'id'                => $id,
                'initial_delay'     => !empty( $int_delay ) ? $int_delay * 1000 : 0,
                'display_duration'  => !empty( $hide_after ) ? $hide_after * 1000 : 0,
                'delay_each'     	=> !empty( $delay_between ) ? $delay_between * 1000 : 0,
                'show_animation'	=> $show_animation,
                'hide_animation'	=> $hide_animation,
                'loop'              => absint( $loop_repeat ),
                'randomize'         => 0,
                'max_per_page'      => !empty( $max_per_page ) ? $max_per_page : 10,
                'source'			=> $source,
			);
			
			ob_start();
            include IBWP_SP_DIR . '/includes/view/front-view.php';
            $content = ob_get_clean();

            $data['content'] = $content;
        }
        echo json_encode($data);
        die;
	}
}

$ibwp_sp_public = new IBWP_Sp_Public();