<?php
/**
 * Config meta html file
 *
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div id="ibwp_sp_config_options" class="ibwp-stabs-cnt ibwp-clearfix" style="display:block;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-title"><?php _e('Configuration', 'inboundwp-lite'); ?></div>
		<span class="ibwp-spw-desc description"><?php _e('Configuration settings.', 'inboundwp-lite'); ?></span>
	</div><br>
	<table class="form-table">
		<tbody>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-enable"><?php _e('Enable', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix ?>enable" id="ibwp-sp-enable" value="1" <?php checked( $enable, 1 ); ?> class="ibwp-checkbox ibwp-sp-enable"><br/>
					<span class="description"><?php _e('Check this box to active notification.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-type"><?php _e('Notification Type', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix ?>type" id="ibwp-sp-type" class="ibwp-select ibwp-sp-type">
						<option <?php selected( $type, 'conversion' ) ?> value="conversion"><?php echo __( 'Conversion', 'inboundwp-lite' ); ?></option>
						<option <?php selected( $type, 'reviews' ) ?> value="reviews" disabled="disabled"><?php echo __( 'Reviews (Pro)', 'inboundwp-lite' ); ?></option>
					</select><br/><br/>
					<div class="ibwp-pro-notice">
						<?php echo sprintf( __('In lite version, you can use only coversion notification. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use review notification as well.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
					</div>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-display-img"><?php _e('Display Image', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix ?>display_img" id="ibwp-sp-display-img" class="ibwp-checkbox ibwp-sp-display-img" value="1" <?php checked( $display_img, 1 );?> ><br/>
					<span class="description"><?php _e('Check this box to enable image in notification.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-link-to"><?php _e('Select Notification Link', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix ?>link_to" id="ibwp-sp-link-to" class="ibwp-select ibwp-sp-link-to">
						<option <?php selected( $link_to, 'none' ) ?> value="none"><?php echo __( 'None', 'inboundwp-lite' ); ?></option>
						<option <?php selected( $link_to, 'product' ) ?> value="product"><?php echo __( 'Product Page', 'inboundwp-lite' ); ?></option>
						<option <?php selected( $link_to, 'custom_url' ) ?> value="custom_url"><?php echo __( 'Custom Url', 'inboundwp-lite' ); ?></option>
					</select><br>
					<span class="description"><?php _e( 'You can link notification with product page or custom URL.', 'inboundwp-lite' ); ?></span>
				</td>
			</tr>
			<tr valign="top" class="ibwp-link-row ibwp-link-custom_url" <?php if( ! $cust_url ) { ?>style="display: none;"<?php } ?> >
				<th scope="row">
					<label for="ibwp-sp-cust-url"><?php _e('Custom Url', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="text" name="<?php echo $prefix ?>cust_url" id="ibwp-sp-cust-url" class="ibwp-url large-text ibwp-sp-cust-url" value="<?php echo esc_url($cust_url); ?>"><br/>
				</td>
			</tr>
		</tbody>
	</table>
</div>