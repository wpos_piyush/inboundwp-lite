<?php
/**
 * Design meta html file
 *
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div id="ibwp_sp_design_options" class="ibwp-stabs-cnt ibwp_sp_design_options ibwp-clearfix"  style="display:block;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-title"><?php _e('Design', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></div>
		<span class="ibwp-spw-desc description"><?php _e('Design settings.', 'inboundwp-lite'); ?></span>
	</div><br>
	<div class="ibwp-pro-notice">
		<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use custom design options.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
	</div>

	<table class="form-table ibwp-pro-disable">
		<tbody>
			<tr>
				<th>
					<label for="ibwp-sp-position"><?php _e('Notification Position', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix ?>position" id="ibwp-sp-position" class="ibwp-select ibwp-sp-position">
						<option <?php selected( $position, '1' ) ?> value="1"><?php _e('Top Left', 'inboundwp-lite'); ?></option>
						<option <?php selected( $position, '2' ) ?> value="2"><?php _e('Top Right', 'inboundwp-lite'); ?></option>
						<option <?php selected( $position, '3' ) ?> value="3"><?php _e('Bottom Left', 'inboundwp-lite'); ?></option>
						<option <?php selected( $position, '4' ) ?> value="4"><?php _e('Bottom Right', 'inboundwp-lite'); ?></option>
					</select><br>
					<span class="description"><?php _e( 'Select position of notification in screen', 'inboundwp-lite' ); ?></span>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-dist-from-screen"><?php _e('Distance From Screen', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="number" min="0" max="160" name="<?php echo $prefix ?>distance" value="<?php echo ibwpl_esc_attr($distance); ?>" class="ibwp-number ibwp-sp-dist-from-screen" id="ibwp-sp-dist-from-screen" /> 
					<?php _e('px','inboundwp-lite');?><br/>
					<span class="description"><?php _e('Distance from screen corner.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-show-animation"><?php _e('Show Time Animation', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix ?>show_animation" class="ibwp-select ibwp-sp-show-animation" id="ibwp-sp-show-animation" >
						<?php ibwp_sp_animate_in_opt($show_animation); ?>
					</select><br>
					<span class="description"><?php _e('Select show time animation.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-hide-animation"><?php _e('Hide Time Animation', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix ?>hide_animation" class="ibwp-select ibwp-sp-hide-animation" id="ibwp-sp-hide-animation">
						<?php ibwp_sp_animate_out_opt($hide_animation); ?>
					</select><br/>
					<span class="description"><?php _e('Select hide time animation.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-border-radius"><?php _e('Border Radius', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="number" name="<?php echo $prefix ?>border_radius" value="<?php echo $border_radius; ?>" class="ibwp-number ibwp-sp-border-radius" id="ibwp-sp-border-radius" min="0" max="50" />
					<?php _e('px','inboundwp-lite');?><br/>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-img-border-radius"><?php _e('Image Border Radius', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="number" name="<?php echo $prefix ?>img_border_radius" value="<?php echo $img_border_radius; ?>" class="ibwp-number ibwp-sp-img-border-radius" id="ibwp-sp-img-border-radius" min="0" max="25" />
					<?php _e('px','inboundwp-lite');?><br/>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-bg-color"><?php _e('Background Color', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="text" name="<?php echo $prefix ?>bg_color" value="<?php echo ibwpl_esc_attr($bg_color); ?>" class="colorpicker ibwp-color-pick ibwp-sp-bg-color" id="ibwp-sp-bg-color" />
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-name-color"><?php _e('Name Text Color', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="text" name="<?php echo $prefix ?>name_color" value="<?php echo ibwpl_esc_attr($name_color); ?>" class="colorpicker ibwp-color-pick ibwp-sp-name-color" id="ibwp-sp-name-color" />
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-title-color"><?php _e('Title Text Color', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="text" name="<?php echo $prefix ?>title_color" value="<?php echo ibwpl_esc_attr($title_color); ?>" class="colorpicker ibwp-color-pick ibwp-sp-title-color" id="ibwp-sp-title-color" />
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-gen-txt-color"><?php _e('General Text Color', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="text" name="<?php echo $prefix ?>gen_txt_color" value="<?php echo ibwpl_esc_attr($gen_txt_color); ?>" class="colorpicker ibwp-color-pick ibwp-sp-gen-txt-color" id="ibwp-sp-gen-txt-color" />
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-star-color"><?php _e('Star Rating Color', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="text" name="<?php echo $prefix ?>star_color" value="<?php echo ibwpl_esc_attr($star_color); ?>" class="colorpicker ibwp-color-pick ibwp-sp-star-color" id="ibwp-sp-star-color" />
				</td>
			</tr>
		</tbody>
	</table>
</div>