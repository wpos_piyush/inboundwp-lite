<?php
/**
 * Content meta html file
 *
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div id="ibwp_sp_content_options" class="ibwp-stabs-cnt ibwp-clearfix"  style="display:block;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-title"><?php _e('Content', 'inboundwp-lite'); ?></div>
		<span class="ibwp-spw-desc description"><?php _e('Content settings.', 'inboundwp-lite'); ?></span>
	</div>

	<table class="form-table">
		<tbody>
			<!-- Display only for conversion -->
			<tr valign="top" class="">
				<th scope="row">
					<label for="ibwp-sp-source"><?php _e('Notification Source', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix ?>source" id="ibwp-sp-source" class="ibwp-select ibwp-sp-source">
						<option <?php selected( $source, 'custom' ) ?> value="custom"><?php echo __( 'Custom', 'inboundwp-lite' ); ?></option>
						<?php if ( class_exists( 'WooCommerce' ) ) { ?>
							<option <?php selected( $source, 'woo' ) ?> value="woo"><?php echo __( 'WooCommerce', 'inboundwp-lite' ); ?></option>
						<?php  } if( function_exists( 'EDD' ) ) { ?>
						<option <?php selected( $source, 'edd' ) ?> value="edd"><?php echo __( 'Easy Digital Download', 'inboundwp-lite' ); ?></option>
					<?php } ?>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-content"><?php _e('Notification Template', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<textarea name="<?php echo $prefix ?>content" id="ibwp-sp-content" class="ibwp-textarea ibwp-sp-content" rows="5" cols="40"><?php echo $content; ?></textarea><br/>
					<span class="description"><?php _e( 'Variables: {{name}}, {{title}}', 'inboundwp-lite' ); ?></span>
				</td>
			</tr>

			<!-- Custom Meta fields start -->
			<tr class="ibwp-source-row ibwp-source-custom <?php if( $source != 'custom' ){ echo 'ibwp-hide'; } ?> ">
				<td colspan="2">
					
					<!-- Coversion custom fields -->
					<div class="ibwp-sp-wrap ibwp-sp-wrap-conversion <?php if( $type != 'conversion' ) { echo 'ibwp-hide'; } ?> ">
						<div class="ibwp-sp-table ibwp-sp-table-conversion">

							<?php foreach ( $conversions as $ckey => $cval ) { 

								$title 		= isset( $cval['title'] )		? $cval['title']		: '';
								$name 		= isset( $cval['name'] )		? $cval['name']			: '';
								$image_url 	= isset( $cval['image_url'] )	? $cval['image_url']	: '';
								$link 		= isset( $cval['link'] )		? $cval['link']			: '';
								$email 		= isset( $cval['email'] )		? $cval['email']		: '';
								$country 	= isset( $cval['country'] )		? $cval['country']		: '';

								?>
								<div class="ibwp-sp-row ibwp-sp-conversion-row" id="conversion_row_<?php echo $ckey; ?>" data-key="<?php echo $ckey; ?>">
									<div class="ibwp-sp-row-header">
										<span class="ibwp-sp-index ibwp-sp-conversion-index"><strong><?php _e('Conversion: ','inboundwp-lite'); ?></strong><span></span></span>
										<span class="ibwp-sp-row-actions">
											<a class="ibwp-sp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a> |
											<a class="ibwp-sp-remove-row ibwp-sp-delete"><?php _e('Remove', 'inboundwp-lite'); ?></a>
										</span>
									</div>
									<div class="ibwp-sp-row-standard-fields">
										<table class="form-table">
											<tr valign="top">
												<th scope="row">
													<label for="ibwp-sp-conver-title-<?php echo $ckey; ?>"><?php _e('Title', 'inboundwp-lite'); ?></label>
												</th>
												<td>
													<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][title]" id="ibwp-sp-conver-title-<?php echo $ckey; ?>" class="ibwp-text ibwp-sp-conver-title-<?php echo $ckey; ?>" value="<?php echo ibwpl_esc_attr($title); ?>"><br/>
												</td>
											</tr>
											<tr valign="top">
												<th scope="row">
													<label for="ibwp-sp-conver-display-name-<?php echo $ckey; ?>"><?php _e('Name', 'inboundwp-lite'); ?></label>
												</th>
												<td>
													<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][name]" id="ibwp-sp-conver-display-name-<?php echo $ckey; ?>" class="ibwp-text ibwp-sp-conver-display-name-<?php echo $ckey; ?>" value="<?php echo ibwpl_esc_attr($name); ?>"><br/>
												</td>
											</tr>
											<tr valign="top">
												<th scope="row">
													<label for="ibwp-sp-conver-email-<?php echo $ckey; ?>"><?php _e('Email Address', 'inboundwp-lite'); ?></label>
												</th>
												<td>
													<input type="email" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][email]" id="ibwp-sp-conver-email-<?php echo $ckey; ?>" class="ibwp-email ibwp-sp-conver-email-<?php echo $ckey; ?>" value="<?php echo ibwpl_esc_attr($email); ?>"><br/>
												</td>
											</tr>
											<tr valign="top">
												<th scope="row">
													<label for="ibwp-sp-conver-country-<?php echo $ckey; ?>"><?php _e('Country', 'inboundwp-lite'); ?></label>
												</th>
												<td>
													<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][country]" id="ibwp-sp-conver-country-<?php echo $ckey; ?>" class="ibwp-text ibwp-sp-conver-country-<?php echo $ckey; ?>" value="<?php echo ibwpl_esc_attr($country); ?>"><br/>
												</td>
											</tr>
											<tr valign="top">
												<?php $img1 = $image_url; ?>
												<th scope="row">
													<label for="ibwp-sp-conver-img-<?php echo $ckey; ?>"><?php _e('Custom Image', 'inboundwp-lite'); ?></label><br>
												</th>
												<td colspan="2">
													<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][image_url]" value="<?php echo esc_url($img1); ?>" id="ibwp-sp-conver-img-<?php echo $ckey; ?>"  class="ibwp-url ibwp-sp-img-upload-input ibwp-sp-conver-img-<?php echo $ckey; ?>" style="width: 80%;">
													<input type="button" name="ibwp_sp_default_img" class="button-secondary ibwp-sp-button ibwp-sp-image-upload" value="<?php _e( 'Upload Image', 'inboundwp-lite'); ?>" data-uploader-title="<?php _e('Choose Image', 'inboundwp-lite'); ?>" data-uploader-button-text="<?php _e('Insert Image', 'inboundwp-lite'); ?>" />
													<input type="button" name="ibwp_sp_default_img" id="ibwp-sp-default-img-clear" class="button button-secondary ibwp-sp-button ibwp-sp-image-clear" value="<?php _e( 'Clear', 'inboundwp-lite'); ?>" /> <br />
													<?php
													if( $img1 ) { 
														$img1 = '<img src="'.$img1.'" alt="" />';
													}
													?>
													<div class="ibwp-sp-img-view"><?php echo $img1; ?></div>
												</td>
											</tr>
											<tr valign="top">
												<th scope="row">
													<label for="ibwp-sp-conver-link-<?php echo $ckey; ?>"><?php _e('Custom Url', 'inboundwp-lite'); ?></label>
												</th>
												<td>
													<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][link]" id="ibwp-sp-conver-link-<?php echo $ckey; ?>" class="ibwp-url large-text ibwp-sp-conver-link-<?php echo $ckey; ?>" value="<?php echo esc_url($link); ?>"><br/>
												</td>
											</tr>
										</table>
									</div>
								</div>
							<?php } ?>
							
						</div>
						<div>
							<div class="ibwp-sp-add-new-row">
								<button type="button" class="button-secondary ibwp-sp-add-conversion"><?php _e('Add New', 'inboundwp-lite'); ?></button>
							</div>
						</div>
					</div>

				</td>
			</tr>
			<!-- Custom Meta fields end -->
		</tbody>
	</table>
</div>


<!-- Start Template for Coversion Clone -->
<template id="tmpl-ibwp-sp-conversion-row">
	<!-- Coversion custom fields -->
	<div class="ibwp-sp-row ibwp-sp-conversion-row" id="conversion_row_<?php echo $ckey; ?>" data-key="<?php echo $ckey; ?>">
		<div class="ibwp-sp-row-header">
			<span class="ibwp-sp-index ibwp-sp-conversion-index"><strong><?php _e('Conversion: ','inboundwp-lite'); ?></strong><span></span></span>
			<span class="ibwp-sp-row-actions">
				<a class="ibwp-sp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a> |
				<a class="ibwp-sp-remove-row ibwp-sp-delete"><?php _e('Remove', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-sp-row-standard-fields">
			<table class="form-table">
				<tr valign="top">
					<th scope="row">
						<label for="ibwp-sp-conver-title-<?php echo $ckey; ?>"><?php _e('Title', 'inboundwp-lite'); ?></label>
					</th>
					<td>
						<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][title]" id="ibwp-sp-conver-title-<?php echo $ckey; ?>" class="ibwp-text ibwp-sp-conver-title-<?php echo $ckey; ?>" value=""><br/>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="ibwp-sp-conver-display-name-<?php echo $ckey; ?>"><?php _e('Name', 'inboundwp-lite'); ?></label>
					</th>
					<td>
						<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][name]" id="ibwp-sp-conver-display-name-<?php echo $ckey; ?>" class="ibwp-text ibwp-sp-conver-display-name-<?php echo $ckey; ?>" value=""><br/>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="ibwp-sp-conver-email-<?php echo $ckey; ?>"><?php _e('Email Address', 'inboundwp-lite'); ?></label>
					</th>
					<td>
						<input type="email" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][email]" id="ibwp-sp-conver-email-<?php echo $ckey; ?>" class="ibwp-email ibwp-sp-conver-email-<?php echo $ckey; ?>" value="<?php echo ibwpl_esc_attr($email); ?>"><br/>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="ibwp-sp-conver-country-<?php echo $ckey; ?>"><?php _e('Country', 'inboundwp-lite'); ?></label>
					</th>
					<td>
						<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][country]" id="ibwp-sp-conver-country-<?php echo $ckey; ?>" class="ibwp-text ibwp-sp-conver-country-<?php echo $ckey; ?>" value="<?php echo ibwpl_esc_attr($country); ?>"><br/>
					</td>
				</tr>
				<tr valign="top">
					<?php $img1 = ""; ?>
					<th scope="row">
						<label for="ibwp-sp-conver-img-1"><?php _e('Custom Image', 'inboundwp-lite'); ?></label><br>
					</th>
					<td colspan="2">
						<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][image_url]" value="<?php echo esc_url($img1); ?>" id="ibwp-sp-conver-img-<?php echo $ckey; ?>" class="ibwp-url ibwp-sp-img-upload-input ibwp-sp-conver-img-<?php echo $ckey; ?>" style="width: 80%;">
						<input type="button" name="ibwp_sp_default_img" class="button-secondary ibwp-sp-button ibwp-sp-image-upload" value="<?php _e( 'Upload Image', 'inboundwp-lite'); ?>" data-uploader-title="<?php _e('Choose Image', 'inboundwp-lite'); ?>" data-uploader-button-text="<?php _e('Insert Image', 'inboundwp-lite'); ?>" />
						<input type="button" name="ibwp_sp_default_img" id="ibwp-sp-default-img-clear" class="button button-secondary ibwp-sp-button ibwp-sp-image-clear" value="<?php _e( 'Clear', 'inboundwp-lite'); ?>" /> <br />
						<?php
						if( $img1 ) { 
							$img1 = '<img src="'.$img1.'" alt="" />';
						}
						?>
						<div class="ibwp-sp-img-view"><?php echo $img1; ?></div>
					</td>
				</tr>
				<tr valign="top">
					<th scope="row">
						<label for="ibwp-sp-conver-link-<?php echo $ckey; ?>"><?php _e('Custom Url', 'inboundwp-lite'); ?></label>
					</th>
					<td>
						<input type="text" name="<?php echo $prefix ?>conversion[<?php echo $ckey; ?>][link]" id="ibwp-sp-conver-link-<?php echo $ckey; ?>" class="ibwp-url ibwp-sp-conver-link-<?php echo $ckey; ?>" value=""><br/>
					</td>
				</tr>
			</table>
		</div>
	</div>
</template>
<!-- End Template for Coversion Clone -->