<?php
/**
 * Custom Post Type Meta box file
 *
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;
$prefix = IBWP_SP_META_PREFIX; // Metabox prefix

// Getting stored metabox values
$selected_tab		= get_post_meta( $post->ID, $prefix. 'tab', true );
$enable				= get_post_meta( $post->ID, $prefix. 'enable', true );
$type				= get_post_meta( $post->ID, $prefix. 'type', true );
$source				= get_post_meta( $post->ID, $prefix. 'source', true );
$content			= get_post_meta( $post->ID, $prefix. 'content', true );
$conversions 		= get_post_meta( $post->ID, $prefix. 'conversion', true );
$conversions 		= !empty( $conversions ) ? $conversions : array( '1' => '' );
$reviews	 		= get_post_meta( $post->ID, $prefix. 'reviews', true );
$reviews 			= !empty( $reviews ) ? $reviews : array( '1' => '' );
$display_img		= get_post_meta( $post->ID, $prefix. 'display_img', true );
$link_to			= get_post_meta( $post->ID, $prefix. 'link_to', true );
$cust_url			= get_post_meta( $post->ID, $prefix. 'cust_url', true );

$display_for 		= get_post_meta( $post->ID, $prefix. 'display_for', true );
$int_delay			= get_post_meta( $post->ID, $prefix. 'int_delay', true );
$hide_after			= get_post_meta( $post->ID, $prefix. 'hide_after', true );
$delay_between		= get_post_meta( $post->ID, $prefix. 'delay_between', true );
$max_per_page		= get_post_meta( $post->ID, $prefix. 'max_per_page', true );
$show_close_btn		= get_post_meta( $post->ID, $prefix. 'show_close_btn', true );
$loop_repeat		= get_post_meta( $post->ID, $prefix. 'loop_repeat', true );
$link_target		= get_post_meta( $post->ID, $prefix. 'link_target', true );
$hide_on_mob		= get_post_meta( $post->ID, $prefix. 'hide_on_mob', true );
$hide_on_desk		= get_post_meta( $post->ID, $prefix. 'hide_on_desk', true );

$position = $distance = $show_animation = $hide_animation = $border_radius = $img_border_radius = $bg_color = $name_color = $title_color = $gen_txt_color = $star_color = '';

?>

<div id="ibwp-sp-settings" class="post-box-container ibwp-sp-settings">
	<div class="meta-box-sortables ui-sortable">
		<div id="general">
			<div class="ibwp-stabs-wrap ibwp-clearfix">			    
				<ul class="ibwp-stabs-nav-wrap">
					<li class="ibwp-stabs-nav ibwp-active-stab">
						<a href="#ibwp_sp_config_options"><?php _e('Config', 'inboundwp-lite'); ?></a>
					</li>
					<li class="ibwp-stabs-nav">
						<a href="#ibwp_sp_content_options"><?php _e('Content', 'inboundwp-lite'); ?></a>
					</li>					
					<li class="ibwp-stabs-nav">
						<a href="#ibwp_sp_appearance_options"><?php _e('Visibility', 'inboundwp-lite'); ?></a>
					</li>
					<li class="ibwp-stabs-nav">
						<a href="#ibwp_sp_design_options"><?php _e('Design', 'inboundwp-lite'); ?></a>
					</li>
				</ul>
				<div class="ibwp-cont-wrap">
					<?php
						include_once( IBWP_SP_DIR . '/includes/admin/metabox/config-meta.php' );					
						include_once( IBWP_SP_DIR . '/includes/admin/metabox/content-meta.php' );					
						include_once( IBWP_SP_DIR . '/includes/admin/metabox/visibility-meta.php' );					
						include_once( IBWP_SP_DIR . '/includes/admin/metabox/design-meta.php' );					
					?>
				<input type="hidden" value="<?php echo ibwpl_esc_attr($selected_tab); ?>" class="ibwp-selected-tab" name="<?php echo $prefix; ?>tab" />	
				</div><!-- end .sh-tabs-cnt-wrp -->
			</div>
		</div>				
	</div>	
</div>