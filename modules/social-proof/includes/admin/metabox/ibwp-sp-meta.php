<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @package Social Proof
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;
$prefix = IBWP_SP_META_PREFIX; // Metabox prefix

$sp_conver_post = get_post_meta( $post->ID, $prefix.'conver_post', true );
$sp_conver_post = !empty($sp_conver_post) ? $sp_conver_post : '';
?>

<table class="form-table ibwp-sp-page-sett-table">
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="ibwp-sp-conver-select"><?php _e('Conversion Notifications', 'inboundwp-lite'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>conver_post" class="ibwp-select ibwp-sp-conver-select" id="ibwp-sp-conver-select">
					<option value=""><?php _e('Select Notification','inboundwp-lite'); ?></option>
					<option value="disable" <?php selected('disable',$sp_conver_post);?>> <?php _e('Disable','inboundwp-lite');?> </option>
					<?php
					$conver_posts = ibwp_sp_post_by_type('conversion');
					foreach ( $conver_posts as $conver_post ) {
						echo '<option value="'. $conver_post->ID .'" '.selected( $sp_conver_post, $conver_post->ID ).'>'. $conver_post->post_title .'</option>';
					} ?>
				</select>
			</td>
		</tr>
	</tbody>
</table>