<?php
/**
 * Visibility meta html file
 *
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div id="ibwp_sp_appearance_options" class="ibwp-stabs-cnt ibwp-clearfix"  style="display:block;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-title"><?php _e('Visibility', 'inboundwp-lite'); ?></div>
		<span class="ibwp-spw-desc description"><?php _e('Visibility settings.', 'inboundwp-lite'); ?></span>
	</div><br>
	
	<table class="form-table">
		<tbody>		
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-display"><?php _e('Notification Display', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<select name="<?php echo $prefix; ?>display_for" id="ibwp-sp-display" class="ibwp-select ibwp-sp-display">
						<option <?php selected( $display_for, 'always' ); ?> value="always"><?php echo __( 'Always', 'inboundwp-lite' ); ?></option>
						<option <?php selected( $display_for, 'log-in' ); ?> value="log-in"><?php echo __( 'Log In Users', 'inboundwp-lite' ); ?></option>
						<option <?php selected( $display_for, 'log-out' ); ?> value="log-out"><?php echo __( 'Log Out Users', 'inboundwp-lite' ); ?></option>
					</select>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-int-delay"><?php _e('Initial Delay', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="number" name="<?php echo $prefix; ?>int_delay" id="ibwp-sp-int-delay" class="ibwp-number ibwp-sp-int-delay" value="<?php echo $int_delay; ?>" min="0" max="60" />
					<?php _e('Seconds','inboundwp-lite');?><br>
					<span class="description"><?php _e('Initial delay of displaying first notification.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top" class="ibwp-pro-disable">
				<th scope="row">
					<label for="ibwp-sp-hide-after"><?php _e('Visibility', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
				</th>
				<td class="no-padding">
					<input type="number" name="<?php echo $prefix; ?>hide_after" id="ibwp-sp-hide-after" class="ibwp-number ibwp-sp-hide-after" value="5" min="0" max="60">
					<?php _e('Seconds','inboundwp-lite');?><br>
					<span class="description"><?php _e('Hide the notification after the time given here.', 'inboundwp-lite'); ?></span><br><br>
				</td>
			</tr>
			<tr>
				<th></th>
				<td class="no-padding">
					<div class="ibwp-pro-notice">
						<?php echo sprintf( __('In lite version, Default visibility set 5 second. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to increase visibility seconds more than 5.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
					</div>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="ibwp-sp-delay-bet-notif"><?php _e('Delay Between Notification', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="number" name="<?php echo $prefix; ?>delay_between" class="ibwp-number ibwp-sp-delay-bet-notif" id="ibwp-sp-delay-bet-notif" value="<?php echo $delay_between; ?>" min="0" max="60" />
					<?php _e('Seconds','inboundwp-lite');?><br/>
					<span class="description"><?php _e('Delay time Between two Notification', 'inboundwp-lite'); ?></span>
					<br/>
				</td>
			</tr>
			<tr valign="top" class="ibwp-pro-disable">
				<th scope="row">
					<label for="ibwp-sp-max-per-page"><?php _e('Max Per Page', 'inboundwp-lite'); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></label>
				</th>
				<td class="no-padding">
					<input type="number" name="<?php echo $prefix; ?>max_per_page" id="ibwp-sp-max-per-page" class="ibwp-number ibwp-sp-max-per-page" value="5" min="0" max="30">
				</td>
			</tr>
			<tr>
				<th></th>
				<td class="no-padding">
					<div class="ibwp-pro-notice">
						<?php echo sprintf( __('In lite version, Default per page notification set only 5. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to increase per page notification more than 5.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
					</div>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-show-close-btn"><?php _e('Show Close Button', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix; ?>show_close_btn" id="ibwp-sp-show-close-btn" class="ibwp-checkbox ibwp-sp-show-close-btn" value="1" <?php checked( $show_close_btn, 1 ); ?>><br/>
					<span class="description"><?php _e('Check this box to enable close button.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-loop-repeat"><?php _e('Loop notification', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix; ?>loop_repeat" id="ibwp-sp-loop-repeat" class="ibwp-checkbox ibwp-sp-loop-repeat" value="1" <?php checked( $loop_repeat, 1 ); ?>><br/>
					<span class="description"><?php _e('Check this box to enable repeat notifications.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-link-target"><?php _e('Link Target', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix; ?>link_target" id="ibwp-sp-link-target" class="ibwp-checkbox ibwp-sp-link-target" value="1" <?php checked( $link_target, 1 ); ?>><br/>
					<span class="description"><?php _e('Check this box to enable link open in new tab.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-hide-on-mobile"><?php _e('Hide On Mobile', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix; ?>hide_on_mob" id="ibwp-sp-hide-on-mobile" class="ibwp-checkbox ibwp-sp-hide-on-mobile" value="1" <?php checked( $hide_on_mob, 1 ); ?>><br/>
					<span class="description"><?php _e('Check this box to disable box on mobile screen.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr valign="top">
				<th scope="row">
					<label for="ibwp-sp-hide-on-desktop"><?php _e('Hide On Desktop', 'inboundwp-lite'); ?></label>
				</th>
				<td>
					<input type="checkbox" name="<?php echo $prefix; ?>hide_on_desk" id="ibwp-sp-hide-on-desktop" class="ibwp-checkbox ibwp-sp-hide-on-desktop" value="1" <?php checked( $hide_on_desk, 1 ); ?> ><br/>
					<span class="description"><?php _e('Check this box to disable box on desktop screen.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
		</tbody>
	</table>
</div>