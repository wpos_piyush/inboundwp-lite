<h1><? _e('How it Wrok Social Proof', 'inboundwp-lite');  ?></h1>
<style type="text/css">
        .ibwp-mrgin-right-20{ margin-right: 20px; }
</style>

	<div class="post-box-container">
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2- ibwp-mrgin-right-20">
			
				<!--How to Use HTML -->
				<div id="post-body-content">
					<div class="metabox-holder">
						<div class="meta-box-sortables ui-sortable">    
							<div class="postbox">
								
								<h3 class="hndle">
									<span><?php _e( 'How It Works', 'inboundwp-lite' ); ?></span>
								</h3>
								
								<div class="inside">
									<table class="form-table">
										<tbody>
											<tr>
												<th>
													<label><?php _e('Short Description', 'inboundwp-lite'); ?>:</label>
												</th>
												<td>
													<p><?php _e('Social Proof is all in plugin that show notification to your visitor of site about your past woocommarce and Edd salse. you can also show cusotm sale notification with manual genrated records.', 'inboundwp-lite'); ?> </p>
												</td>
											</tr>

											<tr>
												<th>
													<label><?php _e('Getting Started Social Proof', 'inboundwp-lite'); ?>:</label>
												</th>
												<td>
													<ul>
														<li><?php _e('Step-1: Activate Social Proof.', 'inboundwp-lite'); ?></li>
														<li><?php _e('Step-2: Select Edd or Woocommarce or Genrated recored.', 'inboundwp-lite'); ?></li>
														<li><?php _e('Step-3: Create new genrated records.', 'inboundwp-lite'); ?></li>
                                                        <li><?php _e('Step-3: Set various settings as your need for social proof notification.', 'inboundwp-lite'); ?></li>
													</ul>
												</td>
											</tr>

											<tr>
												<th>
													<label><?php _e('Check Result', 'inboundwp-lite'); ?></label>
												</th>
												<td>
													<p><?php _e('You can check by visiting your site. how social proof nofification look.', 'inboundwp-lite'); ?></p><br/>
												</td>
											</tr>
										</tbody>
									</table>
								</div><!-- .inside -->
							</div><!-- #general -->
						</div><!-- .meta-box-sortables ui-sortable -->
					</div><!-- .metabox-holder -->
				</div><!-- #post-body-content -->
				
			</div><!-- #post-body -->
		</div><!-- #poststuff -->
	</div><!-- #post-box-container -->