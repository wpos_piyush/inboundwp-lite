<?php
/**
 * General Settings Page
 *
 * @package Social Proof
 * @since 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$glob_locs 				= ibwp_sp_get_option( 'glob_locs', array() );
$conversion 			= ibwp_sp_get_option( 'conversion' );
$conver_glob_locs 		= ibwp_sp_get_option( 'conver_glob_locs', array() );
$conver_target_locs		= ibwp_sp_get_option( 'conver_target_locs' );
$review 				= '';
$review_glob_locs 		= array();
$review_target_locs		= '';

$all_locations = ibwpl_display_locations();
?>
<div class="wrap ibwp-sett-wrap ibwp-analy-settings">
	<h2><?php _e( 'Settings', 'inboundwp-lite' ); ?></h2>

	<?php
	// Reset message
	if( !empty( $_POST['ibwp_sp_reset_settings'] ) ) {
		ibwpl_display_message( 'reset' );
	}

	// Success message
	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {
		ibwpl_display_message( 'update' );
	}
	?>

	<!-- Plugin reset settings form -->
	<form action="" method="post" id="ibwp-sp-reset-sett-form" class="ibwp-right ibwp-sp-reset-sett-form">
		<input type="submit" class="button button-primary right ibwp-btn ibwp-reset-sett ibwp-resett-sett-btn ibwp-sp-reset-sett" name="ibwp_sp_reset_settings" id="ibwp-sp-reset-sett" value="<?php _e( 'Reset All Settings', 'inboundwp-lite' ); ?>" />
	</form>

	<form action="options.php" method="POST" id="ibwp-analy-settings-form" class="ibwp-analy-settings-form">
	
		<?php
			settings_fields( 'ibwp_sp_plugin_options' );
		?>
		<div id="ibwp-analy-mc-sett" class="post-box-container ibwp-analy-mc-sett">

			<div class="textright ibwp-clearfix">
				<input type="submit" name="ibwp_sp_sett_submit" class="button button-primary right ibwp-btn ibwp-sp-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
			</div>

			<!-- Global Setting Options -->
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

						<h3 class="hndle">
							<span><?php _e( 'Global Post Selection Settings', 'inboundwp-lite' ); ?></span>
						</h3>
						<div class="inside">
							<table class="form-table ibwp-analy-mc-sett-tbl">
								<tbody>
									<tr valign="top" class="ibwp-sel-row">
										<th scope="row">
											<label for="ibwp-sp-global-locs"><?php _e('Global Locations', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php 
											$global_locations = ibwpl_display_locations('global');
											foreach ( $global_locations as $gkey => $gval ) { ?>
												<input type="checkbox" name="ibwp_sp_options[glob_locs][<?php echo $gkey; ?>]" id="ibwp-sp-global-locs-<?php echo $gkey; ?>" class="ibwp-checkbox ibwp-sp-global-locs" value="1" <?php if( array_key_exists($gkey, $glob_locs ) ) { checked( true ); } ?> />
												<span><?php echo $gval; ?></span><br/>
											<?php } ?>
											<span class="description"><?php _e('Check this box to enable custom notification options separately on post or pages.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Conversion Setting Options -->
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

						<h3 class="hndle">
							<span><?php _e( 'Conversion Settings', 'inboundwp-lite' ); ?></span>
						</h3>
						<div class="inside">
							<table class="form-table ibwp-analy-mc-sett-tbl">
								<tbody>
									<tr valign="top">
										<th scope="row">
											<label for="ibwp-sp-show-on"><?php _e('Select Conversion', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php $conversions = ibwp_sp_post_by_type('conversion'); ?>
											<select name="ibwp_sp_options[conversion]" class="ibwp-select2">
												<option value=""><?php _e('Select Conversion','inboundwp-lite'); ?></option>
												<?php foreach ($conversions as $key => $value) { ?>
													<option value="<?php echo $value->ID; ?>" <?php selected( $conversion, $value->ID ); ?> ><?php echo $value->post_title; ?></option>
												<?php } ?>
											</select>
										</td>
									</tr>
									<tr valign="top">
										<th scope="row">
											<label for="ibwp-sp-conver-global-locs"><?php _e('Global Locations', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php 
											foreach ( $all_locations as $gkey => $gval ) { ?>
												<input type="checkbox" name="ibwp_sp_options[conver_glob_locs][<?php echo $gkey; ?>]" id="ibwp-sp-conver-global-locs-<?php echo $gkey; ?>" class="ibwp-checkbox ibwp-sp-conver-global-locs" value="1" <?php if( array_key_exists($gkey, $conver_glob_locs ) ) { checked( true ); } ?> />
												<span><?php echo $gval; ?></span><br/>
											<?php } ?>
											<span class="description"><?php _e('Check this box to enable conversion notification globally.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr valign="top">
										<th scope="row">
											<label for="ibwp-sp-conver-target-locs"><?php _e('Target Locations Only', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<textarea name="ibwp_sp_options[conver_target_locs]" id="ibwp-sp-conver-target-locs" class="ibwp-textarea ibwp-sp-conver-target-locs" rows="10" cols="80"><?php echo esc_textarea( $conver_target_locs ); ?></textarea><br/>
											<span class="description"><?php _e('Enter one location fragment per line.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Review Setting Options -->
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

						<h3 class="hndle">
							<span><?php _e( 'Review Settings', 'inboundwp-lite' ); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></span>
						</h3>
						<div class="inside">
							<div class="ibwp-pro-notice">
								<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, To enable Review notification.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
							</div>
							<table class="form-table ibwp-analy-mc-sett-tbl ibwp-pro-disable">
								<tbody>
									<tr valign="top">
										<th scope="row">
											<label for="ibwp-sp-show-on"><?php _e('Select Reviews', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php $reviews = ibwp_sp_post_by_type('reviews'); ?>
											<select name="ibwp_sp_options[reviews]" class="ibwp-select2">
												<option value=""><?php _e('Select Review','inboundwp-lite'); ?></option>
											</select>
										</td>
									</tr>
									<tr valign="top">
										<th scope="row">
											<label for="ibwp-sp-review-global-locs"><?php _e('Global Locations', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php 
											foreach ( $all_locations as $gkey => $gval ) { ?>
												<input type="checkbox" name="ibwp_sp_options[review_glob_locs][<?php echo $gkey; ?>]" id="ibwp-sp-review-global-locs-<?php echo $gkey; ?>" class="ibwp-checkbox ibwp-sp-review-global-locs" value="1" <?php if( array_key_exists($gkey, $review_glob_locs ) ) { checked( true ); } ?>>
												<span><?php echo $gval; ?></span><br/>
											<?php } ?>
											<span class="description"><?php _e('Check this box to enable review notification globally.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr valign="top">
										<th scope="row">
											<label for="ibwp-sp-review-target-locs"><?php _e('Target Locations Only', 'inboundwp-lite'); ?></label>
										</th>
										<td >
											<textarea name="ibwp_sp_options[review_target_locs]" id="ibwp-sp-review-target-locs" class="ibwp-textarea ibwp-sp-review-target-locs" rows="10" cols="80" ><?php echo esc_textarea( $review_target_locs ); ?></textarea><br/>
											<span class="description"><?php _e('Enter one location fragment per line.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="textright ibwp-clearfix">
				<input type="submit" name="ibwp_sp_sett_submit" class="button button-primary right ibwp-btn ibwp-sp-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
			</div>
		</div>
	</form>
</div>