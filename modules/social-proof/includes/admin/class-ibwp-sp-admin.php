<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package InboundWP
 * @subpackage Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class IBWP_Sp_Admin {
	
	function __construct() {

		// Action to register admin menu
		add_action( 'admin_menu', array( $this, 'ibwp_sp_register_menu' ), 9 );

		// Action to register admin menu
		add_action( 'admin_init', array( $this, 'ibwp_sp_register_settings' ) );

		// Action to add metabox
		add_action( 'add_meta_boxes', array( $this, 'ibwp_sp_posts_metabox' ) );

		// Action to save metabox
		add_action( 'save_post', array( $this, 'ibwp_sp_save_metabox' ) );

		// Action to display preview in admin 
		add_action( 'admin_footer', array( $this, 'ibwp_sp_display_notif_view' ) );
	}

	/**
	 * Function to register admin menus
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_register_menu() {

		// Add setting page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_SP_POST_TYPE, __('General Settings', 'inboundwp-lite'), __('Settings', 'inboundwp-lite'), 'manage_options', 'ibwp-sp-settings', array($this, 'ibwp_sp_settings') );

		// Add how it works page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_SP_POST_TYPE, __('How it work', 'inboundwp-lite'), __('How it work', 'inboundwp-lite'), 'manage_options', 'ibwp-sp-how-it-work', array($this, 'ibwp_sp_how_it_work') );
	}

	/**
	 * Function to handle the setting page html
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_settings() {
		include_once( IBWP_SP_DIR . '/includes/admin/settings/ibwp-sp-settings.php' );
	}

	/**
	 * Function to handle the about page html
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_how_it_work() {
		include_once( IBWP_SP_DIR . '/includes/admin/ibwp-sp-how-it-work.php' );
	}

	/**
	 * Function register setings
	 * 
	 * @subpackage Spin Wheel Pro
 	 * @since 1.0
	 */
	function ibwp_sp_register_settings() {

		// Reset default settings
		if( !empty( $_POST['ibwp_sp_reset_settings'] ) ) {
			ibwp_sp_default_settings();
		}

		register_setting( 'ibwp_sp_plugin_options', 'ibwp_sp_options', array($this, 'ibwp_sp_validate_options') );
	}

	/**
	 * Validate Settings Options
	 * 
	 * @subpackage Spin Wheel Pro
 	 * @since 1.0
	 */
	function ibwp_sp_validate_options( $input ) {
		
		$input['glob_locs'] 			= isset( $input['glob_locs'] ) 			? ibwpl_clean( $input['glob_locs'] ) 						: array();
		$input['conversion']			= isset( $input['conversion'] ) 		? ibwpl_clean_number( $input['conversion'] ) 				: '';
		$input['conver_glob_locs'] 		= isset( $input['conver_glob_locs'] ) 	? ibwpl_clean( $input['conver_glob_locs'] ) 					: array();
		$input['conver_target_locs'] 	= isset( $input['conver_target_locs'] ) ? sanitize_textarea_field( $input['conver_target_locs'] ) 	: '';
		
		$conver_target_locs = explode("\n", $input['conver_target_locs'] );
		if( count($conver_target_locs) > 1 ) {
			$conver_target_locs = ibwpl_clean( $conver_target_locs );
			$conver_target_locs = array_unique( $conver_target_locs );
			$conver_target_locs = array_filter( $conver_target_locs );
			$input['conver_target_locs'] = implode("\n", $conver_target_locs);
		}

		return $input;
	}

	/**
	 * Custom Post Type Settings Metabox
	 * 
	 * @package Social Proof
	 * @since 1.0
	 */
	function ibwp_sp_posts_metabox() {

		global $ibwp_sp_options, $pagenow;

		// Adding Social Proof Selection Metabox Globally into Post Types
		if( !empty ( $ibwp_sp_options['glob_locs'] ) ) {
			$sp_post_types = array_keys($ibwp_sp_options['glob_locs']);
			if( !empty( $sp_post_types ) && ( $pagenow == 'post.php' || $pagenow == 'post-new.php' ) ) {
				add_meta_box( 'ibwp-sp-post-type-sett', __( 'Social Proof - IBWP', 'inboundwp-lite' ), array($this, 'ibwp_sp_post_type_sett'), $sp_post_types, 'normal', 'high' );
			}
		}

		// Add Social Proof Setting Metabox  
		add_meta_box( 'ibwp-sp-post-sett', __( 'Social Proof Settings', 'inboundwp-lite' ), array($this, 'ibwp_sp_post_sett') , IBWP_SP_POST_TYPE, 'normal', 'high' );
	}

	/**
	* Social Proof Post Type Setting Page
	*
	* @package Social Proof
	* @since 1.0
	*/
	function ibwp_sp_post_type_sett() {
		require IBWP_SP_DIR .'/includes/admin/metabox/ibwp-sp-meta.php';
	}

	/**
	* Custom Post Type Setting Page
	*
	* @package Social Proof
	* @since 1.0
	*/
	function ibwp_sp_post_sett() {
		require IBWP_SP_DIR .'/includes/admin/metabox/ibwp-sp-cpt-meta.php';
	}

	/**
	* Save metabox value
	* 
	* @package Social Proof
	* @since 1.0
	*/
	function ibwp_sp_save_metabox( $post_id ) {

		global $post_type;
        $prefix = IBWP_SP_META_PREFIX;

    
        // Saving single post meta
        $conver_post_id = isset($_POST[$prefix.'conver_post'])	? ibwpl_clean($_POST[$prefix.'conver_post']) : ''; 
		update_post_meta( $post_id, $prefix.'conver_post', $conver_post_id );
		
		// Verify if this is an auto save routine.
        if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return $post_id;
        }
       
        if ( ( $post_type !=  IBWP_SP_POST_TYPE ) ) {
			if ( ! current_user_can( 'edit_page', $post_id ) || ! current_user_can( 'edit_post', $post_id ) ) {
                return $post_id;
            }
        }

        $data['tab'] 		= isset( $_POST[$prefix.'tab'] ) 		? ibwpl_clean( $_POST[$prefix.'tab'] ) 		: '#ibwp_sp_config_options';

        /*Content Options*/
        $data['enable'] 	= isset( $_POST[$prefix.'enable'] ) 	? 1 										: 0;
		$data['type'] 		= isset( $_POST[$prefix.'type'] ) 		? ibwpl_clean( $_POST[$prefix.'type'] ) 	: __('conversion','inboundwp-lite');
		$data['source'] 	= isset( $_POST[$prefix.'source'] ) 	? ibwpl_clean( $_POST[$prefix.'source'] ) 	: __('custom','inboundwp-lite');
		$data['content'] 	= isset( $_POST[$prefix.'content'] ) 	? ibwpl_clean( $_POST[$prefix.'content'] ) 	: '';
		
		if ( empty( $data['content'] ) ) {
			if( $data['type'] == 'conversion' ) {
				$data['content'] = "{{name}} purchased {{title}}";
			} else {
				$data['content'] = "{{title}} {{name}}";
			}
		}
        
        $conversion = isset( $_POST[$prefix.'conversion'] ) ? ibwpl_clean( $_POST[$prefix.'conversion'] ) : array(); // Need to check
        if ( !empty( $conversion ) ) {
        	foreach ($conversion as $con_key => $con_val) {
        		if( !empty( $con_val['title'] ) ) {
	        		$data['conversion'][$con_key]['title'] 		= isset( $con_val['title'] ) 		? ibwpl_clean( $con_val['title'] ) 			: '';
					$data['conversion'][$con_key]['name'] 		= isset( $con_val['name'] ) 		? ibwpl_clean( $con_val['name'] ) 			: '';
					$data['conversion'][$con_key]['email'] 		= isset( $con_val['email'] ) 		? ibwpl_check_email( $con_val['email'] ) 	: '';
					$data['conversion'][$con_key]['country'] 	= isset( $con_val['country'] )		? ibwpl_clean( $con_val['country'] ) 		: '';
					$data['conversion'][$con_key]['image_url'] 	= isset( $con_val['image_url'] ) 	? ibwpl_clean_url( $con_val['image_url'] ) 	: '';
					$data['conversion'][$con_key]['link'] 		= isset( $con_val['link'] ) 		? ibwpl_clean_url( $con_val['link'] ) 		: '';
					$data['conversion'][$con_key]['time'] 		= current_time('mysql');
				}
        	}
        }

        $data['display_img'] 	= isset( $_POST[$prefix.'display_img'] ) 	? 1 											: 0;
    	$data['link_to'] 		= isset( $_POST[$prefix.'link_to'] ) 		? ibwpl_clean( $_POST[$prefix.'link_to'] ) 		: 'product';
    	if( $data['link_to'] == 'custom_url' ) {
    		$data['cust_url'] 	= isset( $_POST[$prefix.'cust_url'] ) 		? ibwpl_clean_url( $_POST[$prefix.'cust_url'] ) : '';
    	}

        /*Appearence Options*/
		$data['display_for'] 		= isset( $_POST[$prefix.'display_for'] ) 		? ibwpl_clean( $_POST[$prefix.'display_for'] ) 			: __('always','inboundwp-lite');
		$data['int_delay'] 			= !empty( $_POST[$prefix.'int_delay'] ) 		? ibwpl_clean_number( $_POST[$prefix.'int_delay'] ) 	: 10;
		$data['hide_after'] 		= !empty( $_POST[$prefix.'hide_after'] ) 		? ibwpl_clean_number( $_POST[$prefix.'hide_after'] ) 	: 5;
		$data['delay_between'] 		= !empty( $_POST[$prefix.'delay_between'] ) 	? ibwpl_clean_number( $_POST[$prefix.'delay_between'] ) : 30;
		$data['max_per_page'] 		= !empty( $_POST[$prefix.'max_per_page'] ) 		? ibwpl_clean_number( $_POST[$prefix.'max_per_page'] ) 	: 5;
		$data['show_close_btn'] 	= isset( $_POST[$prefix.'show_close_btn'] ) 	? 1 													: 0;
		$data['loop_repeat'] 		= isset( $_POST[$prefix.'loop_repeat'] ) 		? 1 													: 0;
		$data['link_target'] 		= isset( $_POST[$prefix.'link_target'] ) 		? 1 													: 0;
		$data['hide_on_mob'] 		= isset( $_POST[$prefix.'hide_on_mob'] ) 		? 1 													: 0;
		$data['hide_on_desk'] 		= isset( $_POST[$prefix.'hide_on_desk'] ) 		? 1 													: 0;

		if( !empty( $data ) ){
			foreach ($data as $mkey => $mval) {
				update_post_meta( $post_id, $prefix.$mkey, $mval );
			}
		}
    }

    /**
	* Display notification preview in admin footer
	* 
	* @package Social Proof
	* @since 1.0
	*/
    function ibwp_sp_display_notif_view( $data ) { 

    	global $post, $pagenow;
    	
    	if( ( $pagenow == 'post.php' && !empty( $post ) && $post->post_type == 'ibwp_sp' ) || ( $pagenow == 'post-new.php' && isset($_GET['post_type']) && $_GET['post_type'] == 'ibwp_sp' ) ) {
    		include_once IBWP_SP_DIR .'/includes/view/admin-view.php';
    	}
    }
}

$ibwp_sp_admin = new IBWP_Sp_Admin();