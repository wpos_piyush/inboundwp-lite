<?php
/**
 * Functions File
 *
 * @package Social Proof
 * @since 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Update default settings
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_default_settings() {

    global $ibwp_sp_options;

    $ibwp_sp_options = array(
        'glob_locs'             => array(),
        'conversion'            => '',
        'conver_glob_locs'      => array(),
        'conver_target_locs'    => '',
    );

    $default_options = apply_filters('ibwp_sp_options_default_values', $ibwp_sp_options );

    // Update default options
    update_option( 'ibwp_sp_options', $default_options );

    // Overwrite global variable when option is update  
    $ibwp_sp_options = ibwpl_get_settings( 'ibwp_sp_options' ); 
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_get_option( $key = '', $default = false ) {

    global $ibwp_sp_options;
    $value  = ! empty( $ibwp_sp_options[ $key ] ) ? $ibwp_sp_options[ $key ] : $default;
    return $value;
}

/**
 * Animation in value
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_animate_in_opt($val){
    ?>
    <optgroup label="Attention Seekers">
        <option <?php selected( $val, "bounce" ); ?> value="bounce"><?php _e('bounce','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "flash" ); ?> value="flash"><?php _e('flash','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "pulse" ); ?> value="pulse"><?php _e('pulse','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rubberBand" ); ?> value="rubberBand"><?php _e('rubberBand','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "shake" ); ?> value="shake"><?php _e('shake','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "swing" ); ?> value="swing"><?php _e('swing','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "tada" ); ?> value="tada"><?php _e('tada','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "wobble" ); ?> value="wobble"><?php _e('wobble','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "jello" ); ?> value="jello"><?php _e('jello','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "heartBeat" ); ?> value="heartBeat"><?php _e('heartBeat','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Bouncing Entrances">
        <option <?php selected( $val, "bounceIn" ); ?> value="bounceIn"><?php _e('bounceIn','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceInDown" ); ?> value="bounceInDown"><?php _e('bounceInDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceInLeft" ); ?> value="bounceInLeft"><?php _e('bounceInLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceInRight" ); ?> value="bounceInRight"><?php _e('bounceInRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceInUp" ); ?> value="bounceInUp"><?php _e('bounceInUp','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Fading Entrances">
        <option <?php selected( $val, "fadeIn" ); ?> value="fadeIn"><?php _e('fadeIn','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInDown" ); ?> value="fadeInDown"><?php _e('fadeInDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInDownBig" ); ?> value="fadeInDownBig"><?php _e('fadeInDownBig','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInLeft" ); ?> value="fadeInLeft"><?php _e('fadeInLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInLeftBig" ); ?> value="fadeInLeftBig"><?php _e('fadeInLeftBig','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInRight" ); ?> value="fadeInRight"><?php _e('fadeInRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInRightBig" ); ?> value="fadeInRightBig"><?php _e('fadeInRightBig','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInUp" ); ?> value="fadeInUp"><?php _e('fadeInUp','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeInUpBig" ); ?> value="fadeInUpBig"><?php _e('fadeInUpBig','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Flippers Entrances">
        <option <?php selected( $val, "flip" ); ?> value="flip"><?php _e('flip','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "flipInX" ); ?> value="flipInX"><?php _e('flipInX','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "flipInY" ); ?> value="flipInY"><?php _e('flipInY','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Lightspeed">
        <option <?php selected( $val, "lightSpeedIn" ); ?> value="lightSpeedIn"><?php _e('lightSpeedIn','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Rotating Entrances">
        <option <?php selected( $val, "rotateIn" ); ?> value="rotateIn"><?php _e('rotateIn','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateInDownLeft" ); ?> value="rotateInDownLeft"><?php _e('rotateInDownLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateInDownRight" ); ?> value="rotateInDownRight"><?php _e('rotateInDownRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateInUpLeft" ); ?> value="rotateInUpLeft"><?php _e('rotateInUpLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateInUpRight" ); ?> value="rotateInUpRight"><?php _e('rotateInUpRight','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Sliding Entrances">
        <option <?php selected( $val, "slideInUp" ); ?> value="slideInUp"><?php _e('slideInUp','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "slideInDown" ); ?> value="slideInDown"><?php _e('slideInDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "slideInLeft" ); ?> value="slideInLeft"><?php _e('slideInLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "slideInRight" ); ?> value="slideInRight"><?php _e('slideInRight','inboundwp-lite'); ?></option>

    </optgroup>
    
    <optgroup label="Zoom Entrances">
        <option <?php selected( $val, "zoomIn" ); ?> value="zoomIn"><?php _e('zoomIn','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomInDown" ); ?> value="zoomInDown"><?php _e('zoomInDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomInLeft" ); ?> value="zoomInLeft"><?php _e('zoomInLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomInRight" ); ?> value="zoomInRight"><?php _e('zoomInRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomInUp" ); ?> value="zoomInUp"><?php _e('zoomInUp','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Specials">
        <option <?php selected( $val, "hinge" ); ?> value="hinge"><?php _e('hinge','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "jackInTheBox" ); ?> value="jackInTheBox"><?php _e('jackInTheBox','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rollIn" ); ?> value="rollIn"><?php _e('rollIn','inboundwp-lite'); ?></option>
    </optgroup>
    <?php
}

/**
 * Animation out value
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_animate_out_opt($val){
    ?>
    <optgroup label="Bouncing Exits">
        <option <?php selected( $val, "bounceOut" ); ?> value="bounceOut"><?php _e('bounceOut','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceOutDown" ); ?> value="bounceOutDown"><?php _e('bounceOutDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceOutLeft" ); ?> value="bounceOutLeft"><?php _e('bounceOutLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceOutRight" ); ?> value="bounceOutRight"><?php _e('bounceOutRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "bounceOutUp" ); ?> value="bounceOutUp"><?php _e('bounceOutUp','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Fading Exits">
        <option <?php selected( $val, "fadeOut" ); ?> value="fadeOut"><?php _e('fadeOut','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutDown" ); ?> value="fadeOutDown"><?php _e('fadeOutDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutDownBig" ); ?> value="fadeOutDownBig"><?php _e('fadeOutDownBig','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutLeft" ); ?> value="fadeOutLeft"><?php _e('fadeOutLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutLeftBig" ); ?> value="fadeOutLeftBig"><?php _e('fadeOutLeftBig','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutRight" ); ?> value="fadeOutRight"><?php _e('fadeOutRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutRightBig" ); ?> value="fadeOutRightBig"><?php _e('fadeOutRightBig','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutUp" ); ?> value="fadeOutUp"><?php _e('fadeOutUp','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "fadeOutUpBig" ); ?> value="fadeOutUpBig"><?php _e('fadeOutUpBig','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Flippers Exits">
        <option <?php selected( $val, "flipOutX" ); ?> value="flipOutX"><?php _e('flipOutX','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "flipOutY" ); ?> value="flipOutY"><?php _e('flipOutY','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Lightspeed">
        <option <?php selected( $val, "lightSpeedOut" ); ?> value="lightSpeedOut"><?php _e('lightSpeedOut','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Rotating Exits">
        <option <?php selected( $val, "rotateOut" ); ?> value="rotateOut"><?php _e('rotateOut','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateOutDownLeft" ); ?> value="rotateOutDownLeft"><?php _e('rotateOutDownLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateOutDownRight" ); ?> value="rotateOutDownRight"><?php _e('rotateOutDownRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateOutUpLeft" ); ?> value="rotateOutUpLeft"><?php _e('rotateOutUpLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "rotateOutUpRight" ); ?> value="rotateOutUpRight"><?php _e('rotateOutUpRight','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Sliding Exits">
        <option <?php selected( $val, "slideOutUp" ); ?> value="slideOutUp"><?php _e('slideOutUp','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "slideOutDown" ); ?> value="slideOutDown"><?php _e('slideOutDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "slideOutLeft" ); ?> value="slideOutLeft"><?php _e('slideOutLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "slideOutRight" ); ?> value="slideOutRight"><?php _e('slideOutRight','inboundwp-lite'); ?></option>
        
    </optgroup>

    <optgroup label="Zoom Exits">
        <option <?php selected( $val, "zoomOut" ); ?> value="zoomOut"><?php _e('zoomOut','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomOutDown" ); ?> value="zoomOutDown"><?php _e('zoomOutDown','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomOutLeft" ); ?> value="zoomOutLeft"><?php _e('zoomOutLeft','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomOutRight" ); ?> value="zoomOutRight"><?php _e('zoomOutRight','inboundwp-lite'); ?></option>
        <option <?php selected( $val, "zoomOutUp" ); ?> value="zoomOutUp"><?php _e('zoomOutUp','inboundwp-lite'); ?></option>
    </optgroup>

    <optgroup label="Specials">
        <option <?php selected( $val, "rollOut" ); ?> value="rollOut"><?php _e('rollOut','inboundwp-lite'); ?></option>
    </optgroup>
    <?php
}

/**
 * Function to get post data by type
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_post_by_type( $type = '' ) {

    $args = array(
        'post_type'         => IBWP_SP_POST_TYPE, 
        'post_status'       => 'publish', 
        'posts_per_page'    => -1,
        'order'             => 'DESC',
        'suppress_filters'  => false,
    );

    $args['meta_query'][0] = array( 
        'key'       => IBWP_SP_META_PREFIX.'enable',
        'value'     => '1',
        'compare'   => '='
    );

    if( $type == 'conversion' ) {

        $args['meta_query'][1] = array( 
            'key'       => IBWP_SP_META_PREFIX.'type',
            'value'     => 'conversion',
            'compare'   => '='
        );
    }

    if( $type == 'reviews' ) {

        $args['meta_query'][1] = array( 
            'key'       => IBWP_SP_META_PREFIX.'type',
            'value'     => 'reviews',
            'compare'   => '='
        );
    }

    $sp_posts = get_posts( $args );
    return $sp_posts;
}

/**
 * Function to get edd purchase records
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_get_edd_data( $type ) {

    if( $type == 'reviews' ) {

        $args = array(
            'type'        => array( 'edd_review' ),
            'number'      => 5,
            'status'      => 'approve', 
            'post_status' => 'publish', 
            'post_type'   => 'download',
            'meta_query'  => array(
                array(
                    'key'     => 'edd_review_approved',
                    'value'   => 1,
                    'compare' => '='
                ),
            ), 
        );
        remove_action( 'pre_get_comments', array( edd_reviews(), 'hide_reviews' ) );

        $edd_comments = get_comments( $args );
        foreach ( $edd_comments  as $edd_key => $edd_val ) {
            $name       = $edd_val->comment_author;
            $date       = $edd_val->comment_date;
            $time       = human_time_diff( strtotime($date), current_time('timestamp') );
            $title      = get_comment_meta( $edd_val->comment_ID, 'edd_review_title', true );
            $link       = get_permalink($edd_val->comment_post_ID);
            $image_url  = get_the_post_thumbnail_url( $edd_val->comment_post_ID,'thumbnail');
            $star       = get_comment_meta( $edd_val->comment_ID, 'edd_rating', true );
            $response[] = compact('name','time','link','title','star','image_url');
        }

    } else {

        $quantity = 1;
        $args_payments = array(
            'posts_per_page' => 5,
            'post_status' => 'publish',            
            'post_type' => 'edd_payment',
            'order' => 'DESC',
            'orderby' => 'date',
        );
        $payments = get_posts( $args_payments );
        
        foreach ( $payments as $payment ) { 
            
            setup_postdata($payment);
            $meta = get_post_meta($payment->ID, '_edd_payment_meta' );
            
            $fname      = $meta[0]['user_info']['first_name'];
            $lname      = $meta[0]['user_info']['last_name'];
            $name       = $fname." ".$lname;
            $countries  = edd_get_country_list();
            $country    = $countries[ $meta[0]['user_info']['address']['country'] ];
            $date       = $payment->post_date;
            $time       = human_time_diff( strtotime($date), current_time('timestamp') );
            $cart       = $meta[0]['cart_details'];
            $link       = get_permalink($cart[0]['id']);
            $image_url  = get_the_post_thumbnail_url($cart[0]['id']);
            $title      = $cart[0]['name'];
            
            $response[] = compact('name','time','link','title','image_url','country');
        }
    }

    return $response;
}

/**
 * Function to get woo purchase records
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_get_woo_data( $type ) {

    //$wc_total =  wp_count_posts('shop_order');
    //$woo_total = $wc_total->{'wc-processing'} + $wc_total->{'wc-completed'} ;

    if( $type == 'reviews' ) {

         $args = array( 
                'number'      => 5, 
                'status'      => 'approve', 
                'post_status' => 'publish', 
                'post_type'   => 'product' 
        );

        $woo_comments = get_comments( $args );

        foreach ( $woo_comments  as $woo_key => $woo_val ) {
            $name       = $woo_val->comment_author;
            $date       = $woo_val->comment_date;
            $time       = human_time_diff( strtotime($date), current_time('timestamp') );
            $title      = $woo_val->comment_content;
            $link       = get_permalink($woo_val->comment_post_ID);
            $image_url  = get_the_post_thumbnail_url( $woo_val->comment_post_ID,'thumbnail');
            $star       = get_comment_meta( $woo_val->comment_ID, 'rating', true );
            $response[] = compact('name','time','link','title','star','image_url');
        }

    } else {

        $args = array(
            'post_type' => 'shop_order',
            'post_status' => ['wc-processing', 'wc-completed'],
            'posts_per_page' => -1,
            'order' => 'DESC',
            'orderby' => 'date',
        );
        $posts = get_posts( $args );

        foreach( $posts as $post ) {
            
            $order      = new WC_Order($post->ID);
            $name       = $order->get_formatted_billing_full_name();
            $country    = WC()->countries->countries[ $order->shipping_country ];
            $date       = $order->order_date;
            $time       = human_time_diff( strtotime($date), current_time('timestamp') );
            $products   = $order->get_items(); // get products in current order
            
            foreach ($order->get_items() as $item_data) {
                $product    = $item_data->get_product();
                $title      = $product->get_name();
                $image_url  = get_the_post_thumbnail_url( $product->get_id(),'thumbnail');
                $link       = get_permalink($product->get_id());
                break;
            }
            $response[] = compact('name','time','link','title','image_url','country');
        }
    }

    return $response;
}

/**
 * Function to get custom records
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_get_custom_data( $id, $type ) {

    if( ! $id ) {
        return;
    }

    if( $type == 'reviews' ) {

        $reviews = get_post_meta( $id, IBWP_SP_META_PREFIX. 'reviews', true );
        
        foreach ($reviews as $rkey => $rval) {
            $name       = isset( $rval['name'] )        ? $rval['name']         : '';
            $time       = isset( $rval['time'] )        ? $rval['time']         : '';
            $time       = human_time_diff( strtotime($time), current_time('timestamp') );
            $title      = isset( $rval['title'] )       ? $rval['title']        : '';
            $link       = isset( $rval['link'] )        ? $rval['link']         : '';
            $image_url  = isset( $rval['image_url'] )   ? $rval['image_url']    : '';
            $star       = isset( $rval['star'] )        ? $rval['star']         : '';
            $response[] = compact('name','time','link','title','star','image_url');
        }
    }

    if( $type == 'conversion' ) { 

        $conversions = get_post_meta( $id, IBWP_SP_META_PREFIX . 'conversion', true );

        foreach ($conversions as $ckey => $cval) {
            $name       = isset( $cval['name'] )        ? $cval['name']         : '';
            $email      = isset( $cval['email'] )       ? $cval['email']        : '';
            $country    = isset( $rval['country'] )     ? $rval['country']      : '';
            $time       = isset( $cval['time'] )        ? $cval['time']         : '';
            $time       = human_time_diff( strtotime($time), current_time('timestamp') );
            $title      = isset( $cval['title'] )       ? $cval['title']        : '';
            $link       = isset( $cval['link'] )        ? $cval['link']         : '';
            $image_url  = isset( $cval['image_url'] )   ? $cval['image_url']    : '';
            $response[] = compact('name','time','link','title','image_url','email','country');
        }
    }
    
    return $response;
}

/**
 * Function to check target urls
 * 
 * @package Social Proof
 * @since 1.0
 */
function ibwp_sp_match_path( $patterns ) {

    $patterns_safe = array();

    // Get the request URI from WP
    list($url_request) = explode( '?', $_SERVER['REQUEST_URI'] );
    $url_request = trim( trim( $url_request ), '/' );

    $rows = explode( "\n", $patterns );
    foreach ( $rows as $pattern ) {

        // Trim trailing, leading slashes and whitespace
        $pattern = trim( trim( $pattern ), '/' );

        // Escape regex chars
        $pattern = preg_quote( $pattern, '/' );

        // Enable wildcard checks
        $pattern = str_replace( '\*', '.*', $pattern );

        $patterns_safe[] = $pattern;
    }

    // Remove empty patterns
    $patterns_safe = array_filter( $patterns_safe );
    $regexps = sprintf(
            '/^(%s)$/i',
            implode( '|', $patterns_safe )
        );

    return preg_match( $regexps, $url_request );
}