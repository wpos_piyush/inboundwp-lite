jQuery( document ).ready(function($) {
	
	// Media Uploader
	$( document ).on( 'click', '.ibwp-sp-image-upload', function() {
		
		var imgfield,showfield;
		//imgfield = jQuery(this).prev('input').attr('id');
		imgfield = jQuery(this).prev('input');//.attr('id');
		showfield = jQuery(this).parents('td').find('.ibwp-sp-img-view');
    	
					
			var file_frame;
			
			//new media uploader
			var button = jQuery(this);
		
			// If the media frame already exists, reopen it.
			if ( file_frame ) {
				file_frame.open();
			  return;
			}
	
			// Create the media frame.
			file_frame = wp.media.frames.file_frame = wp.media({
				frame: 'post',
				state: 'insert',
				title: button.data( 'uploader-title' ),
				button: {
					text: button.data( 'uploader-button-text' ),
				},
				multiple: false,  // Set to true to allow multiple files to be selected
				type : 'image',
				//size:  'image-size',
			});

			file_frame.on( 'menu:render:default', function(view) {
		        // Store our views in an object.
		        var views = {};
	
		        // Unset default menu items
		        view.unset('library-separator');
		        view.unset('gallery');
		        view.unset('featured-image');
		        view.unset('embed');
	
		        // Initialize the views in our view object.
		        view.set(views);
		    });
	
			// When an image is selected, run a callback.
			file_frame.on( 'insert', function() {
	
				// Get selected size from media uploader
				var selected_size = $('.attachment-display-settings .size').val();
				
				var selection = file_frame.state().get('selection');
				selection.each( function( attachment, index ) {
					attachment = attachment.toJSON();
					attachment_id = attachment.id;
					
					$('#ibwp-sp-attachment-id').val(attachment_id);
					// Selected attachment url from media uploader
					//var attachment_url = attachment.sizes[selected_size].url;
					var attachment_url = attachment.sizes.thumbnail.url;
					
					if(index == 0){
						// place first attachment in field
						//$('#'+imgfield).val(attachment_url);
						imgfield.val(attachment_url);
						showfield.html('<img src="'+attachment_url+'" />');
						
					} else{
						//$('#'+imgfield).val(attachment_url);
						imgfield.val(attachment_url);
						showfield.html('<img src="'+attachment_url+'" />');
					}
				});
			});
	
			// Finally, open the modal
			file_frame.open();
		
	});

	// Clear Media
	$( document ).on( 'click', '.ibwp-sp-image-clear', function() {
		$(this).parent().find('.ibwp-sp-img-upload-input').val('');
		$(this).parent().find('.ibwp-sp-img-view').html('');
		$('#ibwp-sp-attachment-id').val('');
	});

	//on email list change chage url of 
	// $( "#msllc-property-email-pages" ).change(function() {
	// 	var item=$(this).val();
	// 	//alert(item.val())
	// 	$('iframe.msllc-preview').attr('src',item);
	// });

	
});