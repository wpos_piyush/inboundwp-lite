jQuery(document).ready(function ($) { 

    if ( 'undefined' === typeof ibwp_sp_conf ) {
        return;
    }

    if ( ibwp_sp_conf.sp_conversions.length > 0 ) {
        processNotifications( ibwp_sp_conf.sp_conversions );
    }
    
    function processNotifications( ids ) {

        var node = $('<div class="ibwp-sp-conversions"></div>');
        var html = '';

        var data = {
            'action'    : 'ibwp_sp_notifications_display',
            'sp_nonce'  : ibwp_sp_conf.sp_nonce,
            'sp_ids'    : ids,
        };

        $.post( ibwp_sp_conf.ajaxurl, data, function(response) {
            data = JSON.parse( response );
            html = node.html(data.content);
            if ( data.config.randomize === 1 ) {
                var localData = getNotificationLocalData(data.config.source);
                if ( localData ) {
                    html = $(localData);
                } else {
                    if ( html[0].children.length > 0 ) {
                        for (var i = html[0].children.length; i >= 0; i--) {
                            html[0].appendChild(html[0].children[(Math.random() * i) | 0]);
                        }
                    }
                    saveNotificationLocalData(html[0].outerHTML, data.config.source);
                }
            } else {
                removeNotificationLocalData();
            }
            renderConversions(data.config, html);
        });
    }

    /**
     * Render the markup of conversions.
     */
    function renderConversions(config, html)
    {
        var count       = 0,
            elements    = html.find('.ibwp-sp-notification-popup-' + config.id),
            delayCalc   = config.initial_delay,//(config.initial_delay + config.display_duration + config.delay_each) / 1000,
            delayEach   = config.delay_each,
            last        = getLastConversion(config.id, false);

        if ( last >= 0 ) {
            count = last + 1;
        }

        // Remove becuase of every time want to reset content count to zero
        /*if ( config.loop === 0 && elements.length === 1 ) {
            count = 0;
        }*/

        count = 0;

        setTimeout(function() {

            // Show the first notification.
            showConversion( $(elements[count]), config, count );

            setTimeout(function() {

                // Hide the first notification when display duration is expired.
                hideConversion( $(elements[count]), config );
                
                // Increase the sequence.
                count++;

                // Now lets render next notifications.
                var next = setInterval(function() {
                    // Show next notification
                    showConversion( $(elements[count]), config, count );

                    setTimeout(function() {
                        // Again hide this notification once display duration is expired.
                        hideConversion( $(elements[count]), config );

                        // If there is a limit for notifications to be displayed per page,
                        // reset the count, so that it can either start from begining or stop.
                        if ( count >= config.max_per_page - 1 ) {
                            count = 0;
                            // If notifications are not in loop, clear the interval.
                            if ( config.loop === 0 ) {
                                clearInterval(next);
                            }
                        } else if ( count >= elements.length - 1 ) {
                            count = 0;
                            // If notifications are not in loop, clear the interval.
                            if ( config.loop === 0 ) {
                                clearInterval(next);
                            }
                        } else {
                            count++;
                        }

                    }, config.display_duration);

                }, delayEach + config.display_duration); // delayEach + config.display_duration
                
            }, config.display_duration);

        }, config.initial_delay);
    }

    /**
     * Show conversion.
     */
    function showConversion(element, config, count)
    {
        if ( 'undefined' === typeof element || 0 === element.length ) {
            return;
        }

        $('body').append(element);
        element.removeClass(config.hide_animation).addClass(config.show_animation,2000);
        //element.animate({ 'bottom': '20px', 'opacity': '1' }, 1000);

        // Save the sequence of the last notification.
        saveLastConversion( config.id, count );
    }

    /**
     * Hide conversion.
     */
    function hideConversion(element, config)
    {
        element.removeClass(config.show_animation).addClass(config.hide_animation, 1000);

        setTimeout( function() {
            removeConversion(element);
        }, 2000);
    }

    /**
     * Remove conversion.
     */
    function removeConversion(element)
    {
        if ( element.length > 0 ) {
            element.remove();
        }
    }

    /**
     * Get the sequence of the last conversion.
     */
    function getLastConversion(id, obj)
    {
        var last = -1;
        if ( window.localStorage ) {
            var notificationSequenece = window.localStorage.getItem('ibwp_sp_notifications');
            if ( null !== notificationSequenece ) {
                notificationSequenece = JSON.parse(notificationSequenece);
                if ( undefined !== notificationSequenece[id] ) {
                    if ( obj ) {
                        return notificationSequenece;
                    }
                    last = notificationSequenece[id];
                }
            }
        } else {
            console.log('Browser does not support localStorage!');
        }
        return last;
    }

    /**
     * Save notification data in localStorage.
     */
    function saveLastConversion(id, sequence)
        {
            if ( window.localStorage ) {
                var lastConversion = getLastConversion(id, true);
                if ( 'object' === typeof lastConversion ) {
                    lastConversion[id] = sequence;
                } else {
                    lastConversion = new Object;
                    lastConversion[id] = sequence;
                }
                window.localStorage.setItem('ibwp_sp_notifications', JSON.stringify(lastConversion));
            } else {
                console.log('Browser does not support localStorage!');
            }
        }

    /**
     * Get notification data from localStorage.
     */
    function getNotificationLocalData(source)
    {
        if (window.localStorage) {
            
            if ( window.localStorage.getItem('ibwp_sp_notifications_source') !== source ) {
                removeNotificationLocalData();
                return false;
            }

            var html = window.localStorage.getItem('ibwp_sp_notifications_html');
            var expireTime = window.localStorage.getItem("ibwp_sp_notifications_expire");
            var difference = Date.now() - expireTime;
            var hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

            if ( hours >= 3 ) {
                html = '';
                removeNotificationLocalData();
            }

            if ( typeof html === 'undefined' || html === '' ) {
                return false;
            }

            return html;
        }

        return false;
    }

    /**
     * Save notification data in localStorage.
     */
    function saveNotificationLocalData(html, source)
    {
        if (window.localStorage) {
            window.localStorage.setItem('ibwp_sp_notifications_html', html);
            window.localStorage.setItem('ibwp_sp_notifications_source', source);
            window.localStorage.setItem('ibwp_sp_notifications_expire', Date.now());
        }
    }
    
    /**
     * Remove notification data from localStorage.
     */
    function removeNotificationLocalData()
    {
        window.localStorage.removeItem('ibwp_sp_notifications_html');
        window.localStorage.removeItem('ibwp_sp_notifications_source');
        window.localStorage.removeItem('ibwp_sp_notifications_expire');
    }

    $('body').delegate( '.ibwp-sp-main-wrap .ibwp-sp-popup-close', 'click', function() {
        var element = $(this).parents('.ibwp-sp-main-wrap');
        element.animate({ 'bottom': '-250px', 'opacity': '0' }, 1000, function() {
            removeConversion(element);
        });
    } );

});