// Admin js
jQuery( document ).ready(function($) {

    // Initialize Select 2 JS
    $(".ibwp-select2").select2();

    // On change of source type
    $( document ).on( 'change', '.ibwp-sp-source', function() {

        var sel_val = $.trim( $(this).val() );
        
        $( '.ibwp-source-row' ).hide();
        $( '.ibwp-source-'+sel_val ).show();
    });

    // On change of custom url
    $( document ).on( 'change', '.ibwp-sp-link-to', function() {
        
        var sel_val = $.trim( $(this).val() );
        
        $( '.ibwp-link-row' ).hide();
        $( '.ibwp-link-'+sel_val ).show();
    });

    //on change of social proof type
    $( document ).on( 'change', '.ibwp-sp-type', function() {
        
        var sel_val = $.trim( $(this).val() );
        
        if( sel_val == 'conversion' ) {
            $( '.ibwp-sp-wrap-reviews' ).hide();
            $( '.ibwp-sp-wrap-conversion' ).show();
            var txa_content = "{{name}} purchased {{title}}";
        }

        var textarea_val = $('textarea.ibwp-sp-content').val();
        if( textarea_val != 'undefined' ) {
            $('textarea.ibwp-sp-content').val(txa_content);
        }
        
        ibwp_sp_index_value(sel_val);
        ibwp_sp_row_id(sel_val);
        ibwp_sp_add_row(sel_val);
    });

    /* Hide show advance custom setting fields */
    $(document).on('click', '.ibwp-sp-toggle-setting', function(){
        
        var cur_ele = $(this);
        var txt = cur_ele.text()=='Show settings' ? 'Hide settings' : 'Show settings';
        cur_ele.text(txt);
        cur_ele.parent().parent().parent().children('.ibwp-sp-row-standard-fields').slideToggle();

    });
    
    var sel_val = $('.ibwp-sp-type').val();

    function ibwp_sp_add_row(sel_val) {

        $( document ).on('click', '.ibwp-sp-add-'+sel_val, function(){

            var row_count = $('.ibwp-sp-'+sel_val+'-row').length;
            if( row_count >= 5 ) {
                alert("Maximum 5 custom record allowed.");
            } else {

                var last_row_id = $('div[id^="'+sel_val+'_row"]:last');

                var key         = highest = 1;
                last_row_id.each(function() {
                    var current = $(this).data( 'key' );
                    if( parseInt( current ) > highest ) {
                        highest = current;
                    }
                });
                key = highest += 1;
                
                var tempalte = $('#tmpl-ibwp-sp-'+sel_val+'-row').clone();
                $('.ibwp-sp-table-'+sel_val).append( tempalte.html() );

                var last_row_id = $('div[id^="'+sel_val+'_row"]:last');
                
                last_row_id.attr( 'data-key', key );
                last_row_id.find('input, textarea, select, label').each(function() {

                    if( $(this).hasClass('ibwp-sp-button') == false ) {
                        $(this).val(' ');
                    }
                   
                    var name    = $( this ).attr( 'name' );
                    var id      = $( this ).attr( 'id' );
                    var cls      = $( this ).attr( 'class' );
                    var for_id  = $( this ).attr( 'for' );
                    
                    if( name ) {
                        name = name.replace( /\[(\d+)\]/, '[' + parseInt( key ) + ']');
                        $( this ).attr( 'name', name );
                    }
                    if( typeof id != 'undefined' ) {
                        id = id.replace( /(\d+)/, parseInt( key ) );
                        $( this ).attr( 'id', id );
                    }
                    if( typeof cls != 'undefined' ) {
                        cls = cls.replace( /(\d+)/, parseInt( key ) );
                        $( this ).attr( 'class', cls );
                    }
                    if( typeof for_id != 'undefined' ) {
                        for_id = for_id.replace( /(\d+)/, parseInt( key ) );
                        $( this ).attr( 'for', for_id );
                    }
                });

                ibwp_sp_index_value(sel_val);                
                ibwp_sp_row_id(sel_val);
            }

        });
    }
    ibwp_sp_add_row(sel_val);

    // Custom Row Remove Function
    function ibwp_sp_delete_row(sel_val) {
        $( document ).on('click', '.ibwp-sp-delete ', function(){

            var sel_val = $.trim( $('.ibwp-sp-type').val() );
            var row_count = $('.ibwp-sp-'+sel_val+'-row').length;
            var remove;
            remove = confirm(ibwp_sp_admin.remove_row);
            if(remove) {
                if( row_count > 1 ) {
                    var parent_div  = $(this).closest('.ibwp-sp-'+sel_val+'-row');
                    parent_div.remove();
                    ibwp_sp_index_value(sel_val);
                    ibwp_sp_row_id(sel_val);
                    return true;
                } else {
                    alert('Must have at least 1 columns!');
                    return false;
                }
            } else {
                return false;
            }
        });
    }
    ibwp_sp_delete_row(sel_val);

     // Change Custom Index Value
    function ibwp_sp_index_value(sel_val) {
        
        var index_val    = document.getElementsByClassName('ibwp-sp-'+sel_val+'-index');
        
        for (var i = 0; i < index_val.length; i++) {
            $('.ibwp-sp-'+sel_val+'-index span')[i].innerHTML = (i + 1);
        }

    }
    
    ibwp_sp_index_value(sel_val);
    
    // Reset row id on remove row function
    function ibwp_sp_row_id(sel_val) {
        
        var row_id    = document.getElementsByClassName('ibwp-sp-'+sel_val+'-row');
        
        for (var i = 0; i < row_id.length; i++) {
            row_id[i].setAttribute('id', sel_val+'_row' +(i+1));
        }

    }

    ibwp_sp_row_id(sel_val);

    $('.ibwp-color-pick').wpColorPicker();
    
    $( document ).on( "click",'.handlediv' ,function(){
        $(this).parent().find('.inside').toggle();
    });

    $( document ).on( "click",'.accordion' ,function(){
        $(this).next().toggle();
    });

    /* Initialize Select 2 */
    //$(".ibwp-sp-select2").select();
    var myOptions = {
        change: function(event, ui){
            var target_id = event.target.id;
            var target_val = ui.color.toString();

            if ( target_id == "ibwp-sp-bg-color") {
                $( ".ibwp-sp-main-wrap" ).css( "background-color", target_val );
            }

            if ( target_id == "ibwp-sp-name-color") {
                $( ".ibwp-sp-main-wrap .ibwp-sp-name" ).css( "color", target_val );
            }

            if ( target_id == "ibwp-sp-title-color") {
                $( ".ibwp-sp-main-wrap .ibwp-sp-title" ).css( "color", target_val );
            }

            if ( target_id == "ibwp-sp-gen-txt-color") {
                $( ".ibwp-sp-main-wrap .type-conversion" ).css( "color", target_val );
                $( ".ibwp-sp-main-wrap .type-reviews" ).css( "color", target_val );
            }

            if ( target_id == "ibwp-sp-star-color") {
                $( ".ibwp-sp-main-wrap .ibwp-sp-star-rating" ).css( "color", target_val );
            }
        },
    };

    $('.ibwp-color-pick').wpColorPicker(myOptions);

    $( document ).on( 'change focus keyup', '.ibwp_sp_design_options .ibwp-number', function(){

        var input_id = $(this).attr('id');
        var input_val = $(this).val();
         
        if( input_id == "ibwp-sp-border-radius" ){
            $(".ibwp-sp-main-wrap").css( "border-radius", input_val+"px" );
        }

        if( input_id == "ibwp-sp-img-border-radius" ){
            $(".ibwp-sp-main-wrap .ibwp-sp-img img").css( "border-radius", input_val+"px" );
        }
    })
});
