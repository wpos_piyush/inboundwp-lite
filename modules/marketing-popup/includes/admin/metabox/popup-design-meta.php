<!-- Start PopUp Design HTML -->
<div id="ibwp_mp_design_options" class="ibwp-stabs-cnt ibwp-mp-design-options ibwp-clearfix" style="display:none;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-mp-title"><?php _e('PopUp Design Settings', 'inboundwp-lite'); ?></div>
	</div>

	<!-- Start PopUp Design Settings -->
	<div class="ibwp-mp-popup-row">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Design Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<?php
					$i = 1;
					foreach($popup_goal_arr as $popup_goals) {
						foreach($popup_type_arr as $popup_types) { ?>
							<tr class="mp-<?php echo $popup_goals; ?>-design mp-<?php echo $popup_types; ?>-design ibwp-mp-common-cls" style="<?php if($popup_goals == $popup_goal && $popup_types == $popup_type) { echo 'display: block'; } else { echo 'display:none'; } ?>">
								<td>
									<?php foreach($design_arr as $designs) { ?>
										<label class="ibwp-mp-popup-design ibwp-columns ibwp-medium-6">
											<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_design" value="<?php echo ibwpl_esc_attr($designs); ?>" <?php if( $popup_design == $designs || ($popup_design == '' && $i == 1) ) { echo 'checked'; } ?> class="ibwp-radio ibwp-mp-popup-design">
											<div class="ibwp-mp-popup-design-block" style="<?php if( $popup_design == $designs || ($popup_design == '' && $i == 1) ) { echo 'border-color: #000000'; } ?>">
												<img src="<?php echo IBWP_MP_URL; ?>/assets/images/<?php echo $popup_goals; ?>/<?php echo $popup_types; ?>/<?php echo $designs; ?>.png">
											</div>
											<span class="ibwp-mp-designs-cls"><?php echo $designs; ?></span>
										</label>
									<?php $i++; } ?>
								</td>
							</tr>
						<?php }
					} ?>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End PopUp Design Settings -->

	<!-- Start PopUp Modal Settings -->
	<div class="ibwp-mp-popup-row mp-popup-modal-settings ibwp-mp-common-cls" style="<?php if($popup_type == 'popup-modal') { echo 'display:block;'; } else { echo 'display:none;'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Modal PopUp Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-fullscreen-popup"><?php _e('Full Screen','inboundwp-lite'); ?></label><br>
							<input type="checkbox" name="<?php echo ibwpl_esc_attr($prefix); ?>fullscreen_popup" value="1" <?php checked($fullscreen_popup,1);?> class="ibwp-checkbox ibwp-mp-fullscreen-popup" id="ibwp-mp-fullscreen-popup"><br>
							<span class="description"><?php _e('Check this checkbox to display popup in full-screen.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr class="ibwp-mp-modal-popup-sett" style="<?php if($fullscreen_popup == 0) { echo 'display: table-row;'; } else { echo 'display: none;'; } ?>">
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-modal-width"><?php _e('Modal PopUp Width','inboundwp-lite'); ?></label><br>
							<input type="number" name="<?php echo ibwpl_esc_attr($prefix); ?>modal_width" value="<?php echo ibwpl_esc_attr($modal_width); ?>" id="ibwp-mp-modal-width" class="ibwp-number ibwp-mp-modal-width ibwp-medium-11">
							<?php _e('px','inboundwp-lite'); ?><br>
							<span class="description"><?php _e('Set modal popup width.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-modal-position"><?php _e('Modal PopUp Position','inboundwp-lite'); ?></label>
							<select name="<?php echo ibwpl_esc_attr($prefix); ?>modal_position" class="ibwp-select ibwp-mp-modal-position" id="ibwp-mp-modal-position" style="width: 100%;">
								<option value="middle" <?php selected($modal_position,'middle'); ?>><?php _e('Middle','inboundwp-lite'); ?></option>
								<option value="middle-top" <?php selected($modal_position,'middle-top'); ?>><?php _e('Middle Top','inboundwp-lite'); ?></option>
								<option value="middle-bottom" <?php selected($modal_position,'middle-bottom'); ?>><?php _e('Middle Bottom','inboundwp-lite'); ?></option>
								<option value="middle-left" <?php selected($modal_position,'middle-left'); ?>><?php _e('Middle Left','inboundwp-lite'); ?></option>
								<option value="middle-right" <?php selected($modal_position,'middle-right'); ?>><?php _e('Middle Right','inboundwp-lite'); ?></option>
							</select>
							<span class="description"><?php _e('Select modal popup position.','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End PopUp Modal Settings -->

	<!-- Start Target URL Field Settings -->
	<div class="ibwp-mp-popup-row mp-target-url-settings ibwp-mp-common-cls" style="<?php if($popup_goal == 'target-url') { echo 'display:block'; } else { echo 'display:none'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Target URL Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-target-url-link"><?php _e('Link URL','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>target_url_link" value="<?php echo ibwpl_esc_attr($target_url_link); ?>" id="ibwp-mp-target-url-link" class="ibwp-url ibwp-mp-target-url-link large-text">
							<span class="description"><?php _e('Enter your traget url link.<br> Ex: https://www.wponlinesupport.com/','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label class="ibwp-mp-open-link-url"><?php _e('Open Link','inboundwp-lite'); ?></label>
							<select class="ibwp-select ibwp-mp-open-link-url" id="ibwp-mp-open-link-url" name="<?php echo ibwpl_esc_attr($prefix); ?>open_link_url" style="width: 100%;">
								<option value="_self" <?php selected($open_link_url,'_self'); ?>><?php _e('Self','inboundwp-lite'); ?></option>
								<option value="_blank" <?php selected($open_link_url,'_blank'); ?>><?php _e('New Tab','inboundwp-lite'); ?></option>
							</select>
							<span class="description"><?php _e('Select link target. Open link in new tab or current tab.','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Target URL Field Settings -->

	<!-- Start Phone Number Settings -->
	<div class="ibwp-mp-popup-row mp-phone-calls-settings ibwp-mp-common-cls" style="<?php if($popup_goal == 'phone-calls') { echo 'display:block;'; } else { echo 'display:none;'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Phone Number Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td>
							<label for="ibwp-mp-phone-number"><?php _e('Phone Number','inboundwp-lite'); ?></label><br>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>phone_number" value="<?php echo ibwpl_esc_attr($phone_number); ?>" id="ibwp-mp-phone-number" class="ibwp-number ibwp-mp-phone-number" style="width: 50%;"><br>
							<span class="description"><?php _e('Enter your phone number with your country code.<br> Ex: +91-9898989898','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Phone Number Settings -->

	<!-- Start Modal PopUp Display Settings -->
	<div class="ibwp-mp-popup-row">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Display Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr class="mp-popup-modal-settings mp-popup-appear-settings ibwp-mp-common-cls" style="<?php if($popup_type == 'popup-modal') { echo 'display:table-row;'; } else { echo 'display:none;'; } ?>">
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-when-popup-appear"><?php _e('When Popup appear?','inboundwp-lite'); ?></label>
							<select name="<?php echo $prefix; ?>when_popup_appear" id="ibwp-mp-when-popup-appear" class="ibwp-select ibwp-mp-when-popup-appear ibwp-mp-select-box" style="width: 100%;">
								<option value=""><?php _e('Select PopUp Appear','inboundwp-lite'); ?></option>
								<option value="page_load"><?php _e('On Page Load','inboundwp-lite'); ?></option>
								<option value="inactivity" disabled="disabled"><?php _e('After X second of inactivity (Pro)','inboundwp-lite'); ?></option>
								<option value="scroll" disabled="disabled"><?php _e('When Page Scroll (Pro)','inboundwp-lite'); ?></option>
								<option value="exit" disabled="disabled"><?php _e('On Exit Intent (Pro)','inboundwp-lite'); ?></option>
							</select>
							<span class="description"><?php _e('Select when popup appear.', 'inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6 ibwp-mp-appear page_load">
							<label for="ibwp-mp-delay"><?php _e('Popup Delay','inboundwp-lite'); ?></label><br>
							<input name="<?php echo $prefix; ?>delay" value="<?php echo $delay;?>" class="ibwp-number ibwp-mp-delay small-text" id="ibwp-mp-delay" type="number" min="0" style="width: 80%;"> <?php _e('Sec','inboundwp-lite');?><br/>
							<span class="description"><?php _e('Enter no of second to open popup after page load.', 'inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-disappear"><?php _e('Popup Disappear', 'inboundwp-lite'); ?></label>
							<input name="<?php echo $prefix; ?>disappear" value="<?php echo $disappear;?>" class="ibwp-number ibwp-mp-disappear small-text" style="width: 80%;" id="ibwp-mp-disappear" type="number" min="0"> <?php _e('Sec','inboundwp-lite');?><br/>
							<span class="description"><?php _e('Enter no of second to hide popup.', 'inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-exptime"><?php _e('Cookie Expiry Time', 'inboundwp-lite'); ?></label>
							<input name="<?php echo $prefix; ?>exptime" value="<?php echo $exptime;?>" style="width: 80%;" class="ibwp-number ibwp-mp-exptime small-text" id="ibwp-mp-exptime" type="number"> <?php _e('Days.','inboundwp-lite'); ?><br/>
							<span class="description"><?php _e('Enter expiry time when user click on close button. Upon exiry user will see popup again.', 'inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-hideclsbtn"><?php _e('Hide Close Button', 'inboundwp-lite'); ?></label><br>
							<input type="checkbox" name="<?php echo $prefix; ?>hideclsbtn" value="1" class="ibwp-checkbox ibwp-mp-hideclsbtn" id="ibwp-mp-hideclsbtn" <?php checked( $hideclsbtn, 1 ); ?> />
							<span class="description"><?php _e('Check this box if you want to hide the close button of popup.', 'inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-clsonesc"><?php _e('Close Popup On Esc', 'inboundwp-lite'); ?></label><br>
							<input type="checkbox" name="<?php echo $prefix; ?>clsonesc" value="1" class="ibwp-checkbox ibwp-mp-clsonesc" id="ibwp-mp-clsonesc" <?php checked( $clsonesc, 1 ); ?> />
							<span class="description"><?php _e('Check this box if you want to close the popup on esc key.', 'inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Modal PopUp Display Settings -->

	<!-- Start Social Links -->
	<div class="ibwp-mp-popup-row mp-social-traffic-settings ibwp-mp-common-cls" style="<?php if($popup_goal == 'social-traffic') { echo 'display: block;'; } else { echo 'display: none;'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Social Icon Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td colspan="2">
							<label for="ibwp-mp-icon-design"><?php _e('Icon Design','inboundwp-lite'); ?></label><br>
							<select name="<?php echo ibwpl_esc_attr($prefix); ?>icon_design" id="ibwp-mp-icon-design" class="ibwp-select ibwp-mp-icon-design" style="width: 50%;">
								<option value="square" <?php selected($icon_design,'square'); ?>><?php _e('Square','inboundwp-lite'); ?></option>
								<option value="circle" <?php selected($icon_design,'circle'); ?>><?php _e('Circle','inboundwp-lite'); ?></option>
								<option value="radius" <?php selected($icon_design,'radius'); ?>><?php _e('Radius','inboundwp-lite'); ?></option>
							</select><br>
							<span class="description"><?php _e('Set design for icon.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<label class="ibwp-mp-social"><?php _e('Social Links', 'inboundwp-lite') ?></label><br>	
							<div class="ibwp-mp-social-wrp">
								<?php foreach ($social_traffic as $skey => $sval) { 
									$type = isset( $sval['type'] ) ? $sval['type'] : '';
									$link = isset( $sval['link'] ) ? $sval['link'] : '';
									?>
									<div id="ibwp-mp-social-<?php echo $skey; ?>" id="ibwp-mp-social" class="ibwp-mp-social-row" data-key="<?php echo $skey; ?>">
										<div class="ibwp-mp-social-main ibwp-column ibwp-medium-4">
											<select name="<?php echo $prefix; ?>social_traffic[<?php echo $skey; ?>][type]" id="ibwp-mp-social-type-<?php echo $skey; ?>" class="ibwp-select ibwp-mp-select-box ibwp-mp-social-type" style="width: 100%;">
												<option value=""><?php _e('Select Type', 'inboundwp-lite'); ?></option>
												<option value="facebook" <?php selected( 'facebook', $type ); ?>><?php _e( 'Facebook', 'inboundwp-lite' ); ?></option>
												<option value="twitter" <?php selected( 'twitter', $type ); ?>><?php _e( 'Twitter', 'inboundwp-lite' ); ?></option>
												<option value="google" <?php selected( 'google', $type ); ?>><?php _e( 'Google', 'inboundwp-lite' ); ?></option>
												<option value="linkedin" <?php selected( 'linkedin', $type ); ?>><?php _e( 'Linkedin', 'inboundwp-lite' ); ?></option>
												<option value="youtube" <?php selected( 'youtube', $type ); ?>><?php _e( 'Youtube', 'inboundwp-lite' ); ?></option>
												<option value="instagram" <?php selected( 'instagram', $type ); ?>><?php _e( 'Instagram', 'inboundwp-lite' ); ?></option>
												<option value="pinterest" <?php selected( 'pinterest', $type ); ?>><?php _e( 'Pinterest', 'inboundwp-lite' ); ?></option>
												<option value="skype" <?php selected( 'skype', $type ); ?>><?php _e( 'Skype', 'inboundwp-lite' ); ?></option>
											</select>
										</div>
										<div class="ibwp-mp-sl-main ibwp-column ibwp-medium-6">
											<input type="text" name="<?php echo $prefix; ?>social_traffic[<?php echo $skey; ?>][link]" id="ibwp-mp-social-link-<?php echo $skey; ?>" value="<?php echo esc_url($link); ?>" class="ibwp-url ibwp-mp-social-link large-text">
										</div>
										<div class="ibwp-mp-ar-icon ibwp-column ibwp-medium-2">
											<span class="ibwp-mp-icon ibwp-mp-add-social-link ibwp-mp-add-link" title="<?php _e('Add Link', 'inboundwp-lite'); ?>"><i class="dashicons dashicons-plus-alt"></i></span>
											<span class="ibwp-mp-icon ibwp-mp-remove-social-link ibwp-mp-remove-link" title="<?php _e('Remove Link', 'inboundwp-lite'); ?>"><i class="dashicons dashicons-dismiss"></i></span>
										</div>
									</div>
								<?php } ?>		
							</div><!-- end .ibwp-mp-social-wrp -->
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Social Links -->

	<!-- Start Background Settings -->
	<div class="ibwp-mp-popup-row">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Background Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td colspan="2">
							<label for="ibwp-mp-gen-bg-img"><?php _e('Image', 'inboundwp-lite'); ?></label><br>
							<input type="text" name="<?php echo $prefix; ?>gen_bg_img" value="<?php echo $gen_bg_img; ?>" id="ibwp-mp-gen-bg-img" class="ibwp-url regular-text ibwp-mp-default-img ibwp-img-upload-input ibwp-url" style="width: 70%;">
							<input type="button" name="ibwp_mp_default_img" class="button-secondary ibwp-image-upload" value="<?php _e( 'Upload Image', 'inboundwp-lite'); ?>" data-uploader-title="<?php _e('Choose Image', 'inboundwp-lite'); ?>" data-uploader-button-text="<?php _e('Insert Image', 'inboundwp-lite'); ?>" /> 
							<input type="button" name="ibwp_mp_default_img_clear" id="ibwp-mp-gen-bg-img-clear" class="button button-secondary ibwp-image-clear" value="<?php _e( 'Clear', 'inboundwp-lite'); ?>" /> <br />
							<?php
								if( $gen_bg_img != '' ) { 
									$gen_bg_img = '<img src="'.$gen_bg_img.'" alt="" />';
								}
							?>
							<div class="ibwp-wbg-preview-img"><?php echo $gen_bg_img; ?></div>
							<span class="description"><?php _e('Select Popup Image.', 'inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>											
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-gen-bg-color"><?php _e('Background Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_bg_color" value="<?php echo ibwpl_esc_attr($gen_bg_color); ?>" class="ibwp-mp-gen-bg-color ibwp-colorpicker" id="ibwp-mp-gen-bg-color">
							<span class="description"><?php _e('Select the background color.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-gen-bg-size"><?php _e('Image Size', 'inboundwp-lite'); ?></label><br>
							<select name="<?php echo $prefix; ?>gen_bg_size" id="ibwp-mp-gen-bg-size" class="ibwp-select ibwp-mp-gen-bg-size ibwp-mp-select-box" style="width: 100%;">
							<option value="cover" <?php selected($gen_bg_size,'cover'); ?>><?php _e('Cover','inboundwp-lite'); ?></option>	
							<option value="auto" <?php selected($gen_bg_size,'auto'); ?>><?php _e('Auto','inboundwp-lite'); ?></option>
							<option value="contain" <?php selected($gen_bg_size,'contain'); ?>><?php _e('Contain','inboundwp-lite'); ?></option>
							</select><br/>
							<span class="description"><?php _e('Select image size.', 'inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-gen-bg-repeat"><?php _e('Image Repeat', 'inboundwp-lite'); ?></label><br>
							<select name="<?php echo $prefix; ?>gen_bg_img_repeat" id="ibwp-mp-gen-bg-repeat" class="ibwp-select ibwp-mp-gen-bg-repeat ibwp-mp-select-box" style="width: 100%;">
							<?php 
								if(!empty($mp_repeat_options))
									foreach ($mp_repeat_options as $repeat_key => $repeat_val) {
										echo '<option value="'.$repeat_key.'" '.selected($gen_bg_img_repeat,$repeat_key).'>'.$repeat_val.'</option>';
									}
							?>
							</select><br/>
							<span class="description"><?php _e('Select image repeat.', 'inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-gen-bg-img-position"><?php _e('Image Position','inboundwp-lite'); ?></label><br>
							<select name="<?php echo ibwpl_esc_attr($prefix); ?>gen_bg_img_position" id="ibwp-mp-gen-bg-img-position" class="ibwp-select ibwp-mp-gen-bg-img-position" style="width: 100%;">
								<option value="center top" <?php selected($gen_bg_img_position,'center top'); ?>><?php _e('Center Top','inboundwp-lite'); ?></option>
								<option value="center center" <?php selected($gen_bg_img_position,'center center'); ?>><?php _e('Center Center','inboundwp-lite'); ?></option>
								<option value="center bottom" <?php selected($gen_bg_img_position,'center bottom'); ?>><?php _e('Center Bottom','inboundwp-lite'); ?></option>
								<option value="left top" <?php selected($gen_bg_img_position,'left top'); ?>><?php _e('Left Top','inboundwp-lite'); ?></option>
								<option value="left center" <?php selected($gen_bg_img_position,'left center'); ?>><?php _e('Left Center','inboundwp-lite'); ?></option>
								<option value="left bottom" <?php selected($gen_bg_img_position,'left bottom'); ?>><?php _e('Left Bottom','inboundwp-lite'); ?></option>
								<option value="right top" <?php selected($gen_bg_img_position,'right top'); ?>><?php _e('Right Top','inboundwp-lite'); ?></option>
								<option value="right center" <?php selected($gen_bg_img_position,'right center'); ?>><?php _e('Right Center','inboundwp-lite'); ?></option>
								<option value="right bottom" <?php selected($gen_bg_img_position,'right bottom'); ?>><?php _e('Right Bottom','inboundwp-lite'); ?></option>
							</select><br>
							<span class="description"><?php _e('Select image position.','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Background Image Settings -->

	<!-- Start Content Filed Settings -->
	<div class="ibwp-mp-popup-row mp-popup-modal-settings mp-slider-settings mp-push-notification-settings ibwp-mp-common-cls" style="<?php if($popup_type != 'bar') { echo 'display:block;'; } else { echo 'display:none;'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Content Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-main-heading"><?php _e('Main Heading','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_main_heading" value="<?php echo ibwpl_esc_attr($gen_main_heading); ?>" id="ibwp-mp-main-heading" class="ibwp-text ibwp-mp-main-heading large-text">
							<span class="description"><?php _e('Enter Main Heading.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-sub-heading"><?php _e('Sub Heading','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_sub_heading" value="<?php echo ibwpl_esc_attr($gen_sub_heading); ?>" id="ibwp-mp-sub-heading" class="ibwp-text ibwp-mp-sub-heading large-text">
							<span class="description"><?php _e('Enter Sub Heading.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-main-heading-fontsize"><?php _e('Main Heading Font Size','inboundwp-lite'); ?></label>
							<input type="number" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_main_heading_fontsize" value="<?php echo ibwpl_esc_attr($gen_main_heading_fontsize); ?>" id="ibwp-mp-main-heading-fontsize" class="ibwp-number ibwp-mp-main-heading-fontsize">
							<?php _e('px','inboundwp-lite');?><br/>
							<span class="description"><?php _e('Set Main Heading font size.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-sub-heading-fontsize"><?php _e('Sub Heading Font Size','inboundwp-lite'); ?></label>
							<input type="number" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_sub_heading_fontsize" value="<?php echo ibwpl_esc_attr($gen_sub_heading_fontsize); ?>" id="ibwp-mp-sub-heading-fontsize" class="ibwp-number ibwp-mp-sub-heading-fontsize">
							<?php _e('px','inboundwp-lite');?><br/>
							<span class="description"><?php _e('Set Sub Heading font size.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-main-heading-text-color"><?php _e('Main Heading Text Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_main_heading_text_color" value="<?php echo ibwpl_esc_attr($gen_main_heading_text_color); ?>" id="ibwp-mp-main-heading-text-color" class="ibwp-mp-main-heading-text-color ibwp-colorpicker">
							<span class="description"><?php _e('Set Main Heading text color.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-6">
							<label for="ibwp-mp-sub-heading-text-color"><?php _e('Sub Heading Text Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_sub_heading_text_color" value="<?php echo ibwpl_esc_attr($gen_sub_heading_text_color); ?>" id="ibwp-mp-sub-heading-text-color" class="ibwp-mp-sub-heading-text-color ibwp-colorpicker">
							<span class="description"><?php _e('Set Sub Heading text color.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-6">
							<label for="mp-popup-desc-text-color"><?php _e('Content Text Color','inboundwp-lite'); ?></label><br>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>desc_text_color" value="<?php echo ibwpl_esc_attr($desc_text_color); ?>" id="mp-popup-desc-text-color" class="mp-popup-desc-text-color ibwp-colorpicker">
							<span class="description"><?php _e('Set content text color.','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Content Field Settings -->

	<!-- Start Email Field Settings -->
	<div class="ibwp-mp-popup-row mp-email-lists-settings ibwp-mp-common-cls" style="<?php if($popup_goal == 'email-lists') { echo 'display:block'; } else { echo 'display:none;'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Field Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-name-field-hide"><?php _e('Name','inboundwp-lite'); ?></label><br>
							<input type="checkbox" name="<?php echo ibwpl_esc_attr($prefix); ?>hide_name_field" value="1" <?php checked( $hide_name_field, 1 ); ?> id="ibwp-mp-name-field-hide" class="ibwp-checkbox ibwp-mp-name-field-hide"><br>
							<span class="description"><?php _e('Check this checkbox to hide name field.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-phone-field-hide"><?php _e('Phone Number','inboundwp-lite'); ?></label><br>
							<input type="checkbox" name="<?php echo ibwpl_esc_attr($prefix); ?>hide_phone_field" value="1" <?php checked( $hide_phone_field, 1 ); ?> id="ibwp-mp-phone-field-hide" class="ibwp-checkbox ibwp-mp-phone-field-hide"><br>
							<span class="description"><?php _e('Check this checkbox to hide phone number field.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-name-field-ph"><?php _e('Name Placeholder','inboundwp-lite'); ?></label><br>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>name_field_ph" value="<?php echo ibwpl_esc_attr($name_field_ph); ?>" class="ibwp-text ibwp-mp-name-field-ph large-text" id="ibwp-mp-name-field-ph">
							<span class="description"><?php _e('Enter name field placeholder text.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-email-field-ph"><?php _e('Email Placeholder','inboundwp-lite'); ?></label><br>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>email_field_ph" value="<?php echo ibwpl_esc_attr($email_field_ph); ?>" class="ibwp-text ibwp-mp-email-field-ph large-text" id="ibwp-mp-email-field-ph">
							<span class="description"><?php _e('Enter email field placeholder text.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-phone-field-ph"><?php _e('Phone Placeholder','inboundwp-lite'); ?></label><br>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>phone_field_ph" value="<?php echo ibwpl_esc_attr($phone_field_ph); ?>" class="ibwp-text ibwp-mp-phone-field-ph large-text" id="ibwp-mp-phone-field-ph">
							<span class="description"><?php _e('Enter phone field placeholder text.','inboundwp-lite'); ?></span>
						</td>
					</tr>
					<tr>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-field-bg-color"><?php _e('Background Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>field_bg_color" id="ibwp-mp-field-bg-color" value="<?php echo ibwpl_esc_attr($field_bg_color); ?>" class="ibwp-mp-email-field-bg-color ibwp-colorpicker">
							<span class="description"><?php _e('Set Email fields bg color.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-field-text-color"><?php _e('Text Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>field_text_color" id="ibwp-mp-field-text-color" value="<?php echo ibwpl_esc_attr($field_text_color); ?>" class="ibwp-mp-email-field-text-color ibwp-colorpicker">
							<span class="description"><?php _e('Set Email fields text color.','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Email Field Settings -->

	<!-- Start Button Settings -->
	<div class="ibwp-mp-popup-row mp-email-lists-settings mp-target-url-settings mp-phone-calls-settings ibwp-mp-common-cls" style="<?php if($popup_goal == 'announcement' || $popup_goal == 'social-traffic') { echo 'display:none'; } else { echo 'display:block'; } ?>">
		<div class="ibwp-tbl-row-header">
			<span class="ibwp-tbl-row-index"><strong><?php _e('Button Settings','inboundwp-lite'); ?></strong></span>
			<span class="ibwp-tbl-row-actions">
				<a class="ibwp-toggle-setting"><?php _e('Show settings', 'inboundwp-lite'); ?></a>
			</span>
		</div>
		<div class="ibwp-row-standard-fields">
			<table class="form-table ibwp-mp-popup-tbl">
				<tbody>
					<tr>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-gen-btn-text"><?php _e('Button Text','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_btn_text" value="<?php echo ibwpl_esc_attr($gen_btn_text); ?>" id="ibwp-mp-gen-btn-text" class="ibwp-text ibwp-mp-gen-btn-text large-text">
							<span class="description"><?php _e('Enter Button Text.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-gen-btn-bg-color"><?php _e('Background Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_btn_bg_color" value="<?php echo ibwpl_esc_attr($gen_btn_bg_color); ?>" id="ibwp-mp-gen-btn-bg-color" class="ibwp-mp-gen-btn-bg-color ibwp-colorpicker">
							<span class="description"><?php _e('Select button bg color.','inboundwp-lite'); ?></span>
						</td>
						<td class="ibwp-medium-4">
							<label for="ibwp-mp-gen-btn-text-color"><?php _e('Text Color','inboundwp-lite'); ?></label>
							<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>gen_btn_text_color" value="<?php echo ibwpl_esc_attr($gen_btn_text_color); ?>" id="ibwp-mp-gen-btn-text-color" class="ibwp-mp-gen-btn-text-color ibwp-colorpicker">
							<span class="description"><?php _e('Select button text color.','inboundwp-lite'); ?></span>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<!-- End Button Settings -->
</div>
<!-- End PopUp Design HTML -->