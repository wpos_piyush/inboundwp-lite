<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $post,$wp_version;

$prefix = IBWP_MP_META_PREFIX; // Metabox prefix

//Get Popup Goal & Type Tab Settings
$selected_tab				= get_post_meta( $post->ID, $prefix.'tab', true );
$popup_goal 				= get_post_meta( $post->ID, $prefix.'popup_goal', true );
$popup_type 				= get_post_meta( $post->ID, $prefix.'popup_type', true );
$popup_content 				= get_post_meta( $post->ID, $prefix.'popup_content', true );

// Get Popup Design Tab Settings
$popup_design 				= get_post_meta( $post->ID, $prefix.'popup_design', true );
$gen_btn_text 				= get_post_meta( $post->ID, $prefix.'gen_btn_text', true );
$gen_btn_bg_color 			= get_post_meta( $post->ID, $prefix.'gen_btn_bg_color', true );
$gen_btn_text_color 		= get_post_meta( $post->ID, $prefix.'gen_btn_text_color', true );

$hide_name_field 			= get_post_meta( $post->ID, $prefix.'hide_name_field', true );
$hide_phone_field 			= get_post_meta( $post->ID, $prefix.'hide_phone_field', true );
$name_field_ph 				= get_post_meta( $post->ID, $prefix.'name_field_ph', true );
$email_field_ph 			= get_post_meta( $post->ID, $prefix.'email_field_ph', true );
$phone_field_ph 			= get_post_meta( $post->ID, $prefix.'phone_field_ph', true );
$field_bg_color 			= get_post_meta( $post->ID, $prefix.'field_bg_color', true );
$field_text_color 			= get_post_meta( $post->ID, $prefix.'field_text_color', true );

$gen_bg_color 				= get_post_meta( $post->ID, $prefix.'gen_bg_color', true );
$gen_bg_img 				= get_post_meta( $post->ID, $prefix.'gen_bg_img', true );
$gen_bg_size 				= get_post_meta( $post->ID, $prefix.'gen_bg_size', true );
$gen_bg_img_repeat 			= get_post_meta( $post->ID, $prefix.'gen_bg_img_repeat', true );
$gen_bg_img_position 		= get_post_meta( $post->ID, $prefix.'gen_bg_img_position', true );

$gen_main_heading 			= get_post_meta( $post->ID, $prefix.'gen_main_heading', true );
$gen_main_heading_fontsize 	= get_post_meta( $post->ID, $prefix.'gen_main_heading_fontsize', true );
$gen_main_heading_text_color= get_post_meta( $post->ID, $prefix.'gen_main_heading_text_color', true );
$gen_sub_heading 			= get_post_meta( $post->ID, $prefix.'gen_sub_heading', true );
$gen_sub_heading_fontsize	= get_post_meta( $post->ID, $prefix.'gen_sub_heading_fontsize', true );
$gen_sub_heading_text_color = get_post_meta( $post->ID, $prefix.'gen_sub_heading_text_color', true );

$fullscreen_popup 			= get_post_meta( $post->ID, $prefix.'fullscreen_popup', true );
$modal_width 				= get_post_meta( $post->ID, $prefix.'modal_width', true );
$modal_position 			= get_post_meta( $post->ID, $prefix.'modal_position', true );
$desc_text_color 			= get_post_meta( $post->ID, $prefix.'desc_text_color', true );

$phone_number 				= get_post_meta( $post->ID, $prefix.'phone_number', true );

$icon_design 				= get_post_meta( $post->ID, $prefix.'icon_design', true );

$social_traffic		 		= get_post_meta( $post->ID, $prefix.'social_traffic', true );
if( empty($social_traffic) ) { $social_traffic = array(1=>''); }

$target_url_link 			= get_post_meta( $post->ID, $prefix.'target_url_link', true );
$open_link_url 				= get_post_meta( $post->ID, $prefix.'open_link_url', true );

// Get Email Settings Tab
$email_subject 				= get_post_meta( $post->ID, $prefix.'email_subject', true );
$email_heading 				= get_post_meta( $post->ID, $prefix.'email_heading', true );
$thank_you_msg 				= get_post_meta( $post->ID, $prefix.'thank_you_msg', true );
$email_message 				= get_post_meta( $post->ID, $prefix.'email_message', true );

// Get Popup Settings Tab Settings
$when_popup_appear 			= get_post_meta( $post->ID, $prefix.'when_popup_appear', true );
$delay 						= get_post_meta( $post->ID, $prefix.'delay', true );
$disappear 					= get_post_meta( $post->ID, $prefix.'disappear', true );
$exptime 					= get_post_meta( $post->ID, $prefix.'exptime', true );
$hideclsbtn 				= get_post_meta( $post->ID, $prefix.'hideclsbtn', true );
$clsonesc 					= get_post_meta( $post->ID, $prefix.'clsonesc', true );

$mp_repeat_options 			= ibwp_mp_repeat_options();

$design_arr 				= array('design-1','design-2');
$popup_goal_arr				= array('email-lists','target-url','announcement','phone-calls','social-traffic');
$popup_type_arr 			= array('popup-modal');

?>

<div id="ibwp-mp-popups-settings" class="post-box-container ibwp-mp-popups-settings">
	<div class="meta-box-sortables ui-sortable">
		<div id="general">
			<div class="ibwp-stabs-wrap ibwp-clearfix">
				<ul class="ibwp-stabs-nav-wrap">
					<li class="ibwp-stabs-nav ibwp-active-stab">
						<a href="#ibwp_mp_goal_options"><i class="fa fa-bullseye" aria-hidden="true"></i> <?php _e('PopUp Goal', 'inboundwp-lite'); ?></a>
					</li>					
					<li class="ibwp-stabs-nav <?php if(empty($popup_goal)) { echo 'disabled'; } ?>">
						<a href="#ibwp_mp_type_options"><i class="fa fa-window-maximize" aria-hidden="true"></i> <?php _e('PopUp Type', 'inboundwp-lite'); ?></a>
					</li>
					<li class="ibwp-stabs-nav <?php if(empty($popup_type)) { echo 'disabled'; } ?>">
						<a href="#ibwp_mp_content_options"><i class="fa fa-envelope"></i> <?php _e('PopUp Content','inboundwp-lite'); ?></a>	
					</li>
					<li class="ibwp-stabs-nav <?php if(empty($popup_type)) { echo 'disabled'; } ?>">
						<a href="#ibwp_mp_design_options"><i class="fa fa-paint-brush" aria-hidden="true"></i> <?php _e('PopUp Designs', 'inboundwp-lite'); ?></a>
					</li>
					<li class="ibwp-stabs-nav mp-email-lists-settings ibwp-mp-common-cls <?php if(empty($popup_type)) { echo 'disabled'; } ?>" style="<?php if($popup_goal == 'email-lists') { echo 'display: block;'; } else { echo 'display: none;'; } ?>">
						<a href="#ibwp_mp_email_options"><i class="fa fa-envelope"></i> <?php _e('Email Settings','inboundwp-lite'); ?></a>	
					</li>
				</ul>

				<div class="ibwp-cont-wrap">
					<?php 
					include_once( IBWP_MP_DIR . '/includes/admin/metabox/popup-goal-meta.php' );
					include_once( IBWP_MP_DIR . '/includes/admin/metabox/popup-type-meta.php' );
					include_once( IBWP_MP_DIR . '/includes/admin/metabox/popup-content-meta.php' );
					include_once( IBWP_MP_DIR . '/includes/admin/metabox/popup-design-meta.php' );
					include_once( IBWP_MP_DIR . '/includes/admin/metabox/popup-email-meta.php' );
					?>
				</div>
				<input type="hidden" value="<?php echo ibwpl_esc_attr($selected_tab); ?>" class="ibwp-selected-tab" name="<?php echo $prefix; ?>tab" />
			</div>
		</div>
	</div>
</div>