<!-- Start PopUp Type HTML -->
<div id="ibwp_mp_type_options" class="ibwp-stabs-cnt ibwp-mp-type-options ibwp-clearfix"  style="display:none;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-mp-title"><?php _e('PopUp Type Settings', 'inboundwp-lite'); ?></div>
	</div>
	<table class="form-table">
		<tbody>
			<tr>
				<td class="ibwp-columns ibwp-medium-6">
					<label class="ibwp-mp-type-main" data-type="popup-modal" data-goal="">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_type" value="popup-modal" <?php if($popup_type == 'popup-modal') { echo 'checked="checked"'; } ?> class="ibwp-radio ibwp-mp-type-input">
						<div class="ibwp-mp-type-block" style="<?php if($popup_type == 'popup-modal') { echo 'border-color: #000000'; } ?>">
							<img class="ibwp-mp-type-icon" src="<?php echo IBWP_MP_URL; ?>/assets/images/modal.png">
							<h4><?php _e('PopUp Modal','inboundwp-lite'); ?></h4>
							<p><?php _e('A pop up window in the center or full screen of your site.','inboundwp-lite'); ?></p>
						</div>
					</label>
				</td>
				<td class="ibwp-columns ibwp-medium-6 ibwp-pro-disable">
					<label class="ibwp-mp-type-main" data-type="bar" data-goal="">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_type" value="bar" <?php if($popup_type == 'bar') { echo 'checked="checked"'; } ?> class="ibwp-radio ibwp-mp-type-input">
						<div class="ibwp-mp-type-block" style="<?php if($popup_type == 'bar') { echo 'border-color: #000000'; } ?>">
							<img class="ibwp-mp-type-icon" src="<?php echo IBWP_MP_URL; ?>/assets/images/bar.png">
							<h4><?php _e('Bar <span class="ibwp-pro-tag">Pro</span>', 'inboundwp-lite'); ?></h4>
							<p><?php _e('A customizable bar at the top or bottom of your site.','inboundwp-lite'); ?></p>
						</div>
					</label>
				</td>
				<td class="ibwp-columns ibwp-medium-6 ibwp-pro-disable">
					<label class="ibwp-mp-type-main" data-type="slider" data-goal="">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_type" value="slider" <?php if($popup_type == 'slider') { echo 'checked="checked"'; } ?> class="ibwp-radio ibwp-mp-type-input">
						<div class="ibwp-mp-type-block" style="<?php if($popup_type == 'slider') { echo 'border-color: #000000'; } ?>">
							<img class="ibwp-mp-type-icon" src="<?php echo IBWP_MP_URL; ?>/assets/images/slider.png">
							<h4><?php _e('Slider <span class="ibwp-pro-tag">Pro</span>','inboundwp-lite'); ?></h4>
							<p><?php _e('A small window that slides out of the right or left side.','inboundwp-lite'); ?></p>
						</div>
					</label>
				</td>
				<td class="ibwp-columns ibwp-medium-6 ibwp-pro-disable">
					<label class="ibwp-mp-type-main" data-type="push-notification" data-goal="">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_type" value="push-notification" <?php if($popup_type == 'push-notification') { echo 'checked="checked"'; } ?> class="ibwp-radio ibwp-mp-type-input">
						<div class="ibwp-mp-type-block" style="<?php if($popup_type == 'push-notification') { echo 'border-color: #000000'; } ?>">
							<img class="ibwp-mp-type-icon" src="<?php echo IBWP_MP_URL; ?>/assets/images/alert.png">
							<h4><?php _e('Push Notification <span class="ibwp-pro-tag">Pro</span>','inboundwp-lite'); ?></h4>
							<p><?php _e('A small floating button that expands when clicked on.','inboundwp-lite'); ?></p>
						</div>
					</label>
				</td>
			</tr>
		</tbody>
	</table>
	<div class="ibwp-pro-notice">
		<?php echo sprintf( __('In lite version, you can use only modal popup. Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use more Popups like Bar, Slider & Push-Notification.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
	</div>
</div>
<!-- End PopUp Type HTML -->