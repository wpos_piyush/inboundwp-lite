<!-- Start PopUp Goal HTML -->
<div id="ibwp_mp_goal_options" class="ibwp-stabs-cnt ibwp-mp-goal-options ibwp-clearfix"  style="display:block;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-mp-title"><?php _e('PopUp Goal Settings', 'inboundwp-lite'); ?></div>
	</div>
	<table class="form-table">
		<tbody>
			<tr>
				<td>
					<label class="ibwp-mp-goal-main ibwp-columns ibwp-medium-6" data-goal="email-lists">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_goal" value="email-lists" <?php if($popup_goal == 'email-lists') { echo 'checked'; } ?> class="ibwp-radio ibwp-mp-goal-input">
						<div class="ibwp-mp-goal-block" style="<?php if($popup_goal == 'email-lists') { echo 'border-color: #000000'; } ?>">
							<i class="fa fa-envelope"></i>
							<h4><?php _e('Collect Emails','inboundwp-lite'); ?></h4>
							<p><?php _e('I want to collect my visitors email addresses.','inboundwp-lite'); ?></p>
						</div>
					</label>
					<label class="ibwp-mp-goal-main ibwp-columns ibwp-medium-6" data-goal="target-url">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_goal" value="target-url" <?php if($popup_goal == 'target-url') { echo 'checked'; } ?> class="ibwp-radio ibwp-mp-goal-input">
						<div class="ibwp-mp-goal-block" style="<?php if($popup_goal == 'target-url') { echo 'border-color: #000000'; } ?>">
							<i class="fa fa-hand-pointer-o"></i>
							<h4><?php _e('Target URL','inboundwp-lite'); ?></h4>
							<p><?php _e('Drive traffic to a certain part of your site.','inboundwp-lite'); ?></p>
						</div>
					</label>
					<label class="ibwp-mp-goal-main ibwp-columns ibwp-medium-4" data-goal="announcement">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_goal" value="announcement" <?php if($popup_goal == 'announcement') { echo 'checked'; } ?> class="ibwp-radio ibwp-mp-goal-input">
						<div class="ibwp-mp-goal-block" style="<?php if($popup_goal == 'announcement') { echo 'border-color: #000000'; } ?>">
							<i class="fa fa-bullhorn"></i>
							<h4><?php _e('Announcement','inboundwp-lite'); ?></h4>
							<p><?php _e('No buttons, Just announcement text.','inboundwp-lite'); ?></p>
						</div>
					</label>
					<label class="ibwp-mp-goal-main ibwp-columns ibwp-medium-4" data-goal="phone-calls">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_goal" value="phone-calls" <?php if($popup_goal == 'phone-calls') { echo 'checked'; } ?> class="ibwp-radio ibwp-mp-goal-input">
						<div class="ibwp-mp-goal-block" style="<?php if($popup_goal == 'phone-calls') { echo 'border-color: #000000'; } ?>">
							<i class="fa fa-volume-control-phone"></i>
							<h4><?php _e('Get Phone Calls','inboundwp-lite'); ?></h4>
							<p><?php _e('Allow visitors to call you in one click.','inboundwp-lite'); ?></p>
						</div>
					</label>
					<label class="ibwp-mp-goal-main ibwp-columns ibwp-medium-4" data-goal="social-traffic">
						<input type="radio" name="<?php echo ibwpl_esc_attr($prefix) ?>popup_goal" value="social-traffic" <?php if($popup_goal == 'social-traffic') { echo 'checked'; } ?> class="ibwp-radio ibwp-mp-goal-input">
						<div class="ibwp-mp-goal-block" style="<?php if($popup_goal == 'social-traffic') { echo 'border-color: #000000'; } ?>">
							<i class="fa fa-share-alt"></i>
							<h4><?php _e('Social Traffic','inboundwp-lite'); ?></h4>
							<p><?php _e('Point visitors to your social media channels','inboundwp-lite'); ?></p>
						</div>
					</label>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- End PopUp Goal HTML -->