<!-- Start Email Settings -->
<div id="ibwp_mp_email_options" class="ibwp-stabs-cnt ibwp-mp-design-options ibwp-clearfix" style="display:none;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-mp-title"><?php _e('Email Settings', 'inboundwp-lite'); ?></div>
	</div>
	<table class="form-table ibwp-mp-popup-tbl">
		<tbody>
			<tr>
				<td>
					<label for="ibwp-mp-email-heading"><?php _e('Email Subject','inboundwp-lite'); ?></label>
					<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>email_subject" value="<?php echo ibwpl_esc_attr($email_subject); ?>" class="ibwp-text ibwp-mp-email-subject large-text">
					<span class="description"><?php _e('Enter email subject.','inboundwp-lite'); ?></span>
				</td>
				<td>
					<label for="ibwp-mp-email-heading"><?php _e('Email Heading','inboundwp-lite'); ?></label>
					<input type="text" name="<?php echo ibwpl_esc_attr($prefix); ?>email_heading" value="<?php echo ibwpl_esc_attr($email_heading); ?>" class="ibwp-text ibwp-mp-email-heading large-text">
					<span class="description"><?php _e('Enter email heading.','inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="ibwp-mp-thank-you-msg"><?php _e('Thank You Message','inboundwp-lite'); ?></label>
					<?php wp_editor( $thank_you_msg, 'ibwp-mp-thank-you-msg', array('textarea_name' => $prefix.'thank_you_msg', 'textarea_rows' => 5, 'media_buttons' => false, 'class' => 'ibwp-mp-width') ); ?>
					<span class="description"><?php _e('Enter thank you message to display front-end side after submit form.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<label for="ibwp-mp-email-message"><?php _e('Email Message','inboundwp-lite'); ?></label>
					<?php wp_editor( $email_message, 'ibwp-mp-email-message', array('textarea_name' => $prefix.'email_message', 'textarea_rows' => 5, 'media_buttons' => false, 'class' => 'ibwp-mp-width') ); ?>
					<span class="description"><?php _e('Enter email message to send user email address after submit form.<br>{user_name} - Username. <br>{user_email} - User Email.', 'inboundwp-lite'); ?></span>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- End Email Settings -->