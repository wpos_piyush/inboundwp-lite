<!-- Start PopUp Content HTML -->
<div id="ibwp_mp_content_options" class="ibwp-stabs-cnt ibwp-mp-content-options ibwp-clearfix" style="display:none;">
	<div class="ibwp-info-wrap">
		<div class="ibwp-mp-title"><?php _e('PopUp Content Settings', 'inboundwp-lite'); ?></div>
	</div>

    <!-- Start PopUp Content Settings -->
	<div class="ibwp-mp-popup-row">
		<table class="form-table ibwp-mp-popup-tbl">
			<tbody>
				<tr>
					<td>
						<label><?php _e('Popup Content','inboundwp-lite'); ?></label>
						<?php wp_editor( $popup_content, 'ibwp-mp-content', array('textarea_name' => $prefix.'popup_content', 'textarea_rows' => 10, 'media_buttons' => false, 'class' => 'ibwp-mp-content') ); ?>
						<span class="description"><?php _e('Enter your popup content description.','inboundwp-lite'); ?></span>
					<td>
				</tr>
			</tbody>
		</table>
    </div>
</div>
<!-- End PopUp Content HTML -->