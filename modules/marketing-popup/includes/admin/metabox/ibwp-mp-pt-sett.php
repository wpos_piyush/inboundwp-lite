<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $post;

$prefix = IBWP_MP_META_PREFIX; // Metabox prefix

// Getting saved values
$modal_popupid 			= get_post_meta( $post->ID, $prefix.'modal_popupid', true );

// Get All popup posts list
$modal_type 			= ibwp_mp_post_by_type('popup-modal');

?>
<table class="form-table ibwp-mp-post-sett-table">
	<tbody>
		<tr>
			<th scope="row">
				<label for="ibwp-mp-modal-popup"><?php _e('Modal Popup', 'inboundwp-lite'); ?>:</label>
			</th>
			<td>
				<select name="<?php echo $prefix;?>modal_popupid" id="ibwp-mp-modal-popup" class="ibwp-select ibwp-mp-select-box ibwp-mp-modal-popup ibwp-select2">
					<option value=""> <?php _e('Select Modal Popup','inboundwp-lite');?> </option>
					<option value="disable" <?php selected('disable',$modal_popupid);?>> <?php _e('Disable','inboundwp-lite');?> </option>
					<?php
					if(!empty($modal_type )) {
						foreach ($modal_type  as $modal_types) {
							echo '<option value="'.$modal_types->ID.'" '.selected($modal_types->ID,$modal_popupid).'>'.$modal_types->post_title.'</option>';
						}
					}
					?>
				</select>
			</td>
		</tr>
	</tbody>
</table><!-- end .ibwp-mp-post-sett-table-->