<?php
/**
 * Tools Page
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Getting Marketing PopUp Post Type
$ibwp_mp_emails_arg = array(
	'post_type'		=> IBWP_MP_POST_TYPE,
	'orderby'		=> 'title',
	'order'			=> 'ASC',
	'post_status'	=> 'publish',
	'posts_per_page'=> -1
);

$ibwp_mp_emails 	= get_posts( $ibwp_mp_emails_arg );
$current_date 		= date_i18n( 'Y-m-d', current_time( 'timestamp' ) ); 
?>

<div class="wrap wp-ibwp-mp-settings">
	<h2><?php _e( 'Export Tools', 'inboundwp-lite' ); ?></h2><br/>
	<div class="wrap ibwp-tools-main">
		<div id="post-body-content">
			<div class="metabox-holder">
				<div class="postbox ibwp-mp-postbox">
					<h3 class="ibwp-title-border">
						<span><?php _e( 'Export - Filter & Export', 'inboundwp-lite' ); ?></span>
					</h3>
					<div class="inside">
						<p><?php _e('Download a CSV of all emails recorded.', 'inboundwp-lite'); ?></p>
						<form id="ibwp-mp-export-emails-form" class="ibwp-mp-export-form ibwp-mp-export-emails-form" method="post">
							<select name="ibwp_mp_popup_title" class="ibwp-mp-filter-popupname ibwp-export-fields ibwp-select2">
								<option value=""><?php _e('Select PopUp','inboundwp-lite'); ?></option>
								<?php
									if( !empty($ibwp_mp_emails) ) {
										foreach ( $ibwp_mp_emails as $post_key => $post_val ) {
											$popup_title = !empty($post_val->post_title) ? $post_val->post_title : sprintf( __('Email - %d', 'epiec'), $post_val->ID );
											
											echo '<option value="'.$post_val->ID.'" ' . selected( isset( $_GET['ibwp_mp_popup_title'] ) ? $_GET['ibwp_mp_popup_title'] : '', $post_val->ID, false ) . '>' . $popup_title . '</option>';
										}
									}
								?>
							</select>
							<input type="date" name="start_date" value="" class="ibwp-export-fields" placeholder="<?php _e('Choose Start Date', 'inboundwp-lite'); ?>" max="<?php echo ibwpl_esc_attr($current_date); ?>" title="Choose Start Date" />
							<input type="date" name="end_date" value="" class="ibwp-export-fields" placeholder="<?php _e('Choose End Date', 'inboundwp-lite'); ?>" max="<?php echo ibwpl_esc_attr($current_date); ?>" title="Choose End Date" />
							<span class="ibwp-export-emails-btn button"><?php _e('Generate CSV','inboundwp-lite'); ?></span>
							<span class="spinner ibwp-mp-spinner"></span>
						</form><br>
						<div class="ibwp-progress-wrap">
							<div class="ibwp-progress"><div class="ibwp-progress-strip"></div></div>
						</div>
					</div><!-- .inside -->
				</div><!-- #general -->
			</div><!-- .metabox-holder -->
		</div><!-- #post-body-content -->
	</div>
</div>