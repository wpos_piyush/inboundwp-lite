<?php
/**
 * Modal Class - Handles the DB functionality
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

class MP_Email_Modal {

	public function __construct() { }
	/**
	 * Get Email Count
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	public function ibwp_mp_email_count( $args = array() ) {
		global $wpdb;

		$email_table 	= IBWP_MP_EMAIL_TABLE;
		$sql 			= "SELECT COUNT(*) FROM $email_table WHERE 1=1";
		$email_count 	= $wpdb->get_var( $sql );
		return $email_count;
	}

	/**
	 * Get Email Data
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	public function ibwp_mp_email_data( $args = array() ) {
		global $wpdb;

    	$posts_table	= $wpdb->prefix . "posts";
    	$email_table 	= IBWP_MP_EMAIL_TABLE;

		$sql = "SELECT * FROM $email_table INNER JOIN $posts_table ON $email_table.popup_id = $posts_table.id WHERE 1=1";
		$args['limit'] 					= !empty( $args['limit'] ) 				? $args['limit'] 			: 15;
		$args['orderby'] 				= !empty( $args['orderby'] ) 			? $args['orderby'] 			: 'created_date';
		$args['order'] 					= !empty( $args['order'] ) 				? $args['order'] 			: 'DESC';
		$args['search'] 				= isset( $args['search'] ) 				? trim( $args['search'] ) 	: '';
		if( !empty( $args['paged'] ) ) {
			$page = $args['paged'];
		} else if ( !empty( $_GET['paged'] ) ) {
			$page = $_GET['paged'];
		} else {
			$page = 1;
		}
		// Query Offset
        $page_offset = ( ( $page * $args['limit'] ) - $args['limit'] );
        // Search
        if( $args['search'] ) {
        	$sql .= " AND `user_name` LIKE '%{$args['search']}%' OR `user_email` LIKE '%{$args['search']}%' OR `post_title` LIKE '%{$args['search']}%' ";
        }
        
        // Order By
		if( $args['orderby'] ) {
			$sql .= " ORDER BY `{$args['orderby']}` {$args['order']} ";
		}
		// Limit
		if( $args['limit'] ) {
			$sql .= " LIMIT {$page_offset},{$args['limit']} ";
		}
		$email_data = $wpdb->get_results( $sql );
		return $email_data;
	}

	/**
	 * Get Email Site Count
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	public function ibwp_mp_export_emails_count( $args = array() ) {
		global $wpdb;
		$posts_table = $wpdb->prefix.'posts';
		$email_table = IBWP_MP_EMAIL_TABLE;
		$args['popup_id'] 		= !empty( $args['popup_id'] ) 		? $args['popup_id'] 		: 0;
		$args['start_date']		= !empty( $args['start_date'] ) 	? date( 'Y-m-d', strtotime($args['start_date']) ) 	: '';
		$args['end_date']		= !empty( $args['end_date'] ) 		? date( 'Y-m-d', strtotime($args['end_date']) ) 	: '';
		$sql = "SELECT COUNT(*) FROM $email_table
				INNER JOIN $posts_table ON $posts_table.ID = $email_table.popup_id 
				WHERE 1=1";
		// Wheel id
		if( $args['popup_id'] ) {
			$sql .= " ANd $email_table.popup_id={$args['popup_id']} ";
		}
		// Start Date
		if( $args['start_date'] ) {
			$sql .= " AND DATE($email_table.created_date) >= '{$args['start_date']}' ";
		}
		// End Date
		if( $args['end_date'] ) {
			$sql .= " AND DATE($email_table.created_date) <= '{$args['end_date']}' ";
		}
		$emails_count = $wpdb->get_var( $sql );
		return $emails_count;
	}

	/**
	 * Get Email Site Count
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	public function ibwp_mp_export_emails_data( $args = array() ) {
		global $wpdb;
		$posts_table = $wpdb->prefix.'posts';
		$email_table = IBWP_MP_EMAIL_TABLE;
		$args['limit'] 			= !empty( $args['limit'] ) 			? $args['limit'] 			: 15;
		$args['page'] 			= !empty( $args['page'] )			? $args['page'] 			: 1;
		$args['popup_id'] 		= !empty( $args['popup_id'] ) 		? $args['popup_id'] 		: 0;
		$args['start_date']		= !empty( $args['start_date'] ) 	? date( 'Y-m-d', strtotime($args['start_date']) ) 	: '';
		$args['end_date']		= !empty( $args['end_date'] ) 		? date( 'Y-m-d', strtotime($args['end_date']) ) 	: '';
		
		$page_offset 			= ( ( $args['page'] * $args['limit'] ) - $args['limit'] ); // Query Offset
		$sql = "SELECT * FROM $email_table
				INNER JOIN $posts_table ON $posts_table.id = $email_table.popup_id";
		// Wheel id
		if( $args['popup_id'] ) {
			$sql .= " AND $email_table.popup_id={$args['popup_id']} ";
		}
		// Start Date
		if( $args['start_date'] ) {
			$sql .= " AND DATE($email_table.created_date) >= '{$args['start_date']}' ";
		}
		// End Date
		if( $args['end_date'] ) {
			$sql .= " AND DATE($email_table.created_date) <= '{$args['end_date']}' ";
		}
		$sql .= " GROUP BY $email_table.id ";
		// Limit
		if( $args['limit'] ) {
			$sql .= " LIMIT {$page_offset},{$args['limit']} ";
		}
		$emails_data = $wpdb->get_results( $sql );

		return $emails_data;
	}
}