<?php
/**
 * Pro Designs and Plugins Feed
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

?>
	
<style type="text/css">
	.ibwp-pro-box .hndle{background-color:#0073AA; color:#fff;}
	.ibwp-pro-box .postbox{background:#dbf0fa none repeat scroll 0 0; border:1px solid #0073aa; color:#191e23;}
	.postbox-container .ibwp-list li:before{font-family: dashicons; content: "\f139"; font-size:20px; color: #0073aa; vertical-align: middle;}
	.ibwp-mp-wrap .ibwp-button-full{display:block; text-align:center; box-shadow:none; border-radius:0;}
	.ibwp-mp-shortcode-preview{background-color: #e7e7e7; font-weight: bold; padding: 2px 5px; display: inline-block; margin:0 0 2px 0;}
</style>
<div class="wrap ibwp-mp-wrap ibwp-hwit-wrap">
    <h2><?php _e('How It Works - Marketing Popup', 'inboundwp-lite'); ?></h2>
    <div class="ibwp-mp-tab-cnt-wrp">
		<div class="post-box-container">
			<div id="poststuff">
				<div id="post-body" class="metabox-holder">			
					<!--How it workd HTML -->
					<div id="post-body-content">
						<div class="metabox-holder">
							<div class="meta-box-sortables ui-sortable">
								<div class="postbox">
									
									<h3 class="hndle">
										<span><?php _e( 'How It Works - Display', 'inboundwp-lite' ); ?></span>
									</h3>
									
									<div class="inside">
										<table class="form-table">
											<tbody>
												<tr>
													<th>
														<label><?php _e('Getting Started', 'inboundwp-lite'); ?>:</label>
													</th>
													<td>
														<ul>
															<li><?php _e('Step-1: Go to "Marketing PopUp - IBWP --> Add New Popup".', 'inboundwp-lite'); ?></li>
															<li><?php _e('Step-2: Select PopUp Goal.','inboundwp-lite'); ?></li>
															<li><?php _e('Step-3: Select PopUp Type.','inboundwp-lite'); ?></li>
															<li><?php _e('Step-4: You can set PopUp Goal & PopUp Type related settings.','inboundwp-lite'); ?></li>
														</ul>
													</td>
												</tr>

												<tr>
													<th>
														<label><?php _e('How Popup Works', 'inboundwp-lite'); ?>:</label>
													</th>
													<td>
														<ul>
															<li><?php _e('Step-1: Go Marketing PopUp - IBWP -->  Settings', 'inboundwp-lite'); ?></li>
															<li><?php _e('Step-2: Under Popup Global Settings, enable popup.', 'inboundwp-lite'); ?></li>
															<li><?php _e('Step-3: Select Marketing Popup Settings Display On.', 'inboundwp-lite'); ?></li>
															<li><?php _e('Step-4: Select Modal Popup Settings.', 'inboundwp-lite'); ?></li>
														</ul>
													</td>
												</tr>
											</tbody>
										</table>
									</div><!-- .inside -->
								</div><!-- #general -->
							</div><!-- .meta-box-sortables ui-sortable -->
						</div><!-- .metabox-holder -->
					</div><!-- #post-body-content -->
				</div><!-- #post-body -->
			</div><!-- #poststuff -->
		</div><!-- #post-box-container -->
	</div>
</div>