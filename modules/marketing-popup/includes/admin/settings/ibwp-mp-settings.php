<?php
/**
 * Settings Page
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $ibwp_mp_options;

$post_types 				= ibwp_mp_display_locations('global');
$locations 					= ibwp_mp_display_locations('main', true);
$ibwp_mp_post_types 		= ibwp_mp_get_option( 'post_types', array() );
$bar_display_in 			= array();
$modal_display_in 			= $ibwp_mp_options['modal_display_in'];
$slider_display_in 			= array();
$notification_display_in 	= array();

?>

<div class="wrap ibwp-sett-wrap ibwp-mp-settings">
	
	<h2><?php _e( 'Marketing PopUp Lite Settings', 'inboundwp-lite' ); ?></h2><br/>
	
	<?php
	// Reset message
	if( !empty( $_POST['ibwp_mp_reset_settings'] ) ) {
		ibwpl_display_message( 'reset' );
	}

	// Success message
	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) {
		ibwpl_display_message( 'update' );
	}
	?>
	<!-- Plugin reset settings form -->
	<form action="" method="post" id="ibwp-mp-reset-sett-form" class="ibwp-right ibwp-mp-reset-sett-form">
		<input type="submit" class="button button-primary right ibwp-btn ibwp-reset-sett ibwp-resett-sett-btn ibwp-mp-reset-sett" name="ibwp_mp_reset_settings" id="ibwp-mp-reset-sett" value="<?php _e( 'Reset All Settings', 'inboundwp-lite' ); ?>" />
	</form>
	<form action="options.php" method="POST" id="ibwp-mp-settings-form" class="ibwp-mp-settings-form">
	<?php
		settings_fields( 'ibwp_mp_plugin_options' );
		
	   	// Get All popup posts list
		$bar_type 				= array();
		$modal_type 			= ibwp_mp_post_by_type('popup-modal');
		$slider_type 			= array();
		$notification_type 		= array();
	?>	
		<div id="ibwp-mp-global-settings" class="post-box-container ibwp-mp-global-settings">
			<div class="textright ibwp-clearfix">
				<input type="submit" name="ibwp_mp_sett_submit" class="button button-primary right ibwp-btn ibwp-mp-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
			</div>
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="ibwp-mp-settings" class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

							<!-- Settings box title -->
							<h3 class="hndle">
								<span><?php _e( 'Popup Global Settings', 'inboundwp-lite' ); ?></span>
							</h3>
							
							<div class="inside">
							<table class="form-table ibwp-mp-settings-tbl">
								<tbody>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-enable"><?php _e('Enable', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<input id="ibwp-mp-enable" class="ibwp-checkbox" type="checkbox" name="ibwp_mp_options[enable]" value="1" <?php checked( ibwp_mp_get_option('enable'), 1 );?>><br>
											<span class="description"><?php _e('Check this box to enable modal popup.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-type"><?php _e('Marketing Popup Settings Display On', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php 
												foreach ($post_types as $post_key => $post_type) {
													$checked_cls = '';
													if( in_array( $post_key, $ibwp_mp_post_types ) ) {
														$checked_cls = 'checked = "checked"';
													} ?>
													<label for="type-<?php echo $post_type;?>">
														<input type="checkbox" class="ibwp-checkbox" id="type-<?php echo $post_type;?>" name="ibwp_mp_options[post_types][]" value="<?php echo $post_key;?>" <?php echo $checked_cls;?>><?php echo $post_type;?>
													</label><br>
													<?php
												} ?>
											<span class="description"><?php _e('Check post type to display modal popup setting metabox on these post types.', 'inboundwp-lite'); ?></span>											
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-cookie-prefix"><?php _e('Cookie Prefix', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<input type="text" id="ibwp-mp-cookie-prefix" class="ibwp-text" name="ibwp_mp_options[cookie_prefix]" value="<?php echo ibwpl_esc_attr( ibwp_mp_get_option('cookie_prefix') ); ?>"><br>
											<span class="description"><?php _e('You can use it for separate cookies from different sites on the same domain.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-enable-mobile"><?php _e('Enable For Mobile Device', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<input id="ibwp-mp-enable-mobile" class="ibwp-checkbox" type="checkbox" name="ibwp_mp_options[enable_mobile]" value="1" <?php checked( ibwp_mp_get_option('enable_mobile'), 1 ); ?>><br>
											<span class="description"><?php _e('Check this box to enable modal popup in mobile device.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							 </table>
						</div><!-- .inside -->
					</div><!-- #ibwp-mp-settings -->
				</div><!-- .meta-box-sortables ui-sortable -->
			</div><!-- .metabox-holder -->
		</div><!-- #ibwp-mp-global-settings -->

		<!-- Start Bar PopUp Settings -->
		<div id="ibwp-mp-popup-bar-settings" class="mp-settt-page-column mp-padding-right post-box-container ibwp-mp-global-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="ibwp-mp-bar-settings" class="postbox">
						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>
						<h3 class="hndle">
							<span><?php _e( 'Bar Popup Settings', 'inboundwp-lite' ); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></span>
						</h3>
						
						<div class="inside">						
							<div class="ibwp-pro-notice">
								<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use Bar Popup.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
							</div>
							<table class="form-table ibwp-mp-settings-tbl ibwp-pro-disable">
								<tbody>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-bar-def-popup"><?php _e('Select Bar Popup', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<select name="ibwp_mp_options[bar_def_popup]" id="ibwp-mp-bar-def-popup" class="ibwp-select ibwp-select2">
												<option value=""><?php _e('Select Bar Popup','inboundwp-lite');?></option>
												<?php
												if(!empty($bar_type )) {
													foreach ($bar_type  as $bar_types ) {
														echo '<option value="'.$bar_types->ID.'" '.selected($bar_types->ID,ibwp_mp_get_option('bar_def_popup')).'>'.$bar_types->post_title.'</option>';
													}
												} ?>
											</select><br>
											<span class="description"><?php _e('Select default bar popup.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-settings"><?php _e('Display In', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php foreach ($locations as $lkey => $lvalue) { ?>
												<label for="ibwp_mp_bar_display_in_<?php echo $lkey; ?>">
													<input id="ibwp_mp_bar_display_in_<?php echo $lkey; ?>" class="ibwp-checkbox" type="checkbox" name="ibwp_mp_options[bar_display_in][<?php echo $lkey; ?>]" value="1" <?php if( array_key_exists( $lkey, $bar_display_in ) ) { checked( true ); } ?>><?php _e($lvalue,'inboundwp-lite');?>
												</label><br>
											<?php } ?>
											<span class="description"><?php _e('check for default bar popup display on these pages.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Bar PopUp Settings -->

		<!-- Start Modal PopUp Settings -->
		<div id="ibwp-mp-popup-modal-settings" class="mp-settt-page-column mp-padding-left post-box-container ibwp-mp-global-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="ibwp-mp-modal-settings" class="postbox">
						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>
						<h3 class="hndle">
							<span><?php _e( 'Modal Popup Settings', 'inboundwp-lite' ); ?></span>
						</h3>
						
						<div class="inside">						
							<table class="form-table ibwp-mp-settings-tbl">
								<tbody>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-modal-def-popup"><?php _e('Select Modal Popup', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<select name="ibwp_mp_options[modal_def_popup]" id="ibwp-mp-modal-def-popup" class="ibwp-select ibwp-select2">
												<option value=""><?php _e('Select Modal Popup','inboundwp-lite');?></option>
												<?php 
												if(!empty($modal_type )) {
													foreach ($modal_type  as $modal_types) {
														echo '<option value="'.$modal_types->ID.'" '.selected($modal_types->ID,ibwp_mp_get_option('modal_def_popup')).'>'.$modal_types->post_title.'</option>';
													}
												} ?>
											</select><br>
											<span class="description"><?php _e('Select default modal popup.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-settings"><?php _e('Display In', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php foreach ($locations as $lkey => $lvalue) { ?>
												<label for="ibwp_mp_modal_display_in_<?php echo $lkey; ?>">
													<input id="ibwp_mp_modal_display_in_<?php echo $lkey; ?>" class="ibwp-checkbox" type="checkbox" name="ibwp_mp_options[modal_display_in][<?php echo $lkey; ?>]" value="1" <?php if( array_key_exists( $lkey, $modal_display_in ) ) { checked( true ); } ?> ><?php _e($lvalue,'inboundwp-lite');?>
												</label><br>
											<?php } ?>
											<span class="description"><?php _e('check for default modal popup display on these pages.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Modal PopUp Settings -->

		<!-- Start Slider PopUp Settings -->
		<div id="ibwp-mp-popup-slider-settings" class="mp-settt-page-column mp-padding-right post-box-container ibwp-mp-global-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="ibwp-mp-slider-settings" class="postbox">
						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>
						<h3 class="hndle">
							<span><?php _e( 'Slider Popup Settings', 'inboundwp-lite' ); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></span>
						</h3>
						
						<div class="inside">	
							<div class="ibwp-pro-notice">
								<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use Slider Popup.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
							</div>					
							<table class="form-table ibwp-mp-settings-tbl ibwp-pro-disable">
								<tbody>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-slider-def-popup"><?php _e('Select Slider Popup', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<select name="ibwp_mp_options[slider_def_popup]" id="ibwp-mp-slider-def-popup" class="ibwp-select ibwp-select2">
												<option value=""><?php _e('Select Slider Popup','inboundwp-lite');?></option>
												<?php 
												if( ! empty($slider_type) ) {
													foreach ($slider_type  as $slider_types) {
														echo '<option value="'.$slider_types->ID.'" '.selected($slider_types->ID,ibwp_mp_get_option('slider_def_popup')).'>'.$slider_types->post_title.'</option>';
													}
												} ?>
											</select><br>
											<span class="description"><?php _e('Select default slider popup.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-settings"><?php _e('Display In', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php foreach ($locations as $lkey => $lvalue) { ?>
												<label for="ibwp_mp_slider_display_in_<?php echo $lkey; ?>">
													<input id="ibwp_mp_slider_display_in_<?php echo $lkey; ?>" type="checkbox" class="ibwp-checkbox" name="ibwp_mp_options[slider_display_in][<?php echo $lkey; ?>]" value="1" <?php if( array_key_exists( $lkey, $slider_display_in ) ) { checked( true ); } ?>><?php _e($lvalue,'inboundwp-lite');?>
												</label><br>
											<?php } ?>
											<span class="description"><?php _e('check for default slider popup display on these pages.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Slider PopUp Settings -->

		<!-- Start Push Notification PopUp Settings -->
		<div id="ibwp-mp-popup-notification-settings" class="mp-settt-page-column mp-padding-left post-box-container ibwp-mp-global-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="ibwp-mp-notification-settings" class="postbox">
						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>
						<h3 class="hndle">
							<span><?php _e( 'Push Notification Popup Settings', 'inboundwp-lite' ); ?> <span class="ibwp-pro-tag"><?php _e('Pro','inboundwp-lite'); ?></span></span>
						</h3>
						
						<div class="inside">
							<div class="ibwp-pro-notice">
								<?php echo sprintf( __('Kindly <a href="%s" target="_blank">Upgrade to Pro</a>, If you want to use Push Notification Popup.', 'inboundwp-lite'), IBWPL_PRO_LINK ); ?>
							</div>						
							<table class="form-table ibwp-mp-settings-tbl ibwp-pro-disable">
								<tbody>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-notification-def-popup"><?php _e('Select Push Notification Popup', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<select name="ibwp_mp_options[notification_def_popup]" id="ibwp-mp-notification-def-popup" class="ibwp-select ibwp-select2">
												<option value=""><?php _e('Select Push Notification Popup','inboundwp-lite');?></option>
												<?php 
												if(!empty($notification_type )) {
													foreach ($notification_type  as $notification_types) {
														echo '<option value="'.$notification_types->ID.'" '.selected($notification_types->ID,ibwp_mp_get_option('notification_def_popup')).'>'.$notification_types->post_title.'</option>';
													}
												} ?>
											</select><br>
											<span class="description"><?php _e('Select default push notification popup.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="ibwp-mp-settings"><?php _e('Display In', 'inboundwp-lite'); ?></label>
										</th>
										<td>
											<?php foreach ($locations as $lkey => $lvalue) { ?>
												<label for="ibwp_mp_notification_display_in_<?php echo $lkey; ?>">
													<input id="ibwp_mp_notification_display_in_<?php echo $lkey; ?>" type="checkbox" class="ibwp-checkbox" name="ibwp_mp_options[notification_display_in][<?php echo $lkey; ?>]" value="1" <?php if( array_key_exists( $lkey, $notification_display_in ) ) { checked( true ); } ?>><?php _e($lvalue,'inboundwp-lite');?>
												</label><br>
											<?php } ?>
											<span class="description"><?php _e('check for default push notification popup display on these pages.', 'inboundwp-lite'); ?></span>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="textright ibwp-clearfix">
			<input type="submit" name="ibwp_mp_sett_submit" class="button button-primary right ibwp-btn ibwp-spin-wheel-sett-submit" value="<?php _e('Save Changes','inboundwp-lite'); ?>" />
		</div>
		<!-- End Push Notification PopUp Settings -->
	</form><!-- end .ibwp-mp-settings-form -->
</div><!-- end .ibwp-mp-settings -->