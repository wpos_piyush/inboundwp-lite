<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_MP_Admin {
	
	function __construct() {

		// Action to add metabox
		add_action( 'add_meta_boxes', array($this, 'ibwp_mp_post_sett_metabox') );

		// Action to save metabox
		add_action( 'save_post', array($this, 'ibwp_mp_save_metabox_value') );
		
		// Action to register admin menu
		add_action( 'admin_menu', array($this, 'ibwp_mp_register_menu') );

		// Action to register plugin settings
		add_action ( 'admin_init', array($this,'ibwp_mp_register_settings') );

		// Filter to add row data
		add_filter( 'post_row_actions', array($this, 'ibwp_mp_post_row_action'), 10, 2 );

		//Add action to duplicate modap popup
		add_action( 'admin_action_ibwp_mp_duplicate_post_as_draft', array($this,'ibwp_mp_duplicate_post_as_draft' ));

		// Action to add custom column to Slider listing
		add_filter( 'manage_'.IBWP_MP_POST_TYPE.'_posts_columns', array($this, 'ibwp_mp_posts_columns') );

		// Action to add custom column data to Slider listing
		add_action('manage_'.IBWP_MP_POST_TYPE.'_posts_custom_column', array($this, 'ibwp_mp_post_columns_data'), 10, 2);

		// Action to export emails data
		add_action( 'wp_ajax_ibwp_mp_do_ajax_export', array($this, 'ibwp_mp_do_ajax_export') );
	}

	/**
	 * Post Settings Metabox
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.2.5
	 */
	function ibwp_mp_post_sett_metabox() {
		// Add metabox in popup posts
		add_meta_box( 'ibwp-mp-pro-post-sett', __( 'Marketing Popup Pro - Settings', 'inboundwp-lite' ), array($this, 'ibwp_mp_cpt_metabox_sett'), IBWP_MP_POST_TYPE, 'normal', 'high' );

		// Add metabox on  page  and posts for custom popup
		//$ibwp_mp_support_post_type = ibwp_mp_get_option('ibwp_mp_post_types');
		//$post_types 			= ibwp_mp_get_post_types();
		$ibwp_mp_post_types 	= ibwp_mp_get_option( 'post_types', array() );
		if(!empty($ibwp_mp_post_types)){
			add_meta_box( 'ibwp-mp-pro-popup-sett', __( 'Marketing Popup Pro - Select Popup', 'inboundwp-lite' ), array($this, 'ibwp_mp_pt_metabox_sett'), $ibwp_mp_post_types, 'normal', 'high' );
		}	
	}

	/**
	 * Post Settings Metabox HTML
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_cpt_metabox_sett()
	{
		include_once( IBWP_MP_DIR .'/includes/admin/metabox/ibwp-mp-cpt-sett.php');
	}

	/**
	 * Post,Page Select Modal Poup Metabox HTML
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_pt_metabox_sett()
	{
		include_once( IBWP_MP_DIR .'/includes/admin/metabox/ibwp-mp-pt-sett.php');
	}

	/**
	 * Function to save metabox values
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_save_metabox_value( $post_id ) {
		
		global $post_type;
		$prefix 				= IBWP_MP_META_PREFIX; // Taking metabox prefix
		
		// Taking bar popup id from page and post
		$modal_popupid 			= isset($_POST[$prefix.'modal_popupid']) 			? ibwpl_clean($_POST[$prefix.'modal_popupid']) 			: '';
		update_post_meta($post_id, $prefix.'modal_popupid', $modal_popupid);
		
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                	// Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )  	// Check Revision
		|| ( $post_type !=  IBWP_MP_POST_TYPE ) )              					// Check if current post type is supported.
		{
			return $post_id;
		}		
		
		// Main Config Settings
		$data['tab'] 						= isset($_POST[$prefix.'tab']) 							? ibwpl_clean($_POST[$prefix.'tab']) 								: '';
		$data['popup_goal'] 				= isset($_POST[$prefix.'popup_goal'])					? ibwpl_clean($_POST[$prefix.'popup_goal'])							: '';
		$data['popup_type'] 				= isset($_POST[$prefix.'popup_type'])					? ibwpl_clean($_POST[$prefix.'popup_type'])							: '';
		$data['popup_content'] 				= !empty($_POST[$prefix.'popup_content'])				? ibwpl_clean_html($_POST[$prefix.'popup_content'])					: __('Lorem Ipsum is simply dummy text of the printing and typesetting industry.','inboundwp-lite');
		
		// Design setting
		$data['popup_design'] 				= !empty($_POST[$prefix.'popup_design'])				? ibwpl_clean($_POST[$prefix.'popup_design'])						: __('design-1','inboundwp-lite');
		
		// Background Settings
		$data['gen_bg_color'] 				= !empty($_POST[$prefix.'gen_bg_color']) 				? ibwpl_clean_color($_POST[$prefix.'gen_bg_color']) 					: '#ffffff';
		$data['gen_bg_img'] 				= isset($_POST[$prefix.'gen_bg_img']) 					? ibwpl_clean($_POST[$prefix.'gen_bg_img']) 							: '';
		$data['gen_bg_size'] 				= isset($_POST[$prefix.'gen_bg_size']) 					? ibwpl_clean($_POST[$prefix.'gen_bg_size']) 						: '';
		$data['gen_bg_img_repeat'] 			= isset($_POST[$prefix.'gen_bg_img_repeat']) 			? ibwpl_clean($_POST[$prefix.'gen_bg_img_repeat']) 					: '';
		$data['gen_bg_img_position'] 		= isset($_POST[$prefix.'gen_bg_img_position']) 			? ibwpl_clean($_POST[$prefix.'gen_bg_img_position']) 				: '';

		$data['when_popup_appear'] 			= isset($_POST[$prefix.'when_popup_appear'])			? ibwpl_clean($_POST[$prefix.'when_popup_appear'])					: '';
		$data['disappear'] 					= isset($_POST[$prefix.'disappear'])					? ibwpl_clean_number($_POST[$prefix.'disappear'])					: '';
		$data['exptime'] 					= isset($_POST[$prefix.'exptime'])						? ibwpl_clean_number($_POST[$prefix.'exptime'])						: '';
		$data['hideclsbtn'] 				= !empty($_POST[$prefix.'hideclsbtn'])					? 1																	: 0;
		$data['clsonesc'] 					= !empty($_POST[$prefix.'clsonesc'])					? 1																	: 0;
		$data['delay'] 						= isset($_POST[$prefix.'delay'])						? ibwpl_clean_number($_POST[$prefix.'delay'])						: '';

		if( $data['popup_goal'] != 'announcement' || $data['popup_goal'] != 'social-traffic' ) {
			
			// Button Settings
			$data['gen_btn_text'] 				= !empty($_POST[$prefix.'gen_btn_text'])				? ibwpl_clean($_POST[$prefix.'gen_btn_text'])						: __('Subscribe','inboundwp-lite');
			$data['gen_btn_bg_color'] 			= !empty($_POST[$prefix.'gen_btn_bg_color'])			? ibwpl_clean_color($_POST[$prefix.'gen_btn_bg_color'])				: '#191e23';
			$data['gen_btn_text_color'] 		= !empty($_POST[$prefix.'gen_btn_text_color'])			? ibwpl_clean_color($_POST[$prefix.'gen_btn_text_color'])			: '#ffffff';
		}

		if( $data['popup_goal'] == 'email-lists' ) {

			// Field Settings
			$data['hide_name_field'] 			= !empty($_POST[$prefix.'hide_name_field'])				? 1																	: 0;
			$data['hide_phone_field'] 			= !empty($_POST[$prefix.'hide_phone_field'])			? 1																	: 0;
			$data['name_field_ph'] 				= !empty($_POST[$prefix.'name_field_ph'])				? ibwpl_clean($_POST[$prefix.'name_field_ph'])						: __('Your Name','inboundwp-lite');
			$data['email_field_ph'] 			= !empty($_POST[$prefix.'email_field_ph'])				? ibwpl_clean($_POST[$prefix.'email_field_ph'])						: __('Your Email','inboundwp-lite');
			$data['phone_field_ph'] 			= !empty($_POST[$prefix.'phone_field_ph'])				? ibwpl_clean($_POST[$prefix.'phone_field_ph'])						: __('Your Phone Number','inboundwp-lite');
			$data['field_bg_color'] 			= !empty($_POST[$prefix.'field_bg_color'])				? ibwpl_clean_color($_POST[$prefix.'field_bg_color'])				: '#e7e7e7';
			$data['field_text_color'] 			= !empty($_POST[$prefix.'field_text_color'])			? ibwpl_clean_color($_POST[$prefix.'field_text_color'])				: '#000000';
		}

		// Collect Emails settings
		if( $data['popup_goal'] == 'email-lists' ) {
			$data['email_subject'] 				= !empty($_POST[$prefix.'email_subject'])				? ibwpl_clean($_POST[$prefix.'email_subject'])						: __('Subscribe','inboundwp-lite');
			$data['email_heading'] 				= !empty($_POST[$prefix.'email_heading'])				? ibwpl_clean($_POST[$prefix.'email_heading'])						: __('Thank you for subscribe','inboundwp-lite');
			$data['thank_you_msg'] 				= !empty($_POST[$prefix.'thank_you_msg'])				? ibwpl_clean_html($_POST[$prefix.'thank_you_msg'])					: __('Thank you for subscription.','inboundwp-lite');
			$data['email_message'] 				= !empty($_POST[$prefix.'email_message'])				? ibwpl_clean_html($_POST[$prefix.'email_message'])					: __('Dear {user_name},<br>Thank you for subscribing to Really Good Emails.','inboundwp-lite');
		}

		// Target URL Settings
		if( $data['popup_goal'] == 'target-url' ) { 
			$data['target_url_link'] 			= isset($_POST[$prefix.'target_url_link'])				? ibwpl_clean_url($_POST[$prefix.'target_url_link'])					: '';
			$data['open_link_url'] 				= isset($_POST[$prefix.'open_link_url'])				? ibwpl_clean($_POST[$prefix.'open_link_url'])						: '';
		}

		// Get Phone Calls Settings
		if( $data['popup_goal'] == 'phone-calls' ) { 
			$data['phone_number']				= isset($_POST[$prefix.'phone_number'])					? ibwpl_clean($_POST[$prefix.'phone_number'])						: __('+919898989898','inboundwp-lite');
		}

		// Social Traffic Settings
		if( $data['popup_goal'] == 'social-traffic' ) { 

			$data['icon_design'] 				= isset($_POST[$prefix.'icon_design'])					? ibwpl_clean($_POST[$prefix.'icon_design'])							: '';
			$social_data = isset($_POST[$prefix.'social_traffic']) ? ibwpl_clean($_POST[$prefix.'social_traffic']) : "";
			if( ! empty( $social_data ) ) {
				$i = 0;
				foreach ( $social_data as $skey => $sval ) {
					$social_traffic[$i]['type'] 	= isset($sval['type']) 		? ibwpl_clean($sval['type']) 		: ''; 
					$social_traffic[$i]['link'] 	= isset($sval['link']) 		? ibwpl_clean_url($sval['link']) 	: '#';
				$i++; }
				$data['social_traffic'] = array_values(array_map("unserialize", array_unique(array_map("serialize", $social_traffic))));
			}
		}

		// Modal Popup Design Settings
		if( $data['popup_type'] == "popup-modal" ) {

			$data['fullscreen_popup'] 			= isset($_POST[$prefix.'fullscreen_popup'])				? 1																	: 0;
			$data['modal_width'] 				= !empty($_POST[$prefix.'modal_width'])					? ibwpl_clean_number($_POST[$prefix.'modal_width'])					: 600;
			$data['modal_position'] 			= isset($_POST[$prefix.'modal_position'])				? ibwpl_clean($_POST[$prefix.'modal_position'])						: '';

		}

		// Content Settings
		if( $data['popup_type'] == "slider" || $data['popup_type'] == "popup-modal" || $data['popup_type'] == "push-notification" ) {

			$data['gen_main_heading'] 			= isset($_POST[$prefix.'gen_main_heading'])				? ibwpl_clean($_POST[$prefix.'gen_main_heading'])					: '';
			$data['gen_main_heading_fontsize'] 	= !empty($_POST[$prefix.'gen_main_heading_fontsize'])	? ibwpl_clean_number($_POST[$prefix.'gen_main_heading_fontsize'])	: 30;
			$data['gen_main_heading_text_color']= !empty($_POST[$prefix.'gen_main_heading_text_color'])	? ibwpl_clean_color($_POST[$prefix.'gen_main_heading_text_color'])	: '#191e23';
			$data['gen_sub_heading'] 			= isset($_POST[$prefix.'gen_sub_heading'])				? ibwpl_clean($_POST[$prefix.'gen_sub_heading'])						: '';
			$data['gen_sub_heading_fontsize'] 	= !empty($_POST[$prefix.'gen_sub_heading_fontsize'])	? ibwpl_clean_number($_POST[$prefix.'gen_sub_heading_fontsize'])		: 25;
			$data['gen_sub_heading_text_color'] = !empty($_POST[$prefix.'gen_sub_heading_text_color'])	? ibwpl_clean_color($_POST[$prefix.'gen_sub_heading_text_color'])	: '#dc3232';
			$data['desc_text_color'] 			= !empty($_POST[$prefix.'desc_text_color'])				? ibwpl_clean_color($_POST[$prefix.'desc_text_color'])				: '#23282d';
		}

		if( !empty( $data ) ){
			foreach ($data as $mkey => $mval) {
				update_post_meta( $post_id, $prefix.$mkey, $mval );
			}
		}
	}

	/**
	 * Function to register admin menus
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_register_menu() {

		// Add emails page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_MP_POST_TYPE, __('Emails', 'inboundwp-lite'), __('Emails', 'inboundwp-lite'), 'manage_options', 'ibwp-mp-emails', array($this, 'ibwp_mp_emails_page') );

		// Add Tools page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_MP_POST_TYPE, __('Tools', 'inboundwp-lite'), __('Tools', 'inboundwp-lite'), 'manage_options', 'ibwp-mp-tools', array($this, 'ibwp_mp_tools_page') );

		// Add Settings page submenu
		add_submenu_page( 'edit.php?post_type='.IBWP_MP_POST_TYPE, __('Settings', 'inboundwp-lite'), __('Settings', 'inboundwp-lite'), 'manage_options', 'ibwp-mp-pro-settings', array($this, 'ibwp_mp_settings_page') );

		add_submenu_page( 'edit.php?post_type='.IBWP_MP_POST_TYPE, __('How it works - Notification Popup Pro', 'inboundwp-lite'), __('How It Works', 'inboundwp-lite'), 'manage_options', 'ibwp-mp-how-it-work', array( $this, 'ibwp_mp_how_it_work_page' ) );
	}

	/**
	 * Function to handle the emails page html
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_emails_page() {
		include_once( IBWP_MP_DIR . '/includes/admin/emails/ibwp-mp-email-list.php' );
	}

	/**
	 * Function to handle the tools page html
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_tools_page() {
		include_once( IBWP_MP_DIR . '/includes/admin/tools/ibwp-mp-tools.php' );
	}

	/**
	 * Function to handle the setting page html
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_settings_page() {
		include_once( IBWP_MP_DIR . '/includes/admin/settings/ibwp-mp-settings.php' );
	}

	/**
	 * Function to handle the how it work page html
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_how_it_work_page() {
		include_once( IBWP_MP_DIR . '/includes/admin/ibwp-mp-how-it-work.php' );
	}

	/**
	 * Function register setings
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_register_settings(){
		global $wpdb;

		// Reset default settings
		if( !empty( $_POST['ibwp_mp_reset_settings'] ) ) {
			ibwp_mp_default_settings();
		}

		register_setting( 'ibwp_mp_plugin_options', 'ibwp_mp_options', array($this, 'ibwp_mp_validate_options') );


		// Delete Email Action
		if( (isset( $_GET['action'] ) && $_GET['action'] == 'delete' ) || (isset( $_GET['action2'] ) && $_GET['action2'] == 'delete' ) && isset($_GET['page']) && $_GET['page'] == 'ibwp-mp-emails' && ( isset( $_GET['ibwp_mp_nonce'] ) && wp_verify_nonce( $_GET['ibwp_mp_nonce'], 'ibwp-mp-delete-email' ) ) ) {
			$email_id = !empty( $_GET['ibwp_mp_emails'] ) ? $_GET['ibwp_mp_emails'] : '';

			if( !empty($email_id) && is_array( $email_id ) ){
				
				foreach ($email_id as $key => $id) {
					$status = $wpdb->delete( IBWP_MP_EMAIL_TABLE, array( 'id' => $id ) );
				}

				if($status){
					$page_url 	  = add_query_arg( array( 'post_type' => IBWP_MP_POST_TYPE, 'page' => 'ibwp-mp-emails','message' => '1' ), admin_url('edit.php') );
					wp_redirect( $page_url );
					exit;
				}
			}
		}

	}

	/**
	 * Validate Settings Options
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_validate_options( $input ) {

		$input['enable'] 						= isset($input['enable']) 						? 1 								: 0;
		$input['cookie_prefix'] 				= isset($input['cookie_prefix']) 				? $input['cookie_prefix']			: '';
		$input['enable_mobile'] 				= isset($input['enable_mobile']) 				? $input['enable_mobile']			: 0;
		$input['post_types']					= isset($input['post_types']) 					? $input['post_types']				: array();
		
		$input['modal_def_popup'] 			= isset($input['modal_def_popup']) 					? $input['modal_def_popup']			: 0;
		$input['modal_display_in'] 				= isset($input['modal_display_in']) 			? $input['modal_display_in']		: array();
		
		return $input;	

	}

	/**
	 * Function to add custom quick links at post listing page
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_post_row_action($actions, $post ) {
		
		if (current_user_can('edit_posts')) {
		$actions['ibwp_mp_duplicate'] = '<a href="admin.php?action=ibwp_mp_duplicate_post_as_draft&post=' . $post->ID . '" title="Duplicate this item" rel="permalink">'. __('Duplicate','inboundwp-lite').'</a>';
		}
		if( $post->post_type == IBWP_MP_POST_TYPE ) {
			return array_merge( array( 'ibwp_mp_id' => 'ID: ' . $post->ID ), $actions );
		}
		return $actions;
	}

	/**
	 * Function to add duplicate popup
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_duplicate_post_as_draft() {
		global $wpdb;
		
		if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'ibwp_mp_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
			wp_die(__('No post to duplicate has been supplied!','inboundwp-lite'));
		}

		/*
		 * get the original post id
		 */
		$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
		/*
		 * and all the original post data then
		 */
		$post = get_post( $post_id );

		$current_user = wp_get_current_user();
		$new_post_author = $current_user->ID;

		/*
		 * if post data exists, create the post duplicate
		 */
		if (isset( $post ) && $post != null) {

			/*
			 * new post data 
			 */
			$args = array(
				'comment_status' => $post->comment_status,
				'ping_status'    => $post->ping_status,
				'post_author'    => $new_post_author,
				'post_content'   => $post->post_content,
				'post_excerpt'   => $post->post_excerpt,
				'post_name'      => $post->post_name,
				'post_parent'    => $post->post_parent,
				'post_password'  => $post->post_password,
				'post_status'    => 'draft',
				'post_title'     => $post->post_title.' '.__('Duplicate','inboundwp-lite'),
				'post_type'      => $post->post_type,
				'to_ping'        => $post->to_ping,
				'menu_order'     => $post->menu_order
			);

			/*
			 * insert the new duplicate post by wp_insert_post() function
			 */
			$new_post_id = wp_insert_post( $args );
			
			$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
			if (count($post_meta_infos)!=0) {
				$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
				foreach ($post_meta_infos as $meta_info) {
					$meta_key = $meta_info->meta_key;
					$meta_value = addslashes($meta_info->meta_value);
					$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
				}
				$sql_query.= implode(" UNION ALL ", $sql_query_sel);

				$wpdb->query($sql_query);
			}

			/*
			 * redirect to the edit post screen for the new draft
			 */
			wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
			exit;
		}
		else{
			wp_die(__('Post creation failed, could not find original post: ','inboundwp-lite') . $post_id);
		}
	}

	/**
	 * Add custom column to Post listing page
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_posts_columns( $columns ){

		$new_columns['mp_popup_goal'] 	= __( 'PopUp Goal', 'inboundwp-lite' );
		$new_columns['mp_popup_type'] 	= __( 'PopUp Type', 'inboundwp-lite' );
	    
	    $columns = ibwpl_add_array( $columns, $new_columns, 1, true );

	    return $columns;
	}

	/**
	 * Add custom column data to Post listing page
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_post_columns_data( $column, $post_id ) {

		global $post;
		$prefix 		= IBWP_MP_META_PREFIX;

		$popup_goal 	= get_post_meta($post->ID, $prefix.'popup_goal', true);
		$popup_type 	= get_post_meta($post->ID, $prefix.'popup_type', true);

	    switch ($column) {
			case 'mp_popup_goal':				
				echo $popup_goal;
				break;

			case 'mp_popup_type':				
				echo $popup_type;
				break;
		}
	}

	/**
	* Email Filter Data
	* 
	* @subpackage Marketing Popup
	* @since 1.0
	*/
	function ibwp_mp_do_ajax_export() {

		// Taking some variables
		$result = array();

		// Taking passed data
		parse_str( $_POST['form_data'], $form_data );

		$ibwp_mp_action 	= isset($_POST['ibwp_mp_action']) 	? ibwpl_clean( $_POST['ibwp_mp_action'] )	: '';
		$page 				= isset($_POST['page']) 			? $_POST['page'] 							: 1;
		$total_count 		= isset($_POST['total_count']) 		? $_POST['total_count']						: 0;
		$data_process 		= isset($_POST['data_process']) 	? $_POST['data_process'] 					: 0;

		if( empty( $ibwp_mp_action ) ||  ! wp_verify_nonce( $form_data['ibwp_mp_export'], 'ibwp_mp_export' ) ) {
			$result['status'] 	= 0;
			$result['msg'] 		= __('Sorry, Something happened wrong.', 'inboundwp-lite');
		}

		// Gathering all data
		$form_data 					= (array) $form_data;
		$form_data['page'] 			= $page;
		$form_data['total_count'] 	= $total_count;
		$form_data['data_process'] 	= $data_process;
		$form_data['is_ajax'] 		= true;

		if ( function_exists( 'ibwp_mp_'.$ibwp_mp_action ) ) {
			$result = call_user_func( 'ibwp_mp_'.$ibwp_mp_action, $form_data );
		}

		echo json_encode( $result );
		exit;
	}
}
$ibwp_mp_admin = new IBWP_MP_Admin();