<?php
/**
 * Functions File
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Update default settings
 * 
 * @subpackage Marketing Popup
 * @since 1.0
 */
function ibwp_mp_default_settings(){	
	
	global $ibwp_mp_options;
	
	$ibwp_mp_options = array(
							'enable'					=> 1,
							'cookie_prefix'				=> '',
							'post_types'				=> array('page','post'),
							'enable_mobile'				=> 1,
							'modal_def_popup'			=> '',
							'modal_display_in' 			=> 	array(),
						);
	
	$default_options = apply_filters('ibwp_mp_options_default_values', $ibwp_mp_options );
	
	// Update default options
	update_option( 'ibwp_mp_options', $default_options );
	
	// Overwrite global variable when option is update	
	$ibwp_mp_options = ibwpl_get_settings( 'ibwp_mp_options' );
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @subpackage Marketing Popup
 * @since 1.0
 */
function ibwp_mp_get_option( $key = '', $default = false ) {

	global $ibwp_mp_options;
	$value 	= ! empty( $ibwp_mp_options[ $key ] ) ? $ibwp_mp_options[ $key ] : $default;
	return $value;
}

/**
 * Create Email List store table
 * 
 * @subpackage Marketing Popup
 * @since 1.0
 */
function ibwp_mp_create_email_table() {

	global $wpdb;

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE `".IBWP_MP_EMAIL_TABLE."` (
						id INT(20) unsigned NOT NULL AUTO_INCREMENT,
						popup_id INT(20) unsigned NOT NULL,
						page_id INT(20) unsigned NOT NULL,
						user_name varchar(250) NOT NULL,
						user_email varchar(100) NOT NULL,
						user_number varchar(50) NOT NULL,
						popup_goal varchar(250) NOT NULL,
						popup_type varchar(250) NOT NULL,
						created_date datetime NOT NULL,
						modification_date datetime NOT NULL,
						PRIMARY KEY  (id)
					) $charset_collate;";
	dbDelta( $sql );
}

/**
 * Function to get popup featured image
 * 
 * @subpackage Marketing Popup
 * @since 1.0
*/
function ibwp_mp_get_post_featured_image( $post_id = '', $size = 'full', $default_img = false ) {

    $size   = !empty($size) ? $size : 'full';
    $image  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );

    if( !empty($image) ) {
        $image = isset($image[0]) ? $image[0] : '';
    }

    return $image;
}

/**
 * Function get repeat options
 * 
 * @subpackage Marketing Popup
 * @since 1.0
 */
function ibwp_mp_repeat_options() {

	$repeat_options = array(	
							'no-repeat' => __('No-Repeat','inboundwp-lite'),
							'repeat' 	=> __('Repeat','inboundwp-lite'),								
							'repeat-x' 	=> __('Repeat-X','inboundwp-lite'),
							'repeat-y' 	=> __('Repeat-Y','inboundwp-lite'),
						);
	return $repeat_options;
}

/**
 * Get popup post type list
 * 
 * @subpackage Marketing Popup
 * @since 1.0
 */
function ibwp_mp_post_by_type( $type = '' ) {

    $args = array(
        'post_type'         => IBWP_MP_POST_TYPE, 
        'post_status'       => 'publish', 
        'posts_per_page'    => -1,
        'order'             => 'DESC',
        'suppress_filters'  => false,
    );

    if( $type == 'bar' ) {

        $args['meta_query'][0] = array( 
            'key'       => IBWP_MP_META_PREFIX.'popup_type',
            'value'     => 'bar',
            'compare'   => '='
        );
    }

    if( $type == 'popup-modal' ) {

        $args['meta_query'][0] = array( 
            'key'       => IBWP_MP_META_PREFIX.'popup_type',
            'value'     => 'popup-modal',
            'compare'   => '='
        );
    }

    if( $type == 'slider' ) {

        $args['meta_query'][0] = array( 
            'key'       => IBWP_MP_META_PREFIX.'popup_type',
            'value'     => 'slider',
            'compare'   => '='
        );
    }

    if( $type == 'push-notification' ) {

        $args['meta_query'][0] = array( 
            'key'       => IBWP_MP_META_PREFIX.'popup_type',
            'value'     => 'push-notification',
            'compare'   => '='
        );
    }
    $mp_posts = get_posts( $args );
    return $mp_posts;
}

/**
 * Function get popup meta
 * 
 * @subpackage Marketing Popup
 * @since 1.0 
 **/
function ibwp_mp_post_metadata( $post_id ) {

	$prefix 	= IBWP_MP_META_PREFIX; // Metabox prefix
	$post_data 	= array();

	// Set Value in Postdata
	$post_data['popup_goal'] 					= get_post_meta( $post_id, $prefix.'popup_goal', true );
	$post_data['popup_type'] 					= get_post_meta( $post_id, $prefix.'popup_type', true );
	$post_data['popup_content'] 				= get_post_meta( $post_id, $prefix.'popup_content', true );
	$post_data['popup_design'] 					= get_post_meta( $post_id, $prefix.'popup_design', true );
	$post_data['gen_btn_text'] 					= get_post_meta( $post_id, $prefix.'gen_btn_text', true );
	$post_data['gen_btn_bg_color'] 				= get_post_meta( $post_id, $prefix.'gen_btn_bg_color', true );
	$post_data['gen_btn_text_color'] 			= get_post_meta( $post_id, $prefix.'gen_btn_text_color', true );
	$post_data['hide_name_field'] 				= get_post_meta( $post_id, $prefix.'hide_name_field', true );
	$post_data['hide_phone_field'] 				= get_post_meta( $post_id, $prefix.'hide_phone_field', true );
	$post_data['name_field_ph'] 				= get_post_meta( $post_id, $prefix.'name_field_ph', true );
	$post_data['email_field_ph'] 				= get_post_meta( $post_id, $prefix.'email_field_ph', true );
	$post_data['phone_field_ph'] 				= get_post_meta( $post_id, $prefix.'phone_field_ph', true );
	$post_data['name_field_ph'] 				= get_post_meta( $post_id, $prefix.'name_field_ph', true );
	$post_data['name_field_ph'] 				= get_post_meta( $post_id, $prefix.'name_field_ph', true );
	$post_data['field_bg_color'] 				= get_post_meta( $post_id, $prefix.'field_bg_color', true );
	$post_data['field_text_color'] 				= get_post_meta( $post_id, $prefix.'field_text_color', true );
	$post_data['gen_bg_color'] 					= get_post_meta( $post_id, $prefix.'gen_bg_color', true );
	$post_data['gen_bg_img'] 					= get_post_meta( $post_id, $prefix.'gen_bg_img', true );
	$post_data['gen_bg_size'] 					= get_post_meta( $post_id, $prefix.'gen_bg_size', true );
	$post_data['gen_bg_img_repeat'] 			= get_post_meta( $post_id, $prefix.'gen_bg_img_repeat', true );
	$post_data['gen_bg_img_position'] 			= get_post_meta( $post_id, $prefix.'gen_bg_img_position', true );
	$post_data['gen_main_heading'] 				= get_post_meta( $post_id, $prefix.'gen_main_heading', true );
	$post_data['gen_main_heading_fontsize'] 	= get_post_meta( $post_id, $prefix.'gen_main_heading_fontsize', true );
	$post_data['gen_main_heading_text_color']	= get_post_meta( $post_id, $prefix.'gen_main_heading_text_color', true );
	$post_data['gen_sub_heading'] 				= get_post_meta( $post_id, $prefix.'gen_sub_heading', true );
	$post_data['gen_sub_heading_fontsize']		= get_post_meta( $post_id, $prefix.'gen_sub_heading_fontsize', true );
	$post_data['gen_sub_heading_text_color'] 	= get_post_meta( $post_id, $prefix.'gen_sub_heading_text_color', true );
	$post_data['fullscreen_popup'] 				= get_post_meta( $post_id, $prefix.'fullscreen_popup', true );
	$post_data['modal_width'] 					= get_post_meta( $post_id, $prefix.'modal_width', true );
	$post_data['modal_position'] 				= get_post_meta( $post_id, $prefix.'modal_position', true );
	$post_data['desc_text_color'] 				= get_post_meta( $post_id, $prefix.'desc_text_color', true );
	$post_data['phone_number'] 					= get_post_meta( $post_id, $prefix.'phone_number', true );
	$post_data['icon_design'] 					= get_post_meta( $post_id, $prefix.'icon_design', true );
	$post_data['social_traffic'] 				= get_post_meta( $post_id, $prefix.'social_traffic', true );
	$post_data['target_url_link'] 				= get_post_meta( $post_id, $prefix.'target_url_link', true );
	$post_data['open_link_url'] 				= get_post_meta( $post_id, $prefix.'open_link_url', true );
	$post_data['when_popup_appear'] 			= get_post_meta( $post_id, $prefix.'when_popup_appear', true );
	$post_data['delay'] 						= get_post_meta( $post_id, $prefix.'delay', true );
	$post_data['disappear'] 					= get_post_meta( $post_id, $prefix.'disappear', true );
	$post_data['exptime']						= get_post_meta( $post_id, $prefix.'exptime', true );
	$post_data['hideclsbtn'] 					= get_post_meta( $post_id, $prefix.'hideclsbtn', true );
	$post_data['clsonesc'] 						= get_post_meta( $post_id, $prefix.'clsonesc', true );

	return $post_data;
}

/**
 * Function get popup meta
 * 
 * @subpackage Marketing Popup
 * @since 1.0 
 **/
function ibwp_mp_popup_config($post_id)
{
	$prefix 	= IBWP_MP_META_PREFIX; // Metabox prefix
	$post_data 	= array();

	// set values in $post_data
	$post_data['popup_ID'] 				= $post_id;
	$post_data['popup_goal'] 			= get_post_meta( $post_id, $prefix.'popup_goal', true );
	$post_data['popup_type'] 			= get_post_meta( $post_id, $prefix.'popup_type', true );
	$post_data['gen_btn_text'] 			= get_post_meta( $post_id, $prefix.'gen_btn_text', true );
	$post_data['email_subject'] 		= get_post_meta( $post_id, $prefix.'email_subject', true );
	$post_data['email_heading'] 		= get_post_meta( $post_id, $prefix.'email_heading', true );
	$post_data['thank_you_msg'] 		= get_post_meta( $post_id, $prefix.'thank_you_msg', true );
	$post_data['email_message'] 		= get_post_meta( $post_id, $prefix.'email_message', true );
	$post_data['fullscreen_popup'] 		= get_post_meta( $post_id, $prefix.'fullscreen_popup', true );
	$post_data['when_popup_appear'] 	= get_post_meta( $post_id, $prefix.'when_popup_appear', true );
	$post_data['delay'] 				= get_post_meta( $post_id, $prefix.'delay', true );
	$post_data['disappear'] 			= get_post_meta( $post_id, $prefix.'disappear', true );
	$post_data['exptime']				= get_post_meta( $post_id, $prefix.'exptime', true );
	$post_data['hideclsbtn'] 			= get_post_meta( $post_id, $prefix.'hideclsbtn', true );
	$post_data['clsonesc'] 				= get_post_meta( $post_id, $prefix.'clsonesc', true );
	$post_data['slider_slide_time'] 	= get_post_meta( $post_id, $prefix.'slider_slide_time', true );
	
	return $post_data;
}

/**
 * Function check cookie set or not
 * 
 * @subpackage Marketing Popup
 * @since 1.0 
 **/
function ibwp_mp_is_cooke_set( $post_id, $type='bar' ) {
	$ibwp_mp_options 	= ibwpl_get_settings( 'ibwp_mp_options' );
	$cookie_prefix 		= $ibwp_mp_options['cookie_prefix'];
	$cookie_var 		= $cookie_prefix.'ibwp_mp_'.$type.'_'.$post_id;

	if(isset($_COOKIE[$cookie_var])){
		return true;
	}
	return false;
}

/**
 * Function to display location of admin
 * 
 * @package Marketing Popup 
 * @since 1.0
 */
function ibwp_mp_display_locations( $type = 'main', $all = false ) {

    $locations = array();
    $exclude   = array('attachment','revision','nav_menu_item');   
    $all_post_types = get_post_types( array( 'public' => true ), 'name' );
    $post_types     = array();
    
    foreach ($all_post_types as $post_type => $post_data) {
        if(!in_array( $post_type,$exclude ) ){
            $locations[$post_type] = !empty($all) ? "All ". $post_data->label : $post_data->label;
        }   
    }

    if ( 'global' == $type ) {
        return $locations;
    }

    $glocations = array(
        'is_search'      => __( 'Search results', 'inbount-wp' ),
        'is_404'         => __( '404 error page', 'inbount-wp' ),
        'is_archive'     => __( 'All archives', 'inbount-wp' ),
        'all'            => __( 'Whole Site', 'inbount-wp' ),
    );

    $locations = array_merge($locations, $glocations);

    return $locations;
}