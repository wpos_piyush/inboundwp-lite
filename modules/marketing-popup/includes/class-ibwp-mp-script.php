<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_MP_Script {
	
	function __construct() {
		
		// Action to add style on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_mp_front_end_style') );

		// Action to add script on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_mp_front_end_script'), 15 );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_mp_admin_style') );

		// Action to add script in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_mp_admin_script') );
	}
	
	/**
	 * Enqueue front end styles
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_front_end_style(){
		// Registring Public Style
		wp_register_style( 'ibwp-mp-public-css', IBWP_MP_URL.'assets/css/ibwp-mp-public.css', null, IBWP_MP_VERSION );
		wp_enqueue_style('ibwp-mp-public-css');

	}

	/**
	 * Enqueue front script
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_front_end_script() {

		global $post;

		if( empty( $post ) ) {
			return;
		}

		$prefix 		= IBWP_MP_META_PREFIX;
		$page_id 		= $post->ID;

		// Registring Popup script
		wp_register_script( 'ibwp-mp-public-js', IBWP_MP_URL.'assets/js/ibwp-mp-public.js', array('jquery'), IBWP_MP_VERSION, true );
		wp_localize_script( 'ibwp-mp-public-js', 'IBWP_MP_Popup', array(
				'ajaxurl'			=> admin_url( 'admin-ajax.php' ),
				'enable'			=> ibwp_mp_get_option('enable'),
				'cookie_prefix'		=> ibwp_mp_get_option('cookie_prefix'),
				'valid_email_msg'	=> __('Enter a valid email address.','inbounwp-pro'),
				'enter_email_msg'	=> __('Enter your email address.','inbounwp-pro'),
				'page_id'			=> $page_id,
				'plugin_url'		=> IBWP_MP_URL
		));	
		wp_enqueue_script('ibwp-mp-public-js');	
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_admin_style() {
		global $typenow;

		// If page is plugin setting page then enqueue script
		if( $typenow == IBWP_MP_POST_TYPE ) {

			wp_enqueue_style('ibwp-select2-style');

			wp_enqueue_style( 'ibwp-font-awesome' );

			// Enqueu built in style for color picker
			if( wp_style_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_style( 'wp-color-picker' );
			}
			
			wp_register_style( 'ibwp-mp-admin-css', IBWP_MP_URL.'assets/css/ibwp-mp-admin.css', null, IBWP_MP_VERSION );
			wp_enqueue_style('ibwp-mp-admin-css');
		}

		// Registering main admin style 
		wp_enqueue_style( 'ibwp-admin-style' );
	}

	/**
	 * Enqueue admin script
	 * 
	 * @subpackage Marketing Popup
	 * @since 1.0
	 */
	function ibwp_mp_admin_script( $hook ) {

		global $wp_version,$typenow;
		
		$new_ui = $wp_version >= '3.5' ? '1' : '0'; // Check wordpress version for older scripts
		
		// If page is plugin setting page then enqueue script
		if( $typenow == IBWP_MP_POST_TYPE ) {

			// Enqueu built-in script for color picker
			if( wp_script_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_script( 'wp-color-picker' );
			}

			wp_enqueue_script( 'jquery-ui-slider' );
			wp_enqueue_script('ibwp-select2-script');
			
			// Registring admin script
			wp_register_script( 'ibwp-mp-admin-js', IBWP_MP_URL.'assets/js/ibwp-mp-admin.js', array('jquery'), IBWP_MP_VERSION, true );
			wp_enqueue_script( 'ibwp-mp-admin-js' );

			// Registering main admin script
			wp_enqueue_script( 'ibwp-admin-js' );
		}
	}
}

$ibwp_mp_script = new IBWP_MP_Script();