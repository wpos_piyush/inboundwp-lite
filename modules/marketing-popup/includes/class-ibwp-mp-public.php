<?php
/**
 * Public Class
 *
 * Handles the public side functionality of plugin
 *
 * @subpackage Marketing Popup Pro
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_MP_Public {
	
	function __construct() {
		// Action to addd popup on Footer
		add_action( 'wp_footer', array($this, 'ibwp_mp_display_popup') ); 

		// Action to get emails using ajax
		add_action( 'wp_ajax_ibwp_mp_get_emails', array( $this, 'ibwp_mp_get_emails' ) );
		add_action( 'wp_ajax_nopriv_ibwp_mp_get_emails', array( $this, 'ibwp_mp_get_emails' ) );
	}

	/**
	 * Function to display popup to site
	 * 
	 * @subpackage Marketing Popup Pro
	 * @since 1.0 
	 **/
	function ibwp_mp_display_popup() {
		global $post, $ibwp_mp_options;
		
		$prefix 				= IBWP_MP_META_PREFIX; // Metabox prefix
		$is_enable 				= ibwp_mp_get_option('enable');
		$ibwp_mp_enable_mobile 	= ibwp_mp_get_option('enable_mobile');

		if( ! $is_enable ){
			return false;
		}
		if( wp_is_mobile() && ! $ibwp_mp_enable_mobile ){
			return false;
		}

		if( $post ) {
			$post_id = $post->ID;
		} else {
			$post_id = '';
		}

		$modal_popup_id 		= get_post_meta($post_id, $prefix.'modal_popupid', true);
		
		// Modal PopUp Condition
		if( ! empty( $modal_popup_id ) && get_post_status( $modal_popup_id ) == "publish" ) {

			$this->ibwp_mp_create_popup( $modal_popup_id, 'popup-modal' );

		} else {

			$modal_display_in = ibwp_mp_get_option('modal_display_in');
			
			if( ! empty( $modal_display_in ) 
				&& ( array_key_exists( 'all', $modal_display_in ) 
				|| ( is_singular() && array_key_exists( $post->post_type, $modal_display_in ) ) 
				|| ( is_search() && array_key_exists( 'is_search', $modal_display_in ) )
				|| ( is_404() && array_key_exists( 'is_404', $modal_display_in ) )
				|| ( is_archive() && array_key_exists( 'is_archive', $modal_display_in ) ) )
			) {
			 	$modal_popup_id = ibwp_mp_get_option('modal_def_popup');
			 	$this->ibwp_mp_create_popup( $modal_popup_id, 'popup-modal' );
			}
		}
	}

	/**
	 * Function for create popup
	 * 
	 * @subpackage Marketing Popup Pro
	 * @since 1.0 
	 **/
	function ibwp_mp_create_popup( $popup_id, $type = 'bar' ) {

		$prefix = IBWP_MP_META_PREFIX; // Metabox prefix

		if( ! empty( $popup_id ) ) {

			$post_detail = get_post($popup_id);
			
			if( $post_detail ) {
				$is_cookie_set = ibwp_mp_is_cooke_set( $post_detail->ID, $type );	

				if( ! $is_cookie_set ) {

					if( $post_detail && $post_detail->post_status =='publish' ) {
						$this->ibwp_mp_build_popup_layout( $post_detail->ID, $type );
					}
				}
			}
		}
	}

	/**
	 * Function build popup layout
	 * 
	 * @subpackage Marketing Popup Pro
	 * @since 1.0 
	 **/
	function ibwp_mp_build_popup_layout( $post_id, $type = '' ) {

		$prefix 				= IBWP_MP_META_PREFIX;
		$unique_id 				= ibwpl_get_unique();
		$popup_meta 			= ibwp_mp_post_metadata($post_id);
		$popup_config_data 		= ibwp_mp_popup_config($post_id);
		$popup_meta['popup_ID'] = $post_id;
		$popup_id 				= $popup_meta['popup_ID'];

		// Get General PopUp Settings
		$popup_posts 			= get_post($popup_id);
		$popup_desc 			= $popup_meta['popup_content'];
		$popup_goal 			= $popup_meta['popup_goal'];
		$popup_type 			= $popup_meta['popup_type'];
		$popup_design 			= $popup_meta['popup_design'];
		$gen_btn_text 			= $popup_meta['gen_btn_text'];
		$gen_bg_img 			= $popup_meta['gen_bg_img'];
		$gen_bg_img_repeat 		= $popup_meta['gen_bg_img_repeat'];
		$gen_bg_img_position 	= $popup_meta['gen_bg_img_position'];
		$default_bg_img			= !empty($gen_bg_img)		? $gen_bg_img 		: IBWP_MP_URL.'assets/images/default-img.jpg';
		$background_image 		= 'url('.$default_bg_img.') '.$gen_bg_img_repeat.' '.$gen_bg_img_position;
		$hideclsbtn 			= $popup_meta['hideclsbtn'];

		// Full-Screen PopUp class
		$fullscreen_design 	= '';
		$classes 			= '';

		$gen_main_heading 		= $popup_meta['gen_main_heading'];
		$gen_sub_heading 		= $popup_meta['gen_sub_heading'];

		if($popup_goal == 'email-lists') {
			$hide_name_field 		= $popup_meta['hide_name_field'];
			$hide_phone_field 		= $popup_meta['hide_phone_field'];
			$name_field_ph 			= $popup_meta['name_field_ph'];
			$email_field_ph 		= $popup_meta['email_field_ph'];
			$phone_field_ph 		= $popup_meta['phone_field_ph'];
		}
		if($popup_goal == 'phone-calls') {
			$phone_number 			= $popup_meta['phone_number'];
		}
		if($popup_goal == 'target-url') {
			$target_url_link 		= $popup_meta['target_url_link'];
			$target_url_link 		= $target_url_link ? $target_url_link : '#';
			$open_link_url 			= $popup_meta['open_link_url'];
		}
		if($popup_goal == 'social-traffic') {		
			$social_traffic 		= $popup_meta['social_traffic'];
		}
		if($popup_type == 'popup-modal') {
			$fullscreen_popup 		= $popup_meta['fullscreen_popup'];
			$modal_position 		= $popup_meta['modal_position'];
			
			// Modal PopUp Position class
			$modal_classes 			= 'ibwp-mp-'.$modal_position.'-popup';
			
			if($fullscreen_popup == 1 && $popup_type == 'popup-modal') {
				$classes .= 'ibwp-mp-fullscreen-popup ';
			}
		}

		$popup_main_id 		= 'ibwp-mp-popup-'.$popup_type;

		$classes .= 'ibwp-mp-gen-cls ';
		$classes .= 'mp-'.$popup_goal;

		$templ_file_path 		= '/templates/'.$popup_type.'/'.$popup_design.'.php';

		?>
		<div id="<?php echo ibwpl_esc_attr($popup_main_id); ?>" class="ibwp-mp-popup-block ibwp-mp-popup-<?php echo $post_id; ?> <?php echo $popup_design; ?> <?php echo $classes; ?>" data-popup-type="<?php echo ibwpl_esc_attr($popup_type); ?>" data-popup-goal="<?php echo ibwpl_esc_attr($popup_goal); ?>">
			<?php
				// Load CSS function
				echo $this->ibwp_mp_popup_css($post_id,$popup_meta);
				
				// Include Design File
				include( IBWP_MP_DIR . "{$templ_file_path}" );
			?>
			<div class="ibwp-mp-popup-config">
				<?php echo json_encode($popup_config_data);?>
			</div>
		</div>
		<?php
	}

	/**
	 * Function for popup css to site
	 * 
	 * @subpackage Marketing Popup Pro
	 * @since 1.0 
	 **/
	function ibwp_mp_popup_css($post_id,$popup_meta = array()) {
		$popup_goal 				= $popup_meta['popup_goal'];
		$popup_type 				= $popup_meta['popup_type'];

		$popup_design 				= $popup_meta['popup_design'];
		$gen_bg_color 				= $popup_meta['gen_bg_color'];
		$gen_bg_img 				= $popup_meta['gen_bg_img'];
		$gen_bg_img					= !empty($gen_bg_img)		? $gen_bg_img 		: IBWP_MP_URL.'assets/images/default-img.jpg';
		$gen_bg_size 				= $popup_meta['gen_bg_size'];
		$gen_bg_img_repeat 			= $popup_meta['gen_bg_img_repeat'];
		$gen_bg_img_position 		= $popup_meta['gen_bg_img_position'];

		$gen_btn_bg_color 			= $popup_meta['gen_btn_bg_color'];
		$gen_btn_text_color 		= $popup_meta['gen_btn_text_color'];

		$desc_text_color 			= $popup_meta['desc_text_color'];

		$gen_main_heading_text_color	= $popup_meta['gen_main_heading_text_color'];
		$gen_main_heading_fontsize 		= $popup_meta['gen_main_heading_fontsize'];
		$gen_sub_heading_text_color 	= $popup_meta['gen_sub_heading_text_color'];
		$gen_sub_heading_fontsize 		= $popup_meta['gen_sub_heading_fontsize'];

		// Get Modal PopUp Settings
		$fullscreen_popup 			= $popup_meta['fullscreen_popup'];

		$html = '<style type="text/css">';

		$html .= '.ibwp-mp-popup-'.$post_id.' #ibwp-mp-popup-wrp-'.$post_id.'{ bottom: 0; display: none; left: 0; overflow-y: auto; position: fixed; right: 0; top: 0; z-index: 9999999; }';

		$html .= '#ibwp-mp-popup-bar #ibwp-mp-popup-wrp-'.$post_id.'{ display: block; position: relative; overflow-y: unset; z-index: unset; }';

		$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-popup-body { background: '.$gen_bg_color.' }';

		$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-inner-left, .ibwp-mp-popup-'.$post_id.' .ibwp-mp-inner-top { background-size: '.$gen_bg_size.'!important }';

		$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-popup-desc { color: '.$desc_text_color.' }';

		if($popup_goal == 'email-lists') {

			// Get Email Fields Settings
			$field_bg_color 			= $popup_meta['field_bg_color'];
			$field_text_color 			= $popup_meta['field_text_color'];

			$html .= '::placeholder { color: '.$field_text_color.'; }';
			$html .= '::-ms-input-placeholder { color: '.$field_text_color.'; }';
			$html .= '::-ms-input-placeholder { color: '.$field_text_color.'; }';

			$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-email-fields input { background: '.$field_bg_color.'; color: '.$field_text_color.'; }';

		}

		$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-popup-btn .btn-inner { background: '.$gen_btn_bg_color.'; color: '.$gen_btn_text_color.'; }';

		if($popup_type == 'popup-modal' && $fullscreen_popup == 0) {
			
			$modal_width 	= $popup_meta['modal_width'];
			$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-popup-body { width: '.$modal_width.'px; }';
		
		}

		$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-popup-main-heading { font-size: '.$gen_main_heading_fontsize.'px; color: '.$gen_main_heading_text_color.'; }';

		$html .= '.ibwp-mp-popup-'.$post_id.' .ibwp-mp-popup-sub-heading { font-size: '.$gen_sub_heading_fontsize.'px; color: '.$gen_sub_heading_text_color.'; }';

		if($popup_type == 'social-traffic') {

			$icon_design 		= $popup_meta['icon_design'];

			if( $icon_design == 'circle' ) {
				$icon_style		= 'border-radius: 100%;';
			} if( $icon_design == 'radius' ) {
				$icon_style 	= 'border-radius: 0 10px;';
			}

			$html .= '.ibwp-mp-popup-'.$post_id.' .mp-social-icons { '.$icon_style.' }';

		}

		$html .= '.ibwp-mp-popup-'.$post_id.'.design-2 .ibwp-mp-popup-body .ibwp-mp-popup-body-top img { object-position: '.$gen_bg_img_position.' }';

		$html .='</style>';

		return $html;
	}

	/**
	 * Function to get user emails
	 * 
	 * @subpackage Marketing Popup Pro
	 * @since 1.0 
	 **/
	function ibwp_mp_get_emails() {

		global $wpdb;

		$user_name			= isset( $_POST['user_name'] ) 			? sanitize_text_field( $_POST['user_name'] ) 			: 'Sir/Madam';
		$user_email       	= isset( $_POST['user_email'] ) 		? sanitize_email( strtolower( $_POST['user_email'] ) ) 	: '';
		$user_number 		= isset( $_POST['user_number'] )		? $_POST['user_number']									: '';
		$page_id 			= $_POST['page_id'];
		$popup_id 			= $_POST['popup_id'];
		$popup_goal 		= $_POST['popup_goal'];
		$popup_type 		= $_POST['popup_type'];
		$created_datetime	= current_time( 'mysql' );

		$email_subject 		= $_POST['email_subject'];
		$email_heading 		= $_POST['email_heading'];
		$thank_you_msg		= stripcslashes($_POST['thank_you_msg']);
		$email_message 		= stripcslashes($_POST['email_message']);

		$table_name 		= IBWP_MP_EMAIL_TABLE;

		$email_query 		= $wpdb->get_results( "SELECT user_email FROM $table_name WHERE user_email='$user_email' AND popup_id='$popup_id'" );

		if( $wpdb->num_rows > 0 ) {

			$email_exist 	= __('You have already subscribe to this email.','inboundwp-lite');

		} else {
			$insert_email_data = array(
				'popup_id'			=> $popup_id,
				'page_id'			=> $page_id,
				'user_name'			=> $user_name,
				'user_email'		=> $user_email,
				'user_number'		=> $user_number,
				'popup_goal'		=> $popup_goal,
				'popup_type'		=> $popup_type,
				'created_date'		=> $created_datetime,
				'modification_date'	=> $created_datetime,
			);

			$email_data = $wpdb->insert( $table_name, $insert_email_data );

			self::ibwp_mp_send_email($user_email, $user_name, $email_heading, $email_subject, $email_message);
		}

		$user_data = [
			'email_exist'   	=> $email_exist,
			'thank_you_msg'		=> $thank_you_msg,
		];

		wp_send_json( $user_data );

		die;
	}

	/**
	 * Send email function
	 * 
	 * @subpackage Marketing Popup Pro
 	 * @since 1.0
	 **/
	function ibwp_mp_send_email($user_email, $user_name, $email_heading, $email_subject, $email_message) {

		$email_subject = isset( $_POST['email_subject'] ) ? $_POST['email_subject'] : '';

		$email_message = isset( $_POST['email_message'] ) ? $_POST['email_message'] : '';
		$email_message = stripcslashes($email_message);
		$email_message = str_replace( '{message}', wpautop($email_message), $email_message );
		$email_message = str_replace( '{user_name}', $user_name, $email_message );
		$email_message = str_replace( '{user_email}', $user_email, $email_message );
		
		$atts['heading'] = isset( $_POST['email_heading'] ) 		? $_POST['email_heading']			: '';
		$atts['message'] = wpautop( $email_message );
	
		$content = ibwpl_get_template_html( 'notification-popup', 'email-template/template-1.php', $atts );
		$headers = 'Content-Type: text/html';	   
		$subject = stripslashes( $email_subject );

		wp_mail($user_email, $subject, $content, $headers, '');
	}
}
$ibwp_mp_public = new IBWP_MP_Public();