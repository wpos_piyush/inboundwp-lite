// Admin js
jQuery( document ).ready(function($) {

    // Initialize Select 2 JS
    $(".ibwp-select2").select2();
    
    // Initialize Color Picker JS
    $( '.ibwp-colorpicker' ).wpColorPicker();

	//Show selected appear value
	var appear = $('#ibwp-mp-when-popup-appear').val();
	if(appear){
		$(".ibwp-mp-appear").not("." + appear).hide();
		$("." + appear).show();
	} else{
		$(".ibwp-mp-appear").hide();
	}
	// On change of appear
	$(document).on('change', '#ibwp-mp-when-popup-appear', function() {
        $('.mp-popup-appear-settings').css('display','table-row');
		appear = $(this).val();
		if(appear){
			$(".ibwp-mp-appear").not("." + appear).hide();
			$("." + appear).show();
		} else{
			$(".ibwp-mp-appear").hide();
		}
	});

    // On Click PopUp Goal Show/Hide Some Settings
    $(document).on('click', '.ibwp-mp-goal-input', function(){
        var isChecked   = $(this).prop('checked', true);
        var parent      = isChecked.closest('.ibwp-mp-goal-main');
        var data_type = $('.ibwp-mp-type-input:checked').val();
        
        $('.ibwp-mp-goal-main').attr('data-type',data_type);
        
        var popup_goal  = parent.attr('data-goal');
        var popup_type  = parent.attr('data-type');

        $('.ibwp-mp-type-main').attr('data-goal',popup_goal);

        $('.ibwp-mp-common-cls').css('display','none');
        $('.mp-'+ popup_goal +'-settings').css('display','block');
        $('.mp-'+ popup_type +'-settings').css('display','block');
        $('.mp-'+ popup_goal +'-design.mp-'+ popup_type +'-design').css('display','block');

        $('.ibwp-mp-goal-block').css('border-color','#dcdcdc'); 
        parent.find('.ibwp-mp-goal-block').css('border-color','#000000');

        $('.ibwp-stabs-wrap .ibwp-stabs-nav:nth-child(2)').removeClass('disabled');

    });

    // On Click PopUp Type Show/Hide Some Settings
    $(document).on('click', '.ibwp-mp-type-input', function(){
        var isChecked   = $(this).prop('checked', true);
        var parent      = isChecked.closest('.ibwp-mp-type-main');
        var data_goal   = $('.ibwp-mp-goal-input:checked').val();

        $('.ibwp-mp-type-main').attr('data-goal',data_goal);

        var popup_goal  = parent.attr('data-goal');
        var popup_type  = parent.attr('data-type');

        $('.ibwp-mp-goal-main').attr('data-type',popup_type);
        
        $('.ibwp-mp-common-cls').css('display','none');
        $('.mp-'+ popup_type +'-settings').css('display','block');
        $('.mp-'+ popup_goal +'-settings').css('display','block');
        $('.mp-'+ popup_goal +'-design.mp-'+ popup_type +'-design').css('display','block');

        $('.ibwp-mp-type-block').css('border-color','#dcdcdc'); 
        parent.find('.ibwp-mp-type-block').css('border-color','#000000');

        $('.ibwp-stabs-wrap .ibwp-stabs-nav').removeClass('disabled');

    });

    // PopUp Design JS
    $(document).on('click', '.ibwp-mp-popup-design', function(){
        var isChecked       = $(this).prop('checked',true);
        var popup_design    = $(this).find('input').val();

        if(isChecked) {
            $('.ibwp-mp-popup-design-block').css('border-color','#dcdcdc'); 
            $(this).find('.ibwp-mp-popup-design-block').css('border-color','#000000');
        }
        if(popup_design == 'design-1') {
            $('.ibwp-mp-content-bg-sett').show();
        } else {
            $('.ibwp-mp-content-bg-sett').hide();
        }
    });

    /* Page Scroll Slider JS */
    $( "#ibwp-mp-scroll-slider" ).slider({
        value: $( "#mp-scroll-slider" ).val(),
        min: 1,
        max:100,
        step: 1,
        slide: function( event, ui ) {
          $( "#mp-scroll-slider" ).val( ui.value );
        }
    });
    $( "#mp-scroll-slider" ).keyup( function(){
         $( "#ibwp-mp-scroll-slider" ).slider( "value", $(this).val() );
    });

    /* Hide Show Modal PopUp Settings Fields */
    $(document).on('change', '.ibwp-mp-fullscreen-popup', function(){
        if ($(this).is(":checked")) {
            $(".ibwp-mp-modal-popup-sett").hide();
            $('.ibwp-mp-modal-popup-sett').find('input').val('');
        } else {
            $(".ibwp-mp-modal-popup-sett").show();
        }
    });

    // On click Call Email Export Ajax
    $(document).on('click', '.ibwp-export-emails-btn', function() {

        var current_obj = $(this);
        var cls_form    = current_obj.closest('form');
        var cls_ele     = current_obj.closest('.ibwp-mp-postbox');

        current_obj.attr('disabled', 'disabled');
        cls_form.find('.ibwp-mp-email-spinner').css('visibility', 'visible');
        cls_ele.find('.ibwp-progress-wrap').show();
        cls_ele.find('.ibwp-progress-wrap .ibwp-progress-strip').css('width', 0);

        ibwp_mp_export_emails( null, current_obj );

    });
});

// Generate CSV Function
function ibwp_mp_export_emails( data, obj ) {

    var cls_form    = obj.closest('form');
    var cls_ele     = obj.closest('.ibwp-mp-postbox');

    if( ! data ) {

        var data = {
            action          : 'ibwp_mp_do_ajax_export',
            form_data       : cls_form.serialize(),
            ibwp_mp_action  : 'export_emails',
            page            : 1,
        };

    }

    jQuery.post(ajaxurl, data, function(response) {
        
        var result = jQuery.parseJSON(response);

        if( result.status == 0 ) {

            alert( result.msg );

            obj.removeAttr('disabled', 'disabled');
            cls_form.find('.ibwp-mp-spinner').css('visibility', 'hidden');
            cls_ele.find('.ibwp-progress-wrap').hide();

        } else {

            cls_ele.find('.ibwp-progress-wrap .ibwp-progress-strip').css('width', result.percentage+'%');

            /* If data is there then process again */
            if( result.data_process != 0 && (result.data_process < result.total_count) ) {

                data['page']            = result.page;
                data['total_count']     = result.total_count;
                data['data_process']    = result.data_process;

                ibwp_mp_export_emails( data, obj );
            }

            /* If process is done */
            if( result.data_process >= result.total_count ) {

                obj.removeAttr('disabled', 'disabled');
                cls_form.find('.ibwp-mp-spinner').css('visibility', 'hidden');
                cls_ele.find('.ibwp-progress-wrap').delay(500).hide(1);

                if( result.url ) {
                    window.location = result.url;
                }
            }
        }
    });
}

jQuery( document ).ready(function( $ ) {

    /* Add another font */
    $(document).on('click', '.ibwp-mp-add-link', function(){
        
        var row_count = $('.ibwp-mp-social-row').length;
        if( row_count >= 8 ) {
            alert("Maximum 8 custom record allowed.");
        } else {

            key = row_count += 1;
            
            var cls_ele = $(this).closest('.ibwp-mp-social-wrp');
            var cln_ele = $(this).closest('.ibwp-mp-social-row').clone();
            
            cln_ele.find('.ibwp-mp-social-type').val('');
            cln_ele.find('.ibwp-mp-social-link').val('');
            
            cls_ele.append(cln_ele);

            var last_row_id = $(this).closest('.ibwp-mp-social-wrp').find('div[class^="ibwp-mp-social-row"]:last');

            last_row_id.attr( 'data-key', key );
            last_row_id.find('input, textarea, select, label').each(function() {
               
                var name    = $( this ).attr( 'name' );
                var id      = $( this ).attr( 'id' );
                
                if( name ) {
                    name = name.replace( /\[(\d+)\]/, '[' + parseInt( key ) + ']');
                    $( this ).attr( 'name', name );
                }
                if( typeof id != 'undefined' ) {
                    id = id.replace( /(\d+)/, parseInt( key ) );
                    $( this ).attr( 'id', id );
                }
            });

            var row_id    = document.getElementsByClassName('ibwp-mp-social-row');
        
            for (var i = 0; i < row_id.length; i++) {
                row_id[i].setAttribute('id', 'ibwp-mp-social-' +(i+1));
            }
        }
    });


    /* Remove font */
    $(document).on('click', '.ibwp-mp-remove-link', function(){
        
        var row_count = $('.ibwp-mp-social-row').length;
        var remove;
        remove = confirm("Are you sure, you want to remove ? ");
        if(remove) {
            if( row_count > 1 ) {
                var parent_div  = $(this).closest('.ibwp-mp-social-row');
                parent_div.remove();
                var row_id    = document.getElementsByClassName('ibwp-mp-social-row');
                for (var i = 0; i < row_id.length; i++) {
                    row_id[i].setAttribute('id', 'ibwp-mp-social-' +(i+1));
                }
                return true;
            } else {
                alert('Must have at least 1 columns!');
                return false;
            }
        } else {
            return false;
        }
    }); 
});