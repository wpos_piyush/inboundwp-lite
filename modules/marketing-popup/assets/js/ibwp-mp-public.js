/*!
 * IBWP_MP_Popup 1.0
 * http://wponlinesupport.com
 *
 * Copyright (c) 2016 WP Online Support
 */

(function (IBWP_MP_Popup, undefined) {

    'use strict';

    // set default settings
    var settings = {

            // set default cover id
            coverId: 'ibwp-mp-popup-cover',

            // duration (in days) before it pops up again
            expires: 1,

            // close if someone clicks an element with this class and prevent default action
            closeClassNoDefault: 'ibwp-mp-popup-close',

            // close if someone clicks an element with this class and continue default action
            closeClassDefault: 'ibwp-mp-close-go',            

            // on popup open function callback
            onPopUpOpen: null,

            // on popup close function callback
            onPopUpClose: null,

            // hash to append to url to force display of popup
            forceHash: 'splash',

            // hash to append to url to delay popup for 1 day
            delayHash: 'go',

            // close if the user clicks escape
            closeOnEscape:1,

            // set an optional delay (in milliseconds) before showing the popup
            delay: 2000,

            // automatically close the popup after a set amount of time (in milliseconds)
            hideAfter: null,
        },


        // grab the elements to be used
        $el = {
            html: document.getElementsByTagName('html')[0],
            cover: document.getElementById(settings.coverId),
            closeClassDefaultEls: document.querySelectorAll('.' + settings.closeClassDefault),
            closeClassNoDefaultEls: document.querySelectorAll('.' + settings.closeClassNoDefault)
        },

        /**
         * Helper methods
         */

        util = {

            hasClass: function(el, name) {
                return new RegExp('(\\s|^)' + name + '(\\s|$)').test(el.className);
            },

            addClass: function(el, name) {
                if (!util.hasClass(el, name)) {
                    el.className += (el.className ? ' ' : '') + name;
                }
            },

            removeClass: function(el, name) {
                if (util.hasClass(el, name)) {
                    el.className = el.className.replace(new RegExp('(\\s|^)' + name + '(\\s|$)'), ' ').replace(/^\s+|\s+$/g, '');
                }
            },
            removeElement:function(id){
                    var elem = document.getElementById(id);
                    return elem.parentNode.removeChild(elem);
            },
            addListener: function(target, type, handler) {
                if (target.addEventListener) {
                    target.addEventListener(type, handler, false);
                } else if (target.attachEvent) {
                    target.attachEvent('on' + type, handler);
                }
            },

            removeListener: function(target, type, handler) {
                if (target.removeEventListener) {
                    target.removeEventListener(type, handler, false);
                } else if (target.detachEvent) {
                    target.detachEvent('on' + type, handler);
                }
            },

            isFunction: function(functionToCheck) {
                var getType = {};
                return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
            },

            setCookie: function(name, days) {
                var date = new Date();
                date.setTime(+ date + (days * 86400000));
                document.cookie = name + '=true; expires=' + date.toGMTString() + '; path=/';
            },

            hasCookie: function(name) {
                if (document.cookie.indexOf(name) !== -1) {
                    return true;
                }
                return false;
            },

            // check if there is a hash in the url
            hashExists: function(hash) {
                if (window.location.hash.indexOf(hash) !== -1) {
                    return true;
                }
                return false;
            },

            preventDefault: function(event) {
                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }
            },

            mergeObj: function(obj1, obj2) {
                for (var attr in obj2) {
                    obj1[attr] = obj2[attr];
                }
            }
        },


        /**
         * Private Methods
         */

        // close popup when user hits escape button
        onDocUp = function(e) {
          
            if (settings.closeOnEscape) {
                if (e.keyCode === 27) {
                    IBWP_MP_Popup.close();
                }
            }
        },

        openCallback = function() {

            // if not the default setting
            if (settings.onPopUpOpen !== null) {

                // make sure the callback is a function
                if (util.isFunction(settings.onPopUpOpen)) {
                    settings.onPopUpOpen.call();
                } else {
                    throw new TypeError('IBWP_MP_Popup open callback must be a function.');
                }
            }
        },

        closeCallback = function() {

            // if not the default setting
            if (settings.onPopUpClose !== null) {

                // make sure the callback is a function
                if (util.isFunction(settings.onPopUpClose)) {
                    settings.onPopUpClose.call();
                } else {
                    throw new TypeError('IBWP_MP_Popup close callback must be a function.');
                }
            }
        };



    /**
     * Public methods
     */

    //font-size
    jQuery("html").addClass('sp-overflow-hidden');
    check_responsive_font_sizes();
    function check_responsive_font_sizes() {
        //  Apply font sizes
        jQuery(".ibwp_mp_main_heading[data-font-size-init]").each(function(index, el) {
            var p = jQuery(el);
            var data = jQuery( this ).html();

            if ( data.toLowerCase().indexOf("sp_font") >= 0 && data.match("^<div") && data.match("</div>$") ) {
                p.addClass('sp-no-responsive');
            } else {
                p.removeClass('sp-no-responsive');
            }
        });
    }

    IBWP_MP_Popup.open = function(opt) {
      
        var i, len;

        if (util.hashExists(settings.delayHash)) {

            util.setCookie(opt.cookieName, 1); // expire after 1 day
            return;
        }       
        //check is cookie set
        if(util.hasCookie(opt.cookieName)) {
            return;
        }
        
        util.addClass($el.html,opt.coverId);
      
        // bind close events and prevent default event
        if ($el.closeClassNoDefaultEls.length > 0) {
            for (i=0, len = $el.closeClassNoDefaultEls.length; i < len; i++) {
                util.addListener($el.closeClassNoDefaultEls[i], 'click', function(e) {
                    if (e.target === this) {
                        util.preventDefault(e);
                        IBWP_MP_Popup.close(opt);
                    }
                });
            }
        }

        if(opt.closeOnEscape) {
             util.addListener(document, 'keyup',  function (e){
                if(e.keyCode == 27){
                    IBWP_MP_Popup.close(opt);
                }            
            });            
        }
        if (opt.hideAfter) {           
            setTimeout(IBWP_MP_Popup.close, opt.hideAfter,opt);           
        }

        openCallback();  

    };

    IBWP_MP_Popup.close = function(options) {
      
        util.removeClass($el.html,options.coverId);
        util.removeElement(options.coverId);
        util.setCookie(options.cookieName, options.expires);

        // unbind escape detection to document
        util.removeListener(document, 'keyup', onDocUp);
        closeCallback();
    };
    
}(window.IBWP_MP_Popup = window.IBWP_MP_Popup || {}));

 
// Custom Code
jQuery( document ).ready(function($) {
    var ibwp_mp_conf = {};
    //set cookie prefix
    var cookie_prefix = '';
    if(IBWP_MP_Popup.cookie_prefix)
    {
        cookie_prefix =IBWP_MP_Popup.cookie_prefix;
    }

    // Modal PopUp Page Load, Scroll & Inactivity Event JS
    if (jQuery('#ibwp-mp-popup-popup-modal').length > 0) {
        ibwp_mp_conf    = $.parseJSON( $('#ibwp-mp-popup-popup-modal .ibwp-mp-popup-config').text());
        var popup_sett = {
                            'coverId'           : 'ibwp-mp-popup-popup-modal',
                            'display_on'        : ibwp_mp_conf.when_popup_appear,
                            'delay'             : parseInt(ibwp_mp_conf.delay) * 1000,
                            'closeOnEscape'     : (ibwp_mp_conf.clsonesc == '1') ? true : false,
                            'hideclsbtn'        : ibwp_mp_conf.hideclsbtn ? false : true ,
                            'cookie_expires'    : parseInt(ibwp_mp_conf.exptime),
                            'hideAfter'         : parseInt(ibwp_mp_conf.disappear)*1000,
                            'popup_ID'          : parseInt(ibwp_mp_conf.popup_ID),
                            'fullscreen_popup'  : parseInt(ibwp_mp_conf.fullscreen_popup),
                            'cookieName'        : cookie_prefix+'ibwp_mp_popup-modal_'+parseInt(ibwp_mp_conf.popup_ID),
                        };

        if(popup_sett.fullscreen_popup == 1 && $( 'html' ).hasClass( 'ibwp-mp-popup-popup-modal' )) {
            $('html').addClass('modal-popup-fullscreen');
        }

        if (popup_sett.delay === 0) {
            IBWP_MP_Popup.open(popup_sett);
        } else {
            // delay showing the popup
            setTimeout(IBWP_MP_Popup.open,popup_sett.delay ,popup_sett);
        }
    }

    // Email Validation Function
    function ibwp_mp_isValidEmailAddress(emailAddress) {
        
        var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

        return pattern.test(emailAddress);

    }

    $('.ibwp-mp-email-fields').on('keypress', function (e) {
        
        if ($(this).focus() && e.keyCode === 13) {
            $('.ibwp-mp-submit-email').click();
        }

    });

    // Bar PopUp Form Submit Ajax
    $(document).on('click', '.ibwp-mp-submit-email', function(){
        var parent          = $(this).closest('.ibwp-mp-popup-body');
        var email_lists     = $(this).closest('.mp-email-lists');
        var ibwp_mp_conf    = $.parseJSON( email_lists.find('.ibwp-mp-popup-config').text());
        var user_name       = $('.ibwp-mp-user-name').val();
        var user_email      = $('.ibwp-mp-user-email').val();
        var user_number     = $('.ibwp-mp-user-number').val();
        var valid_email_msg = IBWP_MP_Popup.valid_email_msg;
        var enter_email_msg = IBWP_MP_Popup.enter_email_msg;
        var page_id         = IBWP_MP_Popup.page_id;
        var plugin_url      = IBWP_MP_Popup.plugin_url;
        var popup_id        = ibwp_mp_conf.popup_ID;
        var popup_goal      = ibwp_mp_conf.popup_goal;
        var popup_type      = ibwp_mp_conf.popup_type;
        var gen_btn_text    = ibwp_mp_conf.gen_btn_text;
        var email_subject   = ibwp_mp_conf.email_subject;
        var email_heading   = ibwp_mp_conf.email_heading;
        var thank_you_msg   = ibwp_mp_conf.thank_you_msg;
        var email_message   = ibwp_mp_conf.email_message;

        $('.ibwp-mp-user-email').prop('disabled', true);
        $('.ibwp-mp-submit-email .btn-inner').replaceWith('<img src="'+plugin_url+'assets/images/please-wait.gif">');
        $('.ibwp-mp-submit-email').prop('disabled', true);
        $('.ibwp-mp-submit-email').css('cursor', 'default');

        if (user_email != '') {
            if (ibwp_mp_isValidEmailAddress(user_email)) {

                jQuery.post(IBWP_MP_Popup.ajaxurl, {

                    action          : 'ibwp_mp_get_emails',
                    user_name       : user_name,
                    user_email      : user_email,
                    user_number     : user_number,
                    page_id         : page_id,
                    popup_id        : popup_id,
                    popup_goal      : popup_goal,
                    popup_type      : popup_type,
                    email_subject   : email_subject,
                    email_heading   : email_heading,
                    thank_you_msg   : thank_you_msg,
                    email_message   : email_message,

                }, function(data) {

                    var error_msg   = data.email_exist;
                    if(error_msg) {

                        $('.ibwp-mp-user-email').prop('disabled', false);
                        $('.ibwp-mp-submit-email').prop('disabled', false);
                        $('.ibwp-mp-submit-email img').replaceWith('<span class="btn-inner">'+gen_btn_text+'</span>');
                        $('.ibwp-mp-submit-email').css('cursor','pointer');

                        parent.find('.ibwp-mp-user-email').after('<span class="ibwp-mp-error-tooltip">'+ error_msg +'</span>');
                    
                        setTimeout(function(){
                            parent.find('.ibwp-mp-error-tooltip').remove();
                        }, 2000);

                    } else {
                        $('.ibwp-mp-popup-email-main').replaceWith('<div class="ibwp-mp-thank-you-msg">'+data.thank_you_msg+'</div>');
                    }

                });

            } else {

                $('.ibwp-mp-user-email').prop('disabled', false);
                $('.ibwp-mp-submit-email').prop('disabled', false);
                $('.ibwp-mp-submit-email img').replaceWith('<span class="btn-inner">'+gen_btn_text+'</span>');
                $('.ibwp-mp-submit-email').css('cursor','pointer');

                parent.find('.ibwp-mp-user-email').after('<span class="ibwp-mp-error-tooltip">'+ valid_email_msg +'</span>');
    
                setTimeout(function(){
                    parent.find('.ibwp-mp-error-tooltip').remove();
                }, 2000);

                $('.ibwp-mp-user-email').focus();

            }
        } else {

            $('.ibwp-mp-user-email').prop('disabled', false);
            $('.ibwp-mp-submit-email').prop('disabled', false);
            $('.ibwp-mp-submit-email img').replaceWith('<span class="btn-inner">'+gen_btn_text+'</span>');
            $('.ibwp-mp-submit-email').css('cursor','pointer');

            parent.find('.ibwp-mp-user-email').after('<span class="ibwp-mp-error-tooltip">'+ enter_email_msg +'</span>');
    
            setTimeout(function(){
                parent.find('.ibwp-mp-error-tooltip').remove();
            }, 2000);

            $('.ibwp-mp-user-email').focus();

        }
    });

    $(document).on('click', '.mp-tooltop-close', function(){
        var parent  = $(this).closest('.ibwp-mp-error-tooltip');
        parent.remove();
    });

});