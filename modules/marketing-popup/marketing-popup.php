<?php
/*
 * @package InboundWP Pro
 * @subpackage Marketing Popup
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
global $wpdb;

/**
 * Basic plugin definitions
 * 
 * @subpackage Marketing Popup
 * @since 1.0
 */

IBWP_Lite()->define( 'IBWP_MP_VERSION', '1.0' );   // version
IBWP_Lite()->define( 'IBWP_MP_DIR', dirname( __FILE__ ) );   //  dir
IBWP_Lite()->define( 'IBWP_MP_URL', plugin_dir_url( __FILE__ ) );    //  url
IBWP_Lite()->define( 'IBWP_MP_POST_TYPE', 'ibwp_mp_popup' );   //  Post Type
IBWP_Lite()->define( 'IBWP_MP_META_PREFIX', '_ibwp_mp_' );   //  meta prefix
IBWP_Lite()->define( 'IBWP_MP_EMAIL_TABLE', $wpdb->prefix.'ibwp_mp_email_list' );

function ibwp_mp_install(){

	// Custom post type function
    ibwp_mp_register_post_types();

	// IMP need to flush rules for custom registered post type
	flush_rewrite_rules();

	// Get settings for the plugin
	$ibwp_mp_options = get_option( 'ibwp_mp_options' );
	
	if( empty( $ibwp_mp_options ) ) { // Check plugin version option
		
		// set default settings
		ibwp_mp_default_settings();

		// Update plugin version to option
		update_option( 'ibwp_mp_plugin_version', '1.0' );
	}

	ibwp_mp_create_email_table();
}
add_action( 'ibwp_module_activation_hook_marketing-popup', 'ibwp_mp_install' );

// Global Variables
global $ibwp_mp_options;

// Function File
require_once( IBWP_MP_DIR . '/includes/ibwp-mp-functions.php' );
$ibwp_mp_options = ibwpl_get_settings( 'ibwp_mp_options' );

// Plugin Post Type File
require_once( IBWP_MP_DIR . '/includes/ibwp-mp-post-types.php' );

// Email Exports Function File
require_once( IBWP_MP_DIR . '/includes/admin/tools/ibwp-mp-export-functions.php' );

// Script Class
require_once( IBWP_MP_DIR . '/includes/class-ibwp-mp-script.php' );

// Admin Class
require_once( IBWP_MP_DIR . '/includes/admin/class-ibwp-mp-admin.php' );

// Public Class
require_once( IBWP_MP_DIR . '/includes/class-ibwp-mp-public.php' );

// Email lists class
require_once ( IBWP_MP_DIR . '/includes/admin/class-mp-email-list-modal.php' );
