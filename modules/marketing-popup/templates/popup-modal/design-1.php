<?php
/**
 * PopUp Modal Design - 1
 * 
 * @subpackage Marketing Popup
 * @since 1.0 
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
?>
<div class="ibwp-mp-popup-wrp <?php echo ibwpl_esc_attr($modal_classes); ?>" id="ibwp-mp-popup-wrp-<?php echo $post_id;?>" data-position="<?php echo ibwpl_esc_attr($modal_position); ?>">
	<div class="ibwp-mp-popup-body">
		<div class="ibwp-mp-popup-inner">
			<div class="ibwp-col-6 ibwp-mp-inner-left" style="background: <?php echo ibwpl_esc_attr($background_image); ?>;"></div>
			<div class="ibwp-col-6 ibwp-mp-inner-right">
				<?php if($gen_main_heading || $gen_sub_heading) { ?>
					<div class="ibwp-mp-headings">
						<?php if($gen_main_heading) { ?>
							<div class="ibwp-mp-popup-main-heading"><?php echo $gen_main_heading; ?></div>
						<?php } if($gen_sub_heading) { ?>
							<hr class="ibwp-mp-border-cls">
							<div class="ibwp-mp-popup-sub-heading"><?php echo $gen_sub_heading; ?></div>
						<?php } ?>
					</div>
				<?php } ?>
				<div class="ibwp-mp-contents">
					<div class="ibwp-mp-popup-desc"><?php echo wpautop($popup_desc); ?></div>
					<?php if($popup_goal == 'email-lists') { ?>
						<div class="ibwp-mp-popup-email-main">
							<?php if($hide_name_field == '0') { ?>
								<div class="mp-name-fields-main ibwp-mp-email-fields">
									<input type="text" name="<?php echo $prefix; ?>user_name" value="" placeholder="<?php echo ibwpl_esc_attr($name_field_ph); ?>" class="ibwp-mp-user-name">
								</div>
							<?php } ?>
							<div class="mp-email-fields-main mp-name-fields-main ibwp-mp-email-fields">
								<input type="email" name="<?php echo $prefix; ?>user_email" value="" placeholder="<?php echo ibwpl_esc_attr($email_field_ph); ?>" class="ibwp-mp-user-email">
							</div>
							<?php if($hide_phone_field == '0') { ?>
								<div class="mp-name-fields-main ibwp-mp-email-fields">
									<input type="text" name="<?php echo $prefix; ?>user_number" value="" placeholder="<?php echo ibwpl_esc_attr($phone_field_ph); ?>" class="ibwp-mp-user-number">
								</div>
							<?php } ?>
							<button class="ibwp-mp-popup-btn ibwp-mp-submit-email"><span class="btn-inner"><?php echo $gen_btn_text; ?></span></button>
						</div>
					<?php }
					if($popup_goal == 'target-url') { ?>
						<div class="ibwp-mp-gen-btn ibwp-mp-popup-btn">
							<a href="<?php echo ibwpl_esc_attr($target_url_link); ?>" target="<?php echo ibwpl_esc_attr($open_link_url); ?>" class="btn-inner"><?php echo $gen_btn_text; ?></a>
						</div>
					<?php }
					if($popup_goal == 'phone-calls') { ?>
						<div class="ibwp-mp-gen-btn ibwp-mp-popup-btn">
							<a href="tel:<?php echo ibwpl_esc_attr($phone_number); ?>" class="btn-inner"><?php echo $gen_btn_text; ?></a>
						</div>
					<?php }
					if($popup_goal == 'social-traffic') { ?>
						<div class="ibwp-mp-popup-social-links">
							<?php if( ! empty( $social_traffic) ) { 
								foreach ($social_traffic as $skey => $sval) { ?>
								<a href="<?php echo ibwpl_esc_attr($sval['link']); ?>" target="blank" class="mp-social-icons fa fa-<?php echo $sval['type']; ?>"></a>
							<?php } } ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php if($hideclsbtn == 0) { ?>
			<div class="ibwp-mp-popup-close"></div>
		<?php } ?>
	</div>
</div>