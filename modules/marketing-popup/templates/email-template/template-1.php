<!DOCTYPE html>
<html>
<head>
	<title><?php _e('InboundWP Email Template','inboundwp-lite'); ?></title>
</head>
<body>
<div style="background: #f5f5f5; border: 1px solid rgb(0, 166, 157);">
	<h1 style="background: rgb(0, 166, 157); color: rgb(255, 255, 255); margin: 0; padding: 15px;"><?php echo $heading; ?></h1>
	<div style="padding: 15px;"><?php echo $message; ?></div>
</div>
</body>
</html>