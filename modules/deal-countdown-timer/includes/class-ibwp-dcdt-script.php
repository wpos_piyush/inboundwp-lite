<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package Deal Countdown Timer
 * @since 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Ibwp_Dcdt_Script {

	function __construct() {

		// Action to add style at front side
		add_action( 'wp_enqueue_scripts', array( $this, 'ibwp_dcdt_front_style' ) );

		// Action to add script at front side
		add_action( 'wp_enqueue_scripts', array( $this, 'ibwp_dcdt_front_script' ) );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array( $this, 'ibwp_dcdt_admin_style' ) );

		// Action to add script in backend
		add_action( 'admin_enqueue_scripts', array( $this, 'ibwp_dcdt_admin_script' ) );
	}

	/**
	 * Function to add style at front side
	 * 
	 * @package Deal Countdown Timer
 	 * @since 1.0
	 */
	function ibwp_dcdt_front_style() {

		if ( class_exists( 'WooCommerce' ) || class_exists( 'Easy_Digital_Downloads' ) ) {
			// Registring and enqueing public css
			wp_register_style( 'ibwp-dcdt-public-css', IBWP_DCDT_URL.'assets/css/ibwp-dcdt-public.css', array(), IBWP_DCDT_VERSION );
			wp_enqueue_style( 'ibwp-dcdt-public-css' );
		}
		
		wp_enqueue_style( 'ibwp-slick-style' );
	}

	/**
	 * Function to add script at front side
	 * 
	 * @package Deal Countdown Timer
 	 * @since 1.0
	 */
	function ibwp_dcdt_front_script() {

		wp_register_script( 'ibwp-dcdt-public-js', IBWP_DCDT_URL.'assets/js/ibwp-dcdt-public.js', array('jquery'), IBWP_DCDT_VERSION, true );
				
		wp_register_script( 'ibwp-dcdt-countereverest-js', IBWP_DCDT_URL.'assets/js/jquery.counteverest.min.js', array('jquery'), IBWP_DCDT_VERSION, true );
		wp_enqueue_script('ibwp-slick-jquery');
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package Deal Countdown Timer
 	 * @since 1.0
	 */
	function ibwp_dcdt_admin_style( $hook ) {

		global $post_type;

		// If page is plugin setting page then enqueue script
		if( $post_type == IBWP_DCDT_POST_TYPE || $post_type == 'product' || $post_type == 'download') {
			// Enqueue Color Picker CSS
			if( wp_style_is( 'wp-color-picker', 'registered' ) ) {
				wp_enqueue_style( 'wp-color-picker' );
			}

			// Enqueue Select 2 CSS
			wp_enqueue_style( 'ibwp-select2-style' );

			wp_register_style( 'ibwp-dcdt-time-picker-css', IBWP_DCDT_URL.'assets/css/ibwp-dcdt-time-picker.css', null, IBWP_DCDT_VERSION );
			wp_enqueue_style( 'ibwp-dcdt-time-picker-css' );

			wp_register_style( 'ibwp-dcdt-admin-css', IBWP_DCDT_URL.'assets/css/ibwp-dcdt-admin.css', null, IBWP_DCDT_VERSION );
			wp_enqueue_style( 'ibwp-dcdt-admin-css' );
		}
	}

	/**
	 * Enqueue admin script
	 * 
	 * @package Deal Countdown Timer
 	 * @since 1.0
	 */
	function ibwp_dcdt_admin_script( $hook ) {
		global $wp_version, $post_type;

		$new_ui = $wp_version >= '3.5' ? '1' : '0'; // Check wordpress version for older scripts
		
		// If page is plugin setting page then enqueue script
		if( $post_type == IBWP_DCDT_POST_TYPE || $post_type == 'product' || $post_type == 'download') {
			
			// Enqueue Color Picker JS
			if( wp_script_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_script( 'wp-color-picker' );
			}

			wp_enqueue_script( 'jquery-ui-datepicker' );

			// Registring admin script
			wp_register_script( 'ibwp-dcdt-ui-timepicker-addon-js', IBWP_DCDT_URL.'assets/js/ibwp-dcdt-ui-timepicker-addon.js', array('jquery'), IBWP_DCDT_VERSION, true );
			wp_enqueue_script( 'ibwp-dcdt-ui-timepicker-addon-js' );
			

			wp_register_script( 'ibwp-dcdt-admin-js', IBWP_DCDT_URL.'assets/js/ibwp-dcdt-admin.js', array('jquery'), IBWP_DCDT_VERSION, true );
			wp_enqueue_script( 'ibwp-dcdt-admin-js' );
		}
	}
}

$ibwp_dcdt_script = new Ibwp_Dcdt_Script();