jQuery(document).ready(function($) {

	// Vertical Flip JS
	$( '.dcdt-product-for .dcdt-timer-vertical-flip' ).each(function( index ) {

		var current_obj     = $(this);
		firstCalculation 	= true;
		var clock_id		= $(this).find('.dcdt-countdown-timer-vertical-flip').attr('data-id');
		var date_id			= $(this).find('.dcdt-countdown-timer-vertical-flip').attr('id');
		var product_id		= $(this).find('.dcdt-countdown-timer-vertical-flip').attr('data-product');
		var date_id			= date_id+ ' .dcdt-vf-clock';
		var $countdown		= $('#'+date_id);
		var date_conf 		= $.parseJSON( $(this).find('.dcdt-date-conf').attr('data-conf'));
		var is_days 		= date_conf.is_days;
		var is_hours 		= date_conf.is_hours;
		var is_minutes 		= date_conf.is_minutes;
		var is_seconds 		= date_conf.is_seconds;
		var days_text 		= date_conf.days_text;
		var hours_text 		= date_conf.hours_text;
		var minutes_text 	= date_conf.minutes_text;
		var seconds_text 	= date_conf.seconds_text;
		var diff_date   	= date_conf.diff_date;
		var current_date	= date_conf.current_date;
		var timeZone 		= date_conf.timezone;

		current_obj.find('.dcdt-vf-clock').DcdtClock({
			
			day				: diff_date.day,
			month			: diff_date.month,
			year			: diff_date.year,
			hour			: diff_date.hour,
			minute			: diff_date.min,
			second			: diff_date.second,
			currentDateTime	: current_date,
			timeZone		: (timeZone != '')		? parseFloat(timeZone) 	: parseFloat(timezone),
			daysLabel		: (days_text != '')		? days_text				: 'Days',
			hoursLabel		: (hours_text != '')	? hours_text			: 'Hours',
			minutesLabel	: (minutes_text != '')	? minutes_text			: 'Minutes',
			secondsLabel	: (seconds_text != '')	? seconds_text			: 'Seconds',
			onComplete: function() {
				dcdt_timer_slider_over( product_id );
			},
			afterCalculation: function() {
				var plugin = this,
					units = {
						days: this.days,
						hours: this.hours,
						minutes: this.minutes,
						seconds: this.seconds
					},
					/* Max values per unit */
					maxValues = {
						hours: '23',
						minutes: '59',
						seconds: '59'
					},
					actClass = 'active',
					befClass = 'before';

				/* Build necessary elements */
				if (firstCalculation == true) {
					firstCalculation = false;

					 Build necessary markup 
					$countdown.find('.ce-unit-wrap div').each(function () {
						var $this = $(this),
							className = $this.attr('class'),
							value = units[className],
							sub = '',
							dig = '';

						/* Build markup per unit digit */
						for(var x = 0; x < 10; x++) {
							sub += [
								'<div class="digits-inner">',
									'<div class="flip-wrap">',
										'<div class="up">',
											'<div class="shadow"></div>',
											'<div class="inn">' + x + '</div>',
										'</div>',
										'<div class="down">',
											'<div class="shadow"></div>',
											'<div class="inn">' + x + '</div>',
										'</div>',
									'</div>',
								'</div>'
							].join('');
						}

						/* Build markup for number */
						for (var i = 0; i < value.length; i++) {
							dig += '<div class="digits">' + sub + '</div>';
						}
						$this.append(dig);
					});
				}

				/* Iterate through units */
				$.each(units, function(unit) {
					var digitCount = $countdown.find('.' + unit + ' .digits').length,
						maxValueUnit = maxValues[unit],
						maxValueDigit,
						value = plugin.strPad(this, digitCount, '0');

					/* Iterate through digits of an unit */
					for (var i = value.length - 1; i >= 0; i--) {
						var $digitsWrap = $countdown.find('.' + unit + ' .digits:eq(' + (i) + ')'),
							$digits = $digitsWrap.find('div.digits-inner');

						/* Use defined max value for digit or simply 9 */
						if (maxValueUnit) {
							maxValueDigit = (maxValueUnit[i] == 0) ? 9 : maxValueUnit[i];
						} else {
							maxValueDigit = 9;
						}

						/* Which numbers get the active and before class */
						var activeIndex = parseInt(value[i]),
							beforeIndex = (activeIndex == maxValueDigit) ? 0 : activeIndex + 1;

						/* Check if value change is needed */
						if ($digits.eq(beforeIndex).hasClass(actClass)) {
							$digits.parent().addClass('play');
						}

						/* Remove all classes */
						$digits
							.removeClass(actClass)
							.removeClass(befClass);

						/* Set classes */
						$digits.eq(activeIndex).addClass(actClass);
						$digits.eq(beforeIndex).addClass(befClass);
					}
				});
			}
		});
	});

	//Circle, Circle-Fill JS
	$( '.dcdt-product-for .dcdt-timer-circle, .dcdt-product-for .dcdt-timer-circle-fill' ).each(function( index ) {

		var clock_id		= $(this).find('.dcdt-countdown-timer-cf').attr('data-id');
		var date_id			= $(this).find('.dcdt-countdown-timer-cf').attr('id');
		var product_id		= $(this).find('.dcdt-countdown-timer-sf').attr('data-product');
		var date_id			= date_id+ ' .dcdt-clock';
		var date_conf 		= $.parseJSON( $(this).find('.dcdt-date-conf').attr('data-conf'));

		var is_days 		= date_conf.is_days;
		var is_hours 		= date_conf.is_hours;
		var is_minutes 		= date_conf.is_minutes;
		var is_seconds 		= date_conf.is_seconds;
		var days_text 		= date_conf.days_text;
		var hours_text 		= date_conf.hours_text;
		var minutes_text 	= date_conf.minutes_text;
		var seconds_text 	= date_conf.seconds_text;
		var diff_date   	= date_conf.diff_date;
		var current_date	= date_conf.current_date;
		var timeZone 		= date_conf.timezone;

		$("#"+date_id).DcdtClock({
			day				: diff_date.day,
			month			: diff_date.month,
			year			: diff_date.year,
			hour			: diff_date.hour,
			minute			: diff_date.min,
			second			: diff_date.second,
			currentDateTime	: current_date,
			timeZone		: (timeZone != '')		? parseFloat(timeZone) 	: parseFloat(timezone),
			daysLabel		: (days_text != '')		? days_text				: 'Days',
			hoursLabel		: (hours_text != '')	? hours_text			: 'Hours',
			minutesLabel	: (minutes_text != '')	? minutes_text			: 'Minutes',
			secondsLabel	: (seconds_text != '')	? seconds_text			: 'Seconds',
			daysWrapper		: '.ce-days .ce-flip-back',
			hoursWrapper	: '.ce-hours .ce-flip-back',
			minutesWrapper	: '.ce-minutes .ce-flip-back',
			secondsWrapper	: '.ce-seconds .ce-flip-back',
			wrapDigits		: false,
			onComplete: function() {
				dcdt_timer_slider_over( product_id );
			},
			onChange: function() {
				dcdt_animate_timer_cfsf($('.dcdt-clock .ce-col > div'), this);
			}
		});

		/* Fallback for Internet Explorer */
		if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
			$('html').addClass('internet-explorer');
		}
	});

	// Square, Square-Fill JS
	$( '.dcdt-product-for .dcdt-timer-square, .dcdt-product-for .dcdt-timer-square-fill' ).each(function( index ) {

		var clock_id		= $(this).find('.dcdt-countdown-timer-sf').attr('data-id');
		var product_id		= $(this).find('.dcdt-countdown-timer-sf').attr('data-product');
		
		var date_id			= $(this).find('.dcdt-countdown-timer-sf').attr('id');
		var date_id			= date_id+ ' .dcdt-clock';
		var date_conf 		= $.parseJSON( $(this).find('.dcdt-date-conf').attr('data-conf'));

		var is_days 		= date_conf.is_days;
		var is_hours 		= date_conf.is_hours;
		var is_minutes 		= date_conf.is_minutes;
		var is_seconds 		= date_conf.is_seconds;
		var days_text 		= date_conf.days_text;
		var hours_text 		= date_conf.hours_text;
		var minutes_text 	= date_conf.minutes_text;
		var seconds_text 	= date_conf.seconds_text;
		var diff_date   	= date_conf.diff_date;
		var current_date	= date_conf.current_date;
		var timeZone 		= date_conf.timezone;

		$("#"+date_id).DcdtClock({
			day				: diff_date.day,
			month			: diff_date.month,
			year			: diff_date.year,
			hour			: diff_date.hour,
			minute			: diff_date.min,
			second			: diff_date.second,
			currentDateTime	: current_date,
			timeZone		: (timeZone != '')		? parseFloat(timeZone) 	: parseFloat(timezone),
			daysLabel		: (days_text != '')		? days_text				: 'Days',
			hoursLabel		: (hours_text != '')	? hours_text			: 'Hours',
			minutesLabel	: (minutes_text != '')	? minutes_text			: 'Minutes',
			secondsLabel	: (seconds_text != '')	? seconds_text			: 'Seconds',
			daysWrapper		: '.ce-days .ce-flip-back',
			hoursWrapper	: '.ce-hours .ce-flip-back',
			minutesWrapper	: '.ce-minutes .ce-flip-back',
			secondsWrapper	: '.ce-seconds .ce-flip-back',
			wrapDigits		: false,
			onComplete: function() {
				dcdt_timer_slider_over( product_id );
			},
			onChange: function() {
				dcdt_animate_timer_cfsf($('.dcdt-clock .ce-col > div'), this);
			}
		});

		/* Fallback for Internet Explorer */
		if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
			$('html').addClass('internet-explorer');
		}
	});

	// Simple Clock JS
	$( '.dcdt-product-for .dcdt-timer-simple' ).each(function( index ) {

		var clock_id		= $(this).find('.dcdt-countdown-timer-simple').attr('data-id');
		var date_id			= $(this).find('.dcdt-countdown-timer-simple').attr('id');
		var product_id		= $(this).find('.dcdt-countdown-timer-simple').attr('data-product');
		var date_id			= date_id+ ' .dcdt-clock';
		var date_conf 		= $.parseJSON( $(this).find('.dcdt-date-conf').attr('data-conf'));
		var is_days 		= date_conf.is_days;
		var is_hours 		= date_conf.is_hours;
		var is_minutes 		= date_conf.is_minutes;
		var is_seconds 		= date_conf.is_seconds;
		var days_text 		= date_conf.days_text;
		var hours_text 		= date_conf.hours_text;
		var minutes_text 	= date_conf.minutes_text;
		var seconds_text 	= date_conf.seconds_text;
		var diff_date   	= date_conf.diff_date;
		var current_date	= date_conf.current_date;
		var timeZone 		= date_conf.timezone;

		$("#"+date_id).DcdtClock({
			day				: diff_date.day,
			month			: diff_date.month,
			year			: diff_date.year,
			hour			: diff_date.hour,
			minute			: diff_date.min,
			second			: diff_date.second,
			currentDateTime	: current_date,
			timeZone		: (timeZone != '')		? parseFloat(timeZone) 	: parseFloat(timezone),
			daysLabel		: (days_text != '')		? days_text				: 'Days',
			hoursLabel		: (hours_text != '')	? hours_text			: 'Hours',
			minutesLabel	: (minutes_text != '')	? minutes_text			: 'Minutes',
			secondsLabel	: (seconds_text != '')	? seconds_text			: 'Seconds',
			onComplete: function() {
				dcdt_timer_slider_over( product_id );
			},
		});

		/* Fallback for Internet Explorer */
		if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
			$('html').addClass('internet-explorer');
		}
	});

	// Simple 2 Clock JS
	$( '.dcdt-product-for .dcdt-timer-simple-2' ).each(function( index ) {

		var clock_id		= $(this).find('.dcdt-countdown-timer-simple-2').attr('data-id');
		var date_id			= $(this).find('.dcdt-countdown-timer-simple-2').attr('id');
		var product_id		= $(this).find('.dcdt-countdown-timer-simple-2').attr('data-product');
		var date_id			= date_id+ ' .dcdt-clock';
		var date_conf 		= $.parseJSON( $(this).find('.dcdt-date-conf').attr('data-conf'));
		var is_days 		= date_conf.is_days;
		var is_hours 		= date_conf.is_hours;
		var is_minutes 		= date_conf.is_minutes;
		var is_seconds 		= date_conf.is_seconds;
		var days_text 		= date_conf.days_text;
		var hours_text 		= date_conf.hours_text;
		var minutes_text 	= date_conf.minutes_text;
		var seconds_text 	= date_conf.seconds_text;
		var diff_date   	= date_conf.diff_date;
		var current_date	= date_conf.current_date;
		var timeZone 		= date_conf.timezone;

		$("#"+date_id).DcdtClock({
			day				: diff_date.day,
			month			: diff_date.month,
			year			: diff_date.year,
			hour			: diff_date.hour,
			minute			: diff_date.min,
			second			: diff_date.second,
			currentDateTime	: current_date,
			timeZone		: (timeZone != '')		? parseFloat(timeZone) 	: parseFloat(timezone),
			daysLabel		: (days_text != '')		? days_text				: 'Days',
			hoursLabel		: (hours_text != '')	? hours_text			: 'Hours',
			minutesLabel	: (minutes_text != '')	? minutes_text			: 'Minutes',
			secondsLabel	: (seconds_text != '')	? seconds_text			: 'Seconds',
			onComplete: function() {
				dcdt_timer_slider_over( product_id );
			},
		});

		/* Fallback for Internet Explorer */
		if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
			$('html').addClass('internet-explorer');
		}
	});

	// Initialize Sale Product Slider
	$( '.dcdt-sale-slider-shortcode' ).each(function( index ) {
		
		var slider_id   	= $(this).attr('id');
		var nav_id 			= null;
		var slider_conf 	= $.parseJSON( $(this).closest('.dcdt-product-section').find('.ibwp-dcdt-slider-conf').attr('data-conf'));
		
		jQuery('#'+slider_id).slick({
			dots			: (slider_conf.dots) 		== "true" 	? true 	: false,
			infinite		: (slider_conf.loop) 		== "true" 	? true 	: false,
			arrows			: (slider_conf.arrows) 		== "true" 	? true 	: false,
			speed			: parseInt(slider_conf.speed),
			autoplay		: (slider_conf.autoplay) 	== "true" 	? true 	: false,
			autoplaySpeed	: parseInt(slider_conf.autoplay_interval),
			slidesToShow	: 1,
			slidesToScroll	: 1,
			rtl             : (slider_conf.rtl) 		== "true" 	? true 	: false,
			adaptiveHeight	: true
		});
	});
});

/* Timer Animation */
function dcdt_animate_timer_cfsf($el, data) {
	$el.each( function(index) {
		var $this = jQuery(this),
			$flipFront = $this.find('.ce-flip-front'),
			$flipBack = $this.find('.ce-flip-back'),
			field = $flipBack.text(),
			fieldOld = $this.attr('data-old');
		if (typeof fieldOld === 'undefined') {
			$this.attr('data-old', field);
		}
		if (field != fieldOld) {
			$this.addClass('ce-animate');
			window.setTimeout(function() {
				$flipFront.text(field);
				$this
					.removeClass('ce-animate')
					.attr('data-old', field);
			}, 800);
		}
	});
}

/* Simple Clock Animation */
function dcdt_simple_animate_timer($el) {
	$el.each( function(index) {
		var $this		= jQuery(this),
			fieldText	= $this.text(),
			fieldData	= $this.attr('data-value'),
			fieldOld	= $this.attr('data-old');

		if (typeof fieldOld === 'undefined') {
			$this.attr('data-old', fieldText);
		}

		if (fieldText != fieldData) {

			$this
				.attr('data-value', fieldText)
				.attr('data-old', fieldData)
				.addClass('ce-animate');

			window.setTimeout(function() {
				$this
					.removeClass('ce-animate')
					.attr('data-old', fieldText);
			}, 300);
		}
	});
}


// Timer over then  remove sale price and timer and progress 
function dcdt_timer_slider_over( product_id ) {
	
	if( product_id ){

		var data 	= {
				'action'	: 'ibwp_dcdt_on_time_done',
				'post_id'	: product_id
			};

		jQuery.post( dcdt_get_timer_parameter.ajaxurl, data, function(response) {
			if( response.success ) {
					location.reload();
			} 
		});
	}	
}