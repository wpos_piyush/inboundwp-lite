<?php
/**
 * Countdown Module
 *
 * @package InboundWP Lite
 * @subpackage Deal Countdown Timer
 * @since 1.0
 */

/**
 * Basic plugin definitions
 * 
 * @package Deal Countdown Timer
 * @since 1.0
 */

IBWP_Lite()->define( 'IBWP_DCDT_DIR', dirname( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_DCDT_URL', plugin_dir_url( __FILE__ ) );
IBWP_Lite()->define( 'IBWP_DCDT_POST_TYPE', 'ibwp_dcdt_countdown' ); 	// post type
IBWP_Lite()->define( 'IBWP_DCDT_META_PREFIX', '_ibwp_dcdt_' ); 	// meta prefix
IBWP_Lite()->define( 'IBWP_DCDT_VERSION', '1.0' );

/**
 * Plugin Setup (On Activation)
 *
 * Does the initial setup, set default values for the plugin options.
 *
 * @package Deal Countdown Timer
 * @since 1.0
 */
function ibwp_dcdt_install() {

    ibwp_dcdt_register_post_type();

    // IMP need to flush rules for custom registered post type
    flush_rewrite_rules();

    // Update plugin version to option
	update_option( 'ibwp_dcdt_plugin_version', '1.0' );

}
add_action( 'ibwp_module_activation_hook_deal-countdown-timer', 'ibwp_dcdt_install' );

// Functions file
require_once( IBWP_DCDT_DIR . '/includes/ibwp-dcdt-functions.php' );

// Plugin Post Type File
require_once( IBWP_DCDT_DIR . '/includes/ibwp-dcdt-post-types.php' );

// Admin Class File
require_once( IBWP_DCDT_DIR . '/includes/admin/class-ibwp-dcdt-admin.php' );

// Script Class File
require_once( IBWP_DCDT_DIR . '/includes/class-ibwp-dcdt-script.php' );

// Public File
require_once( IBWP_DCDT_DIR . '/includes/class-ibwp-dcdt-public.php' );