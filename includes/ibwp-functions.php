<?php
/**
 * Plugin generic functions file
 *
 * @package InboundWP Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Escape Tags & Slashes
 *
 * Handles escapping the slashes and tags
 *
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_esc_attr($data) {
    return esc_attr( $data );
}

/**
 * Clean variables using sanitize_text_field. Arrays are cleaned recursively.
 * Non-scalar values are ignored.
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_clean( $var ) {
	if ( is_array( $var ) ) {
		return array_map( 'ibwpl_clean', $var );
	} else {
		$data = is_scalar( $var ) ? sanitize_text_field( $var ) : $var;
		return wp_unslash($data);
	}
}

/**
 * Sanitize number value and return fallback value if it is blank
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_clean_number( $var, $fallback = null) {
	$data = absint($var);
	return ( empty($data) && $fallback ) ? $fallback : $data;
}

/**
 * Sanitize color value and return fallback value if it is blank
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_clean_color( $color, $fallback = null ) {
    $data = sanitize_hex_color($color);
	return ( empty($data) && $fallback ) ? $fallback : $data;
}

/**
 * Sanitize url
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_clean_url( $url ) {
	return esc_url_raw( trim($url) );
}

/**
 * Strip Slashes From Array
 *
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_clean_html($data = array(), $flag = false) {

    if($flag != true) {
        $data = ibwpl_nohtml_kses($data);
    }
    $data = stripslashes_deep($data);
    return $data;
}

/**
 * Function to check email with comma separated values
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_check_email( $emails, $type = 'multiple' ) {

	$correct_email	= array();
	
	if( $type == 'multiple' ) {
	
		$email_ids = explode( ',', $emails );
		$email_ids = ibwpl_clean($email_ids);

		foreach ($email_ids as $email_key => $email_value) {
			if( is_email( $email_value ) ){
				$correct_email[] = $email_value;
			}
		}
	} else {
		$emails = ibwpl_clean( $emails );
		$correct_email[] = is_email( $emails ) ? $emails : ''; 
	}

	return implode( ', ', $correct_email );
}

/**
 * Strip Html Tags
 * It will sanitize text input (strip html tags, and escape characters)
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_nohtml_kses($data = array()) {

	if ( is_array($data) ) {

	$data = array_map('ibwp_nohtml_kses', $data);

	} elseif ( is_string( $data ) ) {
		$data = trim( $data );
		$data = wp_filter_nohtml_kses($data);
	}

	return $data;
}

/**
 * Sanitize Multiple HTML class
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_sanitize_html_classes($classes, $sep = " ") {
	$return = "";

	if( $classes && !is_array($classes) ) {
		$classes = explode($sep, $classes);
	}

	if( !empty($classes) ) {
		foreach($classes as $class){
			$return .= sanitize_html_class($class) . " ";
		}
		$return = trim( $return );
	}

	return $return;
}

/**
 * Function to add array after specific key
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_add_array(&$array, $value, $index, $from_last = false) {

    if( is_array($array) && is_array($value) ) {

        if( $from_last ) {
            $total_count    = count($array);
            $index          = (!empty($total_count) && ($total_count > $index)) ? ($total_count-$index): $index;
        }
        
        $split_arr  = array_splice($array, max(0, $index));
        $array      = array_merge( $array, $value, $split_arr);
    }

    return $array;
}

/**
 * Function to unique number value
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_unique() {
    static $unique = 0;
    $unique++;

    return $unique;
}

/**
 * Function to get grid column based on grid
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_grid_column( $grid = '' ) {

	if($grid == '2') {
		$grid_clmn = '6';
	} else if($grid == '3') {
		$grid_clmn = '4';
	}  else if($grid == '4') {
		$grid_clmn = '3';
	} else if ($grid == '1') {
		$grid_clmn = '12';
	} else {
		$grid_clmn = '6';
	}
	return $grid_clmn;
}

/**
 * Get InboundWP Lite main screen ids.
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_main_screen_ids() {

	$screen_ids = array(
		IBWP_SCREEN_ID.'_page_ibwp-about','toplevel_page_ibwp-dashboard',IBWP_SCREEN_ID.'_page_ibwp-license'
	);

	return $screen_ids;
}

/**
 * Get all InboundWP Lite screen ids.
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_screen_ids( $type = 'main' ) {

	$screen_ids = ibwpl_main_screen_ids();

	$module_screen_ids = apply_filters( 'ibwp_screen_ids', array() );
	
	if( !empty($type) && isset($module_screen_ids[$type]) ) {
		if( $type == 'main' ) {
			$screen_ids = array_merge( $screen_ids, (array)$module_screen_ids[$type] );
		} else {
			$screen_ids = $module_screen_ids[$type];
		}
	}

	return $screen_ids;
}

/**
 * Check it is a plugin screen or not
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function is_ibwpl_screen() {
	global $current_screen;

	$screen_ids 	= ibwpl_get_screen_ids();
	$curr_screen_id = $current_screen ? $current_screen->id : '';
	$curr_post_type = isset($current_screen->post_type) ? $current_screen->post_type : '';

	if( in_array($curr_screen_id, $screen_ids) || in_array($curr_post_type, $screen_ids) ) {
		return true;
	}
	return false;
}

/**
 * Function to get post excerpt
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_post_excerpt( $post_id = null, $content = '', $word_length = '55', $more = '...' ) {

    $has_excerpt  = false;
    $word_length  = !empty($word_length) ? $word_length : '55';

    // If post id is passed
    if( !empty($post_id) ) {
        if (has_excerpt($post_id)) {

          $has_excerpt  = true;
          $content    	= get_the_excerpt();

        } else {
          $content = !empty($content) ? $content : get_the_content();
        }
    }

    if( !empty($content) && (!$has_excerpt) ) {
        $content = strip_shortcodes( $content ); // Strip shortcodes
        $content = wp_trim_words( $content, $word_length, $more );
    }
    return $content;
}

/**
 * Function to limit words
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_limit_words($string, $word_limit, $more = '...') {
    $string = wp_trim_words( $string, $word_limit, $more );
    return $string;
}

/**
 * Function to get post featured image
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_post_featured_image( $post_id = '', $size = 'full', $default_img = false ) {
    $size   = !empty($size) ? $size : 'full';
    $image  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );

    if( !empty($image) ) {
        $image = isset($image[0]) ? $image[0] : '';
    }

    // Getting default image
    if( $default_img && empty($image) ) {       
    }

    return $image;
}

/**
 * Function to get post featured image
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_image_src( $attachment_id = '', $size = 'full' ) {

    $size   = !empty($size) ? $size : 'full';
    $image  = wp_get_attachment_image_src( $attachment_id, $size );

    if( !empty($image) ) {
        $image = isset($image[0]) ? $image[0] : '';
    }

    return $image;
}

/**
 * Function to get old browser
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_old_browser() {
    global $is_IE, $is_safari, $is_edge;

    // Only for safari
    $safari_browser = ibwpl_check_browser_safari();

    if( $is_IE || $is_edge || ($is_safari && (isset($safari_browser['version']) && $safari_browser['version'] <= 7.1)) ) {
        return true;
    }
    return false;
}

/**
 * Determine if the browser is Safari or not (last updated 1.7)
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_check_browser_safari() {

    // Takinf some variables
    $browser    = array();
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "";

    if (stripos($user_agent, 'Safari') !== false && stripos($user_agent, 'iPhone') === false && stripos($user_agent, 'iPod') === false) {
        $aresult = explode('/', stristr($user_agent, 'Version'));
        if (isset($aresult[1])) {
            $aversion = explode(' ', $aresult[1]);
            $browser['version'] = ($aversion[0]);
        } else {
            $browser['version'] = '';
        }
        $browser['browser'] = 'safari';
    }
    return $browser;
}

/**
 * Function to sort module key array on priority
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_sort_modules_cat($a, $b) {
	$a_priority = (!empty($a['priority']) && $a['priority'] > 0) ? $a['priority'] : 999;
	$b_priority = (!empty($b['priority']) && $b['priority'] > 0) ? $b['priority'] : 999;
	
	if ($a_priority == $b_priority) {
	    return 0;
	}
    return ($a_priority < $b_priority) ? -1 : 1;
}

/**
 * Function to sort module array on name
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_sort_modules($x, $y) {

	if( !isset($x['name']) || !isset($y['name']) ) {
		return false;
	}

    return strcasecmp($x['name'], $y['name']);
}

/**
 * Function to to filter array to get only keys which has zero value
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_zero($var) {
	return( ($var == 0) );
}

/**
 * Function to register plugin modules categories
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_register_module_cats() {
	$module_cats = array(
		'active_modules'=> array(
							'name'		=> __('Active Modules', 'inboundwp-lite'),
							'desc'		=> __('Activated modules on your website.' ,'inboundwp-lite'),
							'icon'		=> 'dashicons dashicons-dashboard',
							'priority'	=> 1,
						),
		'modules' 		=> array(
							'name'		=> __('Modules', 'inboundwp-lite'),
							'desc'		=> __('Enable various modules for your website.' ,'inboundwp-lite'),
							'icon'		=> 'dashicons dashicons-admin-plugins',
							'priority'	=> 2,
							'is_filter'	=> true,
						),
	);

	$module_cats = apply_filters( 'ibwp_register_site_module_cats', $module_cats );
 	uasort($module_cats, "ibwpl_sort_modules_cat"); // sort array on priority

	return $module_cats;
}

/**
 * Function to register plugin modules
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_register_modules() {

	$active_modules 	= ibwpl_active_modules();
	$widget_link 		= admin_url('widgets.php');
	$admin_url 			= admin_url('admin.php');
	//$widgets_tab_link	= ibwpl_get_plugin_link( 'main', array('tab' => 'widgets') );

	$modules = array(		
		//Social Proof
		'social-proof' => array(
			'name'			=> __('Social Proof', 'inboundwp-lite'),
			'desc'			=> __('Display real time customer activity data from your website. ie this user purchased this product – Time and country. Easily customize the look and feel of notification to match with your site’s branding.' ,'inboundwp-lite'),
			'extra_info'	=> array(
			    'desc'		=> array(
					__('A fully responsive module to show notification.', 'inboundwp-lite'),
			        __('You can able to set conversion globally for whole site.', 'inboundwp-lite'),
			        __('Allows you to show WooCommerce and EDD sales notification to visitor.', 'inboundwp-lite'),
			    ),
			    'supports'	=> array( 'multilanguage', 'responsive' )
			),
			'category'		=> 'modules',
			'path'			=> IBWPL_DIR.'modules/social-proof/social-proof.php',
			'conf_link'		=> add_query_arg( array( 'post_type' => 'ibwp_sp', 'page' => 'ibwp-sp-how-it-work' ), 'edit.php' ),
		),

		//Better Heading
		'better-heading' => array(
			'name'			=> __('Better Heading', 'inboundwp-lite'),
			'desc'			=> __('Better heading plugin is very useful for blogger. It helps author to create  better heading for the article. Plugin provide options where author can add 3 heading for a single article.' ,'inboundwp-lite'),
			'extra_info'	=> array(
			    'desc'		=> array(
			        __('A fully responsive plugin to set 3 title for blog.', 'inboundwp-lite'),
			        __('Able to check daily, weekly and monthly report of every title.', 'inboundwp-lite'),
			        __('Allows to set 3 heading for your blog, so you can choose best title amongs them after anlyzing.', 'inboundwp-lite'),
			    ),
			    'supports'	=> array( 'multilanguage', 'responsive' )
			),
			'category'		=> 'modules',
			'path'			=> IBWPL_DIR.'modules/better-heading/better-heading.php',
			'conf_link'		=> add_query_arg( array('page' => 'ibwp-bh-how-it-work'), 'admin.php' ),
		),		
		
		//Testimonial
		'testimonials' => array(
			'name'			=> __('Testimonials', 'inboundwp-lite'),
			'desc'			=> __('Testimonial is important part where site owner can display testimonial given by other users. We are giving testimonial with grid and slider.' ,'inboundwp-lite'),
			'extra_info'	=> array(
									'desc'		=> array(
														__('A fully responsive module to display testimonials in a beautiful slideshow and grid.', 'inboundwp-lite'),
														__('We have given 4 predefined designs', 'inboundwp-lite'),
													),
									'supports'	=> array('shortcode', 'multilanguage', 'responsive')
								),
			'category'		=> 'modules',
			'path'			=> IBWPL_DIR.'modules/testimonials/ibwp-testimonials.php',
			'conf_link'		=> add_query_arg( array('post_type' => 'ibwp_testimonial', 'page' => 'tmw-shrt-mapper'), 'edit.php' ),
		),

		//Deal Countdown Timer
		'deal-countdown-timer' => array(
		    'name'			=> __('Deal Countdown timer', 'inboundwp-lite'),
		    'desc'			=> __('Sale Countdown Timer helps you to create flash sales and increase revenues. You can add multiple flash sales with a start and end date. Plugin also shows stock progress bar.' ,'inboundwp-lite'),
		    'extra_info'	=> array(
		        'desc'		=> array(
		            __('A fully responsive module to display countdown clock with 2 designs.', 'inboundwp-lite'),
		            __('You can add stock progressbar for WooCommerce product.', 'inboundwp-lite'),
		            __('Work with WooCommerce & EDD single page only.', 'inboundwp-lite'),

		        ),
		        'supports'	=> array('shortcode', 'multilanguage', 'responsive')
		    ),
		    'category'		=> 'modules',
		    'path'			=> IBWPL_DIR.'modules/deal-countdown-timer/deal-countdown-timer.php',
		    'conf_link'		=> add_query_arg( array('post_type' => 'ibwp_dcdt_countdown'), 'edit.php' ),
		),

		//Marketing popup
		'marketing-popup' => array(
		    'name'			=> __('Marketing Popup', 'inboundwp-lite'),
		    'desc'			=> __('Marketing Popup is useful to show offers on website when user land on website, To display subscription form, Call to action, Exit Intent Technology, Announce and When user scroll page to some %.' ,'inboundwp-lite'),
		    'extra_info'	=> array(
		        'desc'		=> array(
		            __('A fully responsive module to display popup based on cookie.', 'inboundwp-lite'),
		            __('In this module 5 types of popup goal: Email-Lists, Target URL, Announcement, Get Phone Calls, Social Traffic.', 'inboundwp-lite'),
		            __('In this module you can use only modal popup.', 'inboundwp-lite'),

		        ),
		        'supports'	=> array( 'multilanguage', 'responsive' )
		    ),
		    'category'		=> 'modules',
		    'path'			=> IBWPL_DIR.'modules/marketing-popup/marketing-popup.php',
		    'conf_link'		=> add_query_arg( array('post_type' => 'ibwp_mp_popup', 'page' => 'ibwp-mp-how-it-work'), 'edit.php' ),
		),

		// Spin Wheel
		'spin-wheel' => array(
			'name'			=> __('Spin Wheel', 'inboundwp-lite'),
			'desc'			=> __('WordPress Spin Wheel offers visitors to fill in their email addresses to spins for prizes(coupon code ). This is the best way to collect email from visitors on your site, they will be pleased to fill in their email address.' ,'inboundwp-lite'),
			'extra_info'	=> array(
									'desc'		=> array(
														__('A fully responsive module to display Spin Wheel with custom design options to attract visitors.', 'inboundwp-lite'),
														__('You can set multiple spin wheel with specific designs.', 'inboundwp-lite'),
														__('You can add only 4 wheel segments.', 'inboundwp-lite'),
														__('You can add coupon of WooCommerce, Easy Digital Download with some special offers.', 'inboundwp-lite'),
														__('Also, you can set custom coupon code in wheel and redirect user to custom url.', 'inboundwp-lite'),
														__('Able to track google analytics event of website.', 'inboundwp-lite'),
														__('Able to add spin wheel globally include defualt pages of website too.', 'inboundwp-lite'),
													),
									'supports'	=> array('multilanguage', 'responsive'),
								),
			'category'		=> 'modules',
			'path'			=> IBWPL_DIR.'modules/spin-wheel/ibwp-spin-wheel.php',
			'conf_link'		=> add_query_arg( array('post_type' => 'ibwp_spw_spin_wheel', 'page' => 'ibwp-spw-how-it-work'), 'edit.php' ),
		),

		//Social Proof
		'whatsapp-chat-support' => array(
			'name'			=> __('WhatsApp Chat Support', 'inboundwp-lite'),
			'desc'			=> __('WhatsApp Chat for WordPress plugin allows you to integrate your WhatsApp experience directly into your website. This is one of the best way to connect and interact with your customer, you can offer support directly as well as build trust and increase customer loyalty.' ,'inboundwp-lite'),
			'extra_info'	=> array(
			    'desc'		=> array(
					__('A fully responsive module to display WhatsApp Chat Support to attract visitors.', 'inboundwp-pro'),
					__('You can add multiple agent but show only one agent.', 'inboundwp-pro'),
					__('You can set agent status with custom messages.', 'inboundwp-pro'),
					__('Also, you can set default message to send directly.', 'inboundwp-pro'),
				),
			    'supports'	=> array( 'multilanguage', 'responsive' )
			),
			'category'		=> 'modules',
			'path'			=> IBWPL_DIR.'modules/whatsapp-chat-support/ibwp-whatsapp-chat-support.php',
			'conf_link'		=> add_query_arg( array( 'post_type' => 'ibwp_wtacs', 'page' => 'ibwp-wtacs-settings' ), 'edit.php' ),
		),
	);

	$modules = (array) apply_filters( 'ibwp_register_site_modules', $modules );
	uasort($modules, "ibwpl_sort_modules"); // sort array on name

	return $modules;
}

/**
 * Function to get plugin modules category wise
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_plugin_modules( $status = 'all', $category = '' ) {

	global $ibwp_options;

	$plugin_modules 	= array();
	$modules 			= IBWP_Lite()->register_modules;
	$active_modules 	= IBWP_Lite()->active_modules;
	$module_cats 		= ibwpl_register_module_cats();

	switch ($status) {
		case 'active':
		case 'in_active':
			$status_modules = ($status == 'in_active') ? IBWP_Lite()->inactive_modules : $active_modules;
			if( !empty($status_modules) ) {
				foreach ($status_modules as $module_key => $module_val) {

					$module_cat = isset($modules[$module_key]['category']) ? $modules[$module_key]['category'] : '';

					if( isset($modules[$module_key]) && $module_cat && isset($module_cats[$module_cat]) ) {
						$plugin_modules[$module_cat][$module_key] = $modules[$module_key];
					}
				}
			}
			break;

		default:
			if( !empty($modules) ) {
				foreach ($modules as $module_key => $module_data) {

					// If key is empty then continue
					if( empty($module_key) || empty($module_data['category']) || !isset($module_cats[$module_data['category']]) ) {
						continue;
					}

					$plugin_modules[$module_data['category']][$module_key] = $module_data;

					// Adding active modules
					if( !empty($active_modules) && !empty($active_modules[$module_key]) ) {
						$plugin_modules['active_modules'][$module_key] = $module_data;
					}
				}
			}
			break;
	}

	// If category is passed
	if( $category ) {
		$plugin_modules = isset($plugin_modules[$category]) ? $plugin_modules[$category] : array();
	}

	return $plugin_modules;
}

/**
 * Function to get active plugin modules
 * This is similar of ibwp_get_active_modules() But this is for a little twik and fix for ibwp_register_modules()
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_active_modules() {

	global $ibwp_options;

	$result_arr			= array();
	$module_cats 		= ibwpl_register_module_cats();

	if( !empty($module_cats) ) {
		foreach ($module_cats as $module_cat_key => $module_cat_val) {

			$module_cat_key = sanitize_title($module_cat_key);

			if( isset($ibwp_options[$module_cat_key.'_pack']) ) {
				$result_arr = array_merge( $result_arr, $ibwp_options[$module_cat_key.'_pack'] );
				$result_arr = array_filter($result_arr);
			}
		}
	}
	return $result_arr;
}

/**
 * Function to get active plugin modules
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_active_modules( $by_post = false ) {

	global $ibwp_options;

	$result_arr			= array();
	$active_modules		= array();
	$register_modules 	= !empty(IBWP_Lite()->register_modules) ? IBWP_Lite()->register_modules : ibwpl_register_modules();
	$module_cats 		= ibwpl_register_module_cats();
	$ibwp_module_opts	= ($by_post && isset($_POST['ibwp_opts'])) ? ibwpl_clean( $_POST['ibwp_opts'] ) : $ibwp_options;

	if( !empty($module_cats) ) {
		foreach ($module_cats as $module_cat_key => $module_cat_val) {

			$module_cat_key = sanitize_title($module_cat_key);

			if( isset($ibwp_module_opts[$module_cat_key.'_pack']) ) {
				$result_arr = array_merge( $result_arr, $ibwp_module_opts[$module_cat_key.'_pack'] );
				$result_arr = array_filter($result_arr);
			}
		}

		// Checking the result array in registered modules so unnecessary module key will not remain
		if( !empty($result_arr) ) {
			foreach ($result_arr as $res_key => $res_val) {
				if( array_key_exists($res_key, $register_modules) ) {
					$active_modules[$res_key] = $res_val;
				}
			}
		}
	}

	return $active_modules;
}

/**
 * Function to get inactive plugin modules
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_inactive_modules( $by_post = false ) {

	global $ibwp_options;

	$result_arr	= array();

	// If post is passed
	if( $by_post && isset($_POST['ibwp_opts']) ) {

		$module_cats 		= ibwpl_register_module_cats();
		$ibwp_module_opts	= ibwpl_clean( $_POST['ibwp_opts'] );

		if( !empty($module_cats) ) {
			foreach ($module_cats as $module_cat_key => $module_cat_val) {

				$module_cat_key = sanitize_title($module_cat_key);

				if( isset($ibwp_module_opts[$module_cat_key.'_pack']) ) {
					$result_arr 		= ($result_arr + $ibwp_module_opts[$module_cat_key.'_pack']);
					$result_arr 		= array_filter( $result_arr, 'ibwp_get_zero' );
				}
			}
		}

	} else {

		$active_modules 	= IBWP_Lite()->active_modules;
		$register_modules 	= IBWP_Lite()->register_modules;

		$active_modules_arr 	= array_keys( $active_modules );
		$register_modules_arr 	= array_keys( $register_modules );

		if( !empty($register_modules) ) {
			foreach ($register_modules as $module_key => $module_val) {
				if( !isset( $active_modules[$module_key] ) ) {
					$result_arr[$module_key] = 0;
				}
			}
		}
	}

	return $result_arr;
}

/**
 * Function to get post type supports like sorting and etc
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_post_supports() {
	return apply_filters('ibwp_post_supports', array());
}

/**
 * Function to get taxonomy supports like sorting and etc
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_taxonomy_supports() {
	return apply_filters('ibwp_taxonomy_supports', array());
}

/**
 * Function to display message, norice etc
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_display_message( $type = 'update', $msg = '', $echo = 1 ) {

	switch ( $type ) {
		case 'reset':
			$msg = !empty( $msg ) ? $msg : __( 'All settings reset successfully.', 'inboundwp-lite');
			$msg_html = '<div id="message" class="updated notice notice-success is-dismissible">
							<p><strong>' . $msg . '</strong></p>
						</div>';
			break;

		default:
			$msg = !empty( $msg ) ? $msg : __('Your changes saved successfully.', 'inboundwp-lite');
			$msg_html = '<div id="message" class="updated notice notice-success is-dismissible">
							<p><strong>'. $msg .'</strong></p>
						</div>';
			break;
	}

	if( $echo ) {
		echo $msg_html;
	} else {
		return $msg_html;
	}
}

/**
 * Function to get plugin links
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_plugin_link( $type = 'main', $url_args = array() ) {

	$admin_url 	= admin_url('admin.php');
	$url_args	= is_array( $url_args ) ? $url_args : array();

	switch ($type) {
		case 'current':
			$link = add_query_arg( $url_args );
			break;

		case 'tour':
			$link = add_query_arg( array_merge( array('page' => IBWPL_PAGE_SLUG, 'tab' => 'modules', 'message' => 'ibwp-tutorial'), $url_args ), admin_url('admin.php') );
			break;

		case 'about':
			$link = add_query_arg( array_merge( array('page' => 'ibwp-about'), $url_args ), $admin_url );
			break;
		
		default:
			$link = add_query_arg( array_merge( array('page' => IBWPL_PAGE_SLUG), $url_args ), $admin_url );
			break;
	}
	return apply_filters('ibwpl_get_plugin_link', $link, $type);
}

/**
 * Function to get module supports
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_module_support_info() {
	$supports = array(
			'shortcode' 	=> array(
									'title' => __('Has Shortcode', 'inboundwp-lite'),
									'icon'	=> 'dashicons-menu',
									),
			'widget'		=> array(
									'title' => __('Has Widget', 'inboundwp-lite'),
									'icon'	=> 'dashicons-welcome-widgets-menus',
									),
			'page_builder'	=> array(
									'title' => __('Has Visual Composer Support', 'inboundwp-lite'),
									'icon'	=> 'dashicons-schedule',
									),
			'multilanguage'	=> array(
									'title' => __('100% Multilanguage', 'inboundwp-lite'),
									'icon'	=> 'dashicons-translation',
									),
			'responsive'	=> array(
									'title' => __('Fully Responsive', 'inboundwp-lite'),
									'icon'	=> 'dashicons-smartphone',
									),
		);
	return apply_filters('ibwpl_module_support_info', $supports);
}

/**
 * Function to get post types
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_pro_get_post_types() {

	$exclude_post 	= array('attachment','revision','nav_menu_item');	
	$all_post_types = get_post_types( array( 'public' => true ), 'name' );
	$post_types 	= array();
	
	foreach ($all_post_types as $post_type => $post_data) {
		if(!in_array( $post_type,$exclude_post)){
			$post_types['labels'][] = $post_data->label;
			$post_types['keys'][] = $post_type;
		}	
	}

	return apply_filters('ibwp_pro_get_post_types', $post_types );	
}

/**
 * Function to get current page URL
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_get_current_page_url( $args = array() ) {
	
	$curent_page_url = is_ssl() ? 'https://' : 'http://';
	
	//check server port is not 80
	if ( $_SERVER["SERVER_PORT"] != "80" ) {
		$curent_page_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$curent_page_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}

	// Remove Query Args
	if( isset( $args['remove_args'] ) ) {
		$curent_page_url = remove_query_arg( $args['remove_args'], $curent_page_url );
	}

	return apply_filters( 'ibwp_get_current_page_url', $curent_page_url );
}

/**
 * Function to display location of admin
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_display_locations( $type = 'main', $all = true, $exclude = array() ) {

    $locations = array();
    $exclude   = array('attachment','revision','nav_menu_item');   
    $all_post_types = get_post_types( array( 'public' => true ), 'name' );
    $post_types     = array();
    
    foreach ($all_post_types as $post_type => $post_data) {
        if(!in_array( $post_type,$exclude ) ){
    		
    		if( $all ) {
		    	$type_label = "All ". $post_data->label;
		    } else {
		    	$type_label = $post_data->label;
		    }
            $locations[$post_type] = $type_label;
        }   
    }

    if ( 'global' == $type ) {
        return $locations;
    }

    $glocations = array(
        'is_search'      => __( 'Search results', 'inbount-wp' ),
        'is_404'         => __( '404 error page', 'inbount-wp' ),
        'is_archive'     => __( 'All archives', 'inbount-wp' ),
        'all'            => __( 'All Site', 'inbount-wp' ),
    );

    $locations = array_merge($locations, $glocations);

    return $locations;
}

/**
 * Function to get status of post
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_post_status( $post_ids ) {

	if( empty( $post_ids ) ) {
		return;
	}

	$new_ids = array();
	if( is_array( $post_ids ) ) {
		foreach ( $post_ids as $key => $id ) {
			$post_status = get_post_status( $id );
			if( $post_status == "publish" ) {
				$new_ids[] = $id; 
			}
		}
	} else { 
		$post_status = get_post_status( $post_ids );
		if( $post_status == "publish" ) {
			$new_ids = $post_ids; 
		}
	}

	return $new_ids;
}