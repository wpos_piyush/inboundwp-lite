<?php
/**
 * Shortcode Preview
 *
 * @package InboundWP Lite
 * @since 1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// If not config array definde then return
if( empty($config_array) ) {
	return;
}

global $current_user;

// Taking some variables
extract( $config_array );

$authenticated      = false;
$ibwp_wp_scripts    = isset($ibwp_wp_scripts) ? array_merge( array('jquery'), (array)$ibwp_wp_scripts ) : array('jquery');

// Getting shortcode value
if( !empty( $_POST['ibwp_customizer_shrt'] ) ) {
	$shortcode_val = ibwpl_clean( $_POST['ibwp_customizer_shrt'] );
} elseif ( !empty($_GET['shortcode']) && isset( $registered_shortcodes[ $_GET['shortcode'] ] ) ) {
	$shortcode_val = '['.$_GET['shortcode'].']';
} else {
	$shortcode_val = '';
}

// For authentication so no one can use page via URL
if( isset($_SERVER['HTTP_REFERER']) ) {
	$url_query  = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY);
	parse_str( $url_query, $referer );

	if( !empty($referer['page']) && $referer['page'] == $referral_page ) {
		$authenticated = true;
	}
}

// Check Authentication else exit
if( !$authenticated ) {
	wp_die( __('Sorry, you are not allowed to access this page.', 'inboundwp-lite') );
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="Imagetoolbar" content="No" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php _e("Shortcode Preview", "inboundwp-pro"); ?></title>

		<?php wp_print_styles('common'); ?>
		<link rel="stylesheet" href="<?php echo IBWPL_URL; ?>assets/css/ibwp-public.css" type="text/css" />
		<?php
			// Module Styles
			if( !empty($module_styles) ) {
				foreach ( (array)$module_styles as $module_style_key => $module_style ) {
					echo '<link rel="stylesheet" href="'.$module_style.'?ver='.IBWPL_VERSION.'" type="text/css" />';
				}
			}
		?>
		<style type="text/css">
			body{background: #fff; overflow-x: hidden;}
			.ibwp-customizer-container{padding:0 16px;}
			.ibwp-customizer-container a[href^="http"]{cursor:not-allowed !important;}
			a:focus, a:active{box-shadow: none; outline: none;}
			.ibwp-link-notice{display: none; position: fixed; color: #a94442; background-color: #f2dede; border:1px solid #ebccd1; max-width:300px; width: 100%; left:0; right:0; bottom:30%; margin:auto; padding:10px; text-align: center; z-index: 1050;}
		</style>
		<?php wp_print_scripts( $ibwp_wp_scripts ); ?>
	</head>
	<body>
		<div id="ibwp-customizer-container" class="ibwp-customizer-container">
			<?php if( $shortcode_val ) {
				echo do_shortcode( $shortcode_val );
			} ?>
		</div>
		<div class="ibwp-link-notice"><?php _e('Sorry, You can not visit the link in preview', 'inboundwp-lite'); ?></div>

		<?php IBWP_Lite()->script->ibwp_global_script(); ?>
		<?php if( !empty( $localize_var['handler'] ) && !empty( $localize_var['value'] ) ) { ?>
			<script type='text/javascript'>
			//<![CDATA[
			var <?php echo $localize_var['handler']; ?> = <?php echo wp_json_encode( $localize_var['value'] ); ?>;
			//]]>
			</script>
		<?php }

			// Module Scripts
			if( !empty($module_scripts) ) {
				foreach ( (array)$module_scripts as $module_script_key => $module_script ) {
					echo '<script type="text/javascript" src="'.$module_script.'?ver='.IBWPL_VERSION.'"></script>';
				}
			}
		?>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(document).on('click', 'a', function(event) {
				
				var href_val = $(this).attr('href');

				if( href_val.indexOf('javascript:') < 0 ) {
					$('.ibwp-link-notice').fadeIn();
				}
				event.preventDefault();

				setTimeout(function() {
					$(".ibwp-link-notice").fadeOut('normal');
				}, 4000 );
			});
		});
		</script>
	</body>
</html>