<?php
/**
 * InboundWP Lite Shortcode Mapper Page
 *
 * @package InboundWP Lite
 * @since 1.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// If not config array definde then return
if( empty($config_array) ) {
	return;
}

// Default configuration
$defaults_config_array = array(
							'registered_shortcodes' 	=> '',
							'page_title'				=> __( 'Shortcode Builder', 'inboundwp-lite' ),
							'preview_shortcode'			=> '',
							'preview_url' 				=> '',
							'shortcode_generator_url' 	=> admin_url(),
							'seperate_page'				=> true,
						);
$config_array = wp_parse_args( $config_array, $defaults_config_array );

// Instantiate the shortcode mapper
if( !class_exists( 'IBWPL_Shortcode_Mapper' ) ) {
	include_once( IBWPL_DIR . '/includes/admin/shortcode-mapper/class-ibwp-shortcode-mapper.php' );
}

// Taking some variables
extract( $config_array );

$shortcode_fields 	= array();
$shortcode_sanitize = str_replace('-', '_', $preview_shortcode);
$wrap_cls			= ( $seperate_page ) ? 'wrap' : 'ibwp-pad-top-20';
?>
<div class="<?php echo $wrap_cls; ?> ibwp-customizer-settings">

	<?php if( $page_title ) { ?>
	<h2><?php echo $page_title; ?></h2>
	<?php }

	if($pro_notice) {
		echo '<div class="ibwp-pro-notice">'.$pro_notice.'</div><br>';
	}

	// If invalid shortocde is passed then simply return
	if( !empty($_GET['shortcode']) && !isset( $registered_shortcodes[ $_GET['shortcode'] ] ) ) {
		echo '<div id="message" class="error notice">
				<p><strong>'.__('Sorry, Something happened wrong.', 'inboundwp-lite').'</strong></p>
			 </div>';
		return;
	}
	?>

	<div class="ibwp-customizer-toolbar">
		<?php if( !empty( $registered_shortcodes ) ) { ?>
			<select class="ibwp-cust-shrt-switcher" id="ibwp-cust-shrt-switcher">
				<option value=""><?php _e('-- Choose Shortcode --', 'inboundwp-lite'); ?></option>
				<?php foreach ($registered_shortcodes as $shrt_key => $shrt_val) {

					if( empty($shrt_key) ) {
						continue;
					}

					$shrt_val 		= !empty($shrt_val) ? $shrt_val : $shrt_key;
					$shortcode_url 	= add_query_arg( array('shortcode' => $shrt_key), $shortcode_generator_url );
				?>
				<option value="<?php echo $shrt_key; ?>" <?php selected( $preview_shortcode, $shrt_key); ?> data-url="<?php echo esc_url( $shortcode_url ); ?>"><?php echo $shrt_val; ?></option>
				<?php } ?>
			</select>
		<?php } ?>		
		<span class="ibwp-cust-shrt-generate-help ibwp-tooltip" data-tooltip-content="#ibwp-shrt-generate-help-cnt"><i class="dashicons dashicons-editor-help"></i></span>
		<div class="ibwp-hide">
			<div id="ibwp-shrt-generate-help-cnt">
				<?php _e("The Shortcode Mapper allows you to preview plugin shortcode. You can choose your desired shortcode from the dropdown and check various parameters from left panel.", 'inboundwp-lite'); ?>
				<p><?php _e('You can paste shortocde to below and press Generate button to preview so each and every time you do not have to choose each parameters!!!', 'inboundwp-lite'); ?></p>
			</div>
		</div>
	</div><!-- end .ibwp-customizer-toolbar -->

	<div class="ibwp-customizer ibwp-clearfix" data-shortcode="<?php echo $preview_shortcode; ?>">
		<div class="ibwp-customizer-control ibwp-clearfix">
			<div class="ibwp-customizer-heading"><?php _e('Shortcode Parameters', 'inboundwp-lite'); ?></div>
			<?php
				if ( function_exists( $shortcode_sanitize.'_shortcode_fields' ) ) {
					$shortcode_fields = call_user_func( $shortcode_sanitize.'_shortcode_fields', $preview_shortcode );
				}
				$shortcode_fields = apply_filters('IBWPL_Shortcode_Mapper_fields', $shortcode_fields, $preview_shortcode );

				$shortcode_mapper = new IBWPL_Shortcode_Mapper();
				$shortcode_mapper->render( $shortcode_fields );
			?>
		</div>

		<div class="ibwp-customizer-preview ibwp-clearfix">
			<div class="ibwp-customizer-shrt-wrp">
				<div class="ibwp-customizer-heading"><?php _e('Shortcode', 'inboundwp-lite'); ?>
					<div class="ibwp-customizer-shrt-tool ibwp-right">
						<button type="button" class="button button-primary button-small ibwp-btn ibwp-cust-shrt-generate"><?php _e('Generate', 'inboundwp-lite') ?></button>
						<i title="<?php _e('Full Preview Mode', 'inboundwp-lite'); ?>" class="ibwp-tooltip ibwp-cust-dwp dashicons dashicons-editor-expand"></i>
					</div>
				</div>
				<form action="<?php echo esc_url($preview_url); ?>" method="post" class="ibwp-customizer-shrt-form" id="ibwp-customizer-shrt-form" target="ibwp_customizer_preview_frame">
					<textarea name="ibwp_customizer_shrt" class="ibwp-customizer-shrt" id="ibwp-customizer-shrt" placeholder="<?php _e('Copy or Paste Shortcode', 'inboundwp-lite'); ?>"></textarea>
				</form>
			</div>
			<div class="ibwp-customizer-heading"><?php _e('Preview Window', 'inboundwp-lite'); ?> <span class="ibwp-cust-heading-info ibwp-tooltip" title="<?php _e('Preview will be displayed according to responsive layout mode. You can check with `Full Preview` mode for better visualization.', 'inboundwp-lite'); ?>">[?]</span></div>
			<div class="ibwp-customizer-window">
				<iframe class="ibwp-customizer-preview-frame" name="ibwp_customizer_preview_frame" src="<?php echo esc_url($preview_url); ?>" scrolling="auto" frameborder="0"></iframe>
				<div class="ibwp-customizer-loader"></div>
				<div class="ibwp-customizer-error"><?php _e('Sorry, Something happened wrong.', 'inboundwp-lite'); ?></div>
			</div>
		</div>
	</div><!-- ibwp-customizer -->

	<br/>
	<div class="ibwp-cust-footer-note"><span class="description"><?php _e('Note: Preview will be displayed according to responsive layout mode. Live preview may display differently when added to your page based on inheritance from some styles.', 'inboundwp-lite'); ?></span></div>

</div><!-- end .wrap -->