<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package InboundWP Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Lite_Admin {

	function __construct() {

		// Action to register admin menu
		add_action( 'admin_menu', array($this, 'ibwp_register_menu'), 5 );

		// Action to register admin menu
		add_action( 'admin_menu', array($this, 'ibwp_register_sub_menu'), 99 );

		// Filter to modify wordpress admin footer text
		add_filter( 'admin_footer_text', array( $this, 'ibwp_admin_footer_text' ), 1 );

		// Action add taxonomy hooks
		add_action( 'admin_init', array($this, 'ibwp_admin_taxonomy_hooks') );

		// Filter to add row data
		add_filter( 'post_row_actions', array($this, 'ibwp_add_post_row_data'), 5, 2 );

		// Action to add menu at admin bar
		add_action( 'admin_bar_menu', array($this, 'ibwp_admin_bar_menu'), 99 );

		// Filter to add plugin action link
		add_filter( 'plugin_action_links_' . IBWPL_BASENAME, array($this, 'ibwp_plugin_action_links') );
	}

	/**
	 * Function to register admin menus
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_register_menu() {
		add_menu_page( __('InboundWP Lite - By WP Online Support', 'inboundwp-lite'), __('InboundWP Lite', 'inboundwp-lite'), 'manage_options', IBWPL_PAGE_SLUG, array($this, 'ibwp_render_dashboard_page'), IBWPL_URL.'assets/images/inbound-logo-20.png', 4 );
	}

	/**
	 * Function to register admin sub menus
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_register_sub_menu() {
		// About Page
		add_submenu_page( IBWPL_PAGE_SLUG, __('About', 'inboundwp-lite'), __('About', 'inboundwp-lite'), 'manage_options', 'ibwp-about', array($this, 'ibwp_render_about_page') );

		// Upgrade to Pro Page
		add_submenu_page( IBWPL_PAGE_SLUG, __('Upgrade to Pro', 'inboundwp-lite'), '<span style="color:#2ECC71">'.__('Upgrade to Pro', 'inboundwp-lite').'</span>', 'manage_options', 'ibwp-upgrade-pro', array($this, 'ibwp_upgrade_pro_page') );
	}

	/**
	 * Function to handle the main dashboard page
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_render_dashboard_page() {
		include_once( IBWPL_DIR . '/includes/admin/ibwp-dashboard.php' );
	}

	/**
	 * Function to handle the about page html
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_render_about_page() {
		include_once( IBWPL_DIR . '/includes/admin/ibwp-about.php' );
	}

	/**
	 * Function to handle the upgrade to pro page html
	 * 
	 * @package InboundWP Lite
	 * @since 1.0.2
	 */
	function ibwp_upgrade_pro_page() {
		include_once( IBWPL_DIR . '/includes/admin/ibwp-upgrade-pro.php' );
	}

	/**
	 * Function to modify WordPress admin footer text
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_admin_footer_text( $footer_text ) {

		$ibwp_screen = is_ibwpl_screen();

		if( $ibwp_screen ) {
			$footer_text = $footer_text .' | '. sprintf( __('Thank you for using <a href="%s" target="_blank">InboundWP Lite</a>. A huge thanks in advance!', 'inboundwp-lite'), IBWPL_FREE_LINK );
		}
		return $footer_text;
	}

	/**
	 * Admin taxonomy hooks
	 * 
	 * @package InboundWP Lite
 	 * @since 1.0
	 */
	function ibwp_admin_taxonomy_hooks() {

		$tax_supports = IBWP_Lite()->taxonomy_supports; 	// Taxonomy supports

		if( !empty( $tax_supports ) ) {
			foreach ($tax_supports as $tax_key => $tax_data) {
				
				// Taxonomy columns
				if( !empty($tax_key) && isset($tax_data['shortcode_clmn']) ) {

					// Filter to add columns and data in category table
					add_filter('manage_edit-'.$tax_key.'_columns', array($this, 'ibwp_manage_category_columns'));
					add_filter('manage_'.$tax_key.'_custom_column', array($this, 'ibwp_cat_columns_data'), 10, 3);
				}

				// Taxonomy columns
				if( !empty($tax_key) && isset($tax_data['row_data_id']) ) {

					// Filter to add row action in category table
					add_filter( $tax_key.'_row_actions', array($this, 'ibwp_add_tax_row_data'), 5, 2 );
				}
			}
		} // End of if
	}

	/**
	 * Function to add custom quick links at post listing page
	 * 
	 * @package InboundWP Lite
 	 * @since 1.0
	 */
	function ibwp_add_post_row_data( $actions, $post ) {

		$post_support = IBWP_Lite()->post_supports; // Post type supports

		// Post row data filter
		if( isset($post_support[$post->post_type]['row_data_post_id']) ) {
			return array_merge( array( 'ibwp_id' => 'ID: ' . $post->ID ), $actions );
		}
		return $actions;
	}

	/**
	 * Function to add category columns
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_manage_category_columns( $columns ) {

		global $taxonomy;

		$tax_support = IBWP_Lite()->taxonomy_supports; // Taxonomy supports

		$new_columns['ibwp_tax_shortcode'] = isset($tax_support[$taxonomy]['shortcode_clmn']['column_name']) ? $tax_support[$taxonomy]['shortcode_clmn']['column_name'] : __('Category Shortcode', 'inboundwp-lite');
		
		$columns = ibwpl_add_array( $columns, $new_columns, 2 );
		
		return $columns;
	}

	/**
	 * Function to add category columns data
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_cat_columns_data( $ouput, $column_name, $tax_id ) {

		global $taxonomy;

		$tax_support = IBWP_Lite()->taxonomy_supports; // Taxonomy supports

		$column_cnt = isset($tax_support[$taxonomy]['shortcode_clmn']['column_cnt']) ? $tax_support[$taxonomy]['shortcode_clmn']['column_cnt'] : '';

		if( $column_name == 'ibwp_tax_shortcode' ){
			$ouput .= str_replace('{cat_id}', $tax_id, $column_cnt);
		}
		return $ouput;
	}

	/**
	 * Function to add category row action
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_add_tax_row_data( $actions, $tag ) {
		return array_merge( array( 'ibwp_id' => 'ID: ' . $tag->term_id ), $actions );
	}

	/**
	 * Function to add menu at admin bar
	 * 
	 * @package InboundWP Lite
 	 * @since 1.0
	 */
	function ibwp_admin_bar_menu() {

		if ( current_user_can( 'manage_options' ) ) {

			global $wp_admin_bar;

			$plugin_link = ibwpl_get_plugin_link();

			$wp_admin_bar->add_menu( array(
				'id'    => 'ibwp-admin-bar-menu',
				'title' => __('InboundWP Lite', 'inboundwp-lite'),
				'href'  => $plugin_link,
				'meta'   => array( 'tabindex' => null ),
			));
		}
	}

	/**
	 * Function to add license plugins link
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_plugin_action_links( $links ) {

		$config_link 		= ibwpl_get_plugin_link();
		$links['config'] 	= '<a href="' . esc_url($config_link) . '" title="' . esc_attr( __( 'Configure Plugin', 'inboundwp-lite' ) ) . '">' . __( 'Configure', 'inboundwp-lite' ) . '</a>';

		return $links;
	}
}

$ibwp_lite_admin = new IBWP_Lite_Admin();