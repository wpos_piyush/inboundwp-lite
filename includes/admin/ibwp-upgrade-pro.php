<?php
/**
 * About InboundWP Lite
 *
 * Handles the upgrade to pro us page HTML
 *
 * @package InboundWP Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit; ?>

<div class="wrap ibwp-cnt-wrap upgrade-pro-wrap ibwp-upgrade-pro-wrap">
	<h1><?php _e('InboundWP Pro - Features','inboundwp-lite'); ?></h1>
	<div class="ibwp-upgrade-pro-btn">
		<a href="<?php echo IBWPL_PRO_LINK; ?>" target="_blank"><?php _e('Upgrade to Pro','inboundwp-lite'); ?></a>
	</div>
	<div class="ibwp-upgrade-pro-inner">
		<div class="row ibwp-clearfix">
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Social Proof','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-image-filter"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('Review Notification','inboundwp-lite'); ?></li>
							<li><?php _e('Increase visibility','inboundwp-lite'); ?></li>
							<li><?php _e('Increase max per page limit','inboundwp-lite'); ?></li>
							<li><?php _e('Custom design options','inboundwp-lite'); ?></li>
							<li><?php _e('Display review global settings','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Testimonial','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-format-quote"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('Google review grid shortcode','inboundwp-lite'); ?></li>
							<li><?php _e('Google review slider shortcode','inboundwp-lite'); ?></li>
							<li><?php _e('Front-end form shortcode','inboundwp-lite'); ?></li>
							<li><?php _e('4+ more designs for all','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Marketing Popup','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-feedback"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('Bar Popup, Slider Popup, Push Notification Popup','inboundwp-lite'); ?></li>
							<li><?php _e('More popup appear settings','inboundwp-lite'); ?></li>
							<li><?php _e('Display individual global settings','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Deal Countdown Timer','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-clock"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('2+ more timer style','inboundwp-lite'); ?></li>
							<li><?php _e('Woocommerce sale slider shortcode','inboundwp-lite'); ?></li>
							<li><?php _e('Shortcode builder','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row ibwp-clearfix">
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('WhatsApp Chat Support','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-format-chat"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('1+ more agent support','inboundwp-lite'); ?></li>
							<li><?php _e('Custom chatbox design options','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Custom CSS & JS','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-editor-code"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('Global CSS & JS','inboundwp-lite'); ?></li>
							<li><?php _e('Page wise CSS & JS','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Better Heading','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-admin-post"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('3+ more headings','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="ibwp-medium-3 ibwp-columns">
				<div class="ibwp-pro-modules-box">
					<div class="ibwp-modules-top">
						<h4 class="ibwp-pro-module"><?php _e('Spin Wheel','inboundwp-lite'); ?></h4>
					</div>
					<div class="ibwp-modules-bottom">
						<div class="ibwp-module-icon">
							<i class="dashicons dashicons-sos"></i>
						</div>
						<ul class="ibwp-pro-features">
							<li><?php _e('4+ more wheel segments','inboundwp-lite'); ?></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ibwp-upgrade-pro-btn">
		<a href="<?php echo IBWPL_PRO_LINK; ?>" target="_blank"><?php _e('Upgrade to Pro','inboundwp-lite'); ?></a>
	</div>
</div>