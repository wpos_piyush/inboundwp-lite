<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package InboundWP Lite
 * @since 1.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class IBWP_Lite_Script {

	function __construct() {

		// Action to define global javascript variable
		add_action( 'wp_print_scripts', array($this, 'ibwp_global_script'), 5 );

		// Action to add style at front side
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_front_styles') );
		
		// Action to add script at front side
		add_action( 'wp_enqueue_scripts', array($this, 'ibwp_front_scripts') );
		
		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_admin_styles') );
		
		// Action to add script at admin side
		add_action( 'admin_enqueue_scripts', array($this, 'ibwp_admin_scripts') );
	}

	/**
	 * Function to define global javascript variable
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_global_script() {

		$ibwp_rtl 			= (is_rtl()) 			? 1 : 0;
		$ibwp_mobile 		= (wp_is_mobile()) 		? 1 : 0;
		$ibwp_old_browser 	= ibwpl_old_browser() 	? 1 : 0;
		$ibwp_ajaxurl 		= admin_url( 'admin-ajax.php', ( is_ssl() ? 'https' : 'http' ) );

		$script  = "<script type='text/javascript'>";
		$script .= "var ibwp_is_rtl = {$ibwp_rtl};";
		$script .= "var ibwp_mobile = {$ibwp_mobile};";
		$script .= "var ibwp_old_browser = {$ibwp_old_browser};";
		$script .= "var ibwp_ajaxurl = '{$ibwp_ajaxurl}';";
		$script .= "</script>";

		echo $script;
	}

	/**
	 * Function to add style at front side
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_front_styles() {

		// Registring and enqueing font awesome css
		if( !wp_style_is( 'ibwp-font-awesome', 'registered' ) ) {
			wp_register_style( 'ibwp-font-awesome', IBWPL_URL.'assets/css/font-awesome.min.css', null, IBWPL_VERSION );
			wp_enqueue_style( 'ibwp-font-awesome' );
		}

		// Registring and enqueing tooltip css
		if( !wp_style_is( 'ibwp-tooltip-style', 'registered' ) ) {
			wp_register_style( 'ibwp-tooltip-style', IBWPL_URL.'assets/css/tooltipster.min.css', null, IBWPL_VERSION );
		}

		// Registring and enqueing slick slider css
		if( !wp_style_is( 'ibwp-slick-style', 'registered' ) ) {
			wp_register_style( 'ibwp-slick-style', IBWPL_URL.'assets/css/slick.css', array(), IBWPL_VERSION );
		}

		// Registring public style
		wp_register_style( 'ibwp-public-style', IBWPL_URL.'assets/css/ibwp-public.css', null, IBWPL_VERSION );
		wp_enqueue_style('ibwp-public-style');
	}

	/**
	 * Function to add script at front side
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_front_scripts() {

		// Enqueue built in script
		wp_enqueue_script( 'jquery' );

		// Registring tooltip script
		if( !wp_script_is( 'ibwp-tooltip-script', 'registered' ) ) {
			wp_register_script( 'ibwp-tooltip-script', IBWPL_URL.'assets/js/tooltipster.min.js', array('jquery'), IBWPL_VERSION, true );
		}

		// Filter JS
		if( !wp_script_is( 'ibwp-filter-script', 'registered' ) ) {
			wp_register_script( 'ibwp-filter-script', IBWPL_URL.'assets/js/jquery.mixitup.min.js', array('jquery'), IBWPL_VERSION, true );
		}

		// Registring slick slider script
		if( !wp_script_is( 'ibwp-slick-jquery', 'registered' ) ) {
			wp_register_script( 'ibwp-slick-jquery', IBWPL_URL.'assets/js/slick.min.js', array('jquery'), IBWPL_VERSION, true );
		}

		// Registring magnific popup script
		wp_register_script( 'ibwp-public-script', IBWPL_URL.'assets/js/ibwp-public.js', array('jquery'), IBWPL_VERSION, true );
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_admin_styles( $hook ) {

		global $current_screen;

		$ibwp_screen 	= is_ibwpl_screen();
		$screen_id 		= $current_screen ? $current_screen->id : '';

		// Registring select 2 style
		wp_register_style( 'ibwp-select2-style', IBWPL_URL.'assets/css/select2.min.css', null, IBWPL_VERSION );

		// Registring select 2 style
		wp_register_style( 'ibwp-tooltip-style', IBWPL_URL.'assets/css/tooltipster.min.css', null, IBWPL_VERSION );

		if( !wp_style_is( 'ibwp-font-awesome', 'registered' ) ) {
			wp_register_style( 'ibwp-font-awesome', IBWPL_URL.'assets/css/font-awesome.min.css', null, IBWPL_VERSION );
		}

		if( $screen_id == 'inboundwp-lite_page_ibwp-upgrade-pro' ) {
			wp_enqueue_style( 'ibwp-font-awesome' );
		}

		// Registring admin script on dashboard page
	    if( $screen_id == 'toplevel_page_ibwp-dashboard' ) {
			wp_enqueue_style( 'ibwp-tooltip-style' );
		}


		// Shortcode builder screen
		$shrt_mapper_screen = ibwpl_get_screen_ids( 'shortcode_mapper' );
		if( in_array( $hook, $shrt_mapper_screen ) ) {
			// Styles
			wp_enqueue_style( 'ibwp-tooltip-style' );
			wp_enqueue_style( 'wp-color-picker' );

		}	

		// Registring admin script
		wp_register_style( 'ibwp-admin-style', IBWPL_URL.'assets/css/ibwp-admin.css', null, IBWPL_VERSION );
		
		// If page is plugin setting page then enqueue script
		if( $ibwp_screen ) {
			wp_enqueue_style( 'ibwp-admin-style' );
		}

		wp_enqueue_style( 'ibwp-admin-style' );
	}

	/**
	 * Function to add script at admin side
	 * 
	 * @package InboundWP Lite
	 * @since 1.0
	 */
	function ibwp_admin_scripts( $hook ) {

		global $current_screen, $post_type, $wp_query, $wp_version;

		$post_support 	= IBWP_Lite()->post_supports; // Post type supports
		$ibwp_screen 	= is_ibwpl_screen();
		$screen_id    	= $current_screen ? $current_screen->id : '';
		$new_ui 		= $wp_version >= '3.5' ? '1' : '0';		

		// Filter JS
		wp_register_script( 'ibwp-filter-script', IBWPL_URL.'assets/js/jquery.mixitup.min.js', array('jquery'), IBWPL_VERSION, true );

		// Registring select 2 script
		wp_register_script( 'ibwp-select2-script', IBWPL_URL.'assets/js/select2.min.js', array('jquery'), IBWPL_VERSION, true );

		// Registring tooltip script
		wp_register_script( 'ibwp-tooltip-script', IBWPL_URL.'assets/js/tooltipster.min.js', array('jquery'), IBWPL_VERSION, true );

	    // Registring admin script on dashboard page
	    if( $screen_id == 'toplevel_page_ibwp-dashboard' ) {
			wp_enqueue_script( 'ibwp-filter-script' );
			wp_enqueue_script( 'ibwp-tooltip-script' );
		}

		// Registring admin script
		wp_register_script( 'ibwp-admin-js', IBWPL_URL.'assets/js/ibwp-admin.js', array('jquery'), IBWPL_VERSION, true );
		wp_localize_script( 'ibwp-admin-js', 'IBWPAdmin', array(
														'new_ui' 				=>	$new_ui,
														'is_mobile' 			=> (wp_is_mobile()) ? 1 : 0,
														'reset_msg'				=> __('Click OK to reset all options. All settings will be lost!', 'inboundwp-lite'),
													));

		// Registring admin script
		wp_register_script( 'ibwp-shortcode-mapper', IBWPL_URL.'assets/js/ibwp-shrt-mapper.js', array('jquery'), IBWPL_VERSION, true );
		wp_localize_script( 'ibwp-shortcode-mapper', 'IBWP_Shrt_Mapper', array(
																'shortocde_err' => __("Sorry, Something happened wrong. Kindly please be sure that you have choosen relevant shortocde from the dropdown.", 'inboundwp-lite'),
															));

		// If page is plugin setting page then enqueue script
		if( $ibwp_screen ) {
			wp_enqueue_script( 'ibwp-admin-js' );
		}

		

		// Shortcode builder screen
		$shrt_mapper_screen = ibwpl_get_screen_ids( 'shortcode_mapper' );
		
		if( in_array( $hook, $shrt_mapper_screen ) ) {
			// Scripts
			wp_enqueue_script( 'shortcode' );
			wp_enqueue_script( 'jquery-ui-accordion' );
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'ibwp-tooltip-script' );

			wp_enqueue_script( 'ibwp-shortcode-mapper' );
			
			wp_enqueue_media(); // For media uploader
		}


	}
}

$ibwp_lite_script = new IBWP_Lite_Script();