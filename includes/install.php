<?php
/**
 * Install Function
 *
 * @package InboundWP Lite
 * @since 1.0
*/

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Activation Hook
 * 
 * Register plugin activation hook.
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_install() {

	// Deactivate free version
	if( is_plugin_active('inboundwp/inboundwp.php') ) {
		add_action('update_option_active_plugins', 'ibwpl_deactivate_pro_version');
	}

	ibwpl_run_install();

	// Getting active modules
	$active_modules = ibwpl_get_active_modules();

	// To call all active modules activation hook
	if( !empty($active_modules) ) {
		$ibwp_modules_activity = array(
									'recently_active_module' => $active_modules,
								);
		set_transient( 'ibwp_modules_activity', $ibwp_modules_activity, HOUR_IN_SECONDS );
	}

	do_action( 'ibwp_activation_hook' );
}
register_activation_hook( IBWPL_PLUGIN_FILE, 'ibwpl_install' );
	
/**
 * Deactivate free plugin
 * 
 * @package InboundWP Lite
 * @since 1.0.2
 */
function ibwpl_deactivate_pro_version() {
	deactivate_plugins('inboundwp/inboundwp.php', true);
}

/**
 * Function to display admin notice of activated plugin.
 * 
 * @package Inboundwp Lite
 * @since 1.0.2
 */
function ibwpl_plugin_admin_notice() {

	global $pagenow;

	$dir				= WP_PLUGIN_DIR . '/inbpundwp/inbpundwp.php';
	$notice_transient	= get_transient( 'ibwpl_install_notice' );

	// If PRO plugin is active and free plugin exist
	if ( $notice_transient == false && file_exists($dir) && $pagenow == 'plugins.php' && current_user_can( 'install_plugins' ) ) {

		$notice_link = add_query_arg( array('message' => 'ibwpl-install-notice'), admin_url('plugins.php') );

		echo '<div id="message" class="updated notice" style="position:relative;">
				<p>'.sprintf( __('<strong>Thank you for activating Inboundwp Lite</strong>.<br /> It looks like you had Pro version <strong>(<em>Inboundwp</em>)</strong> of this plugin activated. To avoid conflicts the extra version has been deactivated and we recommend you delete it.', 'inboundwp-lite') ).'</p>
				<a href="'.esc_url( $notice_link ).'" class="notice-dismiss" style="text-decoration:none;"></a>
			</div>';
	}
}

// Action to display notice
add_action( 'admin_notices', 'ibwpl_plugin_admin_notice');

/**
 * Run the Install process
 *
 * @since  1.0
 */
function ibwpl_run_install() {

	global $wpdb, $ibwp_options;

	// Get settings for the plugin
	$ibwp_options = get_option( 'ibwp_opts' );
	
	if( empty( $ibwp_options ) ) { // Check plugin version option
		
		// set default settings
		ibwpl_default_settings();

		// Update plugin version to option
		update_option( 'ibwp_plugin_version', '1.0' );
	}
}

/**
 * Deactivation Hook
 * 
 * Register plugin deactivation hook.
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
register_deactivation_hook( __FILE__, 'ibwpl_uninstall');

/**
 * Plugin Deactivation
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_uninstall() {
    // Plugin deactivation process
}

/**
 * Set redirect transition on update or activation
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_set_welcome_page_redirect() {

	// return if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) )
		return;

	// Add the transient to redirect
    set_transient( '_ibwp_activation_redirect', true, 30 );
}
add_action('ibwp_activation_hook', 'ibwpl_set_welcome_page_redirect');

/**
 * Redirect to welcome page when plugin is activated
 * 
 * @package InboundWP Lite
 * @since 1.0
 */
function ibwpl_welcome_page_redirect() {

	// If plugin notice is dismissed
	if( isset($_GET['message']) && $_GET['message'] == 'ibwpl-install-notice' ) {
		set_transient( 'ibwpl_install_notice', true, 604800 );
	}

	// return if no activation redirect
    if ( ! get_transient( '_ibwp_activation_redirect' ) ) {
		return;
	}

	// Delete the redirect transient
	delete_transient( '_ibwp_activation_redirect' );

	// return if activating from network, or bulk
	if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
		return;
	}

	$redirect_link = ibwpl_get_plugin_link('about');

	// Redirect to about page
	wp_safe_redirect( $redirect_link );
}
add_action( 'admin_init', 'ibwpl_welcome_page_redirect' );

/**
 * Function to add extra plugins link
 * 
 * @package InboundWP Pro
 * @since 1.0.2
 */
function ibwpl_plugin_action_links( $links ) {
	
	$links['upgrade'] = '<a style="color:#2ECC71;" href="' . esc_url(IBWPL_PRO_LINK) . '" target="_blank" title="' . esc_attr( __( 'Upgrade to Pro', 'inboundwp-pro' ) ) . '">' . __( 'Upgrade', 'inboundwp-pro' ) . '</a>';
	
	return $links;
}
add_filter( 'plugin_action_links_' . IBWPL_BASENAME, 'ibwpl_plugin_action_links' );